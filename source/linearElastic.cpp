#include "linearElastic.hpp"

#define FILENAME "linearElastic.cpp"

using namespace std;

// construct class object
LinearElastic::LinearElastic(Domain &D, string bcRTtype, string bcTTtype, string bcLTtype, string bcBTtype)
  : _D(&D),_delim(D._delim),_inputDir(D._inputDir),_outputDir(D._outputDir),
    _Ny(D._Ny),_Nz(D._Nz),_Ly(D._Ly),_Lz(D._Lz),_dy(D._dq),_dz(D._dr),
    _y(&D._y),_z(&D._z),_mu(NULL),_rho(NULL),_cs(NULL),_bcRShift(NULL),
    _surfDisp(NULL),_rhs(NULL),_u(NULL),_sxy(NULL),_sxz(NULL),
    _computeSxz(0),_computeSdev(0),_linSolver(D._linSolver),_kspTol(D._kspTol),
    _ksp(NULL),_pc(NULL),_sbp(NULL),_bcR(NULL),_bcT(NULL),_bcL(NULL),_bcB(NULL),
    _bcRType(bcRTtype),_bcTType(bcTTtype),_bcLType(bcLTtype),_bcBType(bcBTtype),
    _writeTime(0),_linSolveTime(0),_factorTime(0),_startTime(MPI_Wtime()),
    _miscTime(0), _matrixTime(0), _linSolveCount(0)
{
  // load and set parameters
  loadSettings(D._file);
  checkInput();
  allocateFields();

  if (_D->_ckpt > 0 && _D->_ckptNumber > 0) { // load from previous checkpoint
    loadCheckpoint();
  }
  else { // otherwise set parameters from input file and user-provided Vecs
    setMaterialParameters();
    loadICsFromFiles();
  }

  double startMatrix = MPI_Wtime();
  setUpSBPContext(); // set up matrix operators
  _matrixTime += MPI_Wtime() - startMatrix;

  setSurfDisp();
}


// destructor
LinearElastic::~LinearElastic()
{
  // boundary conditions
  VecDestroy(&_bcL);
  VecDestroy(&_bcR);
  VecDestroy(&_bcT);
  VecDestroy(&_bcB);
  VecDestroy(&_bcRShift);

  // body fields
  VecDestroy(&_rho);
  VecDestroy(&_cs);
  VecDestroy(&_mu);
  VecDestroy(&_rho);
  VecDestroy(&_cs);
  VecDestroy(&_rhs);
  VecDestroy(&_u);
  VecDestroy(&_sxy);
  VecDestroy(&_sxz);
  VecDestroy(&_surfDisp);

  KSPDestroy(&_ksp);

  delete _sbp;
  _sbp = NULL;

  for (map<string,pair<PetscViewer,string> >::iterator it=_viewers1D.begin(); it !=_viewers1D.end(); it++) {
    PetscViewerDestroy(&_viewers1D[it->first].first);
  }
  for (map<string,pair<PetscViewer,string> >::iterator it=_viewers2D.begin(); it !=_viewers2D.end(); it++) {
    PetscViewerDestroy(&_viewers2D[it->first].first);
  }
}


// loads settings from the input text file
PetscErrorCode LinearElastic::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;
  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos+_delim.length(),line.npos);
    }
    rhsFull = rhs; // everything after _delim

    pos = rhs.find(" "); // interpret everything after the appearance of a space on the line as a comment
    rhs = rhs.substr(0,pos); // rhs is everything starting at location 0 and spans pos characters

    if (var == "muVals") { loadVectorFromInputFile(rhsFull,_muVals); }
    else if (var == "muDepths") { loadVectorFromInputFile(rhsFull,_muDepths); }
    else if (var == "rhoVals") { loadVectorFromInputFile(rhsFull,_rhoVals); }
    else if (var == "rhoDepths") { loadVectorFromInputFile(rhsFull,_rhoDepths); }

    // switches for computing extra stresses
    else if (var == "momBal_computeSxz") { _computeSxz = atof( rhs.c_str() ); }
    else if (var == "momBal_computeSdev") { _computeSdev = atof( rhs.c_str() ); }
  }

  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode LinearElastic::checkInput()
{
  PetscErrorCode ierr = 0;
  assert(_muVals.size() == _muDepths.size());
  assert(_muVals.size() != 0);
  assert(_rhoVals.size() == _rhoDepths.size());
  assert(_rhoVals.size() != 0);

  if (_computeSdev == 1) { _computeSxz = 1; }

  return ierr;
}


/*
 * Set up the Krylov Subspace and Preconditioner (KSP) environment. A
 * table of options available through PETSc and linked external packages
 * is available at
 * http://www.mcs.anl.gov/petsc/documentation/linearsolvertable.html.
 *
 * The methods implemented here are:
 *     Algorithm             Package           input file syntax
 * algebraic multigrid       HYPRE                AMG
 * direct LU                 MUMPS                MUMPSLU
 * direct Cholesky           MUMPS                MUMPSCHOLESKY
 *
 * A list of options for each algorithm that can be set can be obtained
 * by running the code with the argument main <input file> -help and
 * searching through the output for "Preconditioner (PC) options" and
 * "Krylov Method (KSP) options".
 *
 * To view convergence information, including number of iterations, use
 * the command line argument: -ksp_converged_reason.
 *
 * For information regarding HYPRE's solver options, especially the
 * preconditioner options, use the User manual online.
 * Use -ksp_view.
 */

PetscErrorCode LinearElastic::setupKSP(KSP& ksp,PC& pc,Mat& A)
{
  PetscErrorCode ierr = 0;
  // create linear solver context
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);

  // set operators, here the matrix that defines the linear system also serves as the preconditioning matrix
  ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);

  // algebraic multigrid from HYPRE
  if (_linSolver == "AMG") {
    ierr = KSPSetType(ksp,KSPRICHARDSON); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(pc,"boomeramg"); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCFactorSetLevels(pc,4); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }

  // direct LU from MUMPS
  else if (_linSolver == "LU") {
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS); CHKERRQ(ierr);
    ierr = PCFactorSetUpMatSolverType(pc); CHKERRQ(ierr);
  }

  // direct Cholesky (RR^T) from MUMPS
  else if (_linSolver == "CHOLESKY") {
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCCHOLESKY); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS); CHKERRQ(ierr);
    ierr = PCFactorSetUpMatSolverType(pc); CHKERRQ(ierr);
  }

  // preconditioned conjugate gradient
  else if (_linSolver == "CG") {
    ierr = KSPSetType(ksp,KSPCG); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  }

  // undefined linear solver
  else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"ERROR: linSolver type not understood\n");
    assert(0);
  }

  // enable command line options to override those specified above, e.g.:
  // -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

  // perform computation of preconditioners now, rather than on first use
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);

  return ierr;
}


// allocate space for member fields
PetscErrorCode LinearElastic::allocateFields()
{
  PetscErrorCode ierr = 0;
  // boundary conditions
  VecDuplicate(_D->_y0,&_bcL);
  VecSet(_bcL,0.0);

  VecDuplicate(_bcL,&_bcRShift);
  VecSet(_bcRShift,0.0);
  VecDuplicate(_bcL,&_bcR);
  VecSet(_bcR,0.);

  VecDuplicate(_D->_z0,&_bcT);
  VecSet(_bcT,0.0);

  VecDuplicate(_bcT,&_bcB);
  VecSet(_bcB,0.0);

  // other fieds
  VecDuplicate(*_z,&_rhs); VecSet(_rhs,0.0);
  VecDuplicate(*_z,&_mu);
  VecDuplicate(*_z,&_rho);
  VecDuplicate(*_z,&_cs);
  VecDuplicate(_rhs,&_u); VecSet(_u,0.0);
  VecDuplicate(_rhs,&_sxy); VecSet(_sxy,0.0);
  if (_computeSxz) { VecDuplicate(_rhs,&_sxz); VecSet(_sxz,0.0); }
  else { _sxz = NULL; }
  if (_computeSdev) { VecDuplicate(_rhs,&_sdev); VecSet(_sdev,0.0); }
  else { _sdev = NULL; }
  VecDuplicate(_bcT,&_surfDisp); PetscObjectSetName((PetscObject) _surfDisp, "_surfDisp");

  return ierr;
}


// set off-fault material properties
PetscErrorCode LinearElastic::setMaterialParameters()
{
  PetscErrorCode ierr = 0;

  ierr = setVec(_mu,*_y,_muVals,_muDepths);CHKERRQ(ierr);
  ierr = setVec(_rho,*_z,_rhoVals,_rhoDepths);CHKERRQ(ierr);
  VecPointwiseDivide(_cs, _mu, _rho);
  VecSqrtAbs(_cs);

  if (_D->_isMMS == 1) {
    if (_Nz == 1) { mapToVec(_mu,zzmms_mu1D,*_y); }
    else { mapToVec(_mu,zzmms_mu,*_y,*_z); }
  }

  return ierr;
}


// parse input file and load values into data members
PetscErrorCode LinearElastic::loadICsFromFiles()
{
  PetscErrorCode ierr = 0;

  ierr = loadVecFromInputFile(_bcL,_inputDir,"momBal_bcL"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_bcRShift,_inputDir,"momBal_bcR"); CHKERRQ(ierr);
  VecSet(_bcR,0.);
  ierr = loadVecFromInputFile(_mu,_inputDir,"momBal_mu"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_rho,_inputDir,"momBal_rho"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_cs,_inputDir,"momBal_cs"); CHKERRQ(ierr);

  return ierr;
}

// load data from a checkpoint
PetscErrorCode LinearElastic::loadCheckpoint()
{
  PetscErrorCode ierr = 0;

  // boundary conditions
  ierr = loadVecFromInputFile(_bcRShift, _outputDir + "chkpt_", "momBal_bcRShift"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_bcR, _outputDir + "chkpt_", "momBal_bcR"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_bcT, _outputDir + "chkpt_", "momBal_bcT"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_bcL, _outputDir + "chkpt_", "momBal_bcL"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_bcB, _outputDir + "chkpt_", "momBal_bcB"); CHKERRQ(ierr);

  // material parameters
  ierr = loadVecFromInputFile(_mu, _outputDir, "momBal_mu"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_rho, _outputDir, "momBal_rho"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_cs, _outputDir, "momBal_cs"); CHKERRQ(ierr);

  return ierr;
}


// set up SBP operators
PetscErrorCode LinearElastic::setUpSBPContext()
{
  PetscErrorCode ierr = 0;

  delete _sbp;
  KSPDestroy(&_ksp);

  if (_D->_yGridSpacingType == "constantGridSpacing") {
    _sbp = new SbpOps_m_constGrid(_D->_order,_Ny,_Nz,_Ly,_Lz,_mu);
  }
  else if (_D->_yGridSpacingType == "variableGridSpacing") {
    _sbp = new SbpOps_m_varGrid(_D->_order,_Ny,_Nz,_Ly,_Lz,_mu);
    if (_Ny > 1 && _Nz > 1) { _sbp->setGrid(_y,_z); }
    else if (_Ny == 1 && _Nz > 1) { _sbp->setGrid(NULL,_z); }
    else if (_Ny > 1 && _Nz == 1) { _sbp->setGrid(_y,NULL); }
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }
  _sbp->setCompatibilityType(_D->_sbpCompatibilityType);
  _sbp->setBCTypes(_bcRType,_bcTType,_bcLType,_bcBType);
  _sbp->setMultiplyByH(1);
  _sbp->setLaplaceType("yz");
  _sbp->setDeleteIntermediateFields(1);
  _sbp->computeMatrices(); // actually create the matrices
  _sbp->getAlpha(_alpha);

  return ierr;
}


// solve momentum balance equation for displacement vector u
PetscErrorCode LinearElastic::computeU()
{
  PetscErrorCode ierr = 0;

  // solve for displacement
  double startTime = MPI_Wtime();
  ierr = KSPSolve(_ksp,_rhs,_u); CHKERRQ(ierr);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;

  setSurfDisp();

  return ierr;
}


// set the right-hand side vector for linear solve
PetscErrorCode LinearElastic::setRHS()
{
  PetscErrorCode ierr = 0;

  VecSet(_rhs,0.0);
  ierr = _sbp->setRhs(_rhs,_bcL,_bcR,_bcT,_bcB); CHKERRQ(ierr);
  return ierr;
}


// change boundary condition types and reset linear solver
PetscErrorCode LinearElastic::changeBCTypes(string bcRTtype,string bcTTtype,string bcLTtype,string bcBTtype)
{
  PetscErrorCode ierr = 0;
  // destroy current KSP context to reset
  KSPDestroy(&_ksp);
  _sbp->changeBCTypes(bcRTtype,bcTTtype,bcLTtype,bcBTtype);

  Mat A;
  _sbp->getA(A);
  setupKSP(_ksp,_pc,A);

  return ierr;
}


// set up surface displacement
PetscErrorCode LinearElastic::setSurfDisp()
{
  PetscErrorCode ierr = 0;

  // extract surface displacement from u
  VecScatterBegin(_D->_scatters["body2T"], _u, _surfDisp, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2T"], _u, _surfDisp, INSERT_VALUES, SCATTER_FORWARD);
  return ierr;
}


// view runtime summary
PetscErrorCode LinearElastic::view(const double totRunTime)
{
  PetscErrorCode ierr = 0;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"\n-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Linear Elastic Runtime Summary:\n"); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent creating matrices (s): %g\n",_matrixTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent writing output (s): %g\n",_writeTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   number of times linear system was solved: %i\n",_linSolveCount); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent solving linear system (s): %g\n",_linSolveTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% time spent solving linear system: %g\n",_linSolveTime/totRunTime*100.); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent solving linear system: %g\n",_linSolveTime/totRunTime*100.); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent creating matrices: %g\n",_matrixTime/totRunTime*100.); CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);

  return ierr;
}


// write out momentum balance context, such as linear solve settings, boundary conditions
PetscErrorCode LinearElastic::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;

  // do nothing for now

  return ierr;
}


// writes out fields of length Ny or Nz at each time step
PetscErrorCode LinearElastic::writeStep1D(PetscInt stepCount, const string outputDir)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  if (_viewers1D.empty()) {
    //initiate_appendVecToOutput(_viewers1D, "surfDisp", _surfDisp, outputDir + "surfDisp", _D->_outFileMode);
    //initiate_appendVecToOutput(_viewers1D, "bcR", _bcR, outputDir + "momBal_bcR", _D->_outFileMode);
    //initiate_appendVecToOutput(_viewers1D, "bcT", _bcT, outputDir + "momBal_bcT", _D->_outFileMode);
    //initiate_appendVecToOutput(_viewers1D, "bcL", _bcL, outputDir + "momBal_bcL", _D->_outFileMode);
    //initiate_appendVecToOutput(_viewers1D, "bcB", _bcB, outputDir + "momBal_bcB", _D->_outFileMode);
    //initiate_appendVecToOutput(_viewers1D, "bcRShift", _bcRShift, outputDir + "momBal_bcRShift", _D->_outFileMode);
  }
  else {
    //ierr = VecView(_surfDisp,_viewers1D["surfDisp"].first); CHKERRQ(ierr);
    //ierr = VecView(_bcL,_viewers1D["bcL"].first); CHKERRQ(ierr);
    //ierr = VecView(_bcR,_viewers1D["bcR"].first); CHKERRQ(ierr);
    //ierr = VecView(_bcB,_viewers1D["bcB"].first); CHKERRQ(ierr);
    //ierr = VecView(_bcT,_viewers1D["bcT"].first); CHKERRQ(ierr);
    //ierr = VecView(_bcRShift,_viewers1D["bcRShift"].first); CHKERRQ(ierr);
  }

  _writeTime += MPI_Wtime() - startTime;

  return ierr;
}


// writes out fields of length Ny*Nz. These take up much more hard drive space, and more runtime to output, so are separate from the 1D fields
PetscErrorCode LinearElastic::writeStep2D(PetscInt stepCount, const string outputDir)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  if (_viewers2D.empty()) {
    initiate_appendVecToOutput(_viewers2D, "u", _u, outputDir + "momBal_u", _D->_outFileMode);
    initiate_appendVecToOutput(_viewers2D, "sxy", _sxy, outputDir + "momBal_sxy", _D->_outFileMode);
    if (_computeSxz) { initiate_appendVecToOutput(_viewers2D, "sxz", _sxz, outputDir + "momBal_sxz", _D->_outFileMode); }
  }
  else {
    ierr = VecView(_u,_viewers2D["u"].first); CHKERRQ(ierr);
    ierr = VecView(_sxy,_viewers2D["sxy"].first); CHKERRQ(ierr);
    if (_computeSxz) { ierr = VecView(_sxz,_viewers2D["sxz"].first); CHKERRQ(ierr); }
  }

  _writeTime += MPI_Wtime() - startTime;

  return ierr;
}

// writes out fields of length Ny or Nz at each time step
PetscErrorCode LinearElastic::writeCheckpoint()
{
  PetscErrorCode ierr = 0;

  ierr = writeVec(_bcR, _outputDir + "chkpt_" +"momBal_bcR"); CHKERRQ(ierr);
  ierr = writeVec(_bcRShift, _outputDir + "chkpt_" + "momBal_bcRShift"); CHKERRQ(ierr);
  ierr = writeVec(_bcT, _outputDir + "chkpt_" + "momBal_bcT"); CHKERRQ(ierr);
  ierr = writeVec(_bcL, _outputDir + "chkpt_" +"momBal_bcL"); CHKERRQ(ierr);
  ierr = writeVec(_bcB, _outputDir + "chkpt_" +"momBal_bcB"); CHKERRQ(ierr);
  return ierr;
}


// explicit time stepping to compute stresses
PetscErrorCode LinearElastic::computeStresses()
{
  PetscErrorCode ierr = 0;

  // solve for shear stress
  ierr = _sbp->muxDy(_u,_sxy); CHKERRQ(ierr);

  // if compute sigma_xz
  if (_computeSxz) {
    ierr = _sbp->muxDz(_u,_sxz); CHKERRQ(ierr);
  }

  // if compute deviatoric stress
  if (_computeSdev) {
    ierr = computeSDev(); CHKERRQ(ierr);
  }

  return ierr;
}


// computes sigmadev = sqrt(sigmaxy^2 + sigmaxz^2)
PetscErrorCode LinearElastic::computeSDev()
{
  PetscErrorCode ierr = 0;

  // deviatoric stress: part 1/3
  VecPointwiseMult(_sdev,_sxy,_sxy);

  if (_Nz > 1) {
    // deviatoric stress: part 2/3
    Vec temp;
    VecDuplicate(_sxz,&temp);
    VecPointwiseMult(temp,_sxz,_sxz);
    VecAXPY(_sdev,1.0,temp);
    VecDestroy(&temp);
  }

  // deviatoric stress: part 3/3
  VecSqrtAbs(_sdev);

  return ierr;
}


// set stress pointers to calculated values
PetscErrorCode LinearElastic::getStresses(Vec& sxy, Vec& sxz, Vec& sdev)
{
  sxy = _sxy;
  sxz = _sxz;
  sdev = _sdev;
  return 0;
}


// set boundary conditions for MMS test
PetscErrorCode LinearElastic::setMMSBoundaryConditions(const double time)
{
  PetscErrorCode ierr = 0;

  // set up boundary conditions: L and R
  PetscScalar y,z,v;
  PetscInt Ii,Istart,Iend;
  ierr = VecGetOwnershipRange(_bcL,&Istart,&Iend);CHKERRQ(ierr);

  if (_Nz == 1) {
    Ii = Istart;

    // left boundary
    y = 0;
    // uAnal(y=0,z), Dirichlet boundary condition
    if (_bcLType == "Dirichlet") {
      v = zzmms_uA1D(y,time);
    }
    // sigma_xy = mu * (du/dy), Neumann boundary condition
    else if (_bcLType == "Neumann") {
      v = zzmms_mu1D(y) * zzmms_uA_y1D(y,time);
    }
    ierr = VecSetValues(_bcL,1,&Ii,&v,INSERT_VALUES); CHKERRQ(ierr);

    // right boundary
    y = _Ly;
    // uAnal(y=Ly,z)
    if (_bcRType == "Dirichlet") {
      v = zzmms_uA1D(y,time);
    }
    // sigma_xy = mu * (du/dy)
    else if (_bcRType == "Neumann") {
      v = zzmms_mu1D(y) * zzmms_uA_y1D(y,time);
    }
    ierr = VecSetValues(_bcR,1,&Ii,&v,INSERT_VALUES); CHKERRQ(ierr);
  }

  else {
    for (Ii = Istart; Ii < Iend; Ii++) {
      ierr = VecGetValues(*_z,1,&Ii,&z); CHKERRQ(ierr);
      //~ z = _dz * Ii;

      // left boundary
      y = 0;
      // uAnal(y=0,z)
      if (_bcLType == "Dirichlet") {
	v = zzmms_uA(y,z,time);
      }
      // sigma_xy = mu * d/dy u
      else if (_bcLType == "Neumann") {
	v = zzmms_mu(y,z) * zzmms_uA_y(y,z,time);
      }
      ierr = VecSetValues(_bcL,1,&Ii,&v,INSERT_VALUES);CHKERRQ(ierr);

      // right boundary
      y = _Ly;
      // uAnal(y=Ly,z)
      if (_bcRType == "Dirichlet") {
	v = zzmms_uA(y,z,time);
      }
      // sigma_xy = mu * d/dy u
      else if (_bcRType == "Neumann") {
	v = zzmms_mu(y,z) * zzmms_uA_y(y,z,time);
      }
      ierr = VecSetValues(_bcR,1,&Ii,&v,INSERT_VALUES);CHKERRQ(ierr);
    }
  }

  // assemble boundary vectors
  ierr = VecAssemblyBegin(_bcL); CHKERRQ(ierr);
  ierr = VecAssemblyBegin(_bcR); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcL); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcR); CHKERRQ(ierr);

  // set up boundary conditions: T and B
  ierr = VecGetOwnershipRange(*_y,&Istart,&Iend);CHKERRQ(ierr);
  for(Ii = Istart; Ii < Iend; Ii++) {
    if (Ii % _Nz == 0) {
      //~ y = _dy * Ii;
      ierr = VecGetValues(*_y,1,&Ii,&y);CHKERRQ(ierr);
      PetscInt Jj = Ii / _Nz;

      // top boundary
      z = 0;
      // uAnal(y, z = 0)
      if (_bcTType == "Dirichlet") {
	v = zzmms_uA(y,z,time);
      }
      else if (_bcTType == "Neumann") {
	v = zzmms_mu(y,z) * (zzmms_uA_z(y,z,time));
      }
      ierr = VecSetValues(_bcT,1,&Jj,&v,INSERT_VALUES); CHKERRQ(ierr);

      // bottom boundary
      z = _Lz;
      // uAnal(y, z = Lz)
      if (_bcBType == "Dirichlet") {
	v = zzmms_uA(y,z,time);
      }
      else if (_bcBType == "Neumann") {
	v = zzmms_mu(y,z) * zzmms_uA_z(y,z,time);
      }
      ierr = VecSetValues(_bcB,1,&Jj,&v,INSERT_VALUES);CHKERRQ(ierr);
    }
  }

  // assemble top and bottom boundary vectors
  ierr = VecAssemblyBegin(_bcT);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(_bcB);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcT);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcB);CHKERRQ(ierr);

  return ierr;
}


// compute source term for MMS test and add it to rhs vector
PetscErrorCode LinearElastic::addRHS_MMSSource(const PetscScalar time,Vec& rhs)
{
  PetscErrorCode ierr = 0;

  Vec source,Hxsource;
  VecDuplicate(_u,&source);
  VecDuplicate(_u,&Hxsource);

  if (_Nz == 1) {
    mapToVec(source,zzmms_uSource1D,*_y,time);
  }
  else {
    mapToVec(source,zzmms_uSource,*_y,*_z,time);
  }
  ierr = _sbp->H(source,Hxsource);

  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    multMatsVec(yq,zr,Hxsource);
  }

  ierr = VecAXPY(_rhs,1.0,Hxsource); CHKERRQ(ierr); // rhs = rhs + H*source

  // free memory
  VecDestroy(&source);
  VecDestroy(&Hxsource);

  return ierr;
}


// set MMS initial conditions
PetscErrorCode LinearElastic::setMMSInitialConditions(const PetscScalar time)
{
  PetscErrorCode ierr = 0;
  string funcName = "LinearElastic::setMMSInitialConditions";
  string fileName = "linearElastic.cpp";

  Vec source,Hxsource;
  VecDuplicate(_u,&source);
  VecDuplicate(_u,&Hxsource);

  if (_Nz == 1) {
    mapToVec(source,zzmms_uSource1D,*_y,time);
  }
  else {
    mapToVec(source,zzmms_uSource,*_y,*_z,time);
  }
  writeVec(source,_outputDir + "mms_uSource");
  ierr = _sbp->H(source,Hxsource); CHKERRQ(ierr);

  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    multMatsVec(yq,zr,Hxsource);
  }

  // set rhs, including body source term
  VecSet(_bcRShift,0.0);
  ierr = setMMSBoundaryConditions(time); CHKERRQ(ierr);
  ierr = _sbp->setRhs(_rhs,_bcL,_bcR,_bcT,_bcB); CHKERRQ(ierr);
  ierr = VecAXPY(_rhs,1.0,Hxsource); CHKERRQ(ierr); // rhs = rhs + H*source

  // solve for displacement
  double startTime = MPI_Wtime();
  ierr = KSPSolve(_ksp,_rhs,_u); CHKERRQ(ierr);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;
  ierr = setSurfDisp();

  // solve for shear stress
  _sbp->muxDy(_u,_sxy);

  // free memory
  VecDestroy(&source);
  VecDestroy(&Hxsource);

  return ierr;
}


// get MMS error to measure convergence
PetscErrorCode LinearElastic::measureMMSError(const PetscScalar time)
{
  PetscErrorCode ierr = 0;

  // measure error between analytical and numerical solution
  Vec uA, sigmaxyA;
  VecDuplicate(_u,&uA);
  VecDuplicate(_u,&sigmaxyA);

  if (_Nz == 1) {
    mapToVec(uA,zzmms_uA1D,*_y,time);
    mapToVec(sigmaxyA,zzmms_sigmaxy1D,*_y,time);
  }
  else {
    mapToVec(uA,zzmms_uA,*_y,*_z,time);
    mapToVec(sigmaxyA,zzmms_sigmaxy,*_y,*_z,time);
  }

  double err2uA = computeNormDiff_2(_u,uA);
  double err2sigmaxy = computeNormDiff_2(_sxy,sigmaxyA);

  writeVec(uA,_outputDir+"uA");
  writeVec(_bcL,_outputDir+"mms_u_bcL");
  writeVec(_bcR,_outputDir+"mms_u_bcR");
  writeVec(_bcT,_outputDir+"mms_u_bcT");
  writeVec(_bcB,_outputDir+"mms_u_bcB");

  Mat H; _sbp->getH(H);
  err2uA = computeNormDiff_Mat(H,_u,uA);
  err2sigmaxy = computeNormDiff_2(_sxy,sigmaxyA);

  PetscPrintf(PETSC_COMM_WORLD,"%i  %3i %.4e %.4e % .15e %.4e % .15e\n",
              _D->_order,_Ny,_dy,err2uA,log2(err2uA),err2sigmaxy,log2(err2sigmaxy));

  // free memory
  VecDestroy(&uA);
  VecDestroy(&sigmaxyA);

  return ierr;
}


// MMS functions
// helper function for uA
double LinearElastic::zzmms_f(const double y,const double z) {
  return cos(y)*sin(z);
}

double LinearElastic::zzmms_f_y(const double y,const double z) {
  return -sin(y)*sin(z);
}

double LinearElastic::zzmms_f_yy(const double y,const double z) {
  return -cos(y)*sin(z);
}

double LinearElastic::zzmms_f_z(const double y,const double z) {
  return cos(y)*cos(z);
}

double LinearElastic::zzmms_f_zz(const double y,const double z) {
  return -cos(y)*sin(z);
}

double LinearElastic::zzmms_g(const double t) {
  return exp(-t/60.0) - exp(-t/3e7) + exp(-t/3e9);
}

double LinearElastic::zzmms_g_t(const double t) {
  return (-1.0/60)*exp(-t/60.0) - (-1.0/3e7)*exp(-t/3e7) +   (-1.0/3e9)*exp(-t/3e9);
}

double LinearElastic::zzmms_uA(const double y,const double z,const double t) {
  return zzmms_f(y,z)*zzmms_g(t);
}

double LinearElastic::zzmms_uA_y(const double y,const double z,const double t) {
  return zzmms_f_y(y,z)*zzmms_g(t);
}

double LinearElastic::zzmms_uA_yy(const double y,const double z,const double t) {
  return zzmms_f_yy(y,z)*zzmms_g(t);
}

double LinearElastic::zzmms_uA_z(const double y,const double z,const double t) {
  return zzmms_f_z(y,z)*zzmms_g(t);
}

double LinearElastic::zzmms_uA_zz(const double y,const double z,const double t) {
  return zzmms_f_zz(y,z)*zzmms_g(t);
}

double LinearElastic::zzmms_uA_t(const double y,const double z,const double t) {
  return zzmms_f(y,z)*zzmms_g_t(t);
}

double LinearElastic::zzmms_mu(const double y,const double z) {
  return sin(y)*sin(z) + 30;
}

double LinearElastic::zzmms_mu_y(const double y,const double z) {
  return cos(y)*sin(z);
}

double LinearElastic::zzmms_mu_z(const double y,const double z) {
  return sin(y)*cos(z);
}

double LinearElastic::zzmms_sigmaxy(const double y,const double z,const double t) {
  return zzmms_mu(y,z)*zzmms_uA_y(y,z,t);
}

double LinearElastic::zzmms_uSource(const double y,const double z,const double t) {
  PetscScalar mu = zzmms_mu(y,z);
  PetscScalar mu_y = zzmms_mu_y(y,z);
  PetscScalar mu_z = zzmms_mu_z(y,z);
  PetscScalar u_y = zzmms_uA_y(y,z,t);
  PetscScalar u_yy = zzmms_uA_yy(y,z,t);
  PetscScalar u_z = zzmms_uA_z(y,z,t);
  PetscScalar u_zz = zzmms_uA_zz(y,z,t);
  return mu*(u_yy + u_zz) + mu_y*u_y + mu_z*u_z;
}


// 1D
// helper function for uA
double LinearElastic::zzmms_f1D(const double y) {
  return cos(y) + 2;
}

double LinearElastic::zzmms_f_y1D(const double y) {
  return -sin(y);
}

double LinearElastic::zzmms_f_yy1D(const double y) {
  return -cos(y);
}

// double LinearElastic::zzmms_f_z1D(const double y) { return 0; }
// double LinearElastic::zzmms_f_zz1D(const double y) { return 0; }

double LinearElastic::zzmms_uA1D(const double y,const double t) {
  return zzmms_f1D(y)*exp(-t);
}

double LinearElastic::zzmms_uA_y1D(const double y,const double t) {
  return zzmms_f_y1D(y)*exp(-t);
}

double LinearElastic::zzmms_uA_yy1D(const double y,const double t) {
  return zzmms_f_yy1D(y)*exp(-t);
}

double LinearElastic::zzmms_uA_z1D(const double y,const double t) {
  return 0;
}

double LinearElastic::zzmms_uA_zz1D(const double y,const double t) {
  return 0;
}

double LinearElastic::zzmms_uA_t1D(const double y,const double t) {
  return -zzmms_f1D(y)*exp(-t);
}

double LinearElastic::zzmms_mu1D(const double y) {
  return sin(y) + 2.0;
}

double LinearElastic::zzmms_mu_y1D(const double y) {
  return cos(y);
}

// double LinearElastic::zzmms_mu_z1D(const double y) { return 0; }

double LinearElastic::zzmms_sigmaxy1D(const double y,const double t) {
  return zzmms_mu1D(y)*zzmms_uA_y1D(y,t);
}

double LinearElastic::zzmms_uSource1D(const double y,const double t) {
  PetscScalar mu = zzmms_mu1D(y);
  PetscScalar mu_y = zzmms_mu_y1D(y);
  PetscScalar u_y = zzmms_uA_y1D(y,t);
  PetscScalar u_yy = zzmms_uA_yy1D(y,t);
  PetscScalar u_zz = zzmms_uA_zz1D(y,t);

  return mu*(u_yy + u_zz) + mu_y*u_y;
}
