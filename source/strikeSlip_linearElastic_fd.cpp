#include "strikeSlip_linearElastic_fd.hpp"

#define FILENAME "strikeSlip_linearElastic_fd.cpp"

using namespace std;

strikeSlip_linearElastic_fd::strikeSlip_linearElastic_fd(Domain&D)
  : _D(&D),_delim(D._delim),_deltaT(-1), _CFL(-1),_y(&D._y),_z(&D._z),
    _alphay(NULL),_inputDir(D._inputDir),_outputDir(D._outputDir),
    _initialConditions("u"),_computeICs(D._computeICs),
    _faultTypeScale(D._faultTypeScale),_stride1D(D._stride1D),
    _stride2D(D._stride2D),_currTime(0),_stepCount(0),_atol(1e-8),
    _yCenterU(0.3), _zCenterU(0.8), _yStdU(5.0), _zStdU(5.0), _ampU(10.0),
    _mat_bcRType("Neumann"),_mat_bcTType("Neumann"),
    _mat_bcLType("Neumann"),_mat_bcBType("Neumann"),
    _timeV1D(NULL),_dtimeV1D(NULL),_timeV2D(NULL),_dtimeV2D(NULL),
    _integrateTime(0),_writeTime(0),_linSolveTime(0),_factorTime(0),
    _startTime(MPI_Wtime()),_miscTime(0), _propagateTime(0),
    _quadWaveEx(NULL),_fault(NULL),_material(NULL)
{
  loadSettings(D._file);
  checkInput();

  // determine if material is symmetric about the fault, or if one side is rigid
  _faultTypeScale = 2.0;
  if (_D->_fd_bcLType == "rigidFault" ) { _faultTypeScale = 1.0; }

  _body2fault = &(D._scatters["body2L"]);
  _fault = new Fault_fd(D, D._scatters["body2L"],_faultTypeScale); // fault
  _material = new LinearElastic(D,_mat_bcRType,_mat_bcTType,_mat_bcLType,_mat_bcBType);
  _cs = _material->_cs;
  _rho = _material->_rho;
  _mu = _material->_mu;
  computePenaltyVectors();

  computeTimeStep(); // compute time step
}


strikeSlip_linearElastic_fd::~strikeSlip_linearElastic_fd()
{
  map<string,Vec>::iterator it;
  for (it = _var.begin(); it!=_var.end(); it++ ) {
    VecDestroy(&it->second);
  }

  for (it = _varPrev.begin(); it!=_varPrev.end(); it++ ) {
    VecDestroy(&it->second);
  }

  PetscViewerDestroy(&_timeV1D);
  PetscViewerDestroy(&_dtimeV1D);
  PetscViewerDestroy(&_timeV2D);

  VecDestroy(&_ay);

  delete _quadWaveEx;      _quadWaveEx = NULL;
  delete _material;        _material = NULL;
  delete _fault;           _fault = NULL;
}


// loads settings from the input text file
PetscErrorCode strikeSlip_linearElastic_fd::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;
  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      continue;
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0,pos);

    if (var == "deltaT") { _deltaT = atof( rhs.c_str() ); }
    else if (var == "CFL") { _CFL = atof( rhs.c_str() ); }
    else if (var == "center_y") { _yCenterU = atof( rhs.c_str() ); }
    else if (var == "center_z") { _zCenterU = atof( rhs.c_str() ); }
    else if (var == "std_y") { _yStdU = atof( rhs.c_str() ); }
    else if (var == "std_z") { _zStdU = atof( rhs.c_str() ); }
    else if (var == "amp_U") { _ampU = atof( rhs.c_str() ); }
    else if (var == "atol") { _atol = atof( rhs.c_str() ); }
    else if (var == "initialConditions") { _initialConditions = rhs.c_str(); }
  }
  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode strikeSlip_linearElastic_fd::checkInput()
{
  PetscErrorCode ierr = 0;
  assert(_atol >= 1e-14);
  return ierr;
}

// initiate variables to be integrated in time
PetscErrorCode strikeSlip_linearElastic_fd::initiateIntegrand()
{
  PetscErrorCode ierr = 0;

  if (_D->_isMMS == 1) { _material->setMMSInitialConditions(_D->_initTime); }

  _fault->initiateIntegrand(_D->_initTime,_var);

  VecDuplicate(*_z, &_var["u"]); VecSet(_var["u"], 0.0);

  return ierr;
}


// monitoring function for explicit integration
PetscErrorCode strikeSlip_linearElastic_fd::timeMonitor(PetscScalar time, PetscScalar deltaT, PetscInt stepCount, int& stopIntegration)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  _deltaT = deltaT;
  _stepCount = stepCount;
  _currTime = time;

  if ( (_stride1D > 0 && _currTime == _D->_maxTime) || (_stride1D > 0 && stepCount % _stride1D == 0) ) {
    ierr = writeStep1D(_stepCount,time,_outputDir); CHKERRQ(ierr);
    ierr = _material->writeStep1D(_stepCount,_outputDir); CHKERRQ(ierr);
    ierr = _fault->writeStep(_stepCount,_outputDir); CHKERRQ(ierr);
  }

  if ( (_stride2D>0 &&_currTime == _D->_maxTime) || (_stride2D>0 && stepCount % _stride2D == 0)) {
    ierr = writeStep2D(_stepCount,time,_outputDir); CHKERRQ(ierr);
    ierr = _material->writeStep2D(_stepCount,_outputDir);CHKERRQ(ierr);
  }

  _writeTime += MPI_Wtime() - startTime;

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_fd::writeStep1D(PetscInt stepCount, PetscScalar time,const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV1D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time1D.txt", _D->_outFileMode, _timeV1D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV1D, "%.15e\n", time); CHKERRQ(ierr);
  }
  if (_dtimeV1D == NULL ) {
    initiateWriteASCII(outputDir, "med_dt1D.txt", _D->_outFileMode, _dtimeV1D, "%.15e\n", _deltaT);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV1D, "%.15e\n", _deltaT); CHKERRQ(ierr);
  }

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_fd::writeStep2D(PetscInt stepCount, PetscScalar time,const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV2D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time2D.txt", _D->_outFileMode, _timeV2D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV2D, "%.15e\n", time); CHKERRQ(ierr);
  }
  if (_dtimeV2D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_dt2D.txt", _D->_outFileMode, _dtimeV2D, "%.15e\n", _deltaT); CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV2D, "%.15e\n", _deltaT); CHKERRQ(ierr);
  }
  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_fd::view()
{
  PetscErrorCode ierr = 0;

  double totRunTime = MPI_Wtime() - _startTime;

  _material->view(_integrateTime);
  _fault->view(_integrateTime);
  int num_proc;
  MPI_Comm_size(MPI_COMM_WORLD, &num_proc);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"strikeSlip_linearElastic_fd Runtime Summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in integration (s): %g\n",_integrateTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent writing output (s): %g\n",_writeTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent propagating the wave (s): %g\n",_propagateTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent writing output: %g\n",_writeTime/totRunTime*100.);CHKERRQ(ierr);
  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_fd::writeContext()
{
  PetscErrorCode ierr = 0;
  // output scalar fields
  string str = _outputDir + "mediator_context.txt";
  PetscViewer    viewer;
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  // time integration settings
  ierr = PetscViewerASCIIPrintf(viewer,"deltaT = %.15e # (s)\n",_deltaT);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"atol = %.15e\n",_atol);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  PetscViewerDestroy(&viewer);

  _D->write();
  _material->writeContext(_outputDir);
  _fault->writeContext(_outputDir);

  return ierr;
}


//======================================================================
// Adaptive time stepping functions
//======================================================================
PetscErrorCode strikeSlip_linearElastic_fd::integrate()
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  initiateIntegrand(); // put initial conditions into var for integration
  _stepCount = 0;

  // initialize time integrator
  _quadWaveEx = new OdeSolver_WaveEq(_D->_maxStepCount,_D->_initTime,_D->_maxTime,_deltaT);
  ierr = _quadWaveEx->setInitialConds(_var);CHKERRQ(ierr);

  ierr = _quadWaveEx->integrate(this);CHKERRQ(ierr);

  _integrateTime += MPI_Wtime() - startTime;
  return ierr;
}


// purely explicit time stepping// note that the heat equation never appears here because it is only ever solved implicitly
PetscErrorCode strikeSlip_linearElastic_fd::d_dt(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev)
{
  PetscErrorCode ierr = 0;

  _fault->updateFields(deltaT, var);

  // momentum balance equation except for fault boundary
  propagateWaves(time, _deltaT, varNext, var, varPrev);

  if (_initialConditions == "tau") { _fault->updatePrestress(time); }
  ierr = _fault->d_dt(time,_deltaT,varNext,var,varPrev); CHKERRQ(ierr);

  // update body u from fault u
  ierr = VecScatterBegin(*_body2fault, _fault->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _fault->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);

  VecCopy(varNext.find("u")->second, _material->_u);
  _material->computeStresses();
  Vec sxy,sxz,sdev;
  ierr = _material->getStresses(sxy,sxz,sdev);
  ierr = VecScatterBegin(*_body2fault, sxy, _fault->_tauP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, sxy, _fault->_tauP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  VecAXPY(_fault->_tauP, 1.0, _fault->_prestress);
  VecAXPY(_fault->_tauP, 1.0, _fault->_tau0);
  VecCopy(_fault->_tauP,_fault->_tauQSP); // keep quasi-static shear stress updated as well

  return ierr;
}


// fully dynamic: off-fault portion of the momentum balance equation
PetscErrorCode strikeSlip_linearElastic_fd::propagateWaves(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev)
{
  PetscErrorCode ierr = 0;
  double startPropagation = MPI_Wtime();

  // compute D2u = (Dyy+Dzz)*u
  Vec D2u, temp;
  VecDuplicate(*_y, &D2u);
  VecDuplicate(*_y, &temp);
  Mat A; _material->_sbp->getA(A);
  ierr = MatMult(A, var.find("u")->second, temp);
  ierr = _material->_sbp->Hinv(temp, D2u);
  VecDestroy(&temp);
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    Vec temp;
    VecDuplicate(D2u, &temp);
    MatMult(Jinv, D2u, temp);
    VecCopy(temp, D2u);
    VecDestroy(&temp);
  }
  ierr = VecScatterBegin(*_body2fault, D2u, _fault->_d2u, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, D2u, _fault->_d2u, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // Propagate waves and compute displacement at the next time step
  // includes boundary conditions except for fault
  PetscInt       Ii,Istart,Iend;
  PetscScalar   *uNextA; // changed in this loop
  const PetscScalar   *u, *uPrev, *d2u, *ay, *rho; // unchchanged in this loop
  ierr = VecGetArray(varNext["u"], &uNextA);
  ierr = VecGetArrayRead(var.find("u")->second, &u);
  ierr = VecGetArrayRead(varPrev.find("u")->second, &uPrev);
  ierr = VecGetArrayRead(_ay, &ay);
  ierr = VecGetArrayRead(D2u, &d2u);
  ierr = VecGetArrayRead(_rho, &rho);

  ierr = VecGetOwnershipRange(varNext["u"],&Istart,&Iend);CHKERRQ(ierr);
  PetscInt       Jj = 0;
  for (Ii = Istart; Ii < Iend; Ii++){
    PetscScalar c1 = deltaT*deltaT / rho[Jj];
    PetscScalar c2 = deltaT*ay[Jj] - 1.0;
    PetscScalar c3 = deltaT*ay[Jj] + 1.0;

    uNextA[Jj] = (c1*d2u[Jj] + 2.*u[Jj] + c2*uPrev[Jj]) / c3;
    Jj++;
  }
  ierr = VecRestoreArray(varNext["u"], &uNextA);
  ierr = VecRestoreArrayRead(var.find("u")->second, &u);
  ierr = VecRestoreArrayRead(varPrev.find("u")->second, &uPrev);
  ierr = VecRestoreArrayRead(_ay, &ay);
  ierr = VecRestoreArrayRead(D2u, &d2u);
  ierr = VecRestoreArrayRead(_rho, &rho);

  VecDestroy(&D2u);
  _propagateTime += MPI_Wtime() - startPropagation;

  return ierr;
}


// compute allowed time step based on CFL condition and user input
// deltaT <= * gcfl * min(dy/cs, dz/cs)
PetscErrorCode strikeSlip_linearElastic_fd::computeTimeStep()
{
  PetscErrorCode ierr = 0;

  // coefficient for CFL condition
  PetscScalar gcfl = 0.7071; // if order = 2
  if (_D->_order == 4) { gcfl = 0.7071/sqrt(1.4498); }
  if (_D->_order == 6) { gcfl = 0.7071/sqrt(2.1579); }

  // compute grid spacing in y and z
  Vec dy, dz;
  VecDuplicate(*_y,&dy);
  VecDuplicate(*_y,&dz);
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    MatGetDiagonal(yq, dy); VecScale(dy,1.0/(_D->_Ny-1));
    MatGetDiagonal(zr, dz); VecScale(dz,1.0/(_D->_Nz-1));
  }
  else {
    VecSet(dy,_D->_Ly/(_D->_Ny-1.0));
    VecSet(dz,_D->_Lz/(_D->_Nz-1.0));
  }

  // compute time for shear wave to travel 1 dy or dz
  Vec ts_dy,ts_dz;
  VecDuplicate(*_y,&ts_dy);
  VecDuplicate(*_z,&ts_dz);
  VecPointwiseDivide(ts_dy,dy,_cs);
  VecPointwiseDivide(ts_dz,dz,_cs);
  PetscScalar min_ts_dy, min_ts_dz;
  VecMin(ts_dy,NULL,&min_ts_dy);
  VecMin(ts_dz,NULL,&min_ts_dz);

  // clean up memory usage
  VecDestroy(&dy);
  VecDestroy(&dz);
  VecDestroy(&ts_dy);
  VecDestroy(&ts_dz);

  // largest possible time step permitted by CFL condition
  PetscScalar max_deltaT = gcfl * min(min_ts_dy,min_ts_dz);

  // compute time step requested by user
  PetscScalar cfl_deltaT = _CFL * gcfl *  max_deltaT;
  PetscScalar request_deltaT = _deltaT;

  _deltaT = max_deltaT; // ensure deltaT is assigned something sensible even if the conditionals have an error
  if (request_deltaT <= 0. && cfl_deltaT <= 0.) {
    // if user did not specify deltaT or CFL
    _deltaT = max_deltaT;
  }
  else if (request_deltaT > 0. && cfl_deltaT <= 0.) {
    // if user specified deltaT but not CFL
    _deltaT = request_deltaT;
    assert(request_deltaT > 0.);
    if (request_deltaT > max_deltaT) {
      PetscPrintf(PETSC_COMM_WORLD,"Warning: requested deltaT of %g is larger than maximum recommended deltaT of %g\n",request_deltaT,max_deltaT);
    }
  }
  else if (request_deltaT <= 0. && cfl_deltaT > 0.) {
    // if user specified CLF but not deltaT
    _deltaT = cfl_deltaT;
    assert(_CFL <= 1. && _CFL >= 0.);
  }
  else if (request_deltaT > 0. && cfl_deltaT > 0.) {
    // if user specified both CLF and deltaT
    _deltaT = request_deltaT;
    if (request_deltaT > max_deltaT) {
      PetscPrintf(PETSC_COMM_WORLD,"Warning: requested deltaT of %g is larger than maximum recommended deltaT of %g\n",request_deltaT,max_deltaT);
    }
  }
  return ierr;
}


// compute alphay and alphaz for use in time stepping routines
PetscErrorCode strikeSlip_linearElastic_fd::computePenaltyVectors()
{
  PetscErrorCode ierr = 0;

  PetscScalar h11y, h11z;
  _material->_sbp->geth11(h11y, h11z);

  Vec alphay,alphaz;
  VecDuplicate(*_y, &alphay); VecSet(alphay,h11y);
  VecDuplicate(*_y, &alphaz); VecSet(alphaz,h11z);
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
    Vec temp1, temp2;
    VecDuplicate(alphay, &temp1);
    VecDuplicate(alphay, &temp2);
    MatMult(yq, alphay, temp1);
    MatMult(zr, alphaz, temp2);
    VecCopy(temp1, alphay);
    VecCopy(temp2, alphaz);
    VecDestroy(&temp1);
    VecDestroy(&temp2);
  }
  VecScatterBegin(_D->_scatters["body2L"], alphay, _fault->_alphay, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2L"], alphay, _fault->_alphay, INSERT_VALUES, SCATTER_FORWARD);
  VecDestroy(&alphay);
  VecDestroy(&alphaz);

  // compute vectors
  VecDuplicate(*_y, &_ay);
  VecSet(_ay, 0.0);

  PetscInt Ii,Istart,Iend;
  VecGetOwnershipRange(_ay,&Istart,&Iend);
  PetscScalar *ay;
  VecGetArray(_ay,&ay);
  PetscInt Jj = 0;
  for (Ii=Istart;Ii<Iend;Ii++) {
    ay[Jj] = 0;

    if ((Ii/_D->_Nz == _D->_Ny-1) && (_D->_fd_bcRType == "outGoingCharacteristics")) {
      ay[Jj] += 0.5 / h11y;
    }
    if ((Ii%_D->_Nz == 0) && (_D->_fd_bcTType == "outGoingCharacteristics")) {
      ay[Jj] += 0.5 / h11z;
    }
    if (((Ii+1)%_D->_Nz == 0) && (_D->_fd_bcBType == "outGoingCharacteristics")) {
      ay[Jj] += 0.5 / h11z;
    }

    if ((Ii/_D->_Nz == 0) && (_D->_fd_bcLType == "outGoingCharacteristics" || _D->_fd_bcLType == "symmFault" || _D->_fd_bcLType == "rigidFault")) {
      ay[Jj] += 0.5 / h11y;
    }
    Jj++;
  }
  VecRestoreArray(_ay,&ay);

  ierr = VecPointwiseMult(_ay, _ay, _cs);

  return ierr;
}
