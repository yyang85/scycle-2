#include "porosityPressure.hpp"

#define FILENAME "porosityPressure.cpp"

using namespace std;


PorosityPressure::PorosityPressure(Domain &D)
  : _D(&D),_phiEvolve("yes"),_kEvolve("yes"),_dilatancy("no"),_viscous("no"),_elastic("yes"),
    _viscBound("no"),_simu("inject"),_file(D._file),_delim(D._delim),
    _outputDir(D._outputDir),_Nz(D._Nz),_Lz(D._Lz),
    _order(D._order), _injectLoc(D._Nz-1), _linSolver(D._linSolver), _kspTol(D._kspTol),
    _maxBEIteration(100), _BETol(0.01)
{
  loadSettings(_file);
  checkInput();
  setFields(D);
  setupSBP(D);
  setupBackEuler(D);

  // load from checkpoint
  if (D._ckpt > 0 && D._ckptNumber > 0) {
    loadCheckpoint();
  }
  else if (_simu == "unsteady") {
    computePhiK();
  }

  loadFieldsFromFiles();
}


// destructor, free memory
PorosityPressure::~PorosityPressure()
{
  VecDestroy(&_p);
  VecDestroy(&_p_t);
  VecDestroy(&_flux);
  VecDestroy(&_z);
  VecDestroy(&_phi);
  VecDestroy(&_phi0);
  VecDestroy(&_k);
  VecDestroy(&_k0);
  VecDestroy(&_rhof);
  VecDestroy(&_beta);
  VecDestroy(&_beta_f);
  VecDestroy(&_beta_phi);
  VecDestroy(&_eta_f);
  VecDestroy(&_sN);
  VecDestroy(&_sNEff);
  VecDestroy(&_rhs);
  VecDestroy(&_coeff);
  VecDestroy(&_bcCoeff);
  VecDestroy(&_bcL);
  VecDestroy(&_bcT);
  VecDestroy(&_bcB);
  VecDestroy(&_v0);
  if (_injectLoc != _Nz-1 && _injectLoc != 0) {
    VecDestroy(&_pSource);
  }
  VecDestroy(&_phi_beta_inv);
  MatDestroy(&_Diag_phi_beta);
  MatDestroy(&_D2_scale);
  VecDestroy(&_rhs_scale);
  KSPDestroy(&_ksp);
  delete _sbp; _sbp = NULL;

  // injection (as in Yang & Dunham, 2021)
  if (_simu == "inject") {
    VecDestroy(&_phie);
    VecDestroy(&_phip);
    VecDestroy(&_phie0);
    VecDestroy(&_phip0);
    VecDestroy(&_dc);
  }
  else {
    VecDestroy(&_L);
  }
  
  // effective viscosity terms
  if (_viscous == "yes") {
    VecDestroy(&_phiv_t);
    VecDestroy(&_rC);
    VecDestroy(&_A);
    VecDestroy(&_T);
    VecDestroy(&_B);
  }

  // dilatancy
  if (_dilatancy == "yes") {
    VecDestroy(&_phip_t);
    VecDestroy(&_phiMax);
  }

  // elastic
  if (_elastic == "yes") {
    VecDestroy(&_phie_t);
  }

  // destroy viewers
  for (map<string,pair<PetscViewer,string>>::iterator it = _viewers.begin(); it != _viewers.end(); it++) {
    PetscViewerDestroy(&_viewers[it->first].first);
  }

  VecScatterDestroy(&_scattersN);
  VecScatterDestroy(&_scatters1);
}


// load field from input file
PetscErrorCode PorosityPressure::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt rank, size;
  ierr = MPI_Comm_size(PETSC_COMM_WORLD, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank); CHKERRQ(ierr);

  ifstream infile(file);
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0, pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos + _delim.length(), line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0, pos);

    // loading vector inputs in the input file
    if (var == "sNEffVals") { loadVectorFromInputFile(rhsFull, _sNEffVals); }
    else if (var == "sNEffDepths") { loadVectorFromInputFile(rhsFull, _sNEffDepths); }
    else if (var == "sNVals") { loadVectorFromInputFile(rhsFull, _sNVals); }
    else if (var == "sNDepths") { loadVectorFromInputFile(rhsFull, _sNDepths); }
    else if (var == "phieVals") { loadVectorFromInputFile(rhsFull, _phieVals); }
    else if (var == "phieDepths") { loadVectorFromInputFile(rhsFull, _phieDepths); }
    else if (var == "phipVals") { loadVectorFromInputFile(rhsFull, _phipVals); }
    else if (var == "phipDepths") { loadVectorFromInputFile(rhsFull, _phipDepths); }
    else if (var == "rhofVals") { loadVectorFromInputFile(rhsFull, _rhofVals); }
    else if (var == "rhofDepths") { loadVectorFromInputFile(rhsFull, _rhofDepths); }
    else if (var == "betafVals") { loadVectorFromInputFile(rhsFull, _betafVals); }
    else if (var == "betafDepths") { loadVectorFromInputFile(rhsFull, _betafDepths); }
    else if (var == "betaphiVals") { loadVectorFromInputFile(rhsFull, _betaphiVals); }
    else if (var == "betaphiDepths") { loadVectorFromInputFile(rhsFull, _betaphiDepths); }
    else if (var == "kVals") { loadVectorFromInputFile(rhsFull, _kVals); }
    else if (var == "kDepths") { loadVectorFromInputFile(rhsFull, _kDepths); }
    else if (var == "etafVals") { loadVectorFromInputFile(rhsFull, _etafVals); }
    else if (var == "etafDepths") { loadVectorFromInputFile(rhsFull, _etafDepths); }
    else if (var == "DcVals") { loadVectorFromInputFile(rhsFull, _DcVals); }
    else if (var == "DcDepths") { loadVectorFromInputFile(rhsFull, _DcDepths); } 
    else if (var == "LVals") { loadVectorFromInputFile(rhsFull, _LVals); }
    else if (var == "LDepths") { loadVectorFromInputFile(rhsFull, _LDepths); } 
    else if (var == "pVals") { loadVectorFromInputFile(rhsFull, _pVals); }
    else if (var == "pDepths") { loadVectorFromInputFile(rhsFull, _pDepths); }

    // variable piecewise linear injection rate
    else if (var == "qVals") { loadVectorFromInputFile(rhsFull, _qVals); }
    else if (var == "qTimes") { loadVectorFromInputFile(rhsFull, _qTimes); }

    else if (var == "simulation_type") { _simu = rhs.c_str(); }
    else if (var == "phiEvolve") { _phiEvolve = rhs.c_str(); }
    else if (var == "kEvolve") { _kEvolve = rhs.c_str(); }
    else if (var == "dilatancy") { _dilatancy = rhs.c_str(); }
    else if (var == "viscous") { _viscous = rhs.c_str(); }
    else if (var == "elastic") { _elastic = rhs.c_str(); }
    else if (var == "viscBound") { _viscBound = rhs.c_str(); }

    else if (var == "alpha") { _alpha = atof(rhs.c_str()); }
    else if (var == "maxVisc") { _maxVisc = atof(rhs.c_str()); }
    else if (var == "phimax") { _phimax = atof(rhs.c_str()); }
    else if (var == "phiref") { _phiref = atof(rhs.c_str()); }
    else if (var == "s") { _s = atof(rhs.c_str()); }    
    else if (var == "eps") { _eps = atof(rhs.c_str()); }
    else if (var == "injectLoc") { _injectLoc = atoi(rhs.c_str()); }
    else if (var == "injectFlux") { _Q = atof(rhs.c_str()); }

    else if (var == "maxBEIteration") { _maxBEIteration = (int)atof(rhs.c_str()); }
    else if (var == "BETol") { _BETol = atof(rhs.c_str()); }

    else if (var == "bcB_type") { _bcB_type = rhs.c_str(); }
    else if (var == "bcB_flux") { _bcB_flux = atof(rhs.c_str()); }
    else if (var == "bcB_pressure") { _bcB_pressure = atof(rhs.c_str()); }
    
    else if (var == "bcT_type") { _bcT_type = rhs.c_str(); }
    else if (var == "bcT_flux") { _bcT_flux = atof(rhs.c_str()); }
    else if (var == "bcT_pressure") { _bcT_pressure = atof(rhs.c_str()); }

    // parameters used to compute effective viscosity
    else if (var == "TVals") { loadVectorFromInputFile(rhsFull,_TVals); }
    else if (var == "TDepths") { loadVectorFromInputFile(rhsFull,_TDepths); }
    else if (var == "AVals") { loadVectorFromInputFile(rhsFull,_AVals); }
    else if (var == "ADepths") { loadVectorFromInputFile(rhsFull,_ADepths); }
    else if (var == "BVals") { loadVectorFromInputFile(rhsFull,_BVals); }
    else if (var == "BDepths") { loadVectorFromInputFile(rhsFull,_BDepths); }
  }
  return ierr;
}


// load vector fields from directory
PetscErrorCode PorosityPressure::loadFieldsFromFiles()
{
  PetscErrorCode ierr = 0;
  
  ierr = loadVecFromInputFile(_p,_D->_inputDir,"p"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_sN,_D->_inputDir,"sN"); CHKERRQ(ierr); 
  ierr = loadVecFromInputFile(_sNEff,_D->_inputDir,"sNEff"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_k,_D->_inputDir,"k"); CHKERRQ(ierr); 
  ierr = loadVecFromInputFile(_phi,_D->_inputDir,"phi"); CHKERRQ(ierr); 

  return ierr;                                                              
}


// set up SBP and initialize vector fields
PetscErrorCode PorosityPressure::setFields(Domain &D)
{
  PetscErrorCode ierr = 0;
  
  // allocate memory and partition accross processors
  VecCreate(PETSC_COMM_WORLD, &_p);
  VecSetSizes(_p, PETSC_DECIDE, _Nz);
  VecSetFromOptions(_p); VecSet(_p, 0);

  VecDuplicate(_p, &_p_t); VecSet(_p_t, 0);
  VecDuplicate(_p, &_flux); VecSet(_flux, 0);
  
  // extract local z from D._z (which is the full 2D field)
  VecDuplicate(_p, &_z);
  PetscInt Istart, Iend;
  PetscScalar z = 0;
  VecGetOwnershipRange(D._z, &Istart, &Iend);
  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    if (Ii < D._Nz) {
      VecGetValues(D._z, 1, &Ii, &z);
      VecSetValue(_z, Ii, z, INSERT_VALUES);
    }
  }
  VecAssemblyBegin(_z);
  VecAssemblyEnd(_z);

  // dr (dz on equidistant grid) at the point of injection
  if (D._zGridSpacingType == "variableGridSpacing") {
    _hinv = 1/(D._dr*1e3);  // convert from km to m
  }
  else {
    _h = _Lz*1e3/(_Nz - 1); // convert from km to m
    _hinv = 1/_h;
  }

  // density and viscosity
  ierr = setVec(_p, _z, _pVals, _pDepths); CHKERRQ(ierr);
  VecDuplicate(_p, &_rhof); VecSet(_rhof, 0);
  ierr = setVec(_rhof, _z, _rhofVals, _rhofDepths); CHKERRQ(ierr);
  VecDuplicate(_p, &_eta_f); VecSet(_eta_f, 0);
  ierr = setVec(_eta_f, _z, _etafVals, _etafDepths); CHKERRQ(ierr);

  // pore compressibility
  VecDuplicate(_p, &_beta_f); VecSet(_beta_f, 0);
  ierr = setVec(_beta_f, _z, _betafVals, _betafDepths); CHKERRQ(ierr);
  VecDuplicate(_p, &_beta_phi); VecSet(_beta_phi, 0);
  ierr = setVec(_beta_phi, _z, _betaphiVals, _betaphiDepths); CHKERRQ(ierr);
  VecDuplicate(_p, &_beta); VecSet(_beta, 0);
  VecWAXPY(_beta, 1, _beta_f, _beta_phi);
  
  // set permeability
  VecDuplicate(_p, &_k); VecSet(_k, 0);
  VecDuplicate(_p, &_k0); VecSet(_k0, 0);
  ierr = setVec(_k0, _z, _kVals, _kDepths); CHKERRQ(ierr);
  VecCopy(_k0, _k);

  // set stresses
  VecDuplicate(_p, &_sN); VecSet(_sN, 0);
  VecDuplicate(_p, &_sNEff); VecSet(_sNEff, 0);
  if (_sNEffVals.size() > 0) {
    ierr = setVec(_sNEff, _z, _sNEffVals, _sNEffDepths); CHKERRQ(ierr);
  }
  if (_sNVals.size() > 0) {
    ierr = setVec(_sN, _z, _sNVals, _sNDepths); CHKERRQ(ierr);
  }	  

  // set porosity values
  VecDuplicate(_p, &_phi0); VecSet(_phi0, 0);
  VecDuplicate(_p, &_phi); VecSet(_phi, 0);
  if (_simu == "inject") {
    VecDuplicate(_p, &_phie); VecSet(_phie, 0);
    ierr = setVec(_phie, _z, _phieVals, _phieDepths); CHKERRQ(ierr);
    VecDuplicate(_p, &_phip); VecSet(_phip, 0);
    ierr = setVec(_phip, _z, _phipVals, _phipDepths); CHKERRQ(ierr);
    VecDuplicate(_p, &_phie0); VecSet(_phie0, 0);
    ierr = setVec(_phie0, _z, _phieVals, _phieDepths); CHKERRQ(ierr);
    VecDuplicate(_p, &_phip0); VecSet(_phip0, 0);
    ierr = setVec(_phip0, _z, _phipVals, _phipDepths); CHKERRQ(ierr);
    VecWAXPY(_phi0, 1, _phie0, _phip0);
    VecCopy(_phi0, _phi);
    VecDuplicate(_p, &_dc); VecSet(_dc, 0);
    ierr = setVec(_dc, _z, _DcVals, _DcDepths); CHKERRQ(ierr);
  }
  else {
    VecDuplicate(_p, &_L); VecSet(_L, 0);
    ierr = setVec(_L, _z, _LVals, _LDepths); CHKERRQ(ierr);
    VecSet(_phi0, _phiref);
    VecSet(_phi, _phiref);
    // compute phiMax = phimax*exp(-sNEff*s)
    VecDuplicate(_p, &_phiMax); VecSet(_phiMax, 0);
    VecCopy(_sNEff, _phiMax);
    VecScale(_phiMax, _s);
    VecScale(_phiMax, -1);
    VecExp(_phiMax);
    VecScale(_phiMax, _phimax);
  }

  // set referece velocity v0, to be used when updating dphip/dt
  VecDuplicate(_p, &_v0); VecSet(_v0, _D->_vL);

  // if injecting from middle of domain
  if (_injectLoc != _Nz-1 && _injectLoc != 0) {
    VecDuplicate(_p, &_pSource);
    VecSet(_pSource, 0);
  }

  // if including viscous porosity changes
  // compaction rate = Rc = phi*sNEff*A*exp(-B/T)
  if (_viscous == "yes") {
    VecDuplicate(_p, &_phiv_t); VecSet(_phiv_t, 0);
    VecDuplicate(_p, &_rC); VecSet(_rC, 0);
    VecDuplicate(_p, &_A); VecSet(_A, 0);
    ierr = setVec(_A, _z, _AVals, _ADepths); CHKERRQ(ierr);
    VecDuplicate(_p, &_T); VecSet(_T, 0);
    ierr = setVec(_T, _z, _TVals, _TDepths); CHKERRQ(ierr);
    VecDuplicate(_p, &_B); VecSet(_B, 0);
    ierr = setVec(_B, _z, _BVals, _BDepths); CHKERRQ(ierr);    
  }

  // if including plastic porosity changes from dilatancy
  if (_dilatancy == "yes") {
    VecDuplicate(_p, &_phip_t); VecSet(_phip_t, 0);
  }

  // if including elastic porosity changes
  if (_elastic == "yes") {
    VecDuplicate(_p, &_phie_t); VecSet(_phie_t, 0);
  }

  // rhs for SAT term
  VecDuplicate(_p, &_rhs); VecSet(_rhs, 0);
  VecDuplicate(_p, &_rhs_scale); VecSet(_rhs_scale, 0);
  
  // variable coefficient
  VecDuplicate(_p, &_coeff); VecSet(_coeff, 0);

  // boundary condition vector
  VecDuplicate(_p, &_bcCoeff); VecSet(_bcCoeff, 0);

  // boundary conditions
  VecDuplicate(_p, &_bcL); VecSet(_bcL, 0);

  VecCreate(PETSC_COMM_WORLD, &_bcT);
  VecSetSizes(_bcT, PETSC_DECIDE, 1);
  VecSetFromOptions(_bcT);

  VecDuplicate(_bcT, &_bcB); VecSet(_bcB, 0);

  // scatter for bottom boundary
  PetscInt *fiN;
  PetscMalloc1(1, &fiN);
  fiN[0] = _Nz - 1;
  IS isfN;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, fiN, PETSC_COPY_VALUES, &isfN);
  PetscInt *tiN;
  PetscMalloc1(1, &tiN);
  tiN[0] = 0;
  IS istN;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, tiN, PETSC_COPY_VALUES, &istN);
  ierr = VecScatterCreate(_p, isfN, _bcB, istN, &_scattersN); CHKERRQ(ierr);
  PetscFree(fiN);
  PetscFree(tiN);
  ISDestroy(&isfN);
  ISDestroy(&istN);

  // scatter for top boundary
  PetscInt *fi1;
  PetscMalloc1(1, &fi1);
  fi1[0] = 0;
  IS isf1;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, fi1, PETSC_COPY_VALUES, &isf1);
  PetscInt *ti1;
  PetscMalloc1(1, &ti1);
  ti1[0] = 0;
  IS ist1;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, ti1, PETSC_COPY_VALUES, &ist1);
  ierr = VecScatterCreate(_p, isf1, _bcT, ist1, &_scatters1); CHKERRQ(ierr);
  PetscFree(fi1);
  PetscFree(ti1);
  ISDestroy(&isf1);
  ISDestroy(&ist1);
  
  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode PorosityPressure::checkInput()
{
  PetscErrorCode ierr = 0;
  assert(_bcB_type == "Q" || _bcB_type == "p");
  assert(_bcT_type == "Q" || _bcT_type == "p");
  assert(_pVals.size()       == _pDepths.size());
  assert(_betafVals.size()   == _betafDepths.size());
  assert(_betaphiVals.size() == _betaphiDepths.size());
  assert(_kVals.size()       == _kDepths.size());
  assert(_etafVals.size()    == _etafDepths.size());
  assert(_DcVals.size()      == _DcDepths.size());
  assert(_qVals.size()       == _qTimes.size());
  assert(_pVals.size()       != 0);
  assert(_betafVals.size()   != 0);
  assert(_betaphiVals.size() != 0);
  assert(_kVals.size()       != 0);
  assert(_etafVals.size()    != 0);
  assert(_DcVals.size()      != 0);
  assert(_Lz > 0);
  assert(_Nz > 0);

  return ierr;
}


PetscErrorCode PorosityPressure::setupSBP(Domain &D)
{
  PetscErrorCode ierr = 0;

  // set up variable coefficient _coeff = k/eta
  computeVarCoeff();

  // Set up linear system
  // inputs: order, Ny, Nz, Ly, Lz, coefficient vector
  if (D._zGridSpacingType == "constantGridSpacing") {
    _sbp = new SbpOps_m_constGrid(_order, 1, _Nz, 1, _Lz, _coeff);
  }
  else if (D._zGridSpacingType == "variableGridSpacing") {
    _sbp = new SbpOps_m_varGrid(_order, 1, _Nz, 1, _Lz, _coeff);
    _sbp->setGrid(NULL, &_z);  // only set z grid for pressure 1D
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }

  // set BC types for top and bottom
  parseBCs();
  
  // set BC types for SBP operator. Inputs: bcR, bcT, bcL, bcB
  _sbp->setBCTypes("Dirichlet", _mat_bcT_type, "Dirichlet", _mat_bcB_type);

  // impose boundary conditions for top and bottom
  imposeBCs();

  // H*A will be symmetric, so H is multiplied everywhere to make the linear system easier to solve (for backward Euler)
  _sbp->setMultiplyByH(1);
  _sbp->setDeleteIntermediateFields(1);
  _sbp->setCompatibilityType(D._sbpCompatibilityType);
  _sbp->setLaplaceType("z");
  _sbp->computeMatrices(); // SBP matrices are constructed after calling this

  return ierr;
}


// set boundary condition matrix types
PetscErrorCode PorosityPressure::parseBCs() {
  PetscErrorCode ierr = 0;

  if (_bcT_type == "Q") {  // fix flux
    _mat_bcT_type = "Neumann";
  }
  else if (_bcT_type == "p") {   // fix pressure
    _mat_bcT_type = "Dirichlet";
  }

  if (_bcB_type == "Q") {   // fix flux
    _mat_bcB_type = "Neumann";
  }
  else if (_bcB_type == "p") {    // fix pressure
    _mat_bcB_type = "Dirichlet";
  }
  
  return ierr;
}


// impose flux or pressure to bottom and/or top boundaries
// dp/dz = -flux*eta/k
PetscErrorCode PorosityPressure::imposeBCs() {
  PetscErrorCode ierr = 0;

  // bottom boundary
  if (_bcB_type == "p") {
    VecSet(_bcB, _bcB_pressure);
  }
  else if (_bcB_type == "Q") {
    VecSet(_bcCoeff, _bcB_flux);
    VecScatterBegin(_scattersN, _bcCoeff, _bcB, INSERT_VALUES, SCATTER_FORWARD);
    VecScatterEnd(_scattersN, _bcCoeff, _bcB, INSERT_VALUES, SCATTER_FORWARD);
  }
    
  // top boundary
  if (_bcT_type == "p") {
    VecSet(_bcT, _bcT_pressure);
  }
  else if (_bcT_type == "Q") {
    VecSet(_bcCoeff, _bcT_flux);
    VecScatterBegin(_scatters1, _bcCoeff, _bcT, INSERT_VALUES, SCATTER_FORWARD);
    VecScatterEnd(_scatters1, _bcCoeff, _bcT, INSERT_VALUES, SCATTER_FORWARD);
  }

  return ierr;
}


// set initial plastic porosity and permeability for the case with dilatancy
// only use for injection (as in Yang & Dunham, 2021)
PetscErrorCode PorosityPressure::setInitialVals(const Vec &V)
{
  PetscErrorCode ierr = 0;
  // phip_ss =  phip,0 + eps * ln(V/v0)), v0 is the reference velocity
  // compute steady state plastic porosity at V
  if (_eps > 0) {
    Vec VV0;
    VecDuplicate(V,&VV0);
    VecPointwiseDivide(VV0, V, _v0);
    VecLog(VV0);
    VecScale(VV0, _eps);  // VV0 = eps*log(V/v0)  
    VecWAXPY(_phip, 1, _phip0, VV0);  // _phip = _phip,0 + eps*log(V/v0)

    // recompute initial phi0
    VecWAXPY(_phi0, 1, _phie0, _phip);
    VecCopy(_phi0, _phi);
    VecDestroy(&VV0);
  }

  if (_alpha > 0) {
    // compute steady state permeability at V: k = k0*(new phi/old phi)^alpha
    // if calculated phi0 < phi, permeability will also be lower
    Vec kinit;
    VecDuplicate(_k, &kinit);
    VecPointwiseDivide(kinit, _phi0, _phi);
    VecPow(kinit, _alpha);
    VecPointwiseMult(kinit, kinit, _k0);

    VecCopy(kinit, _k0);
    VecCopy(kinit, _k);
    VecDestroy(&kinit);
  }
  
  return ierr;
}


// compute phi, k if starting from unsteady state
// phi = phi0*exp(-beta_phi*sNEff)
// k = k0*(phi/phi0)^alpha
PetscErrorCode PorosityPressure::computePhiK()
{
  PetscErrorCode ierr = 0;
  VecPointwiseMult(_phi, _beta_phi, _sNEff);
  VecScale(_phi, -1);
  VecExp(_phi);
  VecScale(_phi, _phiref);

  VecPointwiseDivide(_k, _phi, _phi0);
  VecPow(_k, _alpha);
  VecPointwiseMult(_k, _k, _k0);

  return ierr;
}


// set up KSP, coefficient matrix, boundary conditions for backward Euler
PetscErrorCode PorosityPressure::setupBackEuler(Domain &D)
{
  PetscErrorCode ierr = 0;
  
  Mat D2;
  _sbp->getA(D2); // D2 already has H premultiplied within sbp class
  
  Mat H;
  _sbp->getH(H);

  // set up boundary terms (bcL, bcR, bcT, bcB)
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);

  MatDuplicate(H, MAT_DO_NOT_COPY_VALUES, &_Diag_phi_beta);

  VecDuplicate(_p, &_phi_beta_inv);
  VecSet(_phi_beta_inv, 1);
  VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _phi);
  VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _beta);
  MatDiagonalSet(_Diag_phi_beta, _phi_beta_inv, INSERT_VALUES);
  
  MatMatMult(_Diag_phi_beta, D2, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &_D2_scale);
  if (D._zGridSpacingType == "variableGridSpacing") {
    Mat J, Jinv, qy, rz, yq, zr;
    _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
    transformCoord(Jinv, _D2_scale);
  }

  // _D2_scale = H - dt * _D2_scale
  MatScale(_D2_scale, -D._initDeltaT);
  MatAXPY(_D2_scale, 1, H, SUBSET_NONZERO_PATTERN);

  setupKSP(_ksp, _pc, _D2_scale);

  return ierr;
}


// set up linear solver context
PetscErrorCode PorosityPressure::setupKSP(KSP &ksp,PC &pc,Mat &A)
{
  PetscErrorCode ierr = 0;

  // create linear solver context
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);

  // set operators, here the matrix that defines the linear system also serves as the preconditioning matrix
  ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);

  // algebraic multigrid from HYPRE
  if (_linSolver == "AMG") {
    ierr = KSPSetType(ksp,KSPRICHARDSON); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(pc,"boomeramg"); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCFactorSetLevels(pc,4); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }

  // preconditioned conjugate gradient
  else if (_linSolver == "CG") {
    ierr = KSPSetType(ksp,KSPCG); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  }

  // undefined linear solver
  else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"ERROR: linSolver type not understood\n");
    assert(0);
  }

  // enable command line options to override those specified above, e.g.:
  // -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

  // perform computation of preconditioners now, rather than on first use
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);

  return ierr;
}


// compute dphiv/dt = -sNEff*A*exp(-B/T)*phi
// (A*exp(-B/T) < 1/maxVisc) = 1/maxVisc --> take max(A*exp(-B/t), 1/maxVisc)
// rC = sNEff*A*exp(-B/T)
PetscErrorCode PorosityPressure::computeDPhiv()
{
  PetscErrorCode ierr = 0;

  Vec expTerm;
  VecDuplicate (_p, &expTerm);
  VecPointwiseDivide(expTerm, _B, _T);
  VecScale(expTerm, -1);
  VecExp(expTerm);  // expTerm = exp(-B/T)
  VecPointwiseMult(_phiv_t, _A, expTerm); // phiv_t = A*exp(-B/T)

  if (_viscBound == "yes") {
    PetscScalar minInvVisc = 1/_maxVisc;
    VecSet(expTerm, minInvVisc);
    VecPointwiseMax(_phiv_t, _phiv_t, expTerm); // max(A*exp(-B/T), 1/maxVisc)
  }
  
  VecPointwiseMult(_phiv_t, _phiv_t, _sNEff);
  VecCopy(_phiv_t, _rC);
  VecPointwiseMult(_phiv_t, _phiv_t, _phi);
  VecScale(_phiv_t, -1);

  VecDestroy(&expTerm);

  return ierr;
}


// compute dphip/dt = V/L*(phiMax - phi)
PetscErrorCode PorosityPressure::computeDPhip(const Vec &V)
{
  PetscErrorCode ierr = 0;

  Vec dilate;
  VecDuplicate(_p, &dilate);

  // phiMax = (phimax + eps*ln(V/v0))*exp(-sNEff*s)
  VecSet(_phiMax, _phimax);
  VecPointwiseDivide(dilate, V, _v0);
  VecLog(dilate);
  VecScale(dilate, _eps);
  VecAXPY(_phiMax, 1, dilate);
  
  VecCopy(_sNEff, dilate);
  VecScale(dilate, _s);
  VecScale(dilate, -1);
  VecExp(dilate);
  VecPointwiseMult(_phiMax, _phiMax, dilate);

  VecWAXPY(dilate, -1, _phi, _phiMax);  // dilate = phiMax - phi
  VecPointwiseDivide(_phip_t, V, _L);
  VecPointwiseMult(_phip_t, _phip_t, dilate);

  VecDestroy(&dilate);
  return ierr;
}


// compute variable coefficient vector = k/eta
// k changes in time
PetscErrorCode PorosityPressure::computeVarCoeff()
{
  PetscErrorCode ierr = 0;
  VecCopy(_k, _coeff);
  VecPointwiseDivide(_coeff, _coeff, _eta_f);
  return ierr;
}


// compute flux: q = k/eta*(dp/dz)
PetscErrorCode PorosityPressure::computeFlux(Vec &P)
{
  PetscErrorCode ierr = 0;

  _sbp->Dz(P, _flux);  // Dp
  VecPointwiseMult(_flux, _flux, _coeff);   // k/eta*(dp/dz)
  
  return ierr;
}

 
// transform coordinate system for input vector
PetscErrorCode PorosityPressure::transformCoord(const Mat &trans, Vec &vec)
{
  PetscErrorCode ierr = 0;

  Vec vecTrans;
  VecDuplicate(vec, &vecTrans);
  MatMult(trans, vec, vecTrans);
  VecCopy(vecTrans, vec);
  VecDestroy(&vecTrans);

  return ierr;
}


// transform coordinate system for input matrix
PetscErrorCode PorosityPressure::transformCoord(const Mat &trans, Mat &mat)
{
  PetscErrorCode ierr = 0;

  Mat matTrans;
  MatMatMult(trans, mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &matTrans);
  MatCopy(matTrans, mat, SAME_NONZERO_PATTERN);
  MatDestroy(&matTrans);

  return ierr;
}


// add injection point source term
PetscErrorCode PorosityPressure::injectSource(const PetscScalar time)
{
  PetscErrorCode ierr = 0;

  // linearly interpolate injection rate if specified
  // otherwise, Q = injectFlux (single value in input file)
  if (_qVals.size() > 0) {
    for (unsigned int i = 0; i < _qVals.size()-1; i++) {
      if (time >= _qTimes[i] && time < _qTimes[i+1]) {
	// piecewise linear injection rate interpolation
	_Q = _qVals[i] + (_qVals[i+1] - _qVals[i])*(time - _qTimes[i])/(_qTimes[i+1] - _qTimes[i]);
      }
    }
  }

  // set value of source term = Q/phi/beta/dz
  VecSet(_pSource, 0);
  VecSetValue(_pSource, _injectLoc, _Q, INSERT_VALUES);
  VecAssemblyBegin(_pSource);
  VecAssemblyEnd(_pSource);
  VecScale(_pSource, _hinv);  // divide by dz at injection location
  // convert to correct dz
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Mat J, Jinv, qy, rz, yq, zr;
    _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
    transformCoord(Jinv, _pSource);
  }
  VecPointwiseDivide(_pSource, _pSource, _phi);
  VecPointwiseDivide(_pSource, _pSource, _beta);

  return ierr;
}	


// set up integration, put pressure, permeability and porosity into varEx, varIm
PetscErrorCode PorosityPressure::initiateIntegrand(const PetscScalar time, map<string, Vec> &varEx, map<string, Vec> &varIm)
{
  PetscErrorCode ierr = 0;

  Vec p;
  VecDuplicate(_p, &p);
  VecCopy(_p, p);
  varIm["pressure"] = p;

  if (_simu == "inject") {
    if (_phiEvolve == "yes") {
      Vec phie;
      VecDuplicate(_phie, &phie);
      VecCopy(_phie, phie);
      varIm["phie"] = phie;
    }
  
    if (_dilatancy == "yes") {
      Vec phip;
      VecDuplicate(_phip, &phip);
      VecCopy(_phip, phip);
      varEx["phip"] = phip;
    }
  }
  else {
    if (_phiEvolve == "yes") {
      Vec phi;
      VecDuplicate(_phi, &phi);
      VecCopy(_phi, phi);
      varIm["phi"] = phi;
    }
  }
  
  return ierr;
}


// use to set up time integration for explicitly integrated variables
PetscErrorCode PorosityPressure::updateFields(const map<string, Vec> &varEx)
{
  PetscErrorCode ierr = 0;

  if (_phiEvolve == "yes" && varEx.find("phip") != varEx.end()) {
    VecCopy(varEx.find("phip")->second, _phip);
  }
  
  return ierr;
}


// update _p, _phie, _phip, _phi from values stored in varEx and varIm
// _p, _phie/_phi are implicitly integrated
// _phip is explicitly integrated (injection)
PetscErrorCode PorosityPressure::updateFields(const map<string, Vec> &varEx, const map<string, Vec> &varImo)
{
  PetscErrorCode ierr = 0;

  if (varImo.find("pressure") != varImo.end()) {
    VecCopy(varImo.find("pressure")->second, _p);
  }

  if (_phiEvolve == "yes" && varImo.find("phi") != varImo.end()) {
    VecCopy(varImo.find("phi")->second, _phi);
  }

  if (_phiEvolve == "yes" && varImo.find("phie") != varImo.end()) {
    VecCopy(varImo.find("phie")->second, _phie);
  }
  
  if (_phiEvolve == "yes" && varEx.find("phip") != varEx.end()) {
    VecCopy(varEx.find("phip")->second, _phip);
  }

  // compute new phi = phie + phip for injection
  if (_phiEvolve == "yes" && _simu == "inject") {
    VecWAXPY(_phi,1,_phie,_phip);
  }
    
  if (_kEvolve == "yes") {
    // compute new value of permeability = k0*(phi/phi0)^alpha
    VecPointwiseDivide(_k, _phi, _phi0);
    VecPow(_k, _alpha);
    VecPointwiseMult(_k, _k, _k0);
  }

 return ierr;
}


// update rate of change of total porosity (as in Yang & Dunham, 2022)
// dphi/dt = phi*beta_phi*dp/dt - phi*sNEff*A*exp(-B/T) + V/L*(phi_max - phi)
PetscErrorCode PorosityPressure::backEuler_phi(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // phi_next = (phi_curr + V/L*phi_max*dt)/[1 - dt*(beta_phi*_p_t - rC - V/L)]
  Vec num;
  VecDuplicate(_p, &num);
  VecCopy(varImo.find("phi")->second, num);
  Vec denom;
  VecDuplicate(_p, &denom);
  VecSet(denom, 1);
  Vec terms;
  VecDuplicate(_p, &terms);
  VecSet(terms, 0);

  // elastic part
  if (_elastic == "yes") {
    VecCopy(_p_t, terms);
    VecPointwiseMult(terms, terms, _beta_phi);
    VecScale(terms, dt);
    VecAXPY(denom, -1, terms);  // denominator = 1 - beta(dp/dt)*dt
  }
  
  // with dilatancy
  if (_dilatancy == "yes") {
    // add term to numerator
    Vec vel_abs;
    VecDuplicate(dvarEx.find("slip")->second, &vel_abs);
    VecCopy(dvarEx.find("slip")->second, vel_abs);
    VecAbs(vel_abs);
    computeDPhip(vel_abs);
    VecCopy(_phiMax, terms);
    VecPointwiseMult(terms, terms, vel_abs);
    VecPointwiseDivide(terms, terms, _L);
    VecScale(terms, dt);
    VecAXPY(num, 1, terms);  // numerator = phi_curr + V/L*phi_max*dt

    // add term to denominator
    VecCopy(vel_abs, terms);
    VecPointwiseDivide(terms, terms, _L);
    VecScale(terms, dt);
    VecAXPY(denom, 1, terms);  // denominator = 1 - beta_phi*(dp/dt)*dt + V/L*dt
    VecDestroy(&vel_abs);
  }

  // with viscous porosity change
  if (_viscous == "yes") {
    computeDPhiv();
    VecCopy(_rC, terms);
    VecScale(terms, dt);
    VecAXPY(denom, 1, terms);  // denominator = 1 - beta_phi*(dp/dt)*dt + V/L*dt + rC*dt
  }

  // compute phi_next
  VecPointwiseDivide(_phi, num, denom);
  VecCopy(_phi, varIm["phi"]);
  
  VecDestroy(&num);
  VecDestroy(&denom);
  VecDestroy(&terms);

  return ierr;  
}


// explicit time stepping version for phi, to be called in fd
// dphi/dt = phi*beta_phi*dp/dt
// with viscous porosity changes, -phi*sNEff*A*exp(-B/T)
// with dilatancy, + V/L(phiMax - phi)
PetscErrorCode PorosityPressure::dphi_dt(const Vec &Phi, const Vec &dPdt, const Vec &dPhipv, Vec &dPhi, const Vec &V)
{
  PetscErrorCode ierr = 0;

  if (_phiEvolve == "yes") {
    if (_elastic == "yes") {
      VecCopy(dPdt, dPhi);
      VecPointwiseMult(dPhi,dPhi,_phi);
      VecPointwiseMult(dPhi,dPhi,_beta_phi);
      VecAXPY(dPhi, 1, dPhipv);  // dPhipv can be viscous and/or plastic, or 0
    }
    else {
      VecCopy(dPhipv, dPhi);
    }
  }  
  return ierr;
}


// explicit time stepping version for phiv and phip, to be called in fd
// need as an input to dp_dt
// with viscous porosity changes, -phi*sNEff*A*exp(-B/T)
// with dilatancy, + V/L(phiMax - phi)
PetscErrorCode PorosityPressure::dphipv_dt(Vec &dPhipv, const Vec &V)
{
  PetscErrorCode ierr = 0;

  VecSet(dPhipv, 0);
  if (_phiEvolve == "yes") {
    if (_viscous == "yes") {
      computeDPhiv();
      VecAXPY(dPhipv, 1, _phiv_t);
    } 
    if (_dilatancy == "yes") {
      computeDPhip(V);
      VecAXPY(dPhipv, 1, _phip_t);
    }
  }  
  return ierr;
}


// update rate of change of plastic porosity (as in Yang & Dunham, 2021)
PetscErrorCode PorosityPressure::dphip_dt(const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;

  // dphip/dt = -V/dc*(phip - phip,0 - eps * ln(V/v0))
  // v0 is the reference velocity
  Vec vel_abs;
  VecDuplicate(dvarEx.find("slip")->second, &vel_abs);
  VecCopy(dvarEx.find("slip")->second, vel_abs);
  VecAbs(vel_abs);
  
  Vec dphip = dvarEx["phip"];

  Vec phip;
  VecDuplicate(varEx.find("phip")->second, &phip);
  VecCopy(varEx.find("phip")->second, phip);
  VecAXPY(phip, -1, _phip0);   // phip = phip - phip,0

  Vec Vdc;
  VecDuplicate(vel_abs, &Vdc);
  VecPointwiseDivide(Vdc, vel_abs, _dc);
  VecScale(Vdc, -1);  // Vdc = -V/dc

  VecPointwiseDivide(vel_abs, vel_abs, _v0); // V/v0
  VecLog(vel_abs);  // log(V/v0)
  VecScale(vel_abs, _eps);  // vel_abs = eps*log(V/v0)
  
  VecAXPY(phip, -1, vel_abs);  // phip - phip,0 - eps*log(V/v0)
  VecPointwiseMult(dphip, phip, Vdc);
  // dphip = -V/dc*(phip_p - phip,0 - eps*log(V/v0))
  VecCopy(dphip, _phip_t);

  VecDestroy(&phip);
  VecDestroy(&Vdc);
  VecDestroy(&vel_abs);

  return ierr;  
}


// update rate of change of plastic porosity, to be used in fd
PetscErrorCode PorosityPressure::dphip_dt(const Vec &Phip, const Vec &V, Vec &dPhip)
{
  PetscErrorCode ierr = 0;

  Vec vel_abs;
  VecDuplicate(V, &vel_abs);
  VecCopy(V, vel_abs);
  VecAbs(vel_abs);

  // dphip/dt = -V/dc*(phip - phip,0 - eps * ln(V/v0)), v0 is the reference velocity
  Vec Phip_temp;
  VecDuplicate(Phip, &Phip_temp);
  VecCopy(Phip, Phip_temp);
  VecWAXPY(Phip_temp, -1, _phip0, Phip);   // phip_temp = phip - phip,0

  Vec Vdc;
  VecDuplicate(vel_abs, &Vdc);
  VecPointwiseDivide(Vdc, vel_abs, _dc);
  VecScale(Vdc, -1);  // Vdc = -V/dc

  VecPointwiseDivide(vel_abs, vel_abs, _v0); // V/v0
  VecLog(vel_abs);  // log(V/v0)
  VecScale(vel_abs, _eps);  // vel_abs = eps*log(V/v0)
  
  VecAXPY(Phip_temp, -1, vel_abs);  // phip - phip,0 - eps*log(V/v0)
  // dphip = -V/dc*(phip_p - phip,0 - eps*log(V/v0))
  VecPointwiseMult(dPhip, Phip_temp, Vdc);

  VecDestroy(&Vdc);
  VecDestroy(&Phip_temp);
  VecDestroy(&vel_abs);

  return ierr;  
}


// explicit time stepping version for phie, to be called in fd
// dphie/dt = phi*beta_phi*dp/dt
PetscErrorCode PorosityPressure::dphie_dt(const Vec &Phie, const Vec &dPdt, Vec &dPhie)
{
  PetscErrorCode ierr = 0;

  if (_phiEvolve == "yes") {
    if (_elastic == "yes") {
      VecCopy(dPdt, dPhie);
      VecPointwiseMult(dPhie,dPhie,_phi);
      VecPointwiseMult(dPhie,dPhie,_beta_phi);
    }
    else {
      VecSet(dPhie, 0);
    }
    VecCopy(dPhie, _phie_t);
  }
  return ierr;
}


// explicit time stepping version for p, to be called in fd
PetscErrorCode PorosityPressure::dp_dt(const PetscScalar time, const Vec &P, const Vec &dPhip, Vec &dPdt)
{
  PetscErrorCode ierr = 0;
  
  computeVarCoeff();
  _sbp->updateVarCoeff(_coeff);
  imposeBCs();

  // dP/dt = Hinv*Jinv*(D2*p - rhs)/beta/phi + source term
  // -dphiv/dt/phi/beta for viscous porosity
  // -dphip/dt/beta/phi for dilatancy
  Mat D2;
  _sbp->getA(D2);
  MatMult(D2, P, dPdt);
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
  VecAXPY(dPdt, -1.0, _rhs);

  Vec scaled_dPdt;
  VecDuplicate(dPdt, &scaled_dPdt);
  
  // compute Hinv*Jinv/phi/beta
  VecSet(_phi_beta_inv, 1);
  VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _phi);
  VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _beta);
  MatDiagonalSet(_Diag_phi_beta, _phi_beta_inv, INSERT_VALUES);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Mat J, Jinv, qy, rz, yq, zr;
    _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
    transformCoord(Jinv, _Diag_phi_beta);
  }
  MatMult(_Diag_phi_beta, dPdt, scaled_dPdt);
  Vec Hinv_dPdt;
  VecDuplicate(dPdt, &Hinv_dPdt);
  _sbp->Hinv(scaled_dPdt, Hinv_dPdt);

  // add plastic and viscous porosity contribution if enabled
  Vec pPore;
  VecDuplicate(_p, &pPore);
  VecPointwiseDivide(pPore, dPhip, _phi);
  VecPointwiseDivide(pPore, pPore, _beta);
  VecAXPY(Hinv_dPdt, -1, pPore);
  
  // add source term if injecting from middle of domain
  if (_injectLoc != _Nz-1 && _injectLoc != 0) {
    injectSource(time);
    // add to previous terms
    VecAXPY(Hinv_dPdt, 1, _pSource);
  }

  VecCopy(Hinv_dPdt, dPdt);

  VecDestroy(&pPore);
  VecDestroy(&scaled_dPdt);
  VecDestroy(&Hinv_dPdt);

  return ierr;
}

  
// update elastic porosity, to be called after backEuler_p which computes dp/dt
PetscErrorCode PorosityPressure::backEuler_phie(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // dphie/dt = phi * beta_phi * dp/dt
  // phie_next = phie_curr/(1 - dt*beta_phi*_p_t)
  Vec phie_curr;
  VecDuplicate(_p, &phie_curr);
  VecCopy(varImo.find("phie")->second, phie_curr);
  
  Vec denom;
  VecDuplicate(_p, &denom);
  VecSet(denom, 1);
  Vec terms;
  VecDuplicate(_p, &terms);
  VecCopy(_p_t, terms);

  VecPointwiseMult(terms, terms, _beta_phi);
  VecScale(terms, dt);
  VecAXPY(denom, -1, terms);
  VecPointwiseDivide(phie_curr, phie_curr, denom); // phie_curr becomes phie_next
  VecCopy(phie_curr, _phie);
  VecCopy(phie_curr, varIm["phie"]);
  
  VecDestroy(&phie_curr);
  VecDestroy(&denom);
  VecDestroy(&terms);

  return ierr;  
}


// backward Euler implicit solve, new result for _p goes in varIm
PetscErrorCode PorosityPressure::backEuler_p(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt, const PetscScalar time)
{
  PetscErrorCode ierr = 0;

  // store p_curr
  Vec p_curr;
  VecDuplicate(_p, &p_curr);
  VecCopy(varImo.find("pressure")->second, p_curr);

  Vec Hp;
  VecDuplicate(_p, &Hp);
  _sbp->H(p_curr, Hp);

  // store previous p, phi from fixed-point iteration for residual norm
  Vec p_prev;
  VecDuplicate(_p, &p_prev);
  VecCopy(varImo.find("pressure")->second, p_prev);

  // (H - dt*Jinv*D2_scale)*p_next = H*p_curr - dt*Jinv*rhs_scale + source term
  // if viscous, -dt*H*dphiv/beta/phi
  // if dilatancy, -dt*H*dphip/beta/phi
  PetscInt i = 0;
  for (i = 0; i < _maxBEIteration; i++) {
    computeVarCoeff();
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
    _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
  
    Mat D2;
    _sbp->getA(D2);

    Mat H;
    _sbp->getH(H);
  
    // compute _D2_scale
    VecSet(_phi_beta_inv, 1);
    VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _phi);
    VecPointwiseDivide(_phi_beta_inv, _phi_beta_inv, _beta);
    MatDiagonalSet(_Diag_phi_beta, _phi_beta_inv, INSERT_VALUES);
    if (_D->_zGridSpacingType == "variableGridSpacing") {
      Mat J, Jinv, qy, rz, yq, zr;
      _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
      transformCoord(Jinv, _Diag_phi_beta);
    }

    // _D2_scale = H - dt*Jinv*_Diag_phi_beta*D2
    MatMatMult(_Diag_phi_beta, D2, MAT_REUSE_MATRIX, PETSC_DEFAULT, &_D2_scale);
    MatScale(_D2_scale, -dt);
    MatAXPY(_D2_scale, 1, H, SUBSET_NONZERO_PATTERN);

    // _rhs_scale = H*p_curr - dt*Jinv*_Diag_phi_beta*_rhs
    MatMult(_Diag_phi_beta, _rhs, _rhs_scale);
    VecScale(_rhs_scale, -dt);
    VecAXPY(_rhs_scale, 1, Hp);

    // _rhs_scale = H*p_curr - dt*Jinv*_Diag_phi_beta*_rhs + source term

    // add plastic porosity contribution from dilatancy
    // -dt*H*(dphip/dt)/beta/phi
    if (_dilatancy == "yes") {
      Vec dilate;
      VecDuplicate(_p, &dilate);
      Vec Hdilate;
      VecDuplicate(_p, &Hdilate);
      if (_simu == "inject") {
	VecCopy(dvarEx.find("phip")->second, dilate);
      }
      else {
	computeDPhip(dvarEx.find("slip")->second);
	VecCopy(_phip_t, dilate);
      }
      VecPointwiseDivide(dilate, dilate, _beta);
      VecPointwiseDivide(dilate, dilate, _phi);
      MatMult(H, dilate, Hdilate);
      VecScale(Hdilate, dt);
      VecAXPY(_rhs_scale, -1, Hdilate);
      VecDestroy(&dilate);
      VecDestroy(&Hdilate);  
    }
  
    // add viscous porosity contribution
    if (_viscous == "yes") {
      Vec viscous;
      VecDuplicate(_p, &viscous);
      Vec Hviscous;
      VecDuplicate(_p, &Hviscous);

      computeDPhiv();
      VecCopy(_phiv_t, viscous);
      VecPointwiseDivide(viscous, viscous, _beta);
      VecPointwiseDivide(viscous, viscous, _phi);
      MatMult(H, viscous, Hviscous);
      VecScale(Hviscous, dt);
      VecAXPY(_rhs_scale, -1, Hviscous); // -dt*H*dphiv/phi/beta (dphiv is negative)
      VecDestroy(&viscous);
      VecDestroy(&Hviscous);
    }

    // add source term if injecting from middle of domain
    if (_injectLoc != _Nz-1 && _injectLoc != 0) {
      injectSource(time);
      VecScale(_pSource, dt);
      Vec HpSource;
      VecDuplicate(_pSource, &HpSource);
      MatMult(H, _pSource, HpSource);
      // _rhs_scale += dt*H*Q/beta/phi/dz
      VecAXPY(_rhs_scale, 1, HpSource);
      VecDestroy(&HpSource);
    }
  
    // solve for p_next
    ierr = KSPSetOperators(_ksp, _D2_scale, _D2_scale); CHKERRQ(ierr);
    ierr = KSPSolve(_ksp, _rhs_scale, _p); CHKERRQ(ierr);

    // update effective normal stress
    VecWAXPY(_sNEff, -1, _p, _sN);

    // dp/dt = (p_next - p_curr)/dt
    VecWAXPY(_p_t, -1, p_curr, _p);
    PetscScalar a = 1/dt;
    VecScale(_p_t, a);

    // backward Euler to solve for phi_next
    if (_simu == "inject" && _phiEvolve == "yes" && _elastic == "yes") {
      backEuler_phie(varEx, dvarEx, varIm, varImo, dt);
    }
    else if (_simu != "inject" && _phiEvolve == "yes") {
      backEuler_phi(varEx, dvarEx, varIm, varImo, dt);
    }
    
    // update k using new phi
    if (_kEvolve == "yes") {
      // compute new value of permeability = k0*(phi/phi0)^alpha
      VecPointwiseDivide(_k, _phi, _phi0);
      VecPow(_k, _alpha);
      VecPointwiseMult(_k, _k, _k0);
    }

    // calculate relative error, store in err
    PetscReal err = 0.0;
    relativeErr(p_prev, err);
    
    // break out of loop if drop below tolerance
    if (err < _BETol) {
      PetscPrintf(PETSC_COMM_WORLD, "backEuler breaking at iteration = %i with relative error = %.0e\n", i, err);
      break;
    }
    else {
      VecCopy(_p, p_prev);
    }
  }
  
  // dphie/dt = phi*beta_phi*dp/dt
  if (_elastic == "yes") {
    VecPointwiseMult(_phie_t, _phi, _beta_phi);
    VecPointwiseMult(_phie_t, _phie_t, _p_t);
  }

  // copy variable back to integrator and compute fluid flux
  VecCopy(_p, varIm["pressure"]);
  computeFlux(_p);

  // free memory
  VecDestroy(&p_curr);
  VecDestroy(&p_prev);
  VecDestroy(&Hp);  
  return ierr;
}


// calculate relative error                                              
PetscErrorCode PorosityPressure::relativeErr(Vec &p_prev, PetscReal &err) {    
  PetscErrorCode ierr = 0;                                               
                                                                         
  PetscScalar norm;                                                      
  Vec errVec;                                                            
  VecDuplicate(_p, &errVec);                                             
  VecSet(errVec,0.0);                                                    
  ierr = VecWAXPY(errVec, -1.0, p_prev, _p); CHKERRQ(ierr);              
  VecNorm(errVec, NORM_2, &err);                                         
  VecNorm(p_prev, NORM_2, &norm);                                        
  err = err / norm;                                                      
                                                                         
  VecDestroy(&errVec);                                                   
                                                                         
  return ierr;                                                           
}


// pressure solver for undrained response (ignore diffusion term)
PetscErrorCode PorosityPressure::undrained_p(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt, const PetscScalar time)
{
  PetscErrorCode ierr = 0;

  Vec p_curr;
  VecDuplicate(_p, &p_curr);
  VecCopy(varImo.find("pressure")->second, p_curr);

  // store previous p from fixed-point iteration for residual norm
  Vec p_prev;
  VecDuplicate(_p, &p_prev);
  VecCopy(varImo.find("pressure")->second, p_prev);

  // p_next = p_curr + dt*source term
  // if viscous, -dt*dphiv/beta/phi
  // if dilatancy, -dt*dphip/beta/phi
  PetscInt i = 0;
  for (i = 0; i < _maxBEIteration; i++) {
    VecCopy(p_curr, _rhs);
    
    // add plastic porosity contribution from dilatancy
    // -dt*(dphip/dt)/beta/phi
    if (_dilatancy == "yes") {
      Vec dilate;
      VecDuplicate(_p, &dilate);
      if (_simu == "inject") {
	VecCopy(dvarEx.find("phip")->second, dilate);
      }
      else {
	if (_simu == "testUndrained") {
	  Vec V;
	  VecDuplicate(_p,&V);
	  PetscScalar vt = 1e-7*(sin(time)+1.1);
	  VecSet(V, vt);
	  computeDPhip(V);
	}
	else {
	  computeDPhip(dvarEx.find("slip")->second);
	}
	VecCopy(_phip_t, dilate);
      }
      VecPointwiseDivide(dilate, dilate, _beta);
      VecPointwiseDivide(dilate, dilate, _phi);
      VecScale(dilate, dt);
      VecAXPY(_rhs, -1, dilate);
      VecDestroy(&dilate);
    }
  
    // add viscous porosity contribution
    if (_viscous == "yes") {
      Vec viscous;
      VecDuplicate(_p, &viscous);
      computeDPhiv();
      VecCopy(_phiv_t, viscous);
      VecPointwiseDivide(viscous, viscous, _beta);
      VecPointwiseDivide(viscous, viscous, _phi);
      VecScale(viscous, dt);
      VecAXPY(_rhs, -1, viscous); // -dt*dphiv/phi/beta (dphiv is negative)
      VecDestroy(&viscous);
    }

    // add source term if injecting from middle of domain
    if (_injectLoc != _Nz-1 && _injectLoc != 0) {
      injectSource(time);
      VecScale(_pSource, dt);
      // _rhs += dt*Q/beta/phi/dz
      VecAXPY(_rhs, 1, _pSource);
    }
    
    // update effective normal stress
    VecCopy(_rhs, _p);
    VecWAXPY(_sNEff, -1, _p, _sN);
    
    // dp/dt = (p_next - p_curr)/dt
    VecWAXPY(_p_t, -1, p_curr, _p);
    PetscScalar a = 1/dt;
    VecScale(_p_t, a);

    // backward Euler to solve for phi_next
    if (_simu == "inject" && _phiEvolve == "yes" && _elastic == "yes") {
      backEuler_phie(varEx, dvarEx, varIm, varImo, dt);
    }
    else if (_simu != "inject" && _phiEvolve == "yes") {
      backEuler_phi(varEx, dvarEx, varIm, varImo, dt);
    }
    
    // calculate relative error, store in err
    PetscReal err = 0.0;
    relativeErr(p_prev, err);
    
    // break out of loop if drop below tolerance
    if (err < _BETol) {
      PetscPrintf(PETSC_COMM_WORLD, "backEuler breaking at iteration = %i with relative error = %.0e\n", i, err);
      break;
    }
    else {
      VecCopy(_p, p_prev);
    }
  }
    
  // dphie/dt = phi*beta_phi*dp/dt
  if (_elastic == "yes") {
    VecPointwiseMult(_phie_t, _phi, _beta_phi);
    VecPointwiseMult(_phie_t, _phie_t, _p_t);
  }

  // update k using new phi
  if (_kEvolve == "yes") {
    // compute new value of permeability = k0*(phi/phi0)^alpha
    VecPointwiseDivide(_k, _phi, _phi0);
    VecPow(_k, _alpha);
    VecPointwiseMult(_k, _k, _k0);
  }
  
  // copy variable back to integrator
  VecCopy(_p, varIm["pressure"]);
  computeFlux(_p);
  
  // dp/dt = (p_next - p_curr)/dt
  VecWAXPY(_p_t, -1, p_curr, _p);
  PetscScalar a = 1/dt;
  VecScale(_p_t, a);

  // dphie/dt = phi*beta_phi*dp/dt
  if (_elastic == "yes") {
    VecPointwiseMult(_phie_t, _phi, _beta_phi);
    VecPointwiseMult(_phie_t, _phie_t, _p_t);
  }
  
  // free memory
  VecDestroy(&p_curr);
  VecDestroy(&p_prev);
  return ierr;
}


// purely explicit time integration
PetscErrorCode PorosityPressure::d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;
  if (_simu == "inject" && _phiEvolve == "yes" && _dilatancy == "yes") {
    ierr = dphip_dt(varEx, dvarEx); CHKERRQ(ierr);
  }
  return ierr;
}


// implicit/explicit time integration
PetscErrorCode PorosityPressure::d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;
  if (_simu == "inject" && _phiEvolve == "yes" && _dilatancy == "yes") {
    dphip_dt(varEx, dvarEx);
  }

  if (_simu == "undrained" || _simu == "testUndrained") {
    undrained_p(varEx, dvarEx, varIm, varImo, dt, time);
  }
  else {
    backEuler_p(varEx, dvarEx, varIm, varImo, dt, time);
  }

  return ierr;
}


// =====================================================================
// IO commands

// extends SymmFault's writeContext
PetscErrorCode PorosityPressure::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;
  PetscViewer viewer;

  // write out context for porosityPressure
  string str = outputDir + "hydro_context.txt";
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  ierr = PetscViewerASCIIPrintf(viewer, "injection location index = %i\n", _injectLoc); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "injection flux (m/s) = %.5e\n", _Q); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "alpha = %.5e\n", _alpha); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "epsilon = %.5e\n", _eps); CHKERRQ(ierr);
  if (_bcB_flux > 0) {
    ierr = PetscViewerASCIIPrintf(viewer, "bcB_flux = %.5e\n", _bcB_flux); CHKERRQ(ierr);
  }
  PetscViewerDestroy(&viewer);

  return ierr;
}


// write solution at specified time step, called within timeMonitor, which is called in odeSolver (so does not include hydrostatic pressure, need to manually add)
PetscErrorCode PorosityPressure::writeStep(const PetscInt stepCount, const PetscScalar time, const string outputDir)
{
  PetscErrorCode ierr = 0;
  
  if (_viewers.size() <= 1) {
    ierr = initiate_appendVecToOutput(_viewers, "p", _p, outputDir + "hydro_p",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "k", _k, outputDir + "hydro_k",_D->_outFileMode); CHKERRQ(ierr);

    if (_simu == "inject") {
      ierr = initiate_appendVecToOutput(_viewers, "phie", _phie, outputDir + "hydro_phie",_D->_outFileMode); CHKERRQ(ierr);
      ierr = initiate_appendVecToOutput(_viewers, "phip", _phip, outputDir + "hydro_phip",_D->_outFileMode); CHKERRQ(ierr);
    }

    if (_viscous == "yes") {
      ierr = initiate_appendVecToOutput(_viewers, "phivt", _phiv_t, outputDir + "hydro_phivt",_D->_outFileMode); CHKERRQ(ierr);
      ierr = initiate_appendVecToOutput(_viewers, "rC", _rC, outputDir + "hydro_rC",_D->_outFileMode); CHKERRQ(ierr);
    }

    if (_dilatancy == "yes") {
      ierr = initiate_appendVecToOutput(_viewers, "phipt", _phip_t, outputDir + "hydro_phipt",_D->_outFileMode); CHKERRQ(ierr);
    }

    if (_elastic == "yes") {
      ierr = initiate_appendVecToOutput(_viewers, "phiet", _phie_t, outputDir + "hydro_phiet",_D->_outFileMode); CHKERRQ(ierr);
    }

    ierr = initiate_appendVecToOutput(_viewers, "phi", _phi, outputDir + "hydro_phi",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "flux", _flux, outputDir + "hydro_flux",_D->_outFileMode); CHKERRQ(ierr);
  }

  else {
    ierr = VecView(_p, _viewers["p"].first); CHKERRQ(ierr);
    ierr = VecView(_k, _viewers["k"].first); CHKERRQ(ierr);

    if (_simu == "inject") {
      ierr = VecView(_phie, _viewers["phie"].first); CHKERRQ(ierr);
      ierr = VecView(_phip, _viewers["phip"].first); CHKERRQ(ierr);
    }

    if (_viscous == "yes") {
      ierr = VecView(_phiv_t, _viewers["phivt"].first); CHKERRQ(ierr);
      ierr = VecView(_rC, _viewers["rC"].first); CHKERRQ(ierr);
    }

    if (_dilatancy == "yes") {
      ierr = VecView(_phip_t, _viewers["phipt"].first); CHKERRQ(ierr);
    }

    if (_elastic == "yes") {
      ierr = VecView(_phie_t, _viewers["phiet"].first); CHKERRQ(ierr);
    }

    ierr = VecView(_phi, _viewers["phi"].first); CHKERRQ(ierr);
    ierr = VecView(_flux, _viewers["flux"].first); CHKERRQ(ierr);
  }

  return ierr;
}


// write checkpoint files, called at each checkpoint interval, after writeStep
PetscErrorCode PorosityPressure::writeCheckpoint()
{
  PetscErrorCode ierr = 0;
  ierr = writeVec(_p, _outputDir + "chkpt_p"); CHKERRQ(ierr);
  ierr = writeVec(_k, _outputDir + "chkpt_k"); CHKERRQ(ierr);
  if (_simu == "inject") {
    ierr = writeVec(_phie, _outputDir + "chkpt_phie"); CHKERRQ(ierr);
    ierr = writeVec(_phip, _outputDir + "chkpt_phip"); CHKERRQ(ierr);
  }
  ierr = writeVec(_phi, _outputDir + "chkpt_phi"); CHKERRQ(ierr);
  ierr = writeVec(_p_t, _outputDir + "chkpt_p_t"); CHKERRQ(ierr);
  return ierr;
}


// load checkpoint files
PetscErrorCode PorosityPressure::loadCheckpoint()
{
  PetscErrorCode ierr = 0;
  // load vectors
  ierr = loadVecFromInputFile(_p, _outputDir + "chkpt_", "p"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_k, _outputDir + "chkpt_", "k"); CHKERRQ(ierr);
  if (_simu == "inject") {
    ierr = loadVecFromInputFile(_phie, _outputDir + "chkpt_", "phie"); CHKERRQ(ierr);
    ierr = loadVecFromInputFile(_phip, _outputDir + "chkpt_", "phip"); CHKERRQ(ierr);
  }
  ierr = loadVecFromInputFile(_phi, _outputDir + "chkpt_", "phi"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_p_t, _outputDir + "chkpt_", "p_t"); CHKERRQ(ierr);
  
  return ierr;
}  


// return pressure
PetscErrorCode PorosityPressure::getPressure(Vec &P)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(_p, P); CHKERRQ(ierr);
  return ierr;
}


// set pressure and update effective normal stress
PetscErrorCode PorosityPressure::setPressure(const Vec &P)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(P, _p); CHKERRQ(ierr);
  ierr = VecWAXPY(_sNEff, -1, _p, _sN);
  return ierr;
}


// return elastic porosity
PetscErrorCode PorosityPressure::getPhie(Vec &Phie)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(_phie, Phie); CHKERRQ(ierr);
  return ierr;
}


// set elastic porosity
PetscErrorCode PorosityPressure::setPhie(const Vec &Phie)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(Phie, _phie); CHKERRQ(ierr);
  return ierr;
}


// return plastic porosity
PetscErrorCode PorosityPressure::getPhip(Vec &Phip)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(_phip, Phip); CHKERRQ(ierr);
  return ierr;
}


// set plastic porosity
PetscErrorCode PorosityPressure::setPhip(const Vec &Phip)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(Phip, _phip); CHKERRQ(ierr);
  return ierr;
}


// return plastic porosity
PetscErrorCode PorosityPressure::getPhi(Vec &Phi)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(_phi, Phi); CHKERRQ(ierr);
  return ierr;
}


// set plastic porosity
PetscErrorCode PorosityPressure::setPhi(const Vec &Phi)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(Phi, _phi); CHKERRQ(ierr);
  return ierr;
}


// return permeability
PetscErrorCode PorosityPressure::getPermeability(Vec &K)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(_k, K); CHKERRQ(ierr);
  return ierr;
}


// set permeability
PetscErrorCode PorosityPressure::setPermeability(const Vec &K)
{
  PetscErrorCode ierr = 0;
  ierr = VecCopy(K, _k); CHKERRQ(ierr);
  return ierr;
}


// set effective normal stress
PetscErrorCode PorosityPressure::setSNEff(const Vec &P)
{
  PetscErrorCode ierr = 0;
  ierr = VecWAXPY(_sNEff, -1, P, _sN); CHKERRQ(ierr);
  return ierr;

}
