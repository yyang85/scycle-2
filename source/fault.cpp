#include "fault.hpp"

#define FILENAME "fault.cpp"

using namespace std;

Fault::Fault(Domain &D, VecScatter& scatter2fault, const int& faultTypeScale)
  : _D(&D),_inputFile(D._file),_delim(D._delim),
    _inputDir(D._inputDir),_outputDir(D._outputDir),
    _stateLaw("agingLaw"),_faultTypeScale(faultTypeScale),
    _N(D._Nz),_L(D._Lz),_f0(0.6),_v0(1e-6),
    _sigmaN_cap(1e14),_sigmaN_floor(0.),_fw(0.64),_tau_c(3),_D_fh(5),
    _rootTol(1e-12),_rootIts(0),_maxNumIts(1e4),
    _benchmark("no"),_amax(0.025),_b0(0.015),_sneffWf(50),
    _computeVelTime(0),_stateLawTime(0), _scatterTime(0),
    _body2fault(&scatter2fault)
{
  loadSettings(_inputFile);
  setFields(D);
  loadFieldsFromFiles();
  checkInput();

  // radiation damping parameter: 0.5 * sqrt(mu*rho)
  VecDuplicate(_tauP,&_eta_rad);
  VecPointwiseMult(_eta_rad,_mu,_rho);
  VecSqrtAbs(_eta_rad);
  VecScale(_eta_rad,1.0/_faultTypeScale);
}


// read input file and load fields
PetscErrorCode Fault::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos+_delim.length(),line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after the appearance of a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0,pos);

    if (var == "DcVals") { loadVectorFromInputFile(rhsFull,_DcVals); }
    else if (var == "DcDepths") { loadVectorFromInputFile(rhsFull,_DcDepths); }
    else if (var == "sNVals") { loadVectorFromInputFile(rhsFull,_sigmaNVals); }
    else if (var == "sNDepths") { loadVectorFromInputFile(rhsFull,_sigmaNDepths); }
    else if (var == "sN_cap") { _sigmaN_cap = atof( rhs.c_str() ); }
    else if (var == "sN_floor") { _sigmaN_floor = atof( rhs.c_str() ); }
    else if (var == "aVals") { loadVectorFromInputFile(rhsFull,_aVals); }
    else if (var == "aDepths") { loadVectorFromInputFile(rhsFull,_aDepths); }
    else if (var == "bVals") { loadVectorFromInputFile(rhsFull,_bVals); }
    else if (var == "bDepths") { loadVectorFromInputFile(rhsFull,_bDepths); }
    else if (var == "cohesionVals") { loadVectorFromInputFile(rhsFull,_cohesionVals); }
    else if (var == "cohesionDepths") { loadVectorFromInputFile(rhsFull,_cohesionDepths); }
    else if (var == "muVals") { loadVectorFromInputFile(rhsFull,_muVals); }
    else if (var == "muDepths") { loadVectorFromInputFile(rhsFull,_muDepths); }
    else if (var == "rhoVals") { loadVectorFromInputFile(rhsFull,_rhoVals); }
    else if (var == "rhoDepths") { loadVectorFromInputFile(rhsFull,_rhoDepths); }

    // tolerance for nonlinear solve
    else if (var == "rootTol") { _rootTol = atof( rhs.c_str() ); }

    // friction parameters
    else if (var == "f0") { _f0 = atof( rhs.c_str() ); }
    else if (var == "v0") { _v0 = atof( rhs.c_str() ); }

    // flash heating parameters
    else if (var == "fw") { _fw = atof( rhs.c_str() ); }
    else if (var == "VwVals") { loadVectorFromInputFile(rhsFull, _VwVals); }
    else if (var == "VwDepths") { loadVectorFromInputFile(rhsFull, _VwDepths); }
    else if (var == "TwVals") { loadVectorFromInputFile(rhsFull,_TwVals); }
    else if (var == "TwDepths") { loadVectorFromInputFile(rhsFull,_TwDepths); }
    else if (var == "D") { _D_fh = atof( rhs.c_str() ); }
    else if (var == "tau_c") { _tau_c = atof( rhs.c_str() ); }

    // for locking part of the fault
    else if (var == "lockedVals") { loadVectorFromInputFile(rhsFull,_lockedVals); }
    else if (var == "lockedDepths") { loadVectorFromInputFile(rhsFull,_lockedDepths); }

    // for customizing initial state variable and shear stress on the fault
    else if (var == "psiVals") { loadVectorFromInputFile(rhsFull,_psiVals); }
    else if (var == "psiDepths") { loadVectorFromInputFile(rhsFull,_psiDepths); }
    else if (var == "tauQSVals") { loadVectorFromInputFile(rhsFull,_tauQSVals); }
    else if (var == "tauQSDepths") { loadVectorFromInputFile(rhsFull,_tauQSDepths); }

    // for BP1-FD setting uniform initial shear stress
    else if (var == "benchmark") { _benchmark = rhs.c_str(); }
  }

  return ierr;
}


// load vector fields from directory
PetscErrorCode Fault::loadFieldsFromFiles()
{
  PetscErrorCode ierr = 0;

  ierr = loadVecFromInputFile(_sN,_D->_inputDir,"sN"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_sNEff,_D->_inputDir,"sNEff"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_psi,_D->_inputDir,"psi"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_slip,_D->_inputDir,"slip"); CHKERRQ(ierr);

  // load shear stress: pre-stress, quasistatic, and full
  ierr = loadVecFromInputFile(_tauQSP,_D->_inputDir,"tauQS"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_tauP,_D->_inputDir,"tau"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_prestress,_D->_inputDir,"prestress"); CHKERRQ(ierr);
  VecAXPY(_tauQSP,1.0,_prestress);
  VecCopy(_tauQSP,_tauP);

  // rate and state parameters
  ierr = loadVecFromInputFile(_a,_D->_inputDir,"a"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_b,_D->_inputDir,"b"); CHKERRQ(ierr);

  return ierr;
}


// load a checkpoint
PetscErrorCode Fault::loadCheckpoint()
{
  PetscErrorCode ierr = 0;

  ierr = loadVecFromInputFile(_sN,_outputDir + "chkpt_","sN"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_sNEff,_outputDir + "chkpt_","sNEff"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_psi,_outputDir + "chkpt_","psi"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_slip,_outputDir + "chkpt_","slip"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_slipVel,_outputDir + "chkpt_","slipVel"); CHKERRQ(ierr);

  // load shear stress: pre-stress, quasistatic, and full
  ierr = loadVecFromInputFile(_tauQSP,_outputDir + "chkpt_","tauQS"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_tauP,_outputDir + "chkpt_","tau"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_prestress,_outputDir + "chkpt_","prestress"); CHKERRQ(ierr);

  // rate and state parameters
  ierr = loadVecFromInputFile(_a,_outputDir,"fault_a"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_b,_outputDir,"fault_b"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_Dc,_outputDir,"fault_Dc"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_cohesion,_outputDir,"fault_cohesion"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_locked,_outputDir,"fault_locked"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_locked,_outputDir,"fault_locked"); CHKERRQ(ierr);

  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode Fault::checkInput()
{
  PetscErrorCode ierr = 0;
  assert(_DcVals.size() == _DcDepths.size() );
  assert(_aVals.size() == _aDepths.size() );
  assert(_bVals.size() == _bDepths.size() );
  assert(_sigmaNVals.size() == _sigmaNDepths.size() );
  assert(_cohesionVals.size() == _cohesionDepths.size() );
  assert(_rhoVals.size() == _rhoDepths.size() );
  assert(_muVals.size() == _muDepths.size() );
  assert(_DcVals.size() != 0 );
  assert(_aVals.size() != 0 );
  assert(_bVals.size() != 0 );
  assert(_rhoVals.size() != 0 );
  assert(_muVals.size() != 0 );
  assert(_rootTol >= 1e-14);

  assert(_v0 > 0);
  assert(_f0 > 0);

  if (_D->_stateLaw == "flashHeating") {
    assert(_TwVals.size() == _TwDepths.size() );
    assert(_VwVals.size() == _VwDepths.size() );
    assert(_TwVals.size() != 0 );
    assert(_VwVals.size() != 0 );
  }

  return ierr;
}


// allocate memory for fields, and set some vectors
PetscErrorCode Fault::setFields(Domain& D)
{
  PetscErrorCode ierr = 0;

  // create z from D._z
  VecDuplicate(D._y0,&_z);
  double scatterStart = MPI_Wtime();
  VecScatterBegin(*_body2fault, D._z, _z, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, D._z, _z, INSERT_VALUES, SCATTER_FORWARD);
  _scatterTime += MPI_Wtime() - scatterStart;

  VecDuplicate(_z,&_tauP); VecSet(_tauP,0.0);
  VecDuplicate(_tauP,&_tauQSP); VecSet(_tauQSP,0.0);
  VecDuplicate(_tauP,&_strength); VecSet(_strength,0.0);
  VecDuplicate(_tauP,&_prestress); VecSet(_prestress, 0.0);

  VecDuplicate(_tauP,&_psi); VecSet(_psi,0.0);
  VecDuplicate(_tauP,&_slip); VecSet(_slip,0.0);
  VecDuplicate(_tauP,&_slipVel); VecSet(_slipVel,0.0);
  VecDuplicate(_tauP,&_eta_rad); VecSet(_eta_rad,0.0);
  
  VecDuplicate(_tauP,&_Dc); VecSet(_Dc,0.0);
  VecDuplicate(_tauP,&_a); VecSet(_a,0.0);
  VecDuplicate(_tauP,&_b); VecSet(_b,0.0);
  VecDuplicate(_tauP,&_cohesion);  VecSet(_cohesion,0.0);
  VecDuplicate(_tauP,&_sN); VecSet(_sN,0.0);
  VecDuplicate(_tauP,&_sNEff); VecSet(_sNEff,0.0);
  VecDuplicate(_tauP,&_rho); VecSet(_rho,0.0);
  VecDuplicate(_tauP,&_mu); VecSet(_mu,0.0);
  VecDuplicate(_tauP,&_locked); VecSet(_locked,0.0);
  VecDuplicate(_tauP,&_slip0); VecSet(_slip0, 0.0);

  if (_D->_stateLaw == "flashHeating") {
    VecDuplicate(_tauP,&_T);
    VecDuplicate(_tauP,&_k);
    VecDuplicate(_tauP,&_c);
    VecDuplicate(_tauP,&_Tw);
    ierr = setVec(_Tw,_z,_TwVals,_TwDepths); CHKERRQ(ierr);
    VecDuplicate(_tauP,&_Vw);
    ierr = setVec(_Vw,_z,_VwVals,_VwDepths); CHKERRQ(ierr);
  }
  else { _T = NULL; _k = NULL; _c = NULL; _Tw = NULL; _Vw = NULL; }

  // set fields
  ierr = setVec(_a,_z,_aVals,_aDepths); CHKERRQ(ierr);
  ierr = setVec(_b,_z,_bVals,_bDepths); CHKERRQ(ierr);
  ierr = setVec(_Dc,_z,_DcVals,_DcDepths); CHKERRQ(ierr);
  if (_sigmaNVals.size() > 0) {
    ierr = setVec(_sN,_z,_sigmaNVals,_sigmaNDepths); CHKERRQ(ierr);
  }
  
  if (_lockedVals.size() > 0 ) {
    ierr = setVec(_locked,_z,_lockedVals,_lockedDepths); CHKERRQ(ierr);
  }
  else {
    VecSet(_locked,0.);
  }

  if (_cohesionVals.size() > 0 ) {
    ierr = setVec(_cohesion,_z,_cohesionVals,_cohesionDepths); CHKERRQ(ierr);
  }

  scatterStart = MPI_Wtime();

  Vec temp1;
  VecDuplicate(_D->_y,&temp1);
  ierr = setVec(temp1,_D->_z,_rhoVals,_rhoDepths); CHKERRQ(ierr);
  VecScatterBegin(*_body2fault, temp1, _rho, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, temp1, _rho, INSERT_VALUES, SCATTER_FORWARD);

  ierr = setVec(temp1,_D->_z,_muVals,_muDepths); CHKERRQ(ierr);
  VecScatterBegin(*_body2fault, temp1, _mu, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, temp1, _mu, INSERT_VALUES, SCATTER_FORWARD);

  VecDestroy(&temp1);
  
  _scatterTime += MPI_Wtime() - scatterStart;

  if (_stateVals.size() > 0) {
    ierr = setVec(_psi,_z,_stateVals,_stateDepths); CHKERRQ(ierr);
  }
  else {
    ierr = VecSet(_psi,_f0);CHKERRQ(ierr);
  }

  { // impose floor and ceiling on effective normal stress
    Vec temp;
    VecDuplicate(_sN,&temp);
    VecSet(temp,_sigmaN_cap);
    VecPointwiseMin(_sN,_sN,temp);
    VecSet(temp,_sigmaN_floor);
    VecPointwiseMax(_sN,_sN,temp);
    VecDestroy(&temp);
  }
  VecCopy(_sN,_sNEff);

  return ierr;
}


// set fields needed for flash heating from body fields owned by heat equation
PetscErrorCode Fault::setThermalFields(const Vec& T, const Vec& k, const Vec& c)
{
  PetscErrorCode ierr = 0;

  // scatters values from body fields to the fault
  VecScatterBegin(*_body2fault, T, _T, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, T, _T, INSERT_VALUES, SCATTER_FORWARD);

  VecScatterBegin(*_body2fault, k, _k, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, k, _k, INSERT_VALUES, SCATTER_FORWARD);

  VecScatterBegin(*_body2fault, c, _c, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(*_body2fault, c, _c, INSERT_VALUES, SCATTER_FORWARD);

  return ierr;
}


// update temperature on the fault based on the temperature body field
PetscErrorCode Fault::updateTemperature(const Vec& T)
{
  PetscErrorCode ierr = 0;

  if (_D->_stateLaw == "flashHeating") {
    double scatterStart = MPI_Wtime();
    VecScatterBegin(*_body2fault, T, _T, INSERT_VALUES, SCATTER_FORWARD);
    VecScatterEnd(*_body2fault, T, _T, INSERT_VALUES, SCATTER_FORWARD);
    _scatterTime += MPI_Wtime() - scatterStart;
  }

  return ierr;
}


// use pore pressure to compute total normal stress
// sNEff = sN - rho*g*z - dp
// sN = p + _sNEff
PetscErrorCode Fault::setSN(const Vec& p)
{
  PetscErrorCode ierr = 0;

  ierr = VecWAXPY(_sN, 1, p, _sNEff); CHKERRQ(ierr);

  // impose floor and ceiling on normal stress
  Vec temp;
  VecDuplicate(_sN,&temp);
  VecSet(temp,_sigmaN_cap);
  VecPointwiseMin(_sN,_sN,temp);
  VecSet(temp,_sigmaN_floor);
  VecPointwiseMax(_sN,_sN,temp);

  VecDestroy(&temp);

  return ierr;
}


// compute effective normal stress from normal stress and pore pressure:
// sNEff = sN - rho*g*z - p
PetscErrorCode Fault::setSNEff(const Vec& p)
{
  PetscErrorCode ierr = 0;

  ierr = VecWAXPY(_sNEff, -1, p, _sN); CHKERRQ(ierr);

  // impose floor and ceiling on effective normal stress
  Vec temp;
  VecDuplicate(_sNEff,&temp);
  VecSet(temp,_sigmaN_cap);
  VecPointwiseMin(_sNEff,_sNEff,temp);
  VecSet(temp,_sigmaN_floor);
  VecPointwiseMax(_sNEff,_sNEff,temp);

  VecDestroy(&temp);

  return ierr;
}


// print runtime summary
PetscErrorCode Fault::view(const double totRunTime)
{
  PetscErrorCode ierr = 0;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"\n-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Fault Runtime Summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   compute slip vel time (s): %g\n",_computeVelTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   state law time (s): %g\n",_stateLawTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   scatter time (s): %g\n",_scatterTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent finding slip vel law: %g\n",(_computeVelTime/totRunTime)*100.);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent in state law: %g\n",(_stateLawTime/totRunTime)*100.);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent in scatters: %g\n",(_scatterTime/totRunTime)*100.);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);
  return ierr;
}


// write out parameter settings into "fault_context.txt" file in output directory
// also output vector fields into their respective files in output directory
PetscErrorCode Fault::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;

  PetscViewer    viewer;
  // write out scalar info
  string str = outputDir + "fault_context.txt";
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  ierr = PetscViewerASCIIPrintf(viewer,"rootTol = %.15e\n",_rootTol);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"f0 = %.15e\n",_f0);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"v0 = %.15e\n",_v0);CHKERRQ(ierr);

  // write flash heating parameters if this is enabled
  if (_D->_stateLaw == "flashHeating") {
    ierr = PetscViewerASCIIPrintf(viewer,"fw = %.15e\n",_fw);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"tau_c = %.15e # (GPa)\n",_tau_c);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"D = %.15e # (um)\n",_D);CHKERRQ(ierr);
  }
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  // output vector fields
  if (_D->_stateLaw == "flashHeating" && _D->_thermalCoupling == "coupled") {
    ierr = writeVec(_Tw,outputDir + "fault_Tw"); CHKERRQ(ierr);
  }

  return ierr;
}


// writes out vector fields at each time step (specified by user using stepCount)
PetscErrorCode Fault::writeStep(PetscInt stepCount, const string outputDir)
{
  PetscErrorCode ierr = 0;

  // these files are initiated only for the first time step and when are checkpointing for the first time, since the files don't exist yet
  // writing vectors into binary files
  if (_viewers.empty()) {
    ierr = initiate_appendVecToOutput(_viewers, "slip", _slip, outputDir + "slip", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "slipVel", _slipVel, outputDir + "slipVel", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "tauQSP", _tauQSP, outputDir + "tauQSP", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "strength", _strength, outputDir + "strength", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "psi", _psi, outputDir + "psi", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "sNEff", _sNEff, outputDir + "sNEff", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "prestress", _prestress, outputDir + "prestress", _D->_outFileMode); CHKERRQ(ierr);

    if (_D->_stateLaw == "flashHeating" && _D->_thermalCoupling == "coupled") {
      ierr = initiate_appendVecToOutput(_viewers, "T", _T, outputDir + "fault_T", _D->_outFileMode); CHKERRQ(ierr);
      // if Vw is not constant over time
      ierr = initiate_appendVecToOutput(_viewers, "Vw", _Vw, outputDir + "Vw", _D->_outFileMode); CHKERRQ(ierr);
    }
  }
  else {
    ierr = VecView(_slip,_viewers["slip"].first); CHKERRQ(ierr);
    ierr = VecView(_slipVel,_viewers["slipVel"].first); CHKERRQ(ierr);
    ierr = VecView(_tauQSP,_viewers["tauQSP"].first); CHKERRQ(ierr);
    ierr = VecView(_strength,_viewers["strength"].first); CHKERRQ(ierr);
    ierr = VecView(_psi,_viewers["psi"].first); CHKERRQ(ierr);
    ierr = VecView(_sNEff,_viewers["sNEff"].first); CHKERRQ(ierr);
    ierr = VecView(_prestress,_viewers["prestress"].first); CHKERRQ(ierr);

    if (_D->_stateLaw == "flashHeating" && _D->_thermalCoupling == "coupled") {
      ierr = VecView(_T,_viewers["T"].first); CHKERRQ(ierr);
      ierr = VecView(_Vw,_viewers["Vw"].first); CHKERRQ(ierr);
    }
  }
  return ierr;
}


// writes out vector fields at each time step (specified by user using stepCount)
PetscErrorCode Fault::writeCheckpoint()
{
  PetscErrorCode ierr = 0;

  ierr = writeVec(_slip, _outputDir + "chkpt_" + "slip"); CHKERRQ(ierr);
  ierr = writeVec(_slipVel, _outputDir + "chkpt_" + "slipVel"); CHKERRQ(ierr);
  ierr = writeVec(_psi, _outputDir + "chkpt_" + "psi"); CHKERRQ(ierr);
  ierr = writeVec(_sNEff, _outputDir + "chkpt_" + "sNEff"); CHKERRQ(ierr);
  ierr = writeVec(_sN, _outputDir + "chkpt_" + "sN"); CHKERRQ(ierr);
  ierr = writeVec(_tauP, _outputDir + "chkpt_" + "tau"); CHKERRQ(ierr);
  ierr = writeVec(_tauQSP, _outputDir + "chkpt_" + "tauQS"); CHKERRQ(ierr);
  ierr = writeVec(_prestress, _outputDir + "chkpt_" + "prestress"); CHKERRQ(ierr);

  return ierr;
}


// destructor, frees memory
Fault::~Fault()
{
  // fields that exist on the fault
  VecDestroy(&_z);
  VecDestroy(&_tauQSP);
  VecDestroy(&_tauP);
  VecDestroy(&_strength);
  VecDestroy(&_prestress);

  VecDestroy(&_psi);
  VecDestroy(&_slip);
  VecDestroy(&_slip0);
  VecDestroy(&_slipVel);
  VecDestroy(&_eta_rad);

  VecDestroy(&_locked);
  VecDestroy(&_Dc);
  VecDestroy(&_a);
  VecDestroy(&_b);
  VecDestroy(&_sNEff);
  VecDestroy(&_sN);
  VecDestroy(&_cohesion);
  VecDestroy(&_mu);
  VecDestroy(&_rho);
  VecDestroy(&_Tw);
  VecDestroy(&_Vw);
  VecDestroy(&_k);
  VecDestroy(&_c);
  VecDestroy(&_T);

  for (map<string,pair<PetscViewer,string>>::iterator it = _viewers.begin(); it != _viewers.end(); it++) {
    PetscViewerDestroy(&_viewers[it->first].first);
  }
}


// estimate steady-state based on velocity vInit
PetscErrorCode Fault::guessSS(const PetscScalar vInit)
{
  PetscErrorCode ierr = 0;

  // set slip velocity
  VecSet(_slipVel,vInit);

  if (_benchmark == "no") {
    // set state variable
    if (_stateVals.size() == 0) {
      computePsiSS(vInit);
    }
    strength_psi_Vec(_tauP, _psi, _slipVel, _a, _sNEff, _v0);
    VecCopy(_tauP,_strength);
  }
  // compute BP1-FD benchmark compatible initial conditions
  else {
    PetscScalar tau0;
    tau0 = _sneffWf*_amax*asinh((0.5*vInit/_v0)*exp((_f0 + _b0*log(_v0/vInit))/_amax));
    VecSet(_tauP,tau0);
    VecSet(_strength,tau0);
    computePsiBM(vInit);
  }
    
  return ierr;
}


// compute the state vector psi for steady state solution
PetscErrorCode Fault::computePsiSS(const PetscScalar vInit)
{
  PetscErrorCode ierr = 0;

  PetscInt           Istart,Iend;
  PetscScalar       *psi;
  const PetscScalar *b;
  VecGetOwnershipRange(_psi,&Istart,&Iend);
  VecGetArray(_psi,&psi);
  VecGetArrayRead(_b,&b);

  PetscInt Jj = 0;
  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    psi[Jj] = _f0 - b[Jj]*log(abs(vInit)/_v0);
    Jj++;
  }
  VecRestoreArray(_psi,&psi);
  VecRestoreArrayRead(_b,&b);

  return ierr;
}


// compute the state vector psi for benchmark steady state solution
PetscErrorCode Fault::computePsiBM(const PetscScalar vInit)
{
  PetscErrorCode ierr = 0;

  PetscInt           Istart,Iend;
  PetscScalar       *psi;
  const PetscScalar *a;
  const PetscScalar *b;
  const PetscScalar *strength;
  const PetscScalar *sneff;
  const PetscScalar *dc;
  VecGetOwnershipRange(_psi,&Istart,&Iend);
  VecGetArray(_psi,&psi);
  VecGetArrayRead(_a,&a);  
  VecGetArrayRead(_b,&b);
  VecGetArrayRead(_strength,&strength);
  VecGetArrayRead(_sNEff,&sneff);
  VecGetArrayRead(_Dc,&dc);
  
  PetscInt Jj = 0;
  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    // psi = f0 + b*log(V0*theta/dc)
    psi[Jj] = a[Jj]*log(2*_v0/vInit*sinh(strength[Jj]/(a[Jj]*sneff[Jj])));
    Jj++;
  }
  VecRestoreArray(_psi,&psi);
  VecRestoreArrayRead(_a,&a);  
  VecRestoreArrayRead(_b,&b);
  VecRestoreArrayRead(_strength,&strength);
  VecRestoreArrayRead(_sNEff,&sneff);
  VecRestoreArrayRead(_Dc,&dc);
  
  return ierr;
}


// add random perturbation to psi to seed instability
PetscErrorCode Fault::perturbPsi()
{
  PetscErrorCode ierr = 0;

  // create random vector (uniform distribution)
  PetscRandom rctx;
  PetscRandomCreate(PETSC_COMM_WORLD, &rctx);
  PetscRandomSetFromOptions(rctx);
  PetscRandomSetType(rctx, PETSCRAND);
  Vec randPsi;
  VecDuplicate(_psi, &randPsi);
  VecSetRandom(randPsi, rctx);
  // scale random vector down and add perturbation to psi
  VecScale(randPsi, 0.01);
  VecAXPY(_psi, 1, randPsi);

  VecDestroy(&randPsi);
  PetscRandomDestroy(&rctx);

  return ierr;
}
  

//================================================================================
//================= Functions assuming only + side exists ========================
//================================================================================

// constructor of derived class Fault_qd, initializes the same object as Fault
Fault_qd::Fault_qd(Domain &D, VecScatter& scatter2fault, const int& faultTypeScale)
  : Fault(D,scatter2fault,faultTypeScale)
{
  if (_D->_ckpt > 0 && _D->_ckptNumber > 0) { // load from previous checkpoint
    loadCheckpoint();
  }
  else { // otherwise set parameters from input file and user-provided Vecs
    loadFieldsFromFiles();
  }
}


// destructor
Fault_qd::~Fault_qd(){}


// set psi and tau from input, and compute slip velocity
// for starting fault from non-steady state
PetscErrorCode Fault_qd::setPsiTauV()
{
  PetscErrorCode ierr = 0;
  setVec(_tauQSP,_z,_tauQSVals,_tauQSDepths);

  if (_benchmark == "no") {
    setVec(_psi,_z,_psiVals,_psiDepths);
    computeVel();
  }
  else {
    VecSet(_slipVel, _D->_vInit);
    VecCopy(_tauQSP, _strength);
    computePsiBM(_D->_vInit);
  }

  // set tauP = tauQS - eta_rad *slipVel
  VecCopy(_slipVel,_tauP); // V -> tau
  VecPointwiseMult(_tauP,_eta_rad,_tauP); // tau = V * eta_rad
  VecAYPX(_tauP,-1.0,_tauQSP); // tauP = tauQS - V*eta_rad

  // compute frictional strength of fault based on slip velocity
  strength_psi_Vec(_strength, _psi, _slipVel, _a, _sNEff, _v0);

  return ierr;
}


// initialize variables to be integrated, put them into varEx
PetscErrorCode Fault_qd::initiateIntegrand(const PetscScalar time, map<string,Vec>& varEx)
{
  PetscErrorCode ierr = 0;

  // put variables to be integrated explicitly into varEx
  if (varEx.find("psi") != varEx.end() ) {
    VecCopy(_psi,varEx["psi"]);
  }
  else {
    Vec varPsi;
    VecDuplicate(_psi,&varPsi);
    VecCopy(_psi,varPsi);
    varEx["psi"] = varPsi;
  }

  // slip is initialized in the strikeSlip class's initiateIntegrand function
  return ierr;
}


// update variables in varEx (i.e. state variable psi and slip), copy them into _psi and _slip
PetscErrorCode Fault_qd::updateFields(const map<string,Vec>& varEx)
{
  PetscErrorCode ierr = 0;

  VecCopy(varEx.find("psi")->second,_psi);
  VecCopy(varEx.find("slip")->second,_slip);

  return ierr;
}


// solve for slip velocity
// assumes right-lateral fault
PetscErrorCode Fault_qd::computeVel()
{
  PetscErrorCode ierr = 0;

  // initialize struct to solve for the slip velocity
  PetscScalar *slipVelA;
  const PetscScalar *etaA, *tauQSA, *sNA, *psiA, *aA,*bA,*lockedA,*Co;
  ierr = VecGetArray(_slipVel,&slipVelA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_eta_rad,&etaA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_tauQSP,&tauQSA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_sNEff,&sNA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_psi,&psiA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_a,&aA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_b,&bA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_locked,&lockedA); CHKERRQ(ierr);
  ierr = VecGetArrayRead(_cohesion,&Co); CHKERRQ(ierr);

  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(_slipVel,&Istart,&Iend);CHKERRQ(ierr);
  PetscInt N = Iend - Istart;

  // create ComputeVel_qd struct
  ComputeVel_qd temp(N,etaA,tauQSA,sNA,psiA,aA,bA,_v0,_D->_vL,lockedA,Co);
  ierr = temp.computeVel(slipVelA, _rootTol, _rootIts, _maxNumIts); CHKERRQ(ierr);

  ierr = VecRestoreArray(_slipVel,&slipVelA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_eta_rad,&etaA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_tauQSP,&tauQSA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_sNEff,&sNA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_psi,&psiA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_a,&aA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_b,&bA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_locked,&lockedA); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(_cohesion,&Co); CHKERRQ(ierr);

  return ierr;
}


// time stepping
PetscErrorCode Fault_qd::d_dt(const PetscScalar time, const map<string,Vec>& varEx, map<string,Vec>& dvarEx)
{
  PetscErrorCode ierr = 0;

  // add pre-stress to quasi-static shear stress
  VecAXPY(_tauQSP,1.0,_prestress);

  // compute slip velocity
  double startTime = MPI_Wtime();
  computeVel();
  VecCopy(_slipVel,dvarEx["slip"]);
  _computeVelTime += MPI_Wtime() - startTime;

  // compute rate of state variable
  Vec dstate = dvarEx.find("psi")->second;

  startTime = MPI_Wtime();
  if (_D->_stateLaw == "agingLaw") {
    ierr = agingLaw_psi_Vec(dstate,_psi,_slipVel,_a,_b,_f0,_v0,_Dc); CHKERRQ(ierr);
  }
  else if (_D->_stateLaw == "slipLaw") {
    ierr =  slipLaw_psi_Vec(dstate,_psi,_slipVel,_a,_b,_f0,_v0,_Dc); CHKERRQ(ierr);
  }
  else if (_D->_stateLaw == "flashHeating") {
    ierr = flashHeating_psi_Vec(_Vw, dstate,_psi,_slipVel,_T,_rho,_c,_k,_D_fh,_Tw,_tau_c,_fw,_Dc,_a,_b,_f0,_v0);
    CHKERRQ(ierr);
  }
  else if (_D->_stateLaw == "constantState") {
    VecSet(dstate,0.);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"_D->_stateLaw not understood!\n");
    assert(0);
  }
  _stateLawTime += MPI_Wtime() - startTime;

  // set tauP = tauQS - eta_rad *slipVel
  VecCopy(_slipVel,_tauP); // V -> tau
  VecPointwiseMult(_tauP,_eta_rad,_tauP); // tau = V * eta_rad
  VecAYPX(_tauP,-1.0,_tauQSP); // tauP = tauQS - V*eta_rad

  // compute frictional strength of fault based on slip velocity
  strength_psi_Vec(_strength, _psi, _slipVel, _a, _sNEff, _v0);

  return ierr;
}


// output vector fields into file, and calls writeContext function in Fault
PetscErrorCode Fault_qd::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;

  Fault::writeContext(outputDir);

  return ierr;
}


//======================================================================
// functions for struct ComputeVel_qd
//======================================================================

// constructor for ComputeVel_qd
ComputeVel_qd::ComputeVel_qd(const PetscInt N, const PetscScalar* eta,const PetscScalar* tauQS,const PetscScalar* sN,const PetscScalar* psi,const PetscScalar* a,const PetscScalar* b,const PetscScalar& v0,const PetscScalar& vL,const PetscScalar* locked,const PetscScalar* Co)
  : _a(a),_b(b),_sN(sN),_tauQS(tauQS),_eta(eta),_psi(psi),_locked(locked),_Co(Co),_N(N),_v0(v0),_vL(vL)
{ }


// compute slip velocity for quasidynamic setting
PetscErrorCode ComputeVel_qd::computeVel(PetscScalar *slipVelA, const PetscScalar rootTol, PetscInt &rootIts, const PetscInt maxNumIts)
{
  PetscErrorCode ierr = 0;

  PetscScalar left, right, out;
  PetscInt Jj;
  for (Jj = 0; Jj < _N; Jj++) {
    // hold slip velocity at 0
    if (_locked[Jj] > 0.5) {
      slipVelA[Jj] = 0.;
    }
    // force fault to creep at loading velocity
    else if (_locked[Jj] < -0.5) {
      slipVelA[Jj] = _vL;
    }
    else {
      left = 0.;
      right = _tauQS[Jj] / _eta[Jj];

      // check bounds
      if (std::isnan(left)) {
	PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: left bound evaluated to NaN.\n");
	PetscPrintf(PETSC_COMM_WORLD,"tauQS = %g, eta = %g, left = %g\n",_tauQS[Jj],_eta[Jj],left);
	assert(0);
      }

      if (std::isnan(right)) {
	PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: right bound evaluated to NaN.\n");
	PetscPrintf(PETSC_COMM_WORLD,"tauQS = %g, eta = %g, right = %g\n",_tauQS[Jj],_eta[Jj],right);
	assert(0);
      }

      out = slipVelA[Jj];

      if (abs(left-right)<1e-14) {
	out = left;
      }
      else {
	//Bisect rootFinder(maxNumIts,rootTol);
        //ierr = rootFinder.setBounds(left,right); CHKERRQ(ierr);
	//ierr = rootFinder.findRoot(this,Jj,&out); assert(ierr == 0); CHKERRQ(ierr);
	//rootIts += rootFinder.getNumIts();
	PetscScalar x0 = slipVelA[Jj];
	BracketedNewton rootFinder(maxNumIts,rootTol);
	ierr = rootFinder.setBounds(left,right);CHKERRQ(ierr);
	ierr = rootFinder.findRoot(this,Jj,x0,&out); CHKERRQ(ierr);
	rootIts += rootFinder.getNumIts();
      }
      slipVelA[Jj] = out;
    }
  }

  return ierr;
}


// Compute residual for equation to find slip velocity.
// This form is for root finding algorithms that don't require a Jacobian such as the bisection method.
PetscErrorCode ComputeVel_qd::getResid(const PetscInt Jj,const PetscScalar vel,PetscScalar* out)
{
  PetscErrorCode ierr = 0;
  // frictional strength
  PetscScalar strength = strength_psi(_sN[Jj], _psi[Jj], vel, _a[Jj], _v0);
  // stress on fault
  PetscScalar stress =_tauQS[Jj] - _eta[Jj]*vel;

  *out = strength - stress;
  assert(!std::isnan(*out));
  assert(!std::isinf(*out));

  return ierr;
}


// compute residual for equation to find slip velocity
// for root finding algorithms that require the Jacobian, e.g. bracketed Newton
PetscErrorCode ComputeVel_qd::getResid(const PetscInt Jj,const PetscScalar vel,PetscScalar *out,PetscScalar *J)
{
  PetscErrorCode ierr = 0;
  // frictional strength
  PetscScalar strength = strength_psi(_sN[Jj], _psi[Jj], vel, _a[Jj], _v0);
  // stress on fault
  PetscScalar stress = _tauQS[Jj] - _eta[Jj]*vel;

  *out = strength - stress;
  PetscScalar A = _a[Jj]*_sN[Jj];
  PetscScalar B = exp(_psi[Jj]/_a[Jj]) / (2.*_v0);

  // derivative with respect to slipVel
  *J = A*vel/sqrt(B*B*vel*vel + 1.) + _eta[Jj];

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));
  assert(!std::isnan(*J));
  assert(!std::isinf(*J));

  return ierr;
}


//========================= Fault_fd class ===============================

// constructor for Fault_fd class
Fault_fd::Fault_fd(Domain &D, VecScatter& scatter2fault, const int& faultTypeScale)
  : Fault(D, scatter2fault,faultTypeScale),
    _Phi(NULL), _an(NULL), _fricPen(NULL),
    _u(NULL), _uPrev(NULL), _d2u(NULL),_alphay(NULL),
    _timeMode("None")
{
  // load settings and fields specific to fully-dynamics case
  loadSettings(_inputFile);
  setFields();
  loadFieldsFromFiles();
  loadVecFromInputFile(_tau0,D._inputDir,"tau0");
}


// destructor, frees memory
Fault_fd::~Fault_fd()
{
  VecDestroy(&_tau0);
  VecDestroy(&_Phi);
  VecDestroy(&_an);
  VecDestroy(&_fricPen);
  VecDestroy(&_u);
  VecDestroy(&_uPrev);
  VecDestroy(&_d2u);
  VecDestroy(&_alphay);
}


// load settings from input file
PetscErrorCode Fault_fd::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;

  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos+_delim.length(),line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after the appearance of a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0,pos);

    // Tau dynamic parameters
    if (var == "tCenterTau") { _tCenterTau = atof( rhs.c_str() ); }
    else if (var == "zCenterTau") { _zCenterTau = atof( rhs.c_str() ); }
    else if (var == "tStdTau") { _tStdTau = atof( rhs.c_str() ); }
    else if (var == "zStdTau") { _zStdTau = atof( rhs.c_str() ); }
    else if (var == "ampTau") { _ampTau = atof( rhs.c_str() ); }
    else if (var == "timeMode") { _timeMode = rhs; }
  }
  return ierr;
}


// allocate memory for fields
PetscErrorCode Fault_fd::setFields()
{
  PetscErrorCode ierr = 0;

  // allocate memory for Vec members
  VecDuplicate(_tauP,&_tau0);
  PetscObjectSetName((PetscObject) _tau0, "tau0");
  VecSet(_tau0, 0.0);

  VecDuplicate(_tauP,&_Phi);
  PetscObjectSetName((PetscObject) _Phi, "Phi");
  VecSet(_Phi, 0.0);

  VecDuplicate(_tauP,&_an);
  PetscObjectSetName((PetscObject) _an, "an");
  VecSet(_an, 0.0);

  VecDuplicate(_tauP,&_fricPen);
  PetscObjectSetName((PetscObject) _fricPen, "constraintsFactor");
  VecSet(_fricPen, 0.0);

  VecDuplicate(_tauP,&_u);
  PetscObjectSetName((PetscObject) _u, "uFault");
  VecSet(_u,0.0);

  VecDuplicate(_tauP,&_uPrev);
  PetscObjectSetName((PetscObject) _uPrev, "uPrevFault");
  VecSet(_uPrev,0.0);

  VecDuplicate(_tauP,&_d2u);
  PetscObjectSetName((PetscObject) _d2u, "uPrevFault");
  VecSet(_d2u,0.0);

  VecDuplicate(_tauP,&_alphay);
  PetscObjectSetName((PetscObject) _alphay, "alphay");
  VecSet(_alphay, 17.0/48.0 / (_N-1));

  return ierr;
}


// set up integration, put variables to be integrated into varEx
PetscErrorCode Fault_fd::initiateIntegrand(const PetscScalar time,map<string,Vec>& varEx)
{
  PetscErrorCode ierr = 0;

  // put variables to be integrated explicitly into varEx
  Vec varPsi;
  VecDuplicate(_psi,&varPsi);
  VecCopy(_psi,varPsi);
  varEx["psi"] = varPsi;

  Vec varSlip;
  VecDuplicate(_slip,&varSlip);
  VecCopy(_slip,varSlip);
  varEx["slip"] = varSlip;

  return ierr;
}


// update variables in varEx (i.e. state variable psi and slip)
PetscErrorCode Fault_fd::updateFields(const PetscScalar deltaT, const map<string,Vec>& varEx)
{
  PetscErrorCode ierr = 0;

  _deltaT = deltaT;
  VecCopy(varEx.find("psi")->second,_psi);
  VecCopy(varEx.find("slip")->second,_slip);

  return ierr;
}


// compute slip velocity
// assumes right-lateral fault
PetscErrorCode Fault_fd::computeVel()
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();

  // initialize struct to solve for the slip velocity
  PetscScalar *Phi, *an, *psi, *fricPen, *a,*sneff, *slipVel, *locked;
  VecGetArray(_Phi,&Phi);
  VecGetArray(_an,&an);
  VecGetArray(_psi,&psi);
  VecGetArray(_fricPen,&fricPen);
  VecGetArray(_a,&a);
  VecGetArray(_sNEff,&sneff);
  VecGetArray(_slipVel,&slipVel);
  VecGetArray(_locked, &locked);

  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(_slipVel,&Istart,&Iend);CHKERRQ(ierr);
  PetscInt N = Iend - Istart;

  ComputeVel_fd temp(locked, N,Phi,an,psi,fricPen,a,sneff, _v0, _D->_vL);
  ierr = temp.computeVel(slipVel, _rootTol, _rootIts, _maxNumIts); CHKERRQ(ierr);

  VecRestoreArray(_Phi,&Phi);
  VecRestoreArray(_an,&an);
  VecRestoreArray(_psi,&psi);
  VecRestoreArray(_fricPen,&fricPen);
  VecRestoreArray(_a,&a);
  VecRestoreArray(_sNEff,&sneff);
  VecRestoreArray(_slipVel,&slipVel);
  VecRestoreArray(_locked, &locked);

  _computeVelTime = MPI_Wtime() - startTime;

  return ierr;
}


// compute evolution of state variable psi
PetscErrorCode Fault_fd::computeStateEvolution(Vec& psiNext, const Vec& psi, const Vec& psiPrev)
{
  PetscErrorCode ierr = 0;

  // initialize struct to solve for the slip velocity
  PetscScalar *Dc, *b, *psiNextA, *psiA, *psiPrevA, *slipVel;
  VecGetArray(_Dc,&Dc);
  VecGetArray(_b,&b);
  VecGetArray(psiNext,&psiNextA);
  VecGetArray(psi,&psiA);
  VecGetArray(psiPrev,&psiPrevA);
  VecGetArray(_slipVel,&slipVel);

  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(_slipVel,&Istart,&Iend);CHKERRQ(ierr);
  PetscInt N = Iend - Istart;

  // compute state evolution for the specified aging law
  if (_D->_stateLaw == "agingLaw"){
    ComputeAging_fd temp(N,Dc,b,psiNextA,psiA,psiPrevA, slipVel, _v0, _deltaT, _f0);
    ierr = temp.computeLaw(_rootTol, _rootIts, _maxNumIts); CHKERRQ(ierr);
  }

  else if (_D->_stateLaw == "slipLaw"){
    PetscScalar *a;
    VecGetArray(_a, &a);
    ComputeSlipLaw_fd temp(N,Dc,a, b,psiNextA,psiA,psiPrevA, slipVel, _v0, _deltaT, _f0);
    ierr = temp.computeLaw(_rootTol, _rootIts, _maxNumIts); CHKERRQ(ierr);
    VecRestoreArray(_a, &a);
  }

  else if (_D->_stateLaw == "flashHeating"){
    PetscScalar *a, * Vw;
    VecGetArray(_a, &a);
    VecGetArray(_Vw, &Vw);
    ComputeFlashHeating_fd temp(N,Dc,a,b,psiNextA,psiA,psiPrevA, slipVel, Vw, _v0, _deltaT, _f0, _fw);
    ierr = temp.computeLaw(_rootTol, _rootIts, _maxNumIts); CHKERRQ(ierr);
    VecRestoreArray(_a, &a);
    VecRestoreArray(_Vw, &Vw);
  }

  else{
    assert(0);
  }

  VecRestoreArray(_Dc,&Dc);
  VecRestoreArray(_b,&b);
  VecRestoreArray(psiNext,&psiNextA);
  VecRestoreArray(psi,&psiA);
  VecRestoreArray(psiPrev,&psiPrevA);
  VecRestoreArray(_slipVel,&slipVel);

  return ierr;
}


// update prestress vector tau0 based on time offset
PetscErrorCode Fault_fd::updatePrestress(const PetscScalar currT)
{
  PetscErrorCode ierr = 0;

  PetscScalar *zz, *tau0;
  PetscInt Ii, IBegin, IEnd;
  PetscInt Jj = 0;
  VecGetArray(_z, &zz);
  VecGetArray(_prestress, &tau0);
  VecGetOwnershipRange(_z, &IBegin, &IEnd);

  PetscScalar timeOffset = 1.0;
  PetscScalar exists = 1.0;

  // set timeOffset based on specified time mode
  if (_timeMode == "Gaussian") {
    timeOffset = exp(-pow((currT - _tCenterTau), 2) / pow(_tStdTau, 2));
  }
  else if (_timeMode == "Dirac" && currT > 0){
    timeOffset = 0.0;
  }
  else if (_timeMode == "Heaviside"){
    timeOffset = 1.0;
  }
  else if (_timeMode == "None"){
    exists = 0.0;
  }

  // update tau0 prestress vector
  for (Ii = IBegin; Ii < IEnd; Ii++) {
    tau0[Jj] = exists * (30.0 + _ampTau * exp(-pow((zz[Jj] - _zCenterTau), 2) / (2.0 * pow(_zStdTau, 2))) * timeOffset);
    Jj++;
  }

  VecRestoreArray(_z, &zz);
  VecRestoreArray(_prestress, &tau0);

  return ierr;
}


// calculate slip velocity by calling computeVel(), and performs explicit time stepping, updating fields with the new time step
PetscErrorCode Fault_fd::d_dt(const PetscScalar time,const PetscScalar deltaT, map<string,Vec>& varNext,const map<string,Vec>& var,const map<string,Vec>& varPrev)
{
  PetscErrorCode ierr = 0;

  // update fields with new time step
  _deltaT = deltaT;

  // uPrev = (slip - slip0)/faultTypeScale
  VecWAXPY(_uPrev,-1.0,_slip0,varPrev.find("slip")->second);
  VecScale(_uPrev,1.0/_faultTypeScale);

  // u = (slip - slip0)/2 (this is really du)
  VecWAXPY(_u,-1.0,_slip0,var.find("slip")->second);
  VecScale(_u,1.0/_faultTypeScale);

  // compute slip velocity
  ierr = setPhi(deltaT);

  // computes abs(slipVel)
  ierr = computeVel(); CHKERRQ(ierr);

  PetscInt       Ii,Istart,Iend;
  PetscScalar   *u, *uPrev, *slip, *slipVel; // changed in this loop
  const PetscScalar  *rho, *sNEff, *a, *an, *Phi, *psi, *alphay; // constant in this loop
  ierr = VecGetOwnershipRange(_u,&Istart,&Iend); CHKERRQ(ierr);
  ierr = VecGetArray(_u, &u);
  ierr = VecGetArray(_uPrev, &uPrev);
  ierr = VecGetArray(_slip, &slip);
  ierr = VecGetArray(_slipVel, &slipVel);
  ierr = VecGetArrayRead(_an, &an);
  ierr = VecGetArrayRead(_rho, &rho);
  ierr = VecGetArrayRead(_psi, &psi);
  ierr = VecGetArrayRead(_sNEff, &sNEff);
  ierr = VecGetArrayRead(_a, &a);
  ierr = VecGetArrayRead(_Phi, &Phi);
  ierr = VecGetArrayRead(_alphay, &alphay);

  PetscInt Jj = 0;
  for (Ii = Istart; Ii < Iend; Ii++) {
    if (slipVel[Jj] < 1e-14) {
      // slipVel[Jj] = 0;
    }
    else {
      PetscScalar fric = strength_psi(sNEff[Jj], psi[Jj], slipVel[Jj], a[Jj], _v0);
      PetscScalar alpha = 1.0 / (rho[Jj] * alphay[Jj]) * fric / slipVel[Jj];
      PetscScalar A = 1.0 + alpha * deltaT;
      slipVel[Jj] = Phi[Jj] / (1. + _deltaT * alpha);
      u[Jj] = (2.*u[Jj]  +  (an[Jj] * deltaT*deltaT / rho[Jj])  +  (_deltaT*alpha-1.)*uPrev[Jj]) / A;
    }
    Jj++;
  }

  ierr = VecRestoreArray(_u, &u);
  ierr = VecRestoreArray(_uPrev, &uPrev);
  ierr = VecRestoreArray(_slip, &slip);
  ierr = VecRestoreArray(_slipVel, &slipVel);
  ierr = VecRestoreArrayRead(_an, &an);
  ierr = VecRestoreArrayRead(_rho, &rho);
  ierr = VecRestoreArrayRead(_psi, &psi);
  ierr = VecRestoreArrayRead(_sNEff, &sNEff);
  ierr = VecRestoreArrayRead(_a, &a);
  ierr = VecRestoreArrayRead(_Phi, &Phi);
  ierr = VecRestoreArrayRead(_alphay, &alphay);

  // update state variable
  computeStateEvolution(varNext["psi"], var.find("psi")->second, varPrev.find("psi")->second);
  VecCopy(varNext["psi"],_psi);

  // assemble slip from u
  VecWAXPY(_slip,_faultTypeScale,_u,_slip0); // slip = 2*u + slip0
  VecCopy(_slip,varNext["slip"]);

  // compute frictional strength of fault
  strength_psi_Vec(_strength, _psi, _slipVel, _a, _sNEff, _v0);

  return ierr;
}


// sets value for array Phi
PetscErrorCode Fault_fd::setPhi(const PetscScalar deltaT)
{
  PetscErrorCode ierr = 0;

  PetscInt       Ii,Istart, Iend;
  ierr = VecGetOwnershipRange(_d2u,&Istart,&Iend);CHKERRQ(ierr);

  PetscScalar  *an, *Phi, *fricPen;
  const PetscScalar *u, *uPrev, *d2u, *rho, *tau0, *alphay;

  ierr = VecGetArray(_an, &an);
  ierr = VecGetArray(_Phi, &Phi);
  ierr = VecGetArray(_fricPen, &fricPen);
  ierr = VecGetArrayRead(_u, &u);
  ierr = VecGetArrayRead(_uPrev, &uPrev);
  ierr = VecGetArrayRead(_d2u, &d2u);
  ierr = VecGetArrayRead(_rho, &rho);
  ierr = VecGetArrayRead(_tau0, &tau0);
  ierr = VecGetArrayRead(_alphay, &alphay);

  PetscInt Jj = 0;
  for (Ii = Istart; Ii < Iend; Ii++){
    an[Jj] = d2u[Jj] + tau0[Jj] / alphay[Jj];
    Phi[Jj] = 2.0 / deltaT * (u[Jj] - uPrev[Jj]) + deltaT * an[Jj] / rho[Jj];
    fricPen[Jj] = deltaT / alphay[Jj] / rho[Jj];
    Jj++;
  }

  ierr = VecRestoreArray(_an, &an);
  ierr = VecRestoreArray(_Phi, &Phi);
  ierr = VecRestoreArray(_fricPen, &fricPen);
  ierr = VecGetArrayRead(_u, &u);
  ierr = VecGetArrayRead(_uPrev, &uPrev);
  ierr = VecGetArrayRead(_d2u, &d2u);
  ierr = VecGetArrayRead(_rho, &rho);
  ierr = VecGetArrayRead(_tau0, &tau0);
  ierr = VecGetArrayRead(_alphay, &alphay);

  return ierr;
}


// =================== functions for struct ComputeVel_fd ====================
// to handle the computation of the implicit solvers
// ===========================================================================

// constructor
ComputeVel_fd::ComputeVel_fd(const PetscScalar* locked, const PetscInt N,const PetscScalar* Phi, const PetscScalar* an, const PetscScalar* psi, const PetscScalar* fricPen,const PetscScalar* a,const PetscScalar* sneff, const PetscScalar v0, const PetscScalar vL)
  : _locked(locked), _Phi(Phi),_an(an),_psi(psi),_fricPen(fricPen),_a(a),_sNEff(sneff),_N(N), _v0(v0), _vL(vL)
{ }


// compute absolute value of slip velocity for fully dynamic case
PetscErrorCode ComputeVel_fd::computeVel(PetscScalar* slipVelA, const PetscScalar rootTol, PetscInt& rootIts, const PetscInt maxNumIts)
{
  PetscErrorCode ierr = 0;

  BracketedNewton rootFinder(maxNumIts,rootTol);
  //~ Bisect rootFinder(maxNumIts,rootTol);
  PetscScalar left, right, out, temp;

  for (PetscInt Jj = 0; Jj<_N; Jj++) {
    if (_locked[Jj] > 0.5) { // if fault is locked, hold slip velocity at 0
      slipVelA[Jj] = 0.;
    }
    else if (_locked[Jj] < -0.5) {
      slipVelA[Jj] = _vL;
    }
    else {
      left = 0.;
      right = abs(_Phi[Jj]);
      // check bounds
      if (std::isnan(left)) {
        PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_fd::computeVel: left bound evaluated to NaN.\n");
        assert(0);
      }
      if (std::isnan(right)) {
        PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_fd::computeVel: right bound evaluated to NaN.\n");
        assert(0);
      }
      // correct for left-lateral fault motion
      if (left > right) {
        temp = right;
        right = left;
        left = temp;
      }

      if (abs(left-right)<1e-14) { out = left; }
      else {
        ierr = rootFinder.setBounds(left,right);CHKERRQ(ierr);
        //~ ierr = rootFinder.findRoot(this,Jj,&out);CHKERRQ(ierr);
        ierr = rootFinder.findRoot(this,Jj,abs(slipVelA[Jj]), &out);CHKERRQ(ierr);
        rootIts += rootFinder.getNumIts();
      }
      slipVelA[Jj] = out;
    }
  }
  return ierr;
}


// Compute residual for equation to find slip velocity.
// This form is for root finding algorithms that don't require a Jacobian such as the bisection method.
PetscErrorCode ComputeVel_fd::getResid(const PetscInt Jj,const PetscScalar vel,PetscScalar* out)
{
  PetscErrorCode ierr = 0;
  PetscScalar strength = strength_psi(_sNEff[Jj], _psi[Jj], vel, _a[Jj] , _v0);
  PetscScalar stress = abs(_Phi[Jj]) - vel; // stress on fault

  *out = _fricPen[Jj] * strength - stress;
  assert(!std::isnan(*out));
  assert(!std::isinf(*out));
  return ierr;
}


// compute residual for equation to find slip velocity
// for methods that require a Jacobian, such as bracketed Newton
PetscErrorCode ComputeVel_fd::getResid(const PetscInt Jj,const PetscScalar vel,PetscScalar* out, PetscScalar *J)
{
  PetscErrorCode ierr = 0;
  PetscScalar constraints = strength_psi(_sNEff[Jj], _psi[Jj], vel, _a[Jj] , _v0);

  constraints = _fricPen[Jj] * constraints;
  PetscScalar Phi_temp = _Phi[Jj];
  if (Phi_temp < 0){
    Phi_temp = -Phi_temp;
  }

  PetscScalar stress = Phi_temp - vel; // stress on fault

  *out = constraints - stress;
  PetscScalar A = _a[Jj] * _sNEff[Jj];
  PetscScalar B = exp(_psi[Jj] / _a[Jj]) / (2. * _v0);

  *J = 1 + _fricPen[Jj] * A * B / sqrt(1. + B * B * vel * vel);

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));
  return ierr;
}

// ================================================

// struct for aging law for fully dynamic case
ComputeAging_fd::ComputeAging_fd(const PetscInt N,const PetscScalar* Dc, const PetscScalar* b, PetscScalar* psiNext, const PetscScalar* psi, const PetscScalar* psiPrev, const PetscScalar* slipVel, const PetscScalar v0, const PetscScalar deltaT, const PetscScalar f0)
  : _Dc(Dc),_b(b),_slipVel(slipVel),_psi(psi),_psiPrev(psiPrev),_psiNext(psiNext),
    _N(N), _v0(v0), _deltaT(deltaT), _f0(f0)
{ }


// perform root finding once contextal variables have been set
PetscErrorCode ComputeAging_fd::computeLaw(const PetscScalar rootTol, PetscInt& rootIts, const PetscInt maxNumIts)
{
  PetscErrorCode ierr = 0;

  // RegulaFalsi rootFinder(maxNumIts,rootTol);
  BracketedNewton rootFinder(maxNumIts,rootTol);
  //~ Bisect rootFinder(maxNumIts,rootTol);

  PetscScalar left, right, temp;
  for (PetscInt Jj = 0; Jj<_N; Jj++) {
    left = -2.;
    right = 2.;
    //~ left = 0.;
    //~ right = 2*_psi[Jj];

    // check bounds
    if (std::isnan(left)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeAging_fd::computeLaw: left bound evaluated to NaN.\n");
      assert(0);
    }
    if (std::isnan(right)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeAging_fd::computeLaw: right bound evaluated to NaN.\n");
      assert(0);
    }
    // correct for left-lateral fault motion
    if (left > right) {
      temp = right;
      right = left;
      left = temp;
    }

    if (abs(left-right)<1e-14) {
      _psiNext[Jj] = left;
    }
    else {
      ierr = rootFinder.setBounds(left,right);CHKERRQ(ierr);
      ierr = rootFinder.findRoot(this,Jj,_psi[Jj],&_psiNext[Jj]);CHKERRQ(ierr);
      //~ ierr = rootFinder.findRoot(this,Jj,&_psiNext[Jj]);CHKERRQ(ierr);
      rootIts += rootFinder.getNumIts();
    }
  }
  return ierr;
}


// Compute residual for equation to find slip velocity.
// This form is for root finding algorithms that don't require a Jacobian such as the bisection method.
PetscErrorCode ComputeAging_fd::getResid(const PetscInt Jj,const PetscScalar state,PetscScalar* out)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = agingLaw_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _b[Jj], _f0, _v0, _Dc[Jj]);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;
  assert(!std::isnan(*out));
  assert(!std::isinf(*out));

  return ierr;
}


// this form if for algorithms that require a Jacobian, such as bracketed Newton
PetscErrorCode ComputeAging_fd::getResid(const PetscInt Jj,const PetscScalar state,PetscScalar* out, PetscScalar *J)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = agingLaw_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _b[Jj], _f0, _v0, _Dc[Jj]);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;

  *J = 1 + _deltaT * _v0 / _Dc[Jj] * exp((_f0 - (_psiPrev[Jj] + state)/2.)/_b[Jj]);

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));

  return ierr;
}

// ================================================
// computes slip law for fully dynamic problem

// constructor
ComputeSlipLaw_fd::ComputeSlipLaw_fd(const PetscInt N,const PetscScalar* Dc, const PetscScalar* a,const PetscScalar* b, PetscScalar* psiNext, const PetscScalar* psi, const PetscScalar* psiPrev,const PetscScalar* slipVel, const PetscScalar v0, const PetscScalar deltaT, const PetscScalar f0)
  : _Dc(Dc),_a(a),_b(b),_slipVel(slipVel),_psi(psi),_psiPrev(psiPrev),_psiNext(psiNext), _N(N), _v0(v0), _deltaT(deltaT), _f0(f0)
{}


// perform root-finding
PetscErrorCode ComputeSlipLaw_fd::computeLaw(const PetscScalar rootTol, PetscInt& rootIts, const PetscInt maxNumIts)
{
  PetscErrorCode ierr = 0;

  // RegulaFalsi rootFinder(maxNumIts,rootTol);
  BracketedNewton rootFinder(maxNumIts,rootTol);
  // Bisect rootFinder(maxNumIts,rootTol);
  PetscScalar left, right, temp;
  for (PetscInt Jj = 0; Jj<_N; Jj++) {
    left = -2.;
    right = 2.;
    //~ left = 0.;
    //~ right = 2*_psi[Jj];

    // check bounds
    if (std::isnan(left)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: left bound evaluated to NaN.\n");
      assert(0);
    }
    if (std::isnan(right)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: right bound evaluated to NaN.\n");
      assert(0);
    }
    // correct for left-lateral fault motion
    if (left > right) {
      temp = right;
      right = left;
      left = temp;
    }

    if (abs(left-right)<1e-14) {
      _psiNext[Jj] = left;
    }
    else {
      ierr = rootFinder.setBounds(left,right);CHKERRQ(ierr);
      ierr = rootFinder.findRoot(this,Jj,_psi[Jj],&_psiNext[Jj]);CHKERRQ(ierr);
      // ierr = rootFinder.findRoot(this,Jj,&out);CHKERRQ(ierr);
      rootIts += rootFinder.getNumIts();
    }
  }
  return ierr;
}


// Compute residual for equation to find slip velocity.
// This form is for root finding algorithms that don't require a Jacobian such as the bisection method.
PetscErrorCode ComputeSlipLaw_fd::getResid(const PetscInt Jj,const PetscScalar state,PetscScalar* out)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = slipLaw_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _a[Jj], _b[Jj], _f0, _v0, _Dc[Jj]);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));
  return ierr;
}


// this is for algorithms that require a Jacobian, such as bracketed Newton
PetscErrorCode ComputeSlipLaw_fd::getResid(const PetscInt Jj,const PetscScalar state,PetscScalar* out, PetscScalar *J)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = slipLaw_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _a[Jj], _b[Jj], _f0, _v0, _Dc[Jj]);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;

  PetscScalar A = abs(_slipVel[Jj]) / 2. / _v0 * exp((_psiPrev[Jj] + state) / 2.0 / _a[Jj]);
  *J = 1 + _deltaT * abs(_slipVel[Jj]) / _Dc[Jj] * A / sqrt(1 + A * A);

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));

  return ierr;
}

// ================================================
// struct to compute flash heating

// constructor
ComputeFlashHeating_fd::ComputeFlashHeating_fd(const PetscInt N,const PetscScalar* Dc, const PetscScalar* a, const PetscScalar* b, PetscScalar* psiNext, const PetscScalar* psi, const PetscScalar* psiPrev, const PetscScalar* slipVel, const PetscScalar* Vw,const PetscScalar v0, const PetscScalar deltaT,const PetscScalar f0, const PetscScalar fw)
  : _Dc(Dc),_a(a),_b(b),_slipVel(slipVel),
    _Vw(Vw),_psi(psi),_psiPrev(psiPrev),_psiNext(psiNext),
    _N(N), _v0(v0), _deltaT(deltaT), _f0(f0), _fw(fw)
{ }


// find roots
PetscErrorCode ComputeFlashHeating_fd::computeLaw(const PetscScalar rootTol, PetscInt& rootIts, const PetscInt maxNumIts)
{
  PetscErrorCode ierr = 0;

  // RegulaFalsi rootFinder(maxNumIts,rootTol);
  BracketedNewton rootFinder(maxNumIts,rootTol);
  // Bisect rootFinder(maxNumIts,rootTol);
  PetscScalar left, right, temp;
  for (PetscInt Jj = 0; Jj<_N; Jj++) {
    left = -2.;
    right = 2.;
    //~ left = 0.;
    //~ right = 2*_psi[Jj];

    // check bounds
    if (std::isnan(left)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: left bound evaluated to NaN.\n");
      assert(0);
    }
    if (std::isnan(right)) {
      PetscPrintf(PETSC_COMM_WORLD,"\n\nError in ComputeVel_qd::computeVel: right bound evaluated to NaN.\n");
      assert(0);
    }
    // correct for left-lateral fault motion
    if (left > right) {
      temp = right;
      right = left;
      left = temp;
    }

    if (abs(left-right)<1e-14) {
      _psiNext[Jj] = left;
    }
    else {
      ierr = rootFinder.setBounds(left,right);CHKERRQ(ierr);
      ierr = rootFinder.findRoot(this,Jj,_psi[Jj],&_psiNext[Jj]);CHKERRQ(ierr);
      // ierr = rootFinder.findRoot(this,Jj,&out);CHKERRQ(ierr);
      rootIts += rootFinder.getNumIts();
    }
  }
  return ierr;
}


// Compute residual for equation to find slip velocity.
// for root finding algorithms that don't require a Jacobian, e.g. bisection method
PetscErrorCode ComputeFlashHeating_fd::getResid(const PetscInt Jj, const PetscScalar state, PetscScalar* out)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = flashHeating_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _Vw[Jj], _fw, _Dc[Jj], _a[Jj], _b[Jj], _f0, _v0);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));
  return ierr;
}


// for methods that require a Jacobian, e.g. bracketed Newton.
PetscErrorCode ComputeFlashHeating_fd::getResid(const PetscInt Jj,const PetscScalar state,PetscScalar* out, PetscScalar *J)
{
  PetscErrorCode ierr = 0;

  PetscScalar G = flashHeating_psi((_psiPrev[Jj] + state)/2.0, _slipVel[Jj], _Vw[Jj], _fw, _Dc[Jj], _a[Jj], _b[Jj], _f0, _v0);
  PetscScalar temp = state - _psiPrev[Jj];
  *out = -2 * _deltaT * G + temp;

  PetscScalar A = abs(_slipVel[Jj]) / 2. / _v0 * exp((_psiPrev[Jj] + state) / 2.0 / _a[Jj]);
  *J = 1 + _deltaT * abs(_slipVel[Jj]) / _Dc[Jj] * A / sqrt(1 + A * A);

  assert(!std::isnan(*out));
  assert(!std::isinf(*out));

  return ierr;
}


//===========================================================================
// common rate-and-state functions
//===========================================================================

// state evolution law: aging law, state variable: psi
PetscScalar agingLaw_psi(const PetscScalar& psi, const PetscScalar& slipVel, const PetscScalar& b, const PetscScalar& f0, const PetscScalar& v0, const PetscScalar& Dc)
{
  PetscScalar A = exp( (double) (f0-psi)/b );
  PetscScalar dstate = 0.;
  if ( !std::isinf(A) && b>1e-3 ) {
    dstate = (PetscScalar) (b*v0/Dc)*( A - slipVel/v0 );
  }
  assert(!std::isnan(dstate));
  assert(!std::isinf(dstate));
  return dstate;
}


// applies the aging law to a Vec
PetscErrorCode agingLaw_psi_Vec(Vec& dstate, const Vec& psi, const Vec& slipVel, const Vec& a, const Vec& b, const PetscScalar& f0, const PetscScalar& v0, const Vec& Dc)
{
  PetscErrorCode ierr = 0;

  PetscScalar *dstateA;
  const PetscScalar *psiA,*slipVelA,*aA,*bA,*DcA;
  VecGetArray(dstate,&dstateA);
  VecGetArrayRead(psi,&psiA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(a,&aA);
  VecGetArrayRead(b,&bA);
  VecGetArrayRead(Dc,&DcA);
  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(psi,&Istart,&Iend); // local portion of global Vec index
  for (PetscInt Ii=Istart;Ii<Iend;Ii++) {
    dstateA[Jj] = agingLaw_psi(psiA[Jj], slipVelA[Jj], bA[Jj], f0, v0, DcA[Jj]);
    if ( std::isnan(dstateA[Jj]) || std::isinf(dstateA[Jj]) ) {
      PetscPrintf(PETSC_COMM_WORLD,"[%i]: dpsi = %g, psi = %g, slipVel = %g, a = %g, b = %g, f0 = %g, v0 = %g, Dc = %g\n",
		  Jj,dstateA[Jj], psiA[Jj], slipVelA[Jj], aA[Jj], bA[Jj], f0, v0, DcA[Jj]);
      assert(!std::isnan(dstateA[Jj]));
      assert(!std::isinf(dstateA[Jj]));
    }
    Jj++;
  }
  VecRestoreArray(dstate,&dstateA);
  VecRestoreArrayRead(psi,&psiA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(a,&aA);
  VecRestoreArrayRead(b,&bA);
  VecRestoreArrayRead(Dc,&DcA);

  return ierr;
}


// state evolution law: aging law, state variable: theta
PetscScalar agingLaw_theta(const PetscScalar& theta, const PetscScalar& slipVel, const PetscScalar& Dc)
{
  PetscScalar dstate = 1. - theta*abs(slipVel)/Dc;

  assert(!std::isnan(dstate));
  assert(!std::isinf(dstate));
  return dstate;
}


// applies the aging law to a Vec
PetscErrorCode agingLaw_theta_Vec(Vec& dstate, const Vec& theta, const Vec& slipVel, const Vec& Dc)
{
  PetscErrorCode ierr = 0;

  PetscScalar *dstateA;
  const PetscScalar *thetaA,*slipVelA,*DcA;
  VecGetArray(dstate,&dstateA);
  VecGetArrayRead(theta,&thetaA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(Dc,&DcA);
  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(theta,&Istart,&Iend); // local portion of global Vec index
  for (PetscInt Ii=Istart;Ii<Iend;Ii++) {
    dstateA[Jj] = agingLaw_theta(thetaA[Jj], slipVelA[Jj], DcA[Jj]);
    Jj++;
  }
  VecRestoreArray(dstate,&dstateA);
  VecRestoreArrayRead(theta,&thetaA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(Dc,&DcA);

  return ierr;
}

// state evolution law: slip law, state variable: psi
PetscScalar slipLaw_psi(const PetscScalar& psi, const PetscScalar& slipVel, const PetscScalar& a, const PetscScalar& b, const PetscScalar& f0, const PetscScalar& v0, const PetscScalar& Dc)
{
  if (slipVel == 0) { return 0.0; }

  PetscScalar absV = abs(slipVel);
  PetscScalar fss = f0 + (a-b)*log(absV/v0);
  // this regularized form only works for velocity strengthening
  //PetscScalar fss = (a-b)*asinh((double)absV/(2*v0)*exp(f0/(a-b)));
  
  PetscScalar f = a*asinh( (double) (absV/2./v0)*exp(psi/a) ); // regularized
  PetscScalar dstate = -absV/Dc * (f - fss);
  assert(!std::isnan(dstate));
  assert(!std::isinf(dstate));

  return dstate;
}


// applies the state law to a Vec
PetscErrorCode slipLaw_psi_Vec(Vec& dstate, const Vec& psi, const Vec& slipVel,const Vec& a, const Vec& b, const PetscScalar& f0, const PetscScalar& v0, const Vec& Dc)
{
  PetscErrorCode ierr = 0;

  PetscScalar *dstateA;
  const PetscScalar *psiA,*slipVelA,*aA,*bA,*DcA;
  VecGetArray(dstate,&dstateA);
  VecGetArrayRead(psi,&psiA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(a,&aA);
  VecGetArrayRead(b,&bA);
  VecGetArrayRead(Dc,&DcA);
  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  VecGetOwnershipRange(psi,&Istart,&Iend);  // local portion of vector
  for (PetscInt Ii=Istart;Ii<Iend;Ii++) {
    dstateA[Jj] = slipLaw_psi(psiA[Jj], slipVelA[Jj], aA[Jj], bA[Jj], f0, v0, DcA[Jj]);
    Jj++;
  }
  VecRestoreArray(dstate,&dstateA);
  VecRestoreArrayRead(psi,&psiA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(a,&aA);
  VecRestoreArrayRead(b,&bA);
  VecRestoreArrayRead(Dc,&DcA);

  return ierr;
}


// state evolution law: slip law, state variable: theta
PetscScalar slipLaw_theta(const PetscScalar& state, const PetscScalar& slipVel, const PetscScalar& Dc)
{
  PetscScalar A = state*slipVel/Dc;
  PetscScalar dstate = 0.;
  if (A != 0.) {
    dstate = -A*log(A);
  }
  assert(!std::isnan(dstate));
  assert(!std::isinf(dstate));

  return dstate;
}


// applies the slip law to a Vec
PetscErrorCode slipLaw_theta_Vec(Vec& dstate, const Vec& theta, const Vec& slipVel, const Vec& Dc)
{
  PetscErrorCode ierr = 0;

  PetscScalar *dstateA;
  const PetscScalar *thetaA,*slipVelA,*DcA;
  VecGetArray(dstate,&dstateA);
  VecGetArrayRead(theta,&thetaA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(Dc,&DcA);
  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(theta,&Istart,&Iend); // local portion of global Vec index
  for (PetscInt Ii=Istart;Ii<Iend;Ii++) {
    dstateA[Jj] = slipLaw_theta(thetaA[Jj], slipVelA[Jj], DcA[Jj]);
    Jj++;
  }
  VecRestoreArray(dstate,&dstateA);
  VecRestoreArrayRead(theta,&thetaA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(Dc,&DcA);

  return ierr;
}


// flash heating: compute Vw
PetscScalar flashHeating_Vw(const PetscScalar& T, const PetscScalar& rho, const PetscScalar& c, const PetscScalar& k, const PetscScalar& D, const PetscScalar& Tw, const PetscScalar& tau_c)
{
  PetscScalar rc = rho * c;
  PetscScalar ath = k/rc;
  PetscScalar Vw = (M_PI*ath/D) * pow(rc*(Tw-T)/tau_c,2.);
  return Vw;
}


// flash heating state evolution law
PetscScalar flashHeating_psi(const PetscScalar& psi, const PetscScalar& slipVel, const PetscScalar& Vw, const PetscScalar& fw, const PetscScalar& Dc,const PetscScalar& a,const PetscScalar& b, const PetscScalar& f0, const PetscScalar& v0)
{
  PetscScalar absV = abs(slipVel);

  // compute steady state friction
  PetscScalar fLV = f0 + (a-b)*log(absV/v0);
  // this regularized form only works for velocity strengthening
  //PetscScalar fLV = (a-b)*asinh((double)absV/(2*v0)*exp(f0/(a-b)));

  PetscScalar fss = fLV;
  if (absV > Vw) {
    fss = fw + (fLV - fw)*(Vw/absV);
  }

  // use regularized f; strength is also calculated based on this
  PetscScalar f = a*asinh( (double) (absV/2/v0)*exp(psi/a) );
  PetscScalar dpsi = -absV/Dc *(f - fss);
  assert(!std::isnan(dpsi));
  assert(!std::isinf(dpsi));
  
  return dpsi;
}


// applies the flash heating state law to a Vec
PetscErrorCode flashHeating_psi_Vec(Vec &Vw, Vec &dpsi,const Vec& psi, const Vec& slipVel, const Vec& T, const Vec& rho, const Vec& c, const Vec& k, const PetscScalar& D, const Vec& Tw, const PetscScalar& tau_c, const PetscScalar& fw, const Vec& Dc,const Vec& a,const Vec& b, const PetscScalar& f0, const PetscScalar& v0)
{
  PetscErrorCode ierr = 0;
  // commented out lines are for flash heating calculating Vw at every time step
  // PetscScalar *dpsiA,*VwA;
  // const PetscScalar *psiA,*slipVelA,*DcA,*TA,*TwA,*rhoA,*cA,*kA,*aA,*bA;
  PetscScalar *dpsiA;
  const PetscScalar *VwA,*psiA,*slipVelA,*DcA,*aA,*bA;

  VecGetArray(dpsi,&dpsiA);
  //VecGetArray(Vw,&VwA);  // if not constant
  VecGetArrayRead(Vw, &VwA);
  VecGetArrayRead(psi,&psiA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(Dc,&DcA);
  VecGetArrayRead(a,&aA);
  VecGetArrayRead(b,&bA);
  //VecGetArrayRead(T,&TA);
  //VecGetArrayRead(rho,&rhoA);
  //VecGetArrayRead(c,&cA);
  //VecGetArrayRead(k,&kA);
  //VecGetArrayRead(Tw,&TwA);

  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(psi,&Istart,&Iend); // local portion of global Vec

  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    // compute new Vw based on energy equation
    //VwA[Jj] = flashHeating_Vw(TA[Jj], rhoA[Jj],cA[Jj],kA[Jj],D,TwA[Jj],tau_c);
    dpsiA[Jj] = flashHeating_psi(psiA[Jj],slipVelA[Jj],VwA[Jj],fw,DcA[Jj],aA[Jj],bA[Jj],f0,v0);
    Jj++;
  }

  VecRestoreArray(dpsi,&dpsiA);
  ///VecRestoreArray(Vw,&VwA);
  VecRestoreArrayRead(Vw,&VwA);
  VecRestoreArrayRead(psi,&psiA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(Dc,&DcA);
  VecRestoreArrayRead(a,&aA);
  VecRestoreArrayRead(b,&bA);
  //VecRestoreArrayRead(T,&TA);
  //VecRestoreArrayRead(rho,&rhoA);
  //VecRestoreArrayRead(c,&cA);
  //VecRestoreArrayRead(k,&kA);
  //VecRestoreArrayRead(Tw,&TwA);

  return ierr;
}


// frictional strength, regularized form, for state variable psi
PetscScalar strength_psi(const PetscScalar& sN, const PetscScalar& psi, const PetscScalar& slipVel, const PetscScalar& a, const PetscScalar& v0)
{
  PetscScalar strength = (PetscScalar) a*sN*asinh( (double) (0.5*slipVel/v0)*exp(psi/a) );
  return strength;
}


// computes frictional strength by appying strength_psi to a vector
PetscErrorCode strength_psi_Vec(Vec& strength, const Vec& psi, const Vec& slipVel, const Vec& a,  const Vec& sN, const PetscScalar& v0)
{
  PetscErrorCode ierr = 0;

  PetscScalar *strengthA;
  const PetscScalar *psiA,*slipVelA,*aA,*sNA;
  VecGetArray(strength,&strengthA);
  VecGetArrayRead(psi,&psiA);
  VecGetArrayRead(slipVel,&slipVelA);
  VecGetArrayRead(a,&aA);
  VecGetArrayRead(sN,&sNA);

  PetscInt Jj = 0; // local array index
  PetscInt Istart, Iend;
  ierr = VecGetOwnershipRange(strength,&Istart,&Iend);

  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    strengthA[Jj] = strength_psi( sNA[Jj], psiA[Jj], slipVelA[Jj], aA[Jj], v0);
    Jj++;
  }

  VecRestoreArray(strength,&strengthA);
  VecRestoreArrayRead(psi,&psiA);
  VecRestoreArrayRead(slipVel,&slipVelA);
  VecRestoreArrayRead(a,&aA);
  VecRestoreArrayRead(sN,&sNA);

  return ierr;
}
