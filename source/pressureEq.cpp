#include "pressureEq.hpp"

#define FILENAME "pressureEq.cpp"

using namespace std;

PressureEq::PressureEq(Domain &D)
  : _D(&D), _p(NULL), _k_p(NULL),
    _permSlipDependent("no"), _permPressureDependent("no"),
    _file(D._file), _delim(D._delim), _outputDir(D._outputDir),
    _hydraulicTimeIntType("implicit"),_guessSteadyStateICs(1),
    _z(NULL), _n_p(NULL), _beta_p(NULL), _eta_p(NULL), _rho_f(NULL), _g(9.8),
    _bcB_type("Q"), _bcB_flux(1e-10), _bcB_pressure(0),
    _bcT_type("p"), _bcT_flux(1e-10), _bcT_pressure(0), 
    _maxBEIteration(100), _BETol(0.01), _linSolver("AMG"), _kspTol(1e-8),
    _ksp(NULL), _pc(NULL), _sbp(NULL), _linSolveCount(0), _writeTime(0),
    _linSolveTime(0), _ptTime(0), _startTime(0), _miscTime(0), _invTime(0)
{
  loadSettings(_file);
  checkInput();
  setFields(D);

  if (_D->_ckpt > 0 && _D->_ckptNumber > 0) {
    _guessSteadyStateICs = 0;
    loadCheckpoint();
  }
  else {
    loadFieldsFromFiles();
  }
  
  // calls computeVarCoeff for the first time, and sets up SBP matrix
  setupSBP(_D->_initTime);

  // Backward Eular setup if implicit integration
  if (_hydraulicTimeIntType == "implicit") {
    setupBackEuler(D); // already computes _coeff and BC
  }

  // if not pressure dependent, do not need iterative solver within backward Euler
  if (_permPressureDependent == "no") {
    _maxBEIteration = 1;
  }
  
  // compute initial conditions if not performing MMS
  if (_D->_isMMS == 0 && _guessSteadyStateICs == 1) {
    computeInitialSSPressure();
  }
}


// destructor, free memory
PressureEq::~PressureEq()
{
  // fields that exist on the fault
  VecDestroy(&_n_p);
  VecDestroy(&_beta_p);
  if (_permSlipDependent == "no" && _permPressureDependent == "no") {
    VecDestroy(&_k_p);
  }
  VecDestroy(&_k_slip);
  VecDestroy(&_k_press);

  VecDestroy(&_eta_p);
  VecDestroy(&_rho_f);

  // permeability
  if (_permSlipDependent == "yes") {
    VecDestroy(&_kL_p);
    VecDestroy(&_kT_p);
    VecDestroy(&_kmin_p);
    VecDestroy(&_kmax_p);
  }

  if (_permPressureDependent == "yes") {
    VecDestroy(&_kmin2_p);
    VecDestroy(&_sigma_p);
  }

  // intermediate coefficient matrices/vectors
  MatDestroy(&_Diag_n_beta);
  MatDestroy(&_D2_n_beta);
  VecDestroy(&_rhs);
  VecDestroy(&_coeff);
  VecDestroy(&_hydroBC);
  VecDestroy(&_gSource);
  VecDestroy(&_flux);
  
  // pressure fields
  VecDestroy(&_p);
  VecDestroy(&_bcL);
  VecDestroy(&_bcT);
  VecDestroy(&_bcB);
  VecDestroy(&_p_t);
  VecDestroy(&_z);
  VecDestroy(&_sN);

  if (_D->_isMMS == 1) {
    VecDestroy(&_pA);
    VecDestroy(&_source);
    VecDestroy(&_Hxsource);
  }
  
  delete _sbp; _sbp = NULL;

  KSPDestroy(&_ksp);

  // destroy viewers
  for (map<string,pair<PetscViewer,string>>::iterator it = _viewers.begin(); it != _viewers.end(); it++) {
    PetscViewerDestroy(&_viewers[it->first].first);
  }

  VecScatterDestroy(&_scatters);

}


// return pressure: copy _p to P
PetscErrorCode PressureEq::getPressure(Vec &P)
{
  PetscErrorCode ierr = 0;
  VecCopy(_p, P);
  return ierr;
}


// set pressure, copies P to _p
PetscErrorCode PressureEq::setPressure(const Vec &P)
{
  PetscErrorCode ierr = 0;
  VecCopy(P, _p);
  return ierr;
}


// return permeability: copy _k_p to K
PetscErrorCode PressureEq::getPermeability(Vec &K)
{
  PetscErrorCode ierr = 0;
  VecCopy(_k_p, K);
  return ierr;
}


// set permeability: copy K to _k_p
PetscErrorCode PressureEq::setPremeability(const Vec &K)
{
  PetscErrorCode ierr = 0;
  VecCopy(K, _k_p);
  return ierr;
}



// load field from input file
PetscErrorCode PressureEq::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt rank, size;
  MPI_Comm_size(PETSC_COMM_WORLD, &size);
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  ifstream infile(file);
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0, pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos + _delim.length(), line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0, pos);

    if (var == "hydraulicTimeIntType") { _hydraulicTimeIntType = rhs.c_str(); }
    else if (var == "guessSteadyStateICs") { _guessSteadyStateICs = (int)atof(rhs.c_str()); }
    else if (var == "linSolver") { _linSolver = rhs.c_str(); }
    else if (var == "kspTol_pressure") { _kspTol = atof(rhs.c_str()); }
    else if (var == "bcB_type") { _bcB_type = rhs.c_str(); }
    else if (var == "bcB_flux") { _bcB_flux = atof(rhs.c_str()); }
    else if (var == "bcB_pressure") { _bcB_pressure = atof(rhs.c_str()); }
    
    else if (var == "bcT_type") { _bcT_type = rhs.c_str(); }
    else if (var == "bcT_flux") { _bcT_flux = atof(rhs.c_str()); }
    else if (var == "bcT_pressure") { _bcT_pressure = atof(rhs.c_str()); }
    
    // loading vector inputs in the input file
    else if (var == "sNVals") { loadVectorFromInputFile(rhsFull, _sigmaNVals); }
    else if (var == "sNDepths") { loadVectorFromInputFile(rhsFull, _sigmaNDepths); }
    else if (var == "n_pVals") { loadVectorFromInputFile(rhsFull, _n_pVals); }
    else if (var == "n_pDepths") { loadVectorFromInputFile(rhsFull, _n_pDepths); }
    else if (var == "beta_pVals") { loadVectorFromInputFile(rhsFull, _beta_pVals); }
    else if (var == "beta_pDepths") { loadVectorFromInputFile(rhsFull, _beta_pDepths); }
    else if (var == "k_pVals") { loadVectorFromInputFile(rhsFull, _k_pVals); }
    else if (var == "k_pDepths") { loadVectorFromInputFile(rhsFull, _k_pDepths); }
    else if (var == "eta_pVals") { loadVectorFromInputFile(rhsFull, _eta_pVals); }
    else if (var == "eta_pDepths") { loadVectorFromInputFile(rhsFull, _eta_pDepths); }
    else if (var == "rho_pVals") { loadVectorFromInputFile(rhsFull, _rho_fVals); }
    else if (var == "rho_pDepths") { loadVectorFromInputFile(rhsFull, _rho_fDepths); }
    else if (var == "g") { _g = atof(rhs.c_str()); }
    else if (var == "pVals") { loadVectorFromInputFile(rhsFull, _pVals); }
    else if (var == "pDepths") { loadVectorFromInputFile(rhsFull, _pDepths); }

    // permability evolution parameters - slip-dependent
    else if (var == "permSlipDependent") { _permSlipDependent = rhs.c_str(); }
    else if (var == "kL_pVals") { loadVectorFromInputFile(rhsFull, _kL_pVals); }
    else if (var == "kL_pDepths") { loadVectorFromInputFile(rhsFull, _kL_pDepths); }
    else if (var == "kT_pVals") { loadVectorFromInputFile(rhsFull, _kT_pVals); }
    else if (var == "kT_pDepths") { loadVectorFromInputFile(rhsFull, _kT_pDepths); }
    else if (var == "kmin_pVals") { loadVectorFromInputFile(rhsFull, _kmin_pVals); }
    else if (var == "kmin_pDepths") { loadVectorFromInputFile(rhsFull, _kmin_pDepths); }
    else if (var == "kmax_pVals") { loadVectorFromInputFile(rhsFull, _kmax_pVals); }
    else if (var == "kmax_pDepths") { loadVectorFromInputFile(rhsFull, _kmax_pDepths); }

    // permeability evolution parameters - pressure-dependent
    else if (var == "permPressureDependent") { _permPressureDependent = rhs.c_str(); }
    else if (var == "kmin2_pVals") { loadVectorFromInputFile(rhsFull, _kmin2_pVals); }
    else if (var == "kmin2_pDepths") { loadVectorFromInputFile(rhsFull, _kmin2_pDepths); }
    else if (var == "sigma_pVals") { loadVectorFromInputFile(rhsFull, _sigma_pVals); }
    else if (var == "sigma_pDepths") { loadVectorFromInputFile(rhsFull, _sigma_pDepths); }
    else if (var == "maxBEIteration") { _maxBEIteration = (int)atof(rhs.c_str()); }
    else if (var == "BETol") { _BETol = atof(rhs.c_str()); }
  }

  return ierr;
}


// initialize vector fields
PetscErrorCode PressureEq::setFields(Domain &D)
{
  PetscErrorCode ierr = 0;

  // allocate memory and partition accross processors
  VecCreate(PETSC_COMM_WORLD, &_p);
  VecSetSizes(_p, PETSC_DECIDE, _D->_Nz);
  VecSetFromOptions(_p);
  PetscObjectSetName((PetscObject)_p, "_p");
  VecSet(_p, 0.0);

  VecDuplicate(_p, &_p_t);
  PetscObjectSetName((PetscObject)_p_t, "p_t");
  VecSet(_p_t, 0.0);

  VecDuplicate(_p, &_flux);
  PetscObjectSetName((PetscObject)_flux, "flux");
  VecSet(_flux, 0.0);

  // extract local z from D._z (which is the full 2D field)
  VecDuplicate(_p, &_z);
  PetscInt Istart, Iend;
  PetscScalar z = 0;
  VecGetOwnershipRange(D._z, &Istart, &Iend);
  for (PetscInt Ii = Istart; Ii < Iend; Ii++) {
    if (Ii < _D->_Nz) {
      VecGetValues(D._z, 1, &Ii, &z);
      VecSetValue(_z, Ii, z, INSERT_VALUES);
    }
  }
  VecAssemblyBegin(_z);
  VecAssemblyEnd(_z);

  VecDuplicate(_p, &_n_p);
  PetscObjectSetName((PetscObject)_n_p, "n_p");
  VecSet(_n_p, 0.0);

  VecDuplicate(_p, &_beta_p);
  PetscObjectSetName((PetscObject)_beta_p, "beta_p");
  VecSet(_beta_p, 0.0);

  VecDuplicate(_p, &_k_p);
  PetscObjectSetName((PetscObject)_k_p, "k_p");
  VecSet(_k_p, 0.0);

  VecDuplicate(_p, &_k_slip);
  PetscObjectSetName((PetscObject)_k_slip, "k_slip");
  VecSet(_k_slip, 0.0);

  VecDuplicate(_p, &_k_press);
  PetscObjectSetName((PetscObject)_k_press, "k_press");
  VecSet(_k_press, 0.0);

  VecDuplicate(_p, &_eta_p);
  PetscObjectSetName((PetscObject)_eta_p, "eta_p");
  VecSet(_eta_p, 0.0);

  VecDuplicate(_p, &_rho_f);
  PetscObjectSetName((PetscObject)_rho_f, "rho_f");
  VecSet(_rho_f, 0.0);

  VecDuplicate(_p, &_sN);
  PetscObjectSetName((PetscObject)_sN, "sN");
  VecSet(_sN, 0.0);

  ierr = setVec(_p, _z, _pVals, _pDepths); CHKERRQ(ierr);
  ierr = setVec(_n_p, _z, _n_pVals, _n_pDepths); CHKERRQ(ierr);
  ierr = setVec(_beta_p, _z, _beta_pVals, _beta_pDepths); CHKERRQ(ierr);
  ierr = setVec(_k_slip, _z, _k_pVals, _k_pDepths); CHKERRQ(ierr);
  ierr = setVec(_k_press, _z, _k_pVals, _k_pDepths); CHKERRQ(ierr);
  ierr = setVec(_k_p, _z, _k_pVals, _k_pDepths); CHKERRQ(ierr);
  
  // setting _k_p to point to either pressure or slip-dependent permeability
  if (_permPressureDependent == "yes") {
    _k_p = _k_press;
  }
  else if (_permSlipDependent == "yes") {
    _k_p = _k_slip;
  }
  
  ierr = setVec(_eta_p, _z, _eta_pVals, _eta_pDepths); CHKERRQ(ierr);
  ierr = setVec(_rho_f, _z, _rho_fVals, _rho_fDepths); CHKERRQ(ierr);
  ierr = setVec(_sN, _z, _sigmaNVals, _sigmaNDepths); CHKERRQ(ierr);

  // for slip-dependent permeability
  if (_permSlipDependent == "yes") {
    VecDuplicate(_p, &_kL_p);
    PetscObjectSetName((PetscObject)_kL_p, "kL_p");
    VecSet(_kL_p, 0.0);

    VecDuplicate(_p, &_kT_p);
    PetscObjectSetName((PetscObject)_kT_p, "kT_p");
    VecSet(_kT_p, 0.0);

    VecDuplicate(_p, &_kmin_p);
    PetscObjectSetName((PetscObject)_kmin_p, "kmin_p");
    VecSet(_kmin_p, 0.0);

    VecDuplicate(_p, &_kmax_p);
    PetscObjectSetName((PetscObject)_kmax_p, "kmax_p");
    VecSet(_kmax_p, 0.0);

    // setVec fills the vector (first argument) with the linear interpolation of a [value, depth] pair on the coordinate system (second argument)
    ierr = setVec(_kL_p, _z, _kL_pVals, _kL_pDepths); CHKERRQ(ierr);
    ierr = setVec(_kT_p, _z, _kT_pVals, _kT_pDepths); CHKERRQ(ierr);
    ierr = setVec(_kmin_p, _z, _kmin_pVals, _kmin_pDepths); CHKERRQ(ierr);
    ierr = setVec(_kmax_p, _z, _kmax_pVals, _kmax_pDepths); CHKERRQ(ierr);
  }

  // for pressure-dependent permeability
  if (_permPressureDependent == "yes") {
    VecDuplicate(_p, &_kmin2_p);
    PetscObjectSetName((PetscObject)_kmin2_p, "meanP_p");
    VecSet(_kmin2_p, 0.0);

    VecDuplicate(_p, &_sigma_p);
    PetscObjectSetName((PetscObject)_sigma_p, "sigma_p");
    VecSet(_sigma_p, 0.0);

    ierr = setVec(_kmin2_p, _z, _kmin2_pVals, _kmin2_pDepths); CHKERRQ(ierr);
    ierr = setVec(_sigma_p, _z, _sigma_pVals, _sigma_pDepths); CHKERRQ(ierr);
  }

  // rhs for SAT term
  VecDuplicate(_p, &_rhs);
  VecSet(_rhs, 0.0);

  // variable coefficient
  VecDuplicate(_p, &_coeff);
  VecSet(_coeff, 0.0);

  // hydrostatic added to BC
  VecDuplicate(_p, &_hydroBC);
  VecSet(_hydroBC, 0);

  // gravity source term to pressure equation
  VecDuplicate(_p, &_gSource);
  VecSet(_gSource, 0);

  // boundary conditions
  VecDuplicate(_p, &_bcL);
  PetscObjectSetName((PetscObject)_bcL, "bcL");
  VecSet(_bcL, 0.0);

  VecCreate(PETSC_COMM_WORLD, &_bcT);
  VecSetSizes(_bcT, PETSC_DECIDE, 1);
  VecSetFromOptions(_bcT);
  PetscObjectSetName((PetscObject)_bcT, "bcT");

  VecDuplicate(_bcT, &_bcB);
  PetscObjectSetName((PetscObject)_bcB, "bcB");
  VecSet(_bcB, 0);

  if (_D->_isMMS == 1) {
    VecDuplicate(_p, &_pA);
    PetscObjectSetName((PetscObject)_pA, "pA");
    VecSet(_pA, 0);

    VecDuplicate(_p, &_source);
    PetscObjectSetName((PetscObject)_source, "source");
    VecSet(_source, 0);

    VecDuplicate(_p, &_Hxsource);
    PetscObjectSetName((PetscObject)_Hxsource, "Hxsource");
    VecSet(_Hxsource, 0);
  }

  // scatter for boundary
  PetscInt *fi;
  PetscMalloc1(1, &fi);
  fi[0] = _D->_Nz - 1;
  IS isf;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, fi, PETSC_COPY_VALUES, &isf);
  PetscInt *ti;
  PetscMalloc1(1, &ti);
  ti[0] = 0;

  IS ist;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, 1, ti, PETSC_COPY_VALUES, &ist);
  ierr = VecScatterCreate(_p, isf, _bcB, ist, &_scatters); CHKERRQ(ierr);

  PetscFree(fi);
  PetscFree(ti);
  ISDestroy(&isf);
  ISDestroy(&ist);
  
  return ierr;
}


// load from previous simulations
PetscErrorCode PressureEq::loadFieldsFromFiles()
{
  PetscErrorCode ierr = 0;
  if (_D->_ckptNumber > 0) {
    // other material properties files to load from previous simulation
    ierr = loadVecFromInputFile(_n_p, _outputDir, "hydro_n"); CHKERRQ(ierr);
    ierr = loadVecFromInputFile(_beta_p, _outputDir, "hydro_beta"); CHKERRQ(ierr);
    ierr = loadVecFromInputFile(_eta_p, _outputDir, "hydro_eta"); CHKERRQ(ierr);
    ierr = loadVecFromInputFile(_rho_f, _outputDir, "hydro_rho_f"); CHKERRQ(ierr);
  }
  // load any specified input for pressure and permeability profiles
  else {
    ierr = loadVecFromInputFile(_p, _inputDir, "hydro_p"); CHKERRQ(ierr);
    ierr = loadVecFromInputFile(_k_p, _inputDir, "hydro_k"); CHKERRQ(ierr);
  }

  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode PressureEq::checkInput()
{
  PetscErrorCode ierr = 0;

  assert(_hydraulicTimeIntType == "explicit" || _hydraulicTimeIntType == "implicit");
  assert(_permSlipDependent == "no" || _permSlipDependent == "yes");
  assert(_permPressureDependent == "no" || _permPressureDependent == "yes");
  assert(_bcB_type == "Q" || _bcB_type == "p");
  assert(_bcT_type == "Q" || _bcT_type == "p");

  assert(_pVals.size()       == _pDepths.size());
  assert(_n_pVals.size()     == _n_pDepths.size());
  assert(_beta_pVals.size()  == _beta_pDepths.size());
  assert(_k_pVals.size()     == _k_pDepths.size());
  assert(_eta_pVals.size()   == _eta_pDepths.size());
  assert(_rho_fVals.size()   == _rho_fDepths.size());
  assert(_pVals.size()       != 0);
  assert(_n_pVals.size()     != 0);
  assert(_beta_pVals.size()  != 0);
  assert(_k_pVals.size()     != 0);
  assert(_eta_pVals.size()   != 0);
  assert(_rho_fVals.size()   != 0);
  assert(_g >= 0);

  if (_permSlipDependent == "yes") {
    assert(_kL_pVals.size()    == _kL_pDepths.size());
    assert(_kT_pVals.size()    == _kT_pDepths.size());
    assert(_kmin_pVals.size()  == _kmin_pDepths.size());
    assert(_kmax_pVals.size()  == _kmax_pDepths.size());
    assert(_kL_pVals.size()    != 0);
    assert(_kT_pVals.size()    != 0);
    assert(_kmin_pVals.size()  != 0);
    assert(_kmax_pVals.size()  != 0);
  }

  if (_permPressureDependent == "yes") {
    assert(_kmin2_pVals.size() == _kmin2_pDepths.size());
    assert(_sigma_pVals.size() == _sigma_pDepths.size());
    assert(_sigmaNVals.size()  == _sigmaNDepths.size());
    assert(_kmin2_pVals.size() != 0);
    assert(_sigma_pVals.size() != 0);
    assert(_sigmaNVals.size()  != 0);
  }

  return ierr;
}


PetscErrorCode PressureEq::setupSBP(const double time)
{
  PetscErrorCode ierr = 0;

  // set up variable coefficient _coeff = rho * k / eta
  computeVarCoeff(_coeff);

  // Set up linear system
  // inputs: order, Ny, Nz, Ly, Lz, coefficient vector
  if (_D->_gridSpacingType == "constantGridSpacing") {
    _sbp = new SbpOps_m_constGrid(_D->_order, 1, _D->_Nz, 1, _D->_Lz, _coeff);
  }
  else if (_D->_gridSpacingType == "variableGridSpacing") {
    _sbp = new SbpOps_m_varGrid(_D->_order, 1, _D->_Nz, 1, _D->_Lz, _coeff);
    _sbp->setGrid(NULL, &_z);  // only set z grid for pressure 1D
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }

  // set BC types for top and bottom
  parseBCs();
  
  // set BC types for SBP operator. Inputs: bcR, bcT, bcL, bcB
  _sbp->setBCTypes("Dirichlet", _mat_bcT_type, "Dirichlet", _mat_bcB_type);

  // add boundary conditions for top and bottom
  imposeBCs();

  // H*A will be symmetric, so H is multiplied everywhere to make the linear system easier to solve (for "implicit" case only)
  if (_hydraulicTimeIntType == "explicit") {
    _sbp->setMultiplyByH(0);
  }
  else {
    _sbp->setMultiplyByH(1);
  }

  _sbp->setCompatibilityType(_D->_sbpCompatibilityType);
  _sbp->setLaplaceType("z");
  _sbp->computeMatrices(); // SBP matrices are constructed after calling this

  return ierr;
}


// set boundary condition matrix types
PetscErrorCode PressureEq::parseBCs() {
  PetscErrorCode ierr = 0;

  if (_bcT_type == "Dp" || _bcT_type == "Q") {   // fix dp/dz or flux
    _mat_bcT_type = "Neumann";
  }
  else if (_bcT_type == "p") {   // fix pressure
    _mat_bcT_type = "Dirichlet";
  }

  if (_bcB_type == "Dp" || _bcB_type == "Q") {   // fix dp/dz or flux
    _mat_bcB_type = "Neumann";
  }
  else if (_bcB_type == "p") {    // fix pressure
    _mat_bcB_type = "Dirichlet";
  }
  
  return ierr;
}


// add flux or pressure gradient to bottom and/or top boundaries
PetscErrorCode PressureEq::imposeBCs() {
  PetscErrorCode ierr = 0;

  // bottom boundary
  if (_bcB_type == "p") {
    VecSet(_bcB, _bcB_pressure);
  }
  else if (_bcB_type == "Q") {
    // hydrostatic bc = k/eta*rho*g = coeff*rho*g, added to flux
    VecSet(_hydroBC, _g);
    VecPointwiseMult(_hydroBC, _hydroBC, _rho_f);
    VecPointwiseMult(_hydroBC, _hydroBC, _coeff);
    VecScatterBegin(_scatters, _hydroBC, _bcB, INSERT_VALUES, SCATTER_FORWARD);
    VecScatterEnd(_scatters, _hydroBC, _bcB, INSERT_VALUES, SCATTER_FORWARD);
    // impose flux
    VecShift(_bcB, _bcB_flux);
  }

  // top boundary
  if (_bcT_type == "p") {
    VecSet(_bcT, _bcT_pressure);
  }
  else if (_bcT_type == "Q") {
    VecSet(_bcT, _bcT_flux);
  }  
  return ierr;
}
  
// set up KSP, coefficient matrix, boundary conditions for the hydralic problem with backward Euler (implicit)
PetscErrorCode PressureEq::setupBackEuler(Domain &D)
{
  PetscErrorCode ierr = 0;

  Mat D2;
  _sbp->getA(D2); // D2 already has H premultiplied within sbp class
  
  Mat H;
  _sbp->getH(H);

  // set up boundary terms (bcL, bcR, bcT, bcB)
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);

  // set nonzero diagonal pattern for _Diag_n_beta
  MatDuplicate(H, MAT_DO_NOT_COPY_VALUES, &_Diag_n_beta);

  // compute _Diag_n_beta = diag[1 / (n*beta)] (assumes this stays constant at every time step), to be used in backEuler function
  Vec n_beta;
  VecDuplicate(_p, &n_beta);
  VecSet(n_beta, 1);
  VecPointwiseDivide(n_beta, n_beta, _n_p);
  VecPointwiseDivide(n_beta, n_beta, _beta_p);
  MatDiagonalSet(_Diag_n_beta, n_beta, INSERT_VALUES);

  // apply coordinate transform to Diag_n_beta, which is multipled to D2 and rhs later
  if (_D->_gridSpacingType == "variableGridSpacing") {
    Mat J, Jinv, qy, rz, yq, zr;
    _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
    transformCoord(Jinv, _Diag_n_beta);
  }
  
  // D2_n_beta = Diag_n_beta * D2 
  MatMatMult(_Diag_n_beta, D2, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &_D2_n_beta);

  // H - dt * _D2_n_beta
  MatScale(_D2_n_beta, -_D->_initDeltaT);
  MatAXPY(_D2_n_beta, 1, H, SUBSET_NONZERO_PATTERN);

  setupKSP(_ksp, _pc, _D2_n_beta);

  VecDestroy(&n_beta);

  return ierr;
}


// set up linear solver context
PetscErrorCode PressureEq::setupKSP(KSP& ksp,PC& pc,Mat& A)
{
  PetscErrorCode ierr = 0;

  // create linear solver context
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);

  // set operators, here the matrix that defines the linear system also serves as the preconditioning matrix
  ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);

  // algebraic multigrid from HYPRE
  if (_linSolver == "AMG") {
    ierr = KSPSetType(ksp,KSPRICHARDSON); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(pc,"boomeramg"); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCFactorSetLevels(pc,4); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }

  // direct LU from MUMPS
  else if (_linSolver == "MUMPSLU") {
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS); CHKERRQ(ierr); // new PETSc
    ierr = PCFactorSetUpMatSolverType(pc); CHKERRQ(ierr); // new PETSc
  }

  // direct Cholesky (RR^T) from MUMPS
  else if (_linSolver == "MUMPSCHOLESKY") {
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCCHOLESKY); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS); CHKERRQ(ierr); // new PETSc
    ierr = PCFactorSetUpMatSolverType(pc); CHKERRQ(ierr); // new PETSc
  }

  // preconditioned conjugate gradient
  else if (_linSolver == "CG") {
    ierr = KSPSetType(ksp,KSPCG); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp, PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  }

  // undefined linear solver
  else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"ERROR: linSolver type not understood\n");
    assert(0);
  }

  // enable command line options to override those specified above, e.g.:
  // -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);

  // perform computation of preconditioners now, rather than on first use
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);

  return ierr;
}


// compute initial steady state pressure
PetscErrorCode PressureEq::computeInitialSSPressure()
{
  PetscErrorCode ierr = 0;

  // permeability for plate loading velocity: k = (V/L*kmax + 1.0/T*kmin)/(V/L + 1.0/T)
  
  // set up linear solver context
  KSP ksp = NULL;
  PC pc = NULL;
  Mat D2;
  _sbp->getA(D2);
  setupKSP(ksp, pc, D2);
  
  // for relative error control
  Vec p_prev;
  VecDuplicate(_p, &p_prev);
  VecCopy(_p, p_prev);

  float update_rate = 0.05; // for stability

  PetscInt i = 0;
  for (i = 0; i < _maxBEIteration; i++) {
    if (_permPressureDependent == "yes") {
      updatePermPressure(update_rate);
      computeVarCoeff(_coeff);
      _sbp->updateVarCoeff(_coeff);
      imposeBCs();  // update boundary condition based on new _coeff
    }
    _sbp->getA(D2);
    ierr = KSPSetOperators(ksp, D2, D2); CHKERRQ(ierr);

    // set up boundary conditions
    _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);

    // add source term from gravity
    gravitySource();  // multiplied by H here
    VecPointwiseMult(_gSource, _gSource, _n_p); // the source term is different in computing intial condition
    VecPointwiseMult(_gSource, _gSource, _beta_p);

    if (_D->_gridSpacingType == "variableGridSpacing") {
      Mat J, Jinv, qy, rz, yq, zr;
      _sbp->getCoordTrans(J, Jinv, qy, rz, yq, zr);
      Vec JgSource;
      VecDuplicate(_p, &JgSource);
      MatMult(J,_gSource, JgSource);
      VecCopy(JgSource, _gSource);
      VecDestroy(&JgSource);
    }
    VecAXPY(_rhs, 1, _gSource);
    
    // solve the linear system, result is _p
    double startTime = MPI_Wtime();
    ierr = KSPSolve(ksp, _rhs, _p); CHKERRQ(ierr);
    _linSolveTime += MPI_Wtime() - startTime;
    _linSolveCount++;

    PetscScalar err = 0;
    relativeErr(p_prev, err);
    if (err < _BETol * update_rate) {
      PetscPrintf(PETSC_COMM_WORLD, "ComputeInitialSSPressure breaking at iteration = %i with error = %.0e\n", i, err);
      break;
    }
    else {
      VecCopy(_p, p_prev);
    }
  }

  if (_maxBEIteration > 1 && i == _maxBEIteration) {
    PetscPrintf(PETSC_COMM_WORLD, "Max number of iterations reached in computeInitialSSPressure\n");
  }
  
  VecDestroy(&p_prev);
  KSPDestroy(&ksp);

  return ierr;
}


// compute variable coefficient vector b = k_p / eta
PetscErrorCode PressureEq::computeVarCoeff(Vec &coeff)
{
  PetscErrorCode ierr = 0;

  VecSet(coeff, 1.0);
  // VecPointwiseMult(coeff, coeff, _rho_f);
  VecPointwiseMult(coeff, coeff, _k_p);
  VecPointwiseDivide(coeff, coeff, _eta_p);

  return ierr;
}
  

// transform coordinate system for input vector
PetscErrorCode PressureEq::transformCoord(const Mat &trans, Vec &vec) {
  PetscErrorCode ierr = 0;

  Vec temp;
  VecDuplicate(vec, &temp);
  MatMult(trans, vec, temp);
  VecCopy(temp, vec);
  VecDestroy(&temp);

  return ierr;
}

// transform coordinate system for input matrix
PetscErrorCode PressureEq::transformCoord(const Mat &trans, Mat &mat) {
  PetscErrorCode ierr = 0;
  
  Mat temp;
  MatMatMult(trans, mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &temp);
  MatCopy(temp, mat, SAME_NONZERO_PATTERN);
  MatDestroy(&temp);

  return ierr;
}
  

// set up integration, put puressure and permeability terms into varEx, varIm
PetscErrorCode PressureEq::initiateIntegrand(const PetscScalar time, map<string, Vec> &varEx, map<string, Vec> &varIm)
{
  PetscErrorCode ierr = 0;

  // p will be destroyed when we destroy varEx or varIm from strikeSlip
  Vec p;
  VecDuplicate(_p, &p);
  VecCopy(_p, p);

  // put variable to be integrated explicitly into varEx
  if (_hydraulicTimeIntType == "explicit") {
    varEx["pressure"] = p;
  }

  // put variable to be integrated implicity into varIm
  // pressure can also be implicityl integrated
  else if (_hydraulicTimeIntType == "implicit") {
    varIm["pressure"] = p;
  }

  // permeability is always explicitly integrated
  if (_permSlipDependent == "yes") {
    Vec k_slip;
    VecDuplicate(_k_slip, &k_slip);
    VecCopy(_k_slip, k_slip);
    varEx["permeability"] = k_slip;
  }

  return ierr;
}


// update pressure and permeability for explicit time stepping method
// update _p and _k_p from values that are stored in varEx (which are updated during time integration)
PetscErrorCode PressureEq::updateFields(const map<string, Vec> &varEx)
{
  PetscErrorCode ierr = 0;

  if (_hydraulicTimeIntType == "explicit" && varEx.find("pressure") != varEx.end()) {
    VecCopy(varEx.find("pressure")->second, _p);
  }

  if (varEx.find("permeability") != varEx.end()) {
    VecCopy(varEx.find("permeability")->second, _k_slip);
  }

  return ierr;
}


// update pressure and permeability for implicit time stepping method
// update _p and _k_p from values stored in varEx and varIm
PetscErrorCode PressureEq::updateFields(const map<string, Vec> &varEx, const map<string, Vec> &varIm)
{
  PetscErrorCode ierr = 0;

  if (_hydraulicTimeIntType == "explicit" && varEx.find("pressure") != varEx.end()) {
    VecCopy(varEx.find("pressure")->second, _p);
  }
  else if (_hydraulicTimeIntType == "implicit" && varIm.find("pressure") != varIm.end()) {
    VecCopy(varIm.find("pressure")->second, _p);
  }

  if (varEx.find("permeability") != varEx.end()) {
    VecCopy(varEx.find("permeability")->second, _k_slip);
  }

  return ierr;
}


// update pressure-dependent permeability
PetscErrorCode PressureEq::updatePermPressure(const float rate=1.0)
{
  PetscErrorCode ierr = 0;

  // compute _k_press = (k_slip - kmin2) / exp( (sN-p)/sigma_p ) + kmin2
  Vec temp;
  VecDuplicate(_k_slip, &temp);
  VecCopy(_k_slip, temp);
  VecAXPY(temp, -1.0, _kmin2_p);  // temp = k_slip - kmin2

  Vec k_press_prev;
  VecDuplicate(_k_press, &k_press_prev);
  VecCopy(_k_press, k_press_prev);
  
  VecPointwiseMin(_p, _p, _sN); // force _p entries > _sN to be _sN
  VecWAXPY(_k_press, -1.0, _p, _sN); // sN - p
  VecPointwiseDivide(_k_press, _k_press, _sigma_p); // (sN - p)/sigma_p
  VecExp(_k_press); // exp((sN - p)/sigma_p)
  VecPointwiseDivide(_k_press, temp, _k_press); //(k_slip - kmin2)/exp((sN - p)/sigma_p)
  VecAXPY(_k_press, 1.0, _kmin2_p); // (k_slip - kmin2)/exp((sN - p)/sigma_p) + kmin2
  VecAXPBY(_k_press, 1-rate, rate, k_press_prev);


  // free temporary variable
  VecDestroy(&temp);
  VecDestroy(&k_press_prev);

  return ierr;
}


// compute gravity source term: d/dz (k/eta*rho*g) / (n*beta)
// no coordinate transform here, but H is multiplied if implicit time stepping
PetscErrorCode PressureEq::gravitySource()
{
  PetscErrorCode ierr = 0;

  Vec Dz_gSource;
  VecDuplicate(_p, &Dz_gSource);
  VecSet(_gSource, _g);                           // g
  VecPointwiseMult(_gSource, _gSource, _rho_f);   // rho*g
  VecPointwiseMult(_gSource, _gSource, _coeff);   // rho*g*k/eta
  _sbp->Dz(_gSource, Dz_gSource);                 
  VecCopy(Dz_gSource, _gSource);                  // Dz(rho*g*k/eta)

  if (_hydraulicTimeIntType == "implicit") {
    _sbp->H(_gSource, Dz_gSource);
    VecCopy(Dz_gSource, _gSource);
  }

  VecPointwiseDivide(_gSource, _gSource, _n_p);
  VecPointwiseDivide(_gSource, _gSource, _beta_p);
  
  VecDestroy(&Dz_gSource);  
  return ierr;
}

// compute flux: q = k/eta * (dp/dz - rho*g)
PetscErrorCode PressureEq::computeFlux()
{
  PetscErrorCode ierr = 0;

  Vec rho_g;
  VecDuplicate(_p, &rho_g);
  VecSet(rho_g, _g);  
  VecPointwiseMult(rho_g, rho_g, _rho_f);   // rho*g

  _sbp->Dz(_p, _flux);  // Dp
  VecAXPY(_flux, -1.0, rho_g); // Dp - rho*g
  VecPointwiseMult(_flux, _flux, _coeff);   // k/eta (Dp - rho*g)
  
  VecDestroy(&rho_g);
  return ierr;
}

  
// update rate of change of slip-dependent permeability
PetscErrorCode PressureEq::dk_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime(); // time this section

  Vec vel_abs;
  VecDuplicate(dvarEx.find("slip")->second, &vel_abs);
  VecCopy(dvarEx.find("slip")->second, vel_abs);
  VecAbs(vel_abs);

  Vec dk = dvarEx["permeability"];

  // dk_new = dk_old - (k - kmax)*|V|/L - (k - kmin)/T
  Vec temp;
  VecDuplicate(_p, &temp);

  VecWAXPY(temp, -1.0, _kmax_p, _k_slip);   // temp = k_p - kmax
  VecPointwiseMult(temp, temp, vel_abs); // temp = (k_p - kmax)*|V|
  VecPointwiseDivide(temp, temp, _kL_p); // temp = (k_p - kmax)*|V|/L
  VecAXPY(dk, -1.0, temp);               // dk  -= (k_p - kmax)*|V|/L

  VecWAXPY(temp, -1.0, _kmin_p, _k_slip);   // temp = k_p - kmin
  VecPointwiseDivide(temp, temp, _kT_p); // temp = (k_p - kmin)/T
  VecAXPY(dk, -1.0, temp);               // dk  -= (k_p - kmin)/T

  VecDestroy(&temp);
  VecDestroy(&vel_abs);

  _ptTime += MPI_Wtime() - startTime;
  return ierr;
}


// update rate of change of slip-dependent permeability
// called in qd_fd
PetscErrorCode PressureEq::dk_dt(const PetscScalar time, const Vec slipVel, const Vec &K, Vec &dKdt)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime(); // time this section

  Vec vel_abs;
  VecDuplicate(slipVel, &vel_abs);
  VecCopy(slipVel, vel_abs);
  VecAbs(vel_abs);

  Vec temp;
  VecDuplicate(_p, &temp);

  // dKdt -= (k - kmax)*|V|/L
  VecWAXPY(temp, -1.0, _kmax_p, _k_slip);
  VecPointwiseMult(temp, temp, vel_abs);
  VecPointwiseDivide(temp, temp, _kL_p);
  VecAXPY(dKdt, -1.0, temp);

  // dKdt -= (k - kmin)/T
  VecWAXPY(temp, -1.0, _kmin_p, _k_slip);
  VecPointwiseDivide(temp, temp, _kT_p);
  VecAXPY(dKdt, -1.0, temp);

  VecDestroy(&temp);
  VecDestroy(&vel_abs);

  _ptTime += MPI_Wtime() - startTime;
  return ierr;
}


// purely explicit time integration
PetscErrorCode PressureEq::d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;

  if (_permSlipDependent == "yes") {
    ierr = dk_dt(time, varEx, dvarEx); CHKERRQ(ierr);
  }

  if (_hydraulicTimeIntType == "explicit") {
    if (_D->_isMMS == 1) {
      ierr = d_dt_mms(time, varEx, dvarEx); CHKERRQ(ierr);
    }
    else {
      ierr = dp_dt(time, varEx, dvarEx); CHKERRQ(ierr);
    }
  }

  return ierr;
}


// implicit/explicit time integration
PetscErrorCode PressureEq::d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  if (_permSlipDependent == "yes") {
    ierr = dk_dt(time, varEx, dvarEx); CHKERRQ(ierr);
  }

  if (_hydraulicTimeIntType == "explicit") {
    ierr = dp_dt(time, varEx, dvarEx); CHKERRQ(ierr);
  }
  else {
    if (_D->_isMMS == 1) {
      ierr = backEuler_mms(time, varEx, dvarEx, varIm, varImo, dt); CHKERRQ(ierr);
    }
    else {
      ierr = backEuler(time, varEx, dvarEx, varIm, varImo, dt); CHKERRQ(ierr);
    }
  }

  return ierr;
}


// purely explicit time integration
PetscErrorCode PressureEq::dp_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();
  VecCopy(varEx.find("pressure")->second, _p);

  // impose boundary condition (time-dependent)
  imposeBCs();

  if (_permSlipDependent == "yes" || _permPressureDependent == "yes") {
    if (_permPressureDependent == "yes") {
      updatePermPressure();
    }
    computeVarCoeff(_coeff);
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
  }

  Vec p_t = dvarEx["pressure"];
  
  // dp/dt = (D2*p - Dz*(k/eta*rho*g) - rhs) / (n*beta)
  Mat D2;
  _sbp->getA(D2);
  ierr = MatMult(D2, _p, p_t); CHKERRQ(ierr);
  
  // set up boundary terms
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
  VecAXPY(p_t, -1.0, _rhs);

  Vec scaled_p_t;
  VecDuplicate(p_t, &scaled_p_t);
  MatMult(_Diag_n_beta, p_t, scaled_p_t);
  VecCopy(scaled_p_t, p_t);

  gravitySource(); // computes _gSource
  VecAXPY(p_t, -1, _gSource);

  VecDestroy(&scaled_p_t);

  _ptTime += MPI_Wtime() - startTime;
  return ierr;
}


// this is doing the same thing as the above function, except for dPdt
// used in qd_fd
PetscErrorCode PressureEq::dp_dt(const PetscScalar time, const Vec &P, Vec &dPdt)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime(); // time this section

  // impose boundary condition (time-dependent)
  imposeBCs();

  if (_permSlipDependent == "yes" || _permPressureDependent == "yes") {
    if (_permPressureDependent == "yes") {
      updatePermPressure();
    }
    computeVarCoeff(_coeff);
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
  }
  
  // dP/dt = (D2*p - rhs) / (rho*n*beta)
  Mat D2;
  _sbp->getA(D2);
  ierr = MatMult(D2, P, dPdt); CHKERRQ(ierr);

  // set up boundary terms
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
  VecAXPY(dPdt, -1.0, _rhs);
  
  Vec scaled_dPdt;
  VecDuplicate(dPdt, &scaled_dPdt);
  ierr = MatMult(_Diag_n_beta, dPdt, scaled_dPdt); CHKERRQ(ierr);
  VecCopy(scaled_dPdt, dPdt);

  gravitySource();
  VecAXPY(dPdt, -1, _gSource);
  
  VecDestroy(&scaled_dPdt);
  
  _ptTime += MPI_Wtime() - startTime;
  return ierr;
}


// purely explicit time integration for MMS test
PetscErrorCode PressureEq::d_dt_mms(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime(); // time this section

  VecCopy(varEx.find("pressure")->second, _p);

  // impose boundary condition (time-dependent)
  imposeBCs();

  if (_permSlipDependent == "yes" || _permPressureDependent == "yes") {
    if (_permPressureDependent == "yes") {
      updatePermPressure();
    }
    computeVarCoeff(_coeff);
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
  }

  Vec p_t = dvarEx["pressure"];

  // dp/dt = (D2*p - rhs) / (rho*n*beta)
  Mat D2;
  _sbp->getA(D2);
  ierr = MatMult(D2, _p, p_t); CHKERRQ(ierr);

  // set up boundary terms
  _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
  VecAXPY(p_t, -1.0, _rhs);

  Vec scaled_p_t;
  VecDuplicate(p_t, &scaled_p_t);
  MatMult(_Diag_n_beta, p_t, scaled_p_t);
  VecCopy(scaled_p_t, p_t);

  gravitySource();
  VecAXPY(p_t, -1, _gSource);
  
  // add source term from MMS
  Vec source;
  VecDuplicate(_p, &source);
  mapToVec(source, zzmms_pSource1D, _z, time);
  VecAXPY(p_t, 1.0, source);

  VecDestroy(&scaled_p_t);
  VecDestroy(&source);
  
  _ptTime += MPI_Wtime() - startTime;

  return ierr;
}


// backward Euler implicit solve, new result for _p goes in varIm
PetscErrorCode PressureEq::backEuler(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();

  VecCopy(varImo.find("pressure")->second, _p);

  // impose boundary condition (time-dependent)
  imposeBCs();

  // update variable coefficient (i.e. k) for slip-dependent permeability
  if (_permSlipDependent == "yes") {
    computeVarCoeff(_coeff);
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
  }

  // store previous time step's pressure for residual norm calculation
  Vec p_prev;
  VecDuplicate(_p, &p_prev);
  VecCopy(_p, p_prev);

  Vec Hxp;
  VecDuplicate(_p, &Hxp);
  _sbp->H(varImo.find("pressure")->second, Hxp);

  Vec scaled_rhs;
  VecDuplicate(_rhs, &scaled_rhs);
  
  PetscInt i = 0;
  for (i = 0; i < _maxBEIteration; i++) {
    double tempTime = MPI_Wtime();
    // update pressure-dependent permeability and variable coefficient
    if (_permPressureDependent == "yes") {
      updatePermPressure();
      computeVarCoeff(_coeff);
      _sbp->updateVarCoeff(_coeff);
      imposeBCs();
    }
    
    _miscTime += MPI_Wtime() - tempTime;
    
    Mat D2;
    _sbp->getA(D2);

    Mat H;
    _sbp->getH(H);

    // set up boundary terms (_rhs is SAT term, already multiplied by H)
    _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
        
    // H*p - (dt*rhs)/(rho*n*beta)
    MatMult(_Diag_n_beta, _rhs, scaled_rhs);
    VecCopy(scaled_rhs, _rhs);
    gravitySource();
    VecAXPY(_rhs, 1, _gSource);
    
    VecScale(_rhs, -dt);
    VecAXPY(_rhs, 1, Hxp);

    // H - dt*D2/(rho*n*beta)
    MatMatMult(_Diag_n_beta, D2, MAT_REUSE_MATRIX, PETSC_DEFAULT, &_D2_n_beta);
    MatScale(_D2_n_beta, -dt);
    MatAXPY(_D2_n_beta, 1, H, SUBSET_NONZERO_PATTERN);

    tempTime = MPI_Wtime();

    // solve for p in the next time step from (I - dt*D2)p_next = p_curr - rhs
    ierr = KSPSetOperators(_ksp, _D2_n_beta, _D2_n_beta); CHKERRQ(ierr);
    ierr = KSPSolve(_ksp, _rhs, _p); CHKERRQ(ierr);
    _linSolveCount++;

    // calculate relative error, store in err
    PetscReal err = 0.0;
    relativeErr(p_prev, err);
    
    // break out of loop if drop below tolerance
    if (err < _BETol) {
      if (i > 10) PetscPrintf(PETSC_COMM_WORLD, "backEuler breaking at iteration = %i with error = %.0e\n", i, err);
      break; 
    }
    else {
      VecCopy(_p, p_prev);
    }
    
    _invTime += MPI_Wtime() - tempTime;
  }

  if (_maxBEIteration > 1 && i == _maxBEIteration) {
    PetscPrintf(PETSC_COMM_WORLD, "Max number of backEuler iteration reached:\n");
  }

  // copy variable back to integrator
  VecCopy(_p, varIm["pressure"]);

  _linSolveTime += MPI_Wtime() - startTime;

  // free memory
  VecDestroy(&scaled_rhs);
  VecDestroy(&Hxp);
  VecDestroy(&p_prev);

  _ptTime += MPI_Wtime() - startTime;

  return ierr;
}


// calculate relative error
PetscErrorCode PressureEq::relativeErr(Vec &p_prev, PetscReal &err) {
  PetscErrorCode ierr = 0;

  PetscScalar norm;
  Vec errVec;
  VecDuplicate(_p, &errVec);
  VecSet(errVec,0.0);
  ierr = VecWAXPY(errVec, -1.0, p_prev, _p); CHKERRQ(ierr);
  VecNorm(errVec, NORM_2, &err);
  VecNorm(p_prev, NORM_2, &norm);
  err = err / norm;

  VecDestroy(&errVec);
    
  return ierr;
}
  

// backward Euler implicit solve MMS test
PetscErrorCode PressureEq::backEuler_mms(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();

  VecCopy(varImo.find("pressure")->second, _p);
  
  // impose boundary condition (time-dependent)
  imposeBCs();
  
  // update variable coefficient for slip-dependent permeability
  if (_permSlipDependent == "yes") {
    computeVarCoeff(_coeff);
    _sbp->updateVarCoeff(_coeff);
    imposeBCs();
  }
  
  // store previous time step's pressure for residual norm calculation
  Vec p_prev;
  VecDuplicate(_p, &p_prev);
  VecCopy(_p, p_prev);

  Vec Hxp;
  VecDuplicate(_p, &Hxp);

  Vec scaled_rhs;
  VecDuplicate(_rhs, &scaled_rhs);
  
  PetscInt i = 0;
  for (i = 0; i < _maxBEIteration; i++) {
    double tempTime = MPI_Wtime();
    // update pressure-dependent permeability and variable coefficient
    if (_permPressureDependent == "yes") {
      updatePermPressure();
      computeVarCoeff(_coeff);
      _sbp->updateVarCoeff(_coeff);
      imposeBCs();
    }
    
    _miscTime += MPI_Wtime() - tempTime;
    
    Mat D2;
    _sbp->getA(D2);

    Mat H;
    _sbp->getH(H);

    // set up boundary terms (_rhs is SAT term, already multiplied by H)
    _sbp->setRhs(_rhs, _bcL, _bcL, _bcT, _bcB);
    
    // H * p - (dt * rhs)/(rho * n * beta)
    MatMult(_Diag_n_beta, _rhs, scaled_rhs);
    VecCopy(scaled_rhs, _rhs);
    gravitySource();
    VecAXPY(_rhs, 1, _gSource);
    
    VecScale(_rhs, -dt);
    _sbp->H(varImo.find("pressure")->second, Hxp);
    VecAXPY(_rhs, 1, Hxp);  

    // add source term to _rhs
    mapToVec(_source, zzmms_pSource1D, _z, time);
    _sbp->H(_source, _Hxsource);
    VecAXPY(_rhs, 1, _Hxsource);

    // H - dt * _D2_n_beta
    MatMatMult(_Diag_n_beta, D2, MAT_REUSE_MATRIX, PETSC_DEFAULT, &_D2_n_beta);
    MatScale(_D2_n_beta, -dt);
    MatAXPY(_D2_n_beta, 1, H, SUBSET_NONZERO_PATTERN);

    tempTime = MPI_Wtime();

    ierr = KSPSetOperators(_ksp, _D2_n_beta, _D2_n_beta); CHKERRQ(ierr);
    ierr = KSPSolve(_ksp, _rhs, _p); CHKERRQ(ierr);
    _linSolveCount++;

    // calculate relative error, store in err
    PetscReal err = 0.0;
    relativeErr(p_prev, err);
    
    // break out of loop if drop below tolerance
    if (err < _BETol) {
      break;
    }
    else {
      VecCopy(_p, p_prev);
    }    
    _invTime += MPI_Wtime() - tempTime;
  }

  // print 2-norm difference b/w MMS analytical vs. numerical solutions
  measureMMSError(time);
  		  
  if (_maxBEIteration > 1 && i == _maxBEIteration - 1) {
    PetscPrintf(PETSC_COMM_WORLD, "Max number of backward Euler iteration reached.");
  }

  VecCopy(_p, varIm["pressure"]);

  _linSolveTime += MPI_Wtime() - startTime;

  // free memory
  VecDestroy(&scaled_rhs);
  VecDestroy(&Hxp);
  VecDestroy(&p_prev);

  _ptTime += MPI_Wtime() - startTime;
  
  return ierr;
}


// =====================================================================
// IO commands

PetscErrorCode PressureEq::view(const double totRunTime)
{
  PetscErrorCode ierr = 0;
  ierr = PetscPrintf(PETSC_COMM_WORLD, "-------------------------------\n\n"); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "PressureEq Runtime Summary:\n"); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   max number of backward Euler iteration : %i\n", _maxBEIteration); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   backward Euler tolerance: %e\n", _BETol); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   pressure rate time (s): %g\n", _ptTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   %% integration time spent computing pressure rate: %g\n", _ptTime / totRunTime * 100.); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   delete and create SBP (s): %g\n", _miscTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   number of times linear system was solved: %i\n", _linSolveCount); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "   inversion (s): %g\n", _invTime); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "\n"); CHKERRQ(ierr);
  return ierr;
}


// extends SymmFault's writeContext
PetscErrorCode PressureEq::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;

  PetscViewer viewer;

  // write out context for pressureEq
  string str = outputDir + "hydro_context.txt";
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  ierr = PetscViewerASCIIPrintf(viewer, "g = %.15e\n", _g); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "hydraulicTimeIntType = %s\n", _hydraulicTimeIntType.c_str()); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "permSlipDependent = %s\n", _permSlipDependent.c_str()); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "permPressureDependent = %s\n", _permPressureDependent.c_str()); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "max backward Euler iterations = %i\n", _maxBEIteration); CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "backward Euler tolerance = %e\n", _BETol); CHKERRQ(ierr);  

  // bottom boundary
  ierr = PetscViewerASCIIPrintf(viewer, "bcB_type = %s\n", _bcB_type.c_str()); CHKERRQ(ierr);
  if (_bcB_type == "Q") {
    ierr = PetscViewerASCIIPrintf(viewer, "bcB_flux = %.15e\n", _bcB_flux); CHKERRQ(ierr);
  }
  else if (_bcB_type == "p") {
    ierr = PetscViewerASCIIPrintf(viewer, "bcB_pressure = %.15e\n", _bcB_pressure); CHKERRQ(ierr);
  }    

  // top boundary
  ierr = PetscViewerASCIIPrintf(viewer, "bcT_type = %s\n", _bcT_type.c_str()); CHKERRQ(ierr);
  if (_bcT_type == "Q") {
    ierr = PetscViewerASCIIPrintf(viewer, "bcT_flux = %.15e\n", _bcT_flux); CHKERRQ(ierr);
  }
  else if (_bcT_type == "p") {
    ierr = PetscViewerASCIIPrintf(viewer, "bcT_pressure = %.15e\n", _bcT_pressure); CHKERRQ(ierr);
  }    
  
  PetscViewerDestroy(&viewer);

  
  // write material parameters
  ierr = writeVec(_n_p, outputDir + "hydro_n"); CHKERRQ(ierr);
  ierr = writeVec(_beta_p, outputDir + "hydro_beta"); CHKERRQ(ierr);
  ierr = writeVec(_k_p, outputDir + "hydro_k"); CHKERRQ(ierr);
  ierr = writeVec(_sN, outputDir + "hydro_sN"); CHKERRQ(ierr);
  ierr = writeVec(_p, outputDir + "hydro_p"); CHKERRQ(ierr);
  ierr = writeVec(_eta_p, outputDir + "hydro_eta"); CHKERRQ(ierr);
  ierr = writeVec(_rho_f, outputDir + "hydro_rho_f"); CHKERRQ(ierr);
  computeFlux();
  ierr = writeVec(_flux, outputDir + "hydro_flux"); CHKERRQ(ierr);
  return ierr;
}


PetscErrorCode PressureEq::writeStep(const PetscInt stepCount, const PetscScalar time)
{
  PetscErrorCode ierr = 0;
  writeStep(stepCount, time, _outputDir);
  return ierr;
}

// write solution at specified time step, called within timeMonitor, which is called in odeSolver (so does not include hydrostatic pressure, need to manually add)
PetscErrorCode PressureEq::writeStep(const PetscInt stepCount, const PetscScalar time, const string outputDir)
{
  PetscErrorCode ierr = 0;
  
  // write MMS analytical solution to file
  if (_D->_isMMS == 1) {
    if (_viewers.empty()) {
      ierr = initiate_appendVecToOutput(_viewers, "pA", _pA, outputDir + "hydro_pA", _D->_outFileMode); CHKERRQ(ierr);
    }
    else {
      ierr = VecView(_pA, _viewers["pA"].first); CHKERRQ(ierr);
    }
  }

  // _viewers will already have size 1 if MMS test is done
  if (_viewers.size() <= 1) {
    ierr = initiate_appendVecToOutput(_viewers, "p", _p, outputDir + "hydro_p",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "p_t", _p_t, outputDir + "hydro_p_t",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "k", _k_p, outputDir + "hydro_k",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "k_slip", _k_slip, outputDir + "hydro_k_slip",_D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers, "k_press", _k_press, outputDir + "hydro_k_press",_D->_outFileMode); CHKERRQ(ierr);
    computeFlux();
    ierr = initiate_appendVecToOutput(_viewers, "flux", _flux, outputDir + "hydro_flux",_D->_outFileMode); CHKERRQ(ierr);
  }
  else {
    ierr = VecView(_p, _viewers["p"].first); CHKERRQ(ierr);
    ierr = VecView(_p_t, _viewers["p_t"].first); CHKERRQ(ierr);
    ierr = VecView(_k_p, _viewers["k"].first); CHKERRQ(ierr);
    ierr = VecView(_k_slip, _viewers["k_slip"].first); CHKERRQ(ierr);
    ierr = VecView(_k_press, _viewers["k_press"].first); CHKERRQ(ierr);
    computeFlux();
    ierr = VecView(_flux, _viewers["flux"].first); CHKERRQ(ierr);
  }
  
  return ierr;
}


// write checkpoint files, called at each checkpoint interval, after writeStep
PetscErrorCode PressureEq::writeCheckpoint()
{
  PetscErrorCode ierr = 0;
    
  ierr = writeVec(_p, _outputDir + "chkpt_p"); CHKERRQ(ierr);
  ierr = writeVec(_k_p, _outputDir + "chkpt_k_p"); CHKERRQ(ierr);
  ierr = writeVec(_k_slip, _outputDir + "chkpt_k_slip"); CHKERRQ(ierr);
  ierr = writeVec(_k_press, _outputDir + "chkpt_k_press"); CHKERRQ(ierr);
  
  return ierr;
}  


// load checkpoint files
PetscErrorCode PressureEq::loadCheckpoint()
{
  PetscErrorCode ierr = 0;

  // load vectors
  loadVecFromInputFile(_p, _outputDir + "chkpt_", "p");
  loadVecFromInputFile(_k_p, _outputDir + "chkpt_", "k_p");
  loadVecFromInputFile(_k_slip, _outputDir + "chkpt_", "k_slip");
  loadVecFromInputFile(_k_press, _outputDir + "chkpt_", "k_press");

  return ierr;
}  


// MMS functions
PetscErrorCode PressureEq::measureMMSError(const double totRunTime)
{
  Vec pA;
  VecDuplicate(_p, &pA);
  mapToVec(pA, zzmms_pA1D, _z, totRunTime);
  double err2pA = computeNormDiff_2(_p, pA);
  PetscPrintf(PETSC_COMM_WORLD, "%i  %3i %.4e %.4e % .15e\n", _D->_order, _D->_Nz, _D->_dr, err2pA, log2(err2pA));
  VecDestroy(&pA);
  return 0;
};

double PressureEq::zzmms_pt1D(const double z, const double t)
{
  PetscScalar PI = 3.14159265359;
  PetscScalar T0 = 9e9;
  PetscScalar delta_p = 50;
  PetscScalar omega = 2 * PI / T0;
  PetscScalar kz = 2.5 * PI / 30.;
  PetscScalar p_t = delta_p * sin(kz * z) * omega * cos(omega * t); // correct
  return p_t;
}

double PressureEq::zzmms_pA1D(const double z, const double t)
{
  PetscScalar PI = 3.14159265359;
  PetscScalar T0 = 9e9;
  PetscScalar delta_p = 50;
  PetscScalar omega = 2.0 * PI / T0;
  PetscScalar kz = 2.5 * PI / 30.;
  PetscScalar p_src = delta_p * sin(kz * z) * sin(omega * t); // correct
  return p_src;
}

double PressureEq::zzmms_pSource1D(const double z, const double t)
{
  PetscScalar PI = 3.14159265359;
  PetscScalar T0 = 9e9;
  PetscScalar delta_p = 50;
  PetscScalar omega = 2. * PI / T0;
  PetscScalar kz = 2.5 * PI / 30.;
  PetscScalar beta0 = 10;
  PetscScalar eta0 = 1e-12;
  PetscScalar n0 = 0.1;
  PetscScalar k0 = 1e-19;
  PetscScalar p_src = delta_p * (beta0 * eta0 * n0 * omega * cos(omega * t) + k0 * kz * kz * sin(omega * t)) * sin(kz * z) / (beta0 * eta0 * n0); // correct
  return p_src;
}
