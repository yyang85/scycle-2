#ifndef STRIKESLIP_LINEARELASTIC_QD_H_INCLUDED
#define STRIKESLIP_LINEARELASTIC_QD_H_INCLUDED

#include <petscksp.h>
#include <string>
#include <cmath>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <map>

#include "integratorContextEx.hpp"
#include "integratorContextImex.hpp"

#include "odeSolver.hpp"
#include "odeSolverImex.hpp"
#include "genFuncs.hpp"
#include "domain.hpp"
#include "sbpOps.hpp"
#include "sbpOps_m_constGrid.hpp"
#include "sbpOps_m_varGrid.hpp"
#include "fault.hpp"
#include "porosityPressure.hpp"
#include "heatEquation.hpp"
#include "linearElastic.hpp"

using namespace std;

/*
 * Mediator-level class for the simulation of earthquake cycles on a vertical strike-slip fault
 *  with linear elastic material properties.
 * Uses the quasi-dynamic approximation.
 */


// StrikeSlip_LinearElastic_qd is a derived class from IntegratorContextEx and IntegratorContextImex
class StrikeSlip_LinearElastic_qd: public IntegratorContextEx, public IntegratorContextImex
{
private:
  // disable default copy constructor and assignment operator
  StrikeSlip_LinearElastic_qd(const StrikeSlip_LinearElastic_qd &that);
  StrikeSlip_LinearElastic_qd& operator=(const StrikeSlip_LinearElastic_qd &rhs);

  Domain *_D;

  // IO information
  string       _delim; // format is: var delim value (without the white space)

  // problem properties
  const bool   _isMMS; // true if running mms test
  string       _inputDir;
  string       _outputDir; // output data
  PetscScalar  _faultTypeScale;

  // time stepping data
  map <string,Vec>  _varEx; // holds variables for explicit integration in time
  map <string,Vec>  _varIm; // holds variables for implicit integration in time
  PetscScalar       _currTime,_minDeltaT,_deltaT;  // recalculated during program
  int               _stepCount; // number of time steps

  // runtime data
  double _integrateTime,_writeTime,_linSolveTime,_factorTime,_startTime,_totalRunTime, _miscTime;

  // viewers
  PetscViewer _timeV1D,_dtimeV1D,_timeV2D,_dtimeV2D;

  // forcing term for ice stream problem
  Vec _forcingTerm, _forcingTermPlain; // body forcing term, and a copy for output

  // set matrix type given boundary conditions
  string  _mat_bcRType,_mat_bcTType,_mat_bcLType,_mat_bcBType;

  // 1st (string) argument is key, second is viewer, and 3rd (string) is output directory
  map <string,pair<PetscViewer,string> >  _viewers;

  // for mapping from body fields to the fault
  VecScatter* _body2fault;

  // private member functions
  PetscErrorCode parseBCs(); // parse boundary conditions
  PetscErrorCode computeMinTimeStep(); // compute min allowed time step as dx / cs
  PetscErrorCode constructIceStreamForcingTerm(); // ice stream forcing term

public:

  OdeSolver        *_quadEx; // explicit time stepping
  OdeSolverImex    *_quadImex; // implicit time stepping

  Fault_qd         *_fault;
  LinearElastic    *_material; // linear elastic off-fault material properties
  HeatEquation     *_he;
  PorosityPressure *_p;

  // constructor and destructor
  StrikeSlip_LinearElastic_qd(Domain&D);
  ~StrikeSlip_LinearElastic_qd();

  // compute initial conditions
  PetscErrorCode solveInit();
  PetscErrorCode solveInitBC();


  // time stepping functions
  PetscErrorCode integrate(); // will call OdeSolver method by same name
  PetscErrorCode initiateIntegrand();
  PetscErrorCode solveMomentumBalance(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx);

  // explicit time-stepping methods
  PetscErrorCode d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx);

  // methods for implicit/explicit time stepping
  PetscErrorCode d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx, map<string,Vec>& varIm,const map<string,Vec>& varImo,const PetscScalar dt);

  // IO functions
  PetscErrorCode view();
  PetscErrorCode writeContext();
  PetscErrorCode timeMonitor(PetscScalar time, PetscScalar deltaT, PetscInt stepCount, int& stopIntegration);
  PetscErrorCode writeStep1D(PetscInt stepCount, PetscScalar time, PetscScalar deltaT, const string outputDir);
  PetscErrorCode writeStep2D(PetscInt stepCount, PetscScalar time, PetscScalar deltaT, const string outputDir);

  // checkpointing functions
  PetscErrorCode testLoading();
  PetscErrorCode loadCheckpoint();
  PetscErrorCode writeCheckpoint();

  // debugging and MMS tests
  PetscErrorCode measureMMSError();
};

#endif
