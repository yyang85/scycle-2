#include "heatEquation.hpp"

#define FILENAME "heatEquation.cpp"

using namespace std;

HeatEquation::HeatEquation(Domain& D)
  : _D(&D),_file(D._file),_delim(D._delim),_order(D._order),_Ny(D._Ny),_Nz(D._Nz),
    _Nz_lab(D._Nz),_Ly(D._Ly),_Lz(D._Lz),_dy(D._dq),_dz(D._dr),_Lz_lab(D._Lz),
    _y(&D._y),_z(&D._z),_heatEquationType("transient"),
    _inputDir(D._inputDir),_outputDir(D._outputDir),
    _kTz_z0(NULL),_kTz(NULL),_maxTemp(0),_maxTempV(NULL),
    _wViscShearHeating("no"),_wFrictionalHeating("no"),_wRadioHeatGen("no"),
    _sbp(NULL),_bcR(NULL),_bcT(NULL),_bcL(NULL),_bcB(NULL),
    _linSolver(D._linSolver),_kspTol(D._kspTol),_kspSS(NULL),_kspTrans(NULL),
    _pcSS(NULL),_pcTrans(NULL),_I(NULL),_rcInv(NULL),_B(NULL),_pcMat(NULL),
    _D2ath(NULL),_MapV(NULL),_Gw(NULL),_w(NULL),_Tamb(NULL),_dT(NULL),_T(NULL),
    _k(NULL),_rho(NULL),_c(NULL),_Qrad(NULL),_Qfric(NULL),_Qvisc(NULL),_Q(NULL),
    _linSolveTime(0),_factorTime(0),_beTime(0),_writeTime(0),_miscTime(0),
    _linSolveCount(0)
{
  loadSettings(_file);
  // more loading to be added depending on which fields need to be saved and restarted

  checkInput();
  allocateFields();
  setFields(); // sets material parameters
  loadFieldsFromFiles();

  if (_D->_isMMS == 0 && _D->_ckptNumber == 0) {
    computeInitialSteadyStateTemp();
  }

  if (_heatEquationType == "transient" ) {
    setUpTransientProblem();
  }
  else if (_heatEquationType == "steadyState" ) {
    setUpSteadyStateProblem();
  }
}


HeatEquation::~HeatEquation()
{
  KSPDestroy(&_kspSS);
  KSPDestroy(&_kspTrans);
  MatDestroy(&_B);
  MatDestroy(&_rcInv);
  MatDestroy(&_I);
  MatDestroy(&_D2ath);
  MatDestroy(&_pcMat);

  MatDestroy(&_MapV);
  VecDestroy(&_Gw);
  VecDestroy(&_w);
  VecDestroy(&_Qrad);
  VecDestroy(&_Qfric);
  VecDestroy(&_Qvisc);
  VecDestroy(&_Q);

  VecDestroy(&_k);
  VecDestroy(&_rho);
  VecDestroy(&_c);

  VecDestroy(&_Tamb);
  VecDestroy(&_dT);
  VecDestroy(&_T);
  VecDestroy(&_kTz);
  VecDestroy(&_kTz_z0);

  // boundary conditions
  VecDestroy(&_bcR);
  VecDestroy(&_bcT);
  VecDestroy(&_bcL);
  VecDestroy(&_bcB);

  for (map<string,pair<PetscViewer,string> >::iterator it=_viewers1D.begin(); it!=_viewers1D.end(); it++ ) {
    PetscViewerDestroy(&_viewers1D[it->first].first);
  }

  for (map<string,pair<PetscViewer,string> >::iterator it=_viewers2D.begin(); it!=_viewers2D.end(); it++ ) {
    PetscViewerDestroy(&_viewers2D[it->first].first);
  }
  
  PetscViewerDestroy(&_maxTempV);

  map<string,VecScatter>::iterator it;
  for (it = _scatters.begin(); it!=_scatters.end(); it++ ) {
    VecScatterDestroy(&it->second);
  }

  delete _sbp;
  _sbp = NULL;
}


// return temperature
PetscErrorCode HeatEquation::getTemp(Vec& T)
{
  PetscErrorCode ierr = 0;
  VecCopy(_T,T);
  return ierr;
}


// set temperature
PetscErrorCode HeatEquation::setTemp(const Vec& T)
{
  PetscErrorCode ierr = 0;
  VecCopy(T,_T);
  VecWAXPY(_dT,-1.0,_Tamb,_T);
  return ierr;
}


// loads settings from the input text file
PetscErrorCode HeatEquation::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;
  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line))
    {
      istringstream iss(line);
      pos = line.find(_delim); // find position of the delimiter
      var = line.substr(0,pos);
      rhs = "";
      if (line.length() > (pos + _delim.length())) {
	rhs = line.substr(pos+_delim.length(),line.npos);
      }
      rhsFull = rhs; // everything after _delim

      // interpret everything after the appearance of a space on the line as a comment
      pos = rhs.find(" ");
      rhs = rhs.substr(0,pos);

      if (var == "heatEquationType") { _heatEquationType = rhs.c_str(); }
      else if (var == "withViscShearHeating") { _wViscShearHeating = rhs.c_str(); }
      else if (var == "withFrictionalHeating") { _wFrictionalHeating = rhs.c_str();}
      else if (var == "withRadioHeatGeneration") { _wRadioHeatGen = rhs.c_str(); }

      // if values are set by vector
      else if (var == "rhoVals") { loadVectorFromInputFile(rhsFull,_rhoVals); }
      else if (var == "rhoDepths") { loadVectorFromInputFile(rhsFull,_rhoDepths); }
      else if (var == "kVals") { loadVectorFromInputFile(rhsFull,_kVals); }
      else if (var == "kDepths") { loadVectorFromInputFile(rhsFull,_kDepths); }
      else if (var == "cVals") { loadVectorFromInputFile(rhsFull,_cVals); }
      else if (var == "cDepths") { loadVectorFromInputFile(rhsFull,_cDepths); }

      else if (var == "Nz_lab") { _Nz_lab = atoi( rhs.c_str() ); }
      else if (var == "TVals") { // TVals = [T0 T_lab TN] || [T0 TN]
	loadVectorFromInputFile(rhsFull,_TVals);
      }
      else if (var == "TDepths") {
	loadVectorFromInputFile(rhsFull,_TDepths);
	_Lz_lab = _TDepths[1];
      }

      // finite width shear zone
      else if (var == "wVals") { loadVectorFromInputFile(rhsFull,_wVals); }
      else if (var == "wDepths") { loadVectorFromInputFile(rhsFull,_wDepths); }

      // radioactive heat generation
      else if (var == "he_A0Vals") { loadVectorFromInputFile(rhsFull,_A0Vals); }
      else if (var == "he_A0Depths") { loadVectorFromInputFile(rhsFull,_A0Depths); }
      else if (var == "he_Lrad") { _Lrad = atof( rhs.c_str() ); }
    }

  return ierr;
}


//parse input file and load values into data members
PetscErrorCode HeatEquation::loadFieldsFromFiles()
{
  PetscErrorCode ierr = 0;
  // material properties
  ierr = loadVecFromInputFile(_rho,_inputDir,"rho"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_k,_inputDir,"k"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_Qrad,_inputDir,"Qrad"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(_c,_inputDir,"c"); CHKERRQ(ierr);

  bool chkTamb = 0, chkT = 0, chkdT = 0;

  // load Tamb (background geotherm)
  loadVecFromInputFile(_Tamb,_inputDir,"Tamb",chkTamb);

  // load T
  loadVecFromInputFile(_T,_inputDir,"T",chkT);

  // if Tamb was loaded and T wasn't, copy Tamb into T
  if (chkT!=1 && chkTamb) { VecCopy(_Tamb,_T); }

  // load dT (perturbation from ambient geotherm)
  loadVecFromInputFile(_dT,_inputDir,"dT",chkdT);

  // dT wasn't loaded, compute it from T and Tamb
  if (chkdT!=1 && chkTamb) {
    VecWAXPY(_dT,-1.0,_Tamb,_T);
  }

  return ierr;
}


// allocate memory and parallel data layout for fields
PetscErrorCode HeatEquation::allocateFields()
{
  PetscErrorCode ierr = 0;

  // allocate boundary conditions
  VecDuplicate(_D->_z0,&_bcT); VecSet(_bcT,0.);
  VecDuplicate(_D->_z0,&_bcB); VecSet(_bcB,0.);
  VecDuplicate(_D->_y0,&_bcR); VecSet(_bcR,0.0);
  VecDuplicate(_D->_y0,&_bcL); VecSet(_bcL,0.0);

  VecDuplicate(_bcT,&_kTz_z0); VecSet(_kTz_z0,0.0); // heat flux

  VecDuplicate(*_y,&_k);       VecSet(_k,0.0); // conductivity
  VecDuplicate(_k,&_rho);      VecSet(_rho,0.); // density
  VecDuplicate(_k,&_c);        VecSet(_c,0.); // heat capacity
  VecDuplicate(_k,&_Q);        VecSet(_Q,0.);
  VecDuplicate(_k,&_Qrad);     VecSet(_Qrad,0.);
  VecDuplicate(_k,&_Qfric);    VecSet(_Qfric,0.);
  VecDuplicate(_k,&_Qvisc);    VecSet(_Qvisc,0.);
  VecDuplicate(_k,&_kTz);      VecSet(_kTz,0.0);
  VecDuplicate(_k,&_T);        VecSet(_T,0.);
  VecDuplicate(_k,&_Tamb);     VecSet(_Tamb,0.);
  VecDuplicate(_k,&_dT);       VecSet(_dT,0.);

  // create scatter from body field to top boundary
  // indices to scatter from
  IS isf;
  ierr = ISCreateStride(PETSC_COMM_WORLD, _Ny, 0, _Nz, &isf); CHKERRQ(ierr);

  // indices to scatter to
  PetscInt *ti;
  ierr = PetscMalloc1(_Ny,&ti); CHKERRQ(ierr);
  for (PetscInt Ii=0; Ii<(_Ny); Ii++) { ti[Ii] = Ii; }
  IS ist;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Ny, ti, PETSC_COPY_VALUES, &ist); CHKERRQ(ierr);
  PetscFree(ti);

  // create scatter
  ierr = VecScatterCreate(*_y, isf, _bcT, ist, &_scatters["body2T"]); CHKERRQ(ierr);
  ISDestroy(&isf);
  ISDestroy(&ist);

  return ierr;
}


// initialize values for all fields
PetscErrorCode HeatEquation::setFields()
{
  PetscErrorCode ierr = 0;

  // set each field using it's vals and depths vectors
  if (_D->_isMMS) {
    mapToVec(_k,zzmms_k,*_y,*_z);
    mapToVec(_rho,zzmms_rho,*_y,*_z);
    mapToVec(_c,zzmms_c,*_y,*_z);
    mapToVec(_Qrad,zzmms_h,*_y,*_z);
    mapToVec(_Tamb,zzmms_T,*_y,*_z,_D->_initTime);
    mapToVec(_dT,zzmms_dT,*_y,*_z,_D->_initTime);
    setMMSBoundaryConditions(_D->_initTime,"Dirichlet","Dirichlet","Dirichlet","Dirichlet");
  }
  else {
    ierr = setVec(_k,*_z,_kVals,_kDepths); CHKERRQ(ierr);
    ierr = setVec(_rho,*_z,_rhoVals,_rhoDepths); CHKERRQ(ierr);
    ierr = setVec(_c,*_z,_cVals,_cDepths); CHKERRQ(ierr);
    ierr = setVec(_T,*_z,_TVals,_TDepths); CHKERRQ(ierr);
    ierr = setVec(_Tamb,*_z,_TVals,_TDepths); CHKERRQ(ierr);
  }

  if (_wFrictionalHeating == "yes") { constructMapV(); }

  // set up radioactive heat generation source term
  // Qrad = A0 * exp(-z/Lrad)
  if (_wRadioHeatGen == "yes") {
    Vec A0; VecDuplicate(_Qrad,&A0);
    ierr = setVec(A0,*_z,_A0Vals,_A0Depths); CHKERRQ(ierr);
    VecCopy(*_z,_Qrad);
    VecScale(_Qrad,-1.0/_Lrad);
    VecExp(_Qrad);
    VecPointwiseMult(_Qrad,A0,_Qrad);
    VecDestroy(&A0);
  }
  else {
    VecSet(_Qrad,0.);
  }

  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode HeatEquation::checkInput()
{
  PetscErrorCode ierr = 0;

  assert(_heatEquationType == "transient" ||
	 _heatEquationType == "steadyState" );

  assert(_kVals.size() == _kDepths.size() );
  assert(_rhoVals.size() == _rhoDepths.size() );
  assert(_cVals.size() == _cDepths.size() );
  assert(_TVals.size() == _TDepths.size() );
  assert(_wVals.size() == _wDepths.size() );
  assert(_TVals.size() >= 2 && _TVals.size() <= 4);
  assert(_Nz_lab <= _Nz);
  assert(_Lz_lab <= _Lz);

  if (_wRadioHeatGen == "yes") {
    assert(_A0Vals.size() == _A0Depths.size() );
    if (_A0Vals.size() == 0) {
      _A0Vals.push_back(0); _A0Depths.push_back(0);
    }
  }

  return ierr;
}


// create scatters for communication from full domain Vecs to lithosphere only Vecs
// SCATTER_FORWARD is from full domain -> lithosphere
PetscErrorCode HeatEquation::constructScatters(Vec& T, Vec& T_l)
{
  PetscErrorCode ierr = 0;

  // create scatter from 2D full domain to 2D lithosphere only
  // indices to scatter from
  PetscInt *fi;
  ierr = PetscMalloc1(_Ny*_Nz_lab,&fi); CHKERRQ(ierr);
  PetscInt count = 0;

  for (PetscInt Ii=0; Ii<_Ny; Ii++) {
    for (PetscInt Jj=0; Jj<_Nz_lab; Jj++) {
      fi[count] = Ii*_Nz + Jj;
      count++;
    }
  }

  IS isf;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Ny*_Nz_lab, fi, PETSC_COPY_VALUES, &isf); CHKERRQ(ierr);
  PetscFree(fi);

  // indices to scatter to
  PetscInt *ti;
  ierr = PetscMalloc1(_Ny*_Nz_lab,&ti); CHKERRQ(ierr);
  for (PetscInt Ii=0; Ii<(_Ny*_Nz_lab); Ii++) {
    ti[Ii] = Ii;
  }

  IS ist;
  ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Ny*_Nz_lab, ti, PETSC_COPY_VALUES, &ist); CHKERRQ(ierr);
  PetscFree(ti);

  // create scatter
  ierr = VecScatterCreate(_T, isf, T_l, ist, &_scatters["bodyFull2bodyLith"]); CHKERRQ(ierr);
  // free memory
  ISDestroy(&isf);
  ISDestroy(&ist);

  return ierr;
}


// create matrix to map slip velocity, which lives on the fault, to a 2D body field
PetscErrorCode HeatEquation::constructMapV()
{
  PetscErrorCode ierr = 0;

  MatCreate(PETSC_COMM_WORLD,&_MapV);
  MatSetSizes(_MapV,PETSC_DECIDE,PETSC_DECIDE,_Ny*_Nz,_Nz);
  MatSetFromOptions(_MapV);
  MatMPIAIJSetPreallocation(_MapV,1,NULL,1,NULL);
  MatSeqAIJSetPreallocation(_MapV,1,NULL);
  MatSetUp(_MapV);

  PetscScalar v=1.0;
  PetscInt Ii=0,Istart=0,Iend=0,Jj=0;
  MatGetOwnershipRange(_MapV,&Istart,&Iend);
  for (Ii = Istart; Ii < Iend; Ii++) {
    Jj = Ii % _Nz;
    MatSetValues(_MapV,1,&Ii,1,&Jj,&v,INSERT_VALUES);
  }

  MatAssemblyBegin(_MapV,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(_MapV,MAT_FINAL_ASSEMBLY);

  // construct Gw = exp(-y^2/(2*w)) / sqrt(2*pi)/w
  VecDuplicate(_k,&_Gw); VecSet(_Gw,0.);
  VecDuplicate(_k,&_w);

  if (_wVals.size() > 0 ) {
    ierr = setVec(_w,*_z,_wVals,_wDepths); CHKERRQ(ierr);
    VecScale(_w,1e-3); // convert from m to km
  }
  else { VecSet(_w,0.); }
  VecMax(_w,NULL,&_wMax);

  if (_wVals.size() > 0 ) {
    PetscScalar const *y,*w;
    PetscScalar *g;
    VecGetOwnershipRange(_Gw,&Istart,&Iend);
    VecGetArrayRead(*_y,&y);
    VecGetArrayRead(_w,&w);
    VecGetArray(_Gw,&g);
    Jj = 0;
    for (Ii=Istart;Ii<Iend;Ii++) {
      g[Jj] = exp(-y[Jj]*y[Jj] / (2.*w[Jj]*w[Jj])) / sqrt(2. * M_PI) / w[Jj];
      assert(!std::isnan(g[Jj]));
      assert(!std::isinf(g[Jj]));
      Jj++;
    }
    VecRestoreArrayRead(*_y,&y);
    VecRestoreArrayRead(_w,&w);
    VecRestoreArray(_Gw,&g);
  }

  return ierr;
}


// compute 1D steady-state geotherm in thelithosphere, optionally
// including radioactive decay as a heat source
PetscErrorCode HeatEquation::computeInitialSteadyStateTemp()
{
  PetscErrorCode ierr = 0;

  // no need for linear solve step if Nz == 1
  if (_Nz == 1) {
    VecSet(_Tamb,_TVals[0]);
    VecSet(_T,_TVals[0]);
    VecSet(_dT,0.0);
    return 0;
  }

  // otherwise:
  // boundary conditions
  Vec bcT,bcB;
  ierr = VecCreate(PETSC_COMM_WORLD,&bcT); CHKERRQ(ierr);
  ierr = VecSetSizes(bcT,PETSC_DECIDE,_Ny); CHKERRQ(ierr);
  ierr = VecSetFromOptions(bcT); CHKERRQ(ierr);
  VecDuplicate(bcT,&bcB);
  PetscScalar bcTval = (_TVals[1] - _TVals[0])/(_TDepths[1]-_TDepths[0]) * (0-_TDepths[0]) + _TVals[0];
  VecSet(bcT,bcTval);
  PetscScalar bcBval = (_TVals[1] - _TVals[0])/(_TDepths[1]-_TDepths[0]) * (_Lz_lab-_TDepths[0]) + _TVals[0];
  VecSet(bcB,bcBval);

  // fields that live only in the lithosphere
  Vec y,z,k,Qrad,Tamb_l;
  ierr = VecCreate(PETSC_COMM_WORLD,&y); CHKERRQ(ierr);
  ierr = VecSetSizes(y,PETSC_DECIDE,_Ny*_Nz_lab); CHKERRQ(ierr);
  ierr = VecSetFromOptions(y); CHKERRQ(ierr);
  VecSet(y,0.0);
  VecDuplicate(y,&z); VecSet(z,0.0);
  VecDuplicate(y,&k); VecSet(k,0.0);
  VecDuplicate(y,&Qrad); VecSet(Qrad,0.0);
  VecDuplicate(y,&Tamb_l); VecSet(Tamb_l,0.0);

  constructScatters(_T,Tamb_l);

  VecScatterBegin(_scatters["bodyFull2bodyLith"], *_y, y, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_scatters["bodyFull2bodyLith"], *_y, y, INSERT_VALUES, SCATTER_FORWARD);

  VecScatterBegin(_scatters["bodyFull2bodyLith"], *_z, z, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_scatters["bodyFull2bodyLith"], *_z, z, INSERT_VALUES, SCATTER_FORWARD);

  VecScatterBegin(_scatters["bodyFull2bodyLith"], _k, k, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_scatters["bodyFull2bodyLith"], _k, k, INSERT_VALUES, SCATTER_FORWARD);

  VecScatterBegin(_scatters["bodyFull2bodyLith"], _Qrad, Qrad, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_scatters["bodyFull2bodyLith"], _Qrad, Qrad, INSERT_VALUES, SCATTER_FORWARD);

  // create SBP operators, 1D in z-direction only, only in lithosphere
  SbpOps* sbp;
  if (_D->_zGridSpacingType == "constantGridSpacing") {
    sbp = new SbpOps_m_constGrid(_order,_Ny,_Nz_lab,_Ly,_Lz,k);
  }
  else if (_D->_zGridSpacingType == "variableGridSpacing") {
    sbp = new SbpOps_m_varGrid(_order,_Ny,_Nz_lab,_Ly,_Lz,k);
    if (_Ny > 1 && _Nz_lab > 1) { sbp->setGrid(&y,&z); }
    else if (_Ny == 1 && _Nz_lab > 1) { sbp->setGrid(NULL,&z); }
    else if (_Ny > 1 && _Nz_lab == 1) { sbp->setGrid(&y,NULL); }
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }

  sbp->setCompatibilityType(_D->_sbpCompatibilityType);
  sbp->setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
  sbp->setMultiplyByH(1);
  sbp->setLaplaceType("z");
  sbp->setDeleteIntermediateFields(1);
  sbp->computeMatrices(); // actually create the matrices

  // radioactive heat generation source term
  // Vec QradR,Qtemp;
  Vec Qtemp;
  if (_wRadioHeatGen == "yes") {
    VecDuplicate(Qrad,&Qtemp);
    if (_D->_zGridSpacingType == "variableGridSpacing") {
      Vec temp1; VecDuplicate(Qrad,&temp1);
      Mat J,Jinv,qy,rz,yq,zr;
      ierr = sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
      ierr = MatMult(J,Qrad,temp1);
      Mat H; sbp->getH(H);
      ierr = MatMult(H,temp1,Qtemp);
      VecDestroy(&temp1);
    }
    else{
      Mat H; sbp->getH(H);
      ierr = MatMult(H,Qrad,Qtemp); CHKERRQ(ierr);
    }
  }

  Mat A; sbp->getA(A);
  setupKSP(_kspSS, _pcSS, A); // set up KSP for steady-state problem

  Vec rhs; VecDuplicate(k,&rhs); VecSet(rhs,0.);
  sbp->setRhs(rhs,_bcL,_bcR,bcT,bcB);
  if (_wRadioHeatGen == "yes") {
    VecAXPY(rhs,-1.0,Qtemp);
    VecDestroy(&Qtemp);
  }

  // solve for ambient temperature in the lithosphere
  double startTime = MPI_Wtime();
  ierr = KSPSolve(_kspSS,rhs,Tamb_l);CHKERRQ(ierr);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;

  // scatter Tamb_l to Tamb and T
  VecScatterBegin(_scatters["bodyFull2bodyLith"], Tamb_l,_Tamb, INSERT_VALUES, SCATTER_REVERSE);
  VecScatterEnd(_scatters["bodyFull2bodyLith"], Tamb_l,_Tamb, INSERT_VALUES, SCATTER_REVERSE);

  KSPDestroy(&_kspSS); _kspSS = NULL;
  delete sbp; sbp = NULL;
  VecDestroy(&y);
  VecDestroy(&z);
  VecDestroy(&k);
  VecDestroy(&Qrad);
  VecDestroy(&Tamb_l);
  VecDestroy(&bcT);
  VecDestroy(&bcB);

  // now overwrite Tamb(z>=LAB) with mantle adiabat
  if (_Nz_lab < _Nz && _Lz_lab < _Lz && _TVals.size() > 3) {
    int len = _TVals.size();
    PetscScalar a = (_TVals[len-1] - _TVals[len-2])/(_TDepths[len-1]-_TDepths[len-2]); // adiabat slope
    PetscScalar const *zz;
    PetscScalar *Tamb;
    PetscInt Ii,Istart,Iend;
    VecGetOwnershipRange(_T,&Istart,&Iend);
    VecGetArrayRead(*_z,&zz);
    VecGetArray(_Tamb,&Tamb);
    PetscInt Jj = 0;
    for (Ii=Istart;Ii<Iend;Ii++) {
      if (zz[Jj] >= _Lz_lab) { Tamb[Jj] = a * (zz[Jj]-_TDepths[len-2]) + _TVals[len-2]; }
      Jj++;
    }
    VecRestoreArrayRead(*_z,&zz);
    VecRestoreArray(_Tamb,&Tamb);
  }

  // update _T, _dT
  VecSet(_dT,0.0);
  VecCopy(_Tamb,_T);

  return ierr;
}


// set up linear solver context
PetscErrorCode HeatEquation::setupKSP(KSP &ksp, PC &pc, Mat& A)
{
  PetscErrorCode ierr = 0;

  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);
  if (_linSolver == "AMG") { // algebraic multigrid from HYPRE
    // uses HYPRE's solver AMG (not HYPRE's preconditioners)
    ierr = KSPSetType(ksp,KSPRICHARDSON); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCHYPRESetType(pc,"boomeramg"); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCFactorSetLevels(pc,4); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }
  else if (_linSolver == "MUMPSLU") { // direct LU from MUMPS
    // use direct LU from MUMPS
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);                 CHKERRQ(ierr);
    ierr = PCFactorSetUpMatSolverType(pc);                              CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }
  else if (_linSolver == "MUMPSCHOLESKY") { // direct Cholesky (RR^T) from MUMPS
    // use direct LL^T (Cholesky factorization) from MUMPS
    ierr = KSPSetType(ksp,KSPPREONLY); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_TRUE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCCHOLESKY); CHKERRQ(ierr);
    ierr = PCFactorSetMatSolverType(pc,MATSOLVERMUMPS);                 CHKERRQ(ierr);
    ierr = PCFactorSetUpMatSolverType(pc);                              CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }
  else if (_linSolver == "CG") { // conjugate gradient
    ierr = KSPSetType(ksp,KSPCG); CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,A,A); CHKERRQ(ierr);
    ierr = KSPSetReusePreconditioner(ksp,PETSC_FALSE); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCHYPRE); CHKERRQ(ierr);
    ierr = PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
    ierr = KSPSetInitialGuessNonzero(ksp,PETSC_TRUE); CHKERRQ(ierr);
  }
  else {
    ierr = PetscPrintf(PETSC_COMM_WORLD,"ERROR: linSolver type not understood\n");
    assert(0);
  }

  // accept command line options
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

  // perform computation of preconditioners now, rather than on first use
  double startTime = MPI_Wtime();
  ierr = KSPSetUp(ksp);CHKERRQ(ierr);
  _factorTime += MPI_Wtime() - startTime;

  return ierr;
}


PetscErrorCode HeatEquation::initiateIntegrand(const PetscScalar time,map<string,Vec>& varEx,map<string,Vec>& varIm)
{
  PetscErrorCode ierr = 0;

  // put variables to be integrated implicity into varIm
  Vec T;
  VecDuplicate(_Tamb,&T);
  VecCopy(_T,T);
  varIm["Temp"] = T;

  return ierr;
}


PetscErrorCode HeatEquation::updateFields(const PetscScalar time,const map<string,Vec>& varEx,const map<string,Vec>& varIm)
{
  PetscErrorCode ierr = 0;

  // not needed for implicit solve

  return ierr;
}


PetscErrorCode HeatEquation::setMMSBoundaryConditions(const double time, string bcRType, string bcTType, string bcLType, string bcBType)
{
  PetscErrorCode ierr = 0;

  // set up boundary conditions: L and R
  PetscScalar y,z,v;
  PetscInt Ii,Istart,Iend;
  ierr = VecGetOwnershipRange(_bcL,&Istart,&Iend);CHKERRQ(ierr);
  for(Ii=Istart;Ii<Iend;Ii++) {
    ierr = VecGetValues(*_z,1,&Ii,&z);CHKERRQ(ierr);
    y = 0;
    if (bcLType == "Dirichlet") { v = zzmms_T(y,z,time); }
    else if (bcLType == "Neumann") { v = zzmms_k(y,z)*zzmms_T_y(y,z,time); }
    ierr = VecSetValues(_bcL,1,&Ii,&v,INSERT_VALUES);CHKERRQ(ierr);

    y = _Ly;
    if (bcRType == "Dirichlet") { v = zzmms_T(y,z,time); }
    else if (bcRType == "Neumann") { v = zzmms_k(y,z)*zzmms_T_y(y,z,time); }
    ierr = VecSetValues(_bcR,1,&Ii,&v,INSERT_VALUES);CHKERRQ(ierr);
  }

  ierr = VecAssemblyBegin(_bcL);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(_bcR);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcL);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcR);CHKERRQ(ierr);

  // set up boundary conditions: T and B
  ierr = VecGetOwnershipRange(*_y,&Istart,&Iend);CHKERRQ(ierr);
  for(Ii=Istart;Ii<Iend;Ii++) {
    if (Ii % _Nz == 0) {
      ierr = VecGetValues(*_y,1,&Ii,&y);CHKERRQ(ierr);
      PetscInt Jj = Ii / _Nz;

      z = 0;
      if (bcTType == "Dirichlet") { v = zzmms_T(y,z,time); }
      else if (bcTType == "Neumann") {
        v = zzmms_k(y,z)*zzmms_T_z(y,z,time);
      }
      ierr = VecSetValues(_bcT,1,&Jj,&v,INSERT_VALUES);CHKERRQ(ierr);

      z = _Lz;
      if (bcBType == "Dirichlet") { v = zzmms_T(y,z,time); }
      else if (bcBType == "Neumann") {
	v = zzmms_k(y,z)*zzmms_T_z(y,z,time);
      }
      ierr = VecSetValues(_bcB,1,&Jj,&v,INSERT_VALUES);CHKERRQ(ierr);
    }
  }

  ierr = VecAssemblyBegin(_bcT);CHKERRQ(ierr);
  ierr = VecAssemblyBegin(_bcB);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcT);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(_bcB);CHKERRQ(ierr);

  return ierr;
}


PetscErrorCode HeatEquation::measureMMSError(const PetscScalar time)
{
  PetscErrorCode ierr = 0;

  // measure error between analytical and numerical solution
  Vec dTA;
  VecDuplicate(_dT,&dTA);
  mapToVec(dTA,zzmms_T,*_y,*_z,time);
  writeVec(dTA,_outputDir+"mms_dTA");
  writeVec(_dT,_outputDir+"mms_dT");
  writeVec(_bcL,_outputDir+"mms_he_bcL");
  writeVec(_bcR,_outputDir+"mms_he_bcR");
  writeVec(_bcT,_outputDir+"mms_he_bcT");
  writeVec(_bcB,_outputDir+"mms_he_bcB");

  double err2u = computeNormDiff_2(_dT,dTA);

  PetscPrintf(PETSC_COMM_WORLD,"%i %3i %.4e %.4e % .15e\n",
              _order,_Ny,_dy,err2u,log2(err2u));

  VecDestroy(&dTA);
  return ierr;
}


// for thermomechanical coupling with explicit time stepping
// Note: This actually returns d/dt (T - Tamb), where Tamb is the 1D steady-state geotherm
PetscErrorCode HeatEquation::d_dt(const PetscScalar time,const Vec slipVel,const Vec& tau,const Vec& sdev, const Vec& dgxy, const Vec& dgxz, const Vec& T, Vec& dTdt)
{
  PetscErrorCode ierr = 0;
  // update fields
  VecCopy(T,_T);
  VecWAXPY(_dT,-1.0,_Tamb,T);

  // set up boundary conditions and source terms

  // compute source term: Q = Qrad + Qfric + Qvisc
  // radioactive heat generation Qrad
  VecSet(_Q,0.0);

  // frictional heat generation: Qfric or bcL depending on shear zone width
  if (_wFrictionalHeating == "yes") {
    computeFrictionalShearHeating(tau,slipVel);
    VecAXPY(_Q,1.0,_Qfric);
  }

  // viscous shear heating: Qvisc
  if (_wViscShearHeating == "yes" && dgxy!=NULL && dgxz!=NULL && sdev!=NULL) {
    computeViscousShearHeating(sdev, dgxy, dgxz);
    VecAXPY(_Q,1.0,_Qvisc);
  }

  // rhs = -H*J*(SAT bc terms) + H*J*Q
  Vec rhs;
  VecDuplicate(_k,&rhs);
  VecSet(rhs,0.0);
  _sbp->setRhs(rhs,_bcL,_bcR,_bcT,_bcB); // put SAT terms in temp
  VecScale(rhs,-1.); // sign convention in setRhs is opposite of what's needed for explicit time stepping

  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Vec temp1; VecDuplicate(_Q,&temp1);
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_Q,temp1);
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,temp1,rhs,rhs); CHKERRQ(ierr); // rhs = H*temp1 + temp
    VecDestroy(&temp1);
  }
  else {
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,_Q,rhs,rhs); CHKERRQ(ierr); // rhs = H*temp1 + temp
  }

  // add H*J*D2 * dTn
  Mat A; _sbp->getA(A);
  MatMultAdd(A,_dT,rhs,rhs); // rhs = A*dTn + rhs

  // dT = 1/(rho*c) * Hinv *Jinv * rhs
  VecPointwiseDivide(rhs,rhs,_rho);
  VecPointwiseDivide(rhs,rhs,_c);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Vec temp1; VecDuplicate(_Q,&temp1);
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(Jinv,rhs,temp1);
    _sbp->Hinv(temp1,dTdt);
    VecDestroy(&temp1);
  }
  else {
    _sbp->Hinv(rhs,dTdt);
  }

  VecDestroy(&rhs);

  computeHeatFlux();

  return ierr;
}


// MMS test for thermomechanical coupling with explicity time stepping
PetscErrorCode HeatEquation::d_dt_mms(const PetscScalar time,const Vec& T, Vec& dTdt)
{
  PetscErrorCode ierr = 0;

  VecCopy(T,_dT); // so that the correct temperature is written out

  // update boundary conditions
  ierr = setMMSBoundaryConditions(time,"Dirichlet","Dirichlet","Neumann","Dirichlet"); CHKERRQ(ierr);

  Mat A;
  _sbp->getA(A);
  ierr = MatMult(A,T,dTdt); CHKERRQ(ierr);
  Vec rhs;
  VecDuplicate(T,&rhs);
  ierr = _sbp->setRhs(rhs,_bcL,_bcR,_bcT,_bcB);CHKERRQ(ierr);
  ierr = VecAXPY(dTdt,-1.0,rhs);CHKERRQ(ierr);
  VecDestroy(&rhs);

  Vec temp;
  VecDuplicate(dTdt,&temp);
  _sbp->Hinv(dTdt,temp);
  VecCopy(temp,dTdt);
  VecDestroy(&temp);

  VecPointwiseDivide(dTdt,dTdt,_rho);
  VecPointwiseDivide(dTdt,dTdt,_c);

  return ierr;
}


// for thermomechanical coupling using backward Euler (implicit time stepping)
PetscErrorCode HeatEquation::be(const PetscScalar time,const Vec slipVel,const Vec& tau, const Vec& sdev, const Vec& dgxy,const Vec& dgxz,Vec& T,const Vec& To,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  double beStartTime = MPI_Wtime();

  if (_D->_isMMS == 1 && _heatEquationType == "transient") {
    assert(0);
  }
  else if (_D->_isMMS == 1 && _heatEquationType == "steadyState") {
    be_steadyStateMMS(time,slipVel,tau,sdev,dgxy,dgxz,T,To,dt);
  }
  else if (_D->_isMMS == 0 && _heatEquationType == "transient") {
    be_transient(time,slipVel,tau,sdev,dgxy,dgxz,T,To,dt);
  }
  else if (_D->_isMMS == 0 && _heatEquationType == "steadyState") {
    be_steadyState(time,slipVel,tau,sdev,dgxy,dgxz,T,To,dt);
  }

  computeHeatFlux();

  _beTime += MPI_Wtime() - beStartTime;

  return ierr;
}


// for thermomechanical problem using implicit time stepping (backward Euler)
// Note: This function uses the KSP algorithm to solve for dT, where T = Tamb + dT
PetscErrorCode HeatEquation::be_transient(const PetscScalar time,const Vec slipVel,const Vec& tau, const Vec& sdev, const Vec& dgxy,const Vec& dgxz,Vec& T,const Vec& Tn,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // update fields
  VecCopy(Tn,_T);

  // set up matrix
  MatCopy(_D2ath,_B,SAME_NONZERO_PATTERN);
  MatScale(_B,-dt);
  MatAXPY(_B,1.0,_I,SUBSET_NONZERO_PATTERN);
  if (_kspTrans == NULL) {
    KSPDestroy(&_kspSS);
    setupKSP(_kspTrans,_pcTrans,_B);
  }
  ierr = KSPSetOperators(_kspTrans,_B,_B);CHKERRQ(ierr);

  // set up boundary conditions and source terms: Q = Qfric + Qvisc
  // Note: there is no Qrad because radioactive heat generation is already included in Tamb
  Vec rhs,temp;
  VecDuplicate(_k,&rhs);
  VecDuplicate(_k,&temp);
  VecSet(rhs,0.0);
  VecSet(temp,0.0);
  VecSet(_Q,0.); // radioactive heat generation is already included in Tamb

  // frictional heat generation: Qfric or bcL depending on shear zone width
  if (_wFrictionalHeating == "yes") {
    computeFrictionalShearHeating(tau,slipVel);
    VecAXPY(_Q,1.0,_Qfric);
  }

  // viscous shear heating: Qvisc
  if (_wViscShearHeating == "yes"
      && dgxy!=NULL && dgxz!=NULL && sdev!=NULL) {
    computeViscousShearHeating(sdev, dgxy, dgxz);
    VecAXPY(_Q,1.0,_Qvisc);
  }

  ierr = _sbp->setRhs(temp,_bcL,_bcR,_bcT,_bcB);CHKERRQ(ierr);
  Vec temp1; VecDuplicate(_Q,&temp1);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_Q,temp1);
  }

  Mat H; _sbp->getH(H);
  ierr = MatMultAdd(H,temp1,temp,temp); CHKERRQ(ierr);
  VecDestroy(&temp1);
  MatMult(_rcInv,temp,rhs);
  VecScale(rhs,dt);

  // solve in terms of dT
  // add H * dTn to rhs
  VecSet(temp,0.0);
  VecWAXPY(_dT,-1.0,_Tamb,Tn); // dTn = Tn - Tamb
  _sbp->H(_dT,temp);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    Vec temp1; VecDuplicate(temp,&temp1);
    MatMult(J,temp,temp1);
    VecCopy(temp1,temp);
    VecDestroy(&temp1);
  }
  VecAXPY(rhs,1.0,temp);
  VecDestroy(&temp);

  // solve for temperature and record run time required
  double startTime = MPI_Wtime();
  KSPSolve(_kspTrans,rhs,_dT);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;
  VecDestroy(&rhs);

  // update total temperature: _T (internal variable) and T (output)
  VecWAXPY(_T,1.0,_Tamb,_dT); // T = dT + Tamb
  VecCopy(_T,T);
  computeHeatFlux();

  return ierr;
}


// for thermomechanical problem when solving only the steady-state heat equation
// Note: This function uses the KSP algorithm to solve for dT, where T = Tamb + dT
PetscErrorCode HeatEquation::be_steadyState(const PetscScalar time,const Vec slipVel,const Vec& tau,const Vec& sdev, const Vec& dgxy,const Vec& dgxz,Vec& T,const Vec& Tn,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // update fields
  VecCopy(Tn,_T);

  if (_kspSS == NULL) {
    KSPDestroy(&_kspTrans);
    Mat A; _sbp->getA(A);
    setupKSP(_kspSS,_pcSS,A);
  }

  // set up boundary conditions and source terms: Q = Qrad + Qfric + Qvisc
  Vec rhs; VecDuplicate(_k,&rhs); VecSet(rhs,0.0);

  // compute heat source terms
  // Note: this does not include Qrad because that is included in the ambient geotherm
  VecSet(_Q,0.);

  // frictional heat generation: Qfric or bcL depending on shear zone width
  if (_wFrictionalHeating == "yes") {
    // set bcL and/or Qfric depending on shear zone width
    computeFrictionalShearHeating(tau,slipVel);
    VecAXPY(_Q,-1.0,_Qfric);
  }

  // viscous shear heating: Qvisc
  if (_wViscShearHeating == "yes"
      && dgxy!=NULL && dgxz!=NULL && sdev!=NULL) {
    computeViscousShearHeating(sdev, dgxy, dgxz);
    VecAXPY(_Q,-1.0,_Qvisc);
  }

  // rhs = J*H*Q + (SAT BC terms)
  ierr = _sbp->setRhs(rhs,_bcL,_bcR,_bcT,_bcB);CHKERRQ(ierr);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Vec temp1; VecDuplicate(_Q,&temp1);
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_Q,temp1);
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,temp1,rhs,rhs);
    VecDestroy(&temp1);
  }
  else{
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,_Q,rhs,rhs); CHKERRQ(ierr);
  }

  // solve for dT and record run time required
  VecWAXPY(_dT,-1.0,_Tamb,Tn); // dT = Tn - Tamb
  double startTime = MPI_Wtime();
  KSPSolve(_kspSS,rhs,_dT);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;
  VecDestroy(&rhs);

  // update total temperature: _T (internal variable) and T (output)
  VecWAXPY(_T,1.0,_Tamb,_dT);
  VecCopy(_T,T);

  computeHeatFlux();

  return ierr;
}


// for thermomechanical coupling solving only the steady-state heat equation with MMS test
PetscErrorCode HeatEquation::be_steadyStateMMS(const PetscScalar time,const Vec slipVel,const Vec& tau,	const Vec& sdev, const Vec& dgxy,const Vec& dgxz,Vec& T,const Vec& To,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;
  /*
  // set up boundary conditions and source terms
  Vec rhs,temp;
  VecDuplicate(_dT,&rhs);
  VecDuplicate(_dT,&temp);
  VecSet(rhs,0.0);
  VecSet(temp,0.0);


  setMMSBoundaryConditions(time,"Dirichlet","Dirichlet","Neumann","Dirichlet");
  ierr = _sbp->setRhs(rhs,_bcL,_bcR,_bcT,_bcB);CHKERRQ(ierr);
  Vec source,Hxsource;
  VecDuplicate(_dT,&source);
  VecDuplicate(_dT,&Hxsource);
  //~ mapToVec(source,zzmms_SSdTsource,*_y,*_z,time);
  mapToVec(source,zzmms_SSTsource,*_y,*_z,time);
  ierr = _sbp->H(source,Hxsource);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
  Mat J,Jinv,qy,rz,yq,zr;
  ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
  multMatsVec(yq,zr,Hxsource);
  }
  //~ writeVec(source,_outputDir+"mms_SSdTsource");
  VecDestroy(&source);
  ierr = VecAXPY(rhs,1.0,Hxsource);CHKERRQ(ierr); // rhs = rhs + H*source
  VecDestroy(&Hxsource);


  //~ // compute shear heating component
  //~ if (_wViscShearHeating == "yes" && dgxy!=NULL && dgxz!=NULL) {
  //~ Vec shearHeat;
  //~ computeViscousShearHeating(shearHeat,sdev, dgxy, dgxz);
  //~ VecSet(shearHeat,0.);
  //~ VecAXPY(temp,1.0,shearHeat);
  //~ VecDestroy(&shearHeat);
  //~ }

  // solve for temperature and record run time required
  double startTime = MPI_Wtime();
  //~ VecCopy(To,_dT); // plausible guess
  KSPSolve(_ksp,rhs,_dT);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;
  VecCopy(_dT,T);
  //~ VecWAXPY(T,1.0,_dT,_Tamb); // T = dT + T0

  //~ mapToVec(_dT,zzmms_T,*_y,*_z,time);

  VecDestroy(&rhs);
  * */
  assert(0);

  return ierr;
}


PetscErrorCode HeatEquation::initiateVarSS(map<string,Vec>& varSS)
{
  PetscErrorCode ierr = 0;

  // put variables to be integrated implicity into varIm
  Vec T;
  VecDuplicate(_Tamb,&T);
  VecWAXPY(T,1.0,_Tamb,_dT);
  varSS["Temp"] = T;

  return ierr;
}


PetscErrorCode HeatEquation::updateSS(map<string,Vec>& varSS)
{
  PetscErrorCode ierr = 0;

  Vec slipVel = varSS.find("slipVel")->second;
  Vec tau = varSS.find("tau")->second;
  Vec sDev = varSS.find("sDev")->second;
  Vec gVxy_t = varSS.find("gVxy_t")->second;
  Vec gVxz_t = varSS.find("gVxz_t")->second;

  // final argument is output
  ierr = computeSteadyStateTemp(0,slipVel,tau,sDev,gVxy_t,gVxz_t,varSS["Temp"]); CHKERRQ(ierr);

  return ierr;
}


// compute steady-state temperature given boundary conditions and shear heating source terms (assuming these remain constant) Qfric and Qvisc
// Note: solves for dT, where dT = T - Tamb and Tamb also satisfies the steady-state heat equation
// and can includes radioactive heat generation.
PetscErrorCode HeatEquation::computeSteadyStateTemp(const PetscScalar time,const Vec slipVel,const Vec& tau,const Vec& sdev, const Vec& dgxy,const Vec& dgxz,Vec& T)
{
  PetscErrorCode ierr = 0;
  double beStartTime = MPI_Wtime();

  if (_kspSS == NULL) {
    KSPDestroy(&_kspTrans);
    Mat A; _sbp->getA(A);
    setupKSP(_kspSS,_pcSS,A);
  }

  // set up boundary conditions and source terms
  VecSet(_Q,0.);

  // left boundary: heat generated by fault motion: bcL or Qfric depending on shear zone width
  if (_wFrictionalHeating == "yes") {
    computeFrictionalShearHeating(tau,slipVel);
    VecAXPY(_Q,-1.0,_Qfric);
    VecScale(_bcL,-1.);
  }

  // compute shear heating component
  if (_wViscShearHeating == "yes" && dgxy!=NULL && dgxz!=NULL && sdev!=NULL) {
    computeViscousShearHeating(sdev, dgxy, dgxz);
    VecAXPY(_Q,-1.0,_Qvisc);
  }

  // rhs = J*H*Q + (SAT BC terms)
  Vec rhs; VecDuplicate(_k,&rhs); VecSet(rhs,0.0);
  ierr = _sbp->setRhs(rhs,_bcL,_bcR,_bcT,_bcB);CHKERRQ(ierr);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Vec temp1; VecDuplicate(_Q,&temp1);
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_Q,temp1);
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,temp1,rhs,rhs); // rhs = rhs + H*temp1
    VecDestroy(&temp1);
  }
  else{
    Mat H; _sbp->getH(H);
    ierr = MatMultAdd(H,_Q,rhs,rhs); CHKERRQ(ierr);
  }

  // solve for temperature and record run time required
  double startTime = MPI_Wtime();
  KSPSolve(_kspSS,rhs,_dT);
  _linSolveTime += MPI_Wtime() - startTime;
  _linSolveCount++;
  VecDestroy(&rhs);

  // compute total temperature _T (internal variable) and T (output variable)
  VecWAXPY(_T,1.0,_dT,_Tamb);
  VecCopy(_T,T);

  computeHeatFlux();

  _beTime += MPI_Wtime() - beStartTime;

  return ierr;
}


// compute viscous shear heating term (uses temperature from previous time step)
PetscErrorCode HeatEquation::computeViscousShearHeating(const Vec& sdev, const Vec& dgxy, const Vec& dgxz)
{
  PetscErrorCode ierr = 0;

  // shear heating terms: sdev * dgv  (stresses times viscous strain rates)
  // sdev = sqrt(sxy^2 + sxz^2)
  // dgv = sqrt(dgVxy^2 + dgVxz^2)
  VecSet(_Qvisc,0.0);

  // compute dgv
  VecPointwiseMult(_Qvisc,dgxy,dgxy);
  Vec temp;
  VecDuplicate(sdev,&temp);
  VecPointwiseMult(temp,dgxz,dgxz);
  VecAXPY(_Qvisc,1.0,temp);
  VecDestroy(&temp);
  VecSqrtAbs(_Qvisc);

  // multiply by deviatoric stress
  VecPointwiseMult(_Qvisc,sdev,_Qvisc); // Qvisc = sdev * Qvisc

  return ierr;
}


// compute frictional shear heating term (uses temperature from previous time step)
PetscErrorCode HeatEquation::computeFrictionalShearHeating(const Vec& tau, const Vec& slipVel)
{
  PetscErrorCode ierr = 0;

  // compute bcL = q = tau * slipVel
  VecPointwiseMult(_bcL,tau,slipVel);

  // if left boundary condition is heat flux: q = bcL = tau*slipVel/2
  if (_wMax == 0) {
    VecScale(_bcL,-0.5);
    VecSet(_Qfric,0.);
  }

  // if using finite width shear zone: Qfric = slipVel*tau * Gw
  else {
    ierr = MatMult(_MapV,_bcL,_Qfric); CHKERRQ(ierr); // Qfric = tau * slipVel (now a body field)
    VecPointwiseMult(_Qfric,_Qfric,_Gw); // Qfric = tau * slipVel * Gw
    VecSet(_bcL,0.); // q = 0, no flux
  }

  return ierr;
}


// set up KSP, matrices, boundary conditions for the steady state heat equation problem
PetscErrorCode HeatEquation::setUpSteadyStateProblem()
{
  PetscErrorCode ierr = 0;

  VecSet(_bcR,0.0);
  VecSet(_bcT,0.0);
  VecSet(_bcB,0.0);

  delete _sbp; _sbp = NULL;

  string bcRType = "Dirichlet";
  string bcTType = "Dirichlet";
  string bcLType = "Neumann";
  string bcBType = "Dirichlet";

  // construct matrices
  if (_D->_zGridSpacingType == "constantGridSpacing") {
    _sbp = new SbpOps_m_constGrid(_order,_Ny,_Nz,_Ly,_Lz,_k);
  }
  else if (_D->_zGridSpacingType == "variableGridSpacing") {
    _sbp = new SbpOps_m_varGrid(_order,_Ny,_Nz,_Ly,_Lz,_k);
    if (_Ny > 1 && _Nz > 1) { _sbp->setGrid(_y,_z); }
    else if (_Ny == 1 && _Nz > 1) { _sbp->setGrid(NULL,_z); }
    else if (_Ny > 1 && _Nz == 1) { _sbp->setGrid(_y,NULL); }
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }
  _sbp->setCompatibilityType(_D->_sbpCompatibilityType);
  _sbp->setBCTypes(bcRType,bcTType,bcLType,bcBType);
  _sbp->setMultiplyByH(1);
  _sbp->setLaplaceType("yz");
  _sbp->setDeleteIntermediateFields(1);
  _sbp->computeMatrices(); // actually create the matrices

  return ierr;
}


// set up KSP, matrices, boundary conditions for the transient heat equation problem
PetscErrorCode HeatEquation::setUpTransientProblem()
{
  PetscErrorCode ierr = 0;

  // ensure BCs are all 0
  VecSet(_bcR,0.);
  VecSet(_bcT,0.);
  VecSet(_bcL,0.);
  VecSet(_bcB,0.);

  delete _sbp; _sbp = NULL;
  // construct matrices
  // BC order: right,top, left, bottom
  if (_D->_zGridSpacingType == "constantGridSpacing") {
    _sbp = new SbpOps_m_constGrid(_order,_Ny,_Nz,_Ly,_Lz,_k);
  }
  else if (_D->_zGridSpacingType == "variableGridSpacing") {
    _sbp = new SbpOps_m_varGrid(_order,_Ny,_Nz,_Ly,_Lz,_k);
    if (_Ny > 1 && _Nz > 1) { _sbp->setGrid(_y,_z); }
    else if (_Ny == 1 && _Nz > 1) { _sbp->setGrid(NULL,_z); }
    else if (_Ny > 1 && _Nz == 1) { _sbp->setGrid(_y,NULL); }
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: SBP type type not understood\n");
    assert(0); // automatically fail
  }
  _sbp->setCompatibilityType(_D->_sbpCompatibilityType);
  _sbp->setBCTypes("Dirichlet","Dirichlet","Neumann","Dirichlet");
  _sbp->setMultiplyByH(1);
  _sbp->setLaplaceType("yz");
  _sbp->computeMatrices(); // actually create the matrices

  // create identity matrix I (multiplied by H)
  Mat H;
  _sbp->getH(H);
  if (_D->_zGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    MatMatMult(J,H,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_I);
  }
  else {
    MatDuplicate(H,MAT_COPY_VALUES,&_I);
  }

  // create (rho*c)^-1 vector and matrix
  Vec rhocV;
  VecDuplicate(_rho,&rhocV);
  VecSet(rhocV,1.);
  VecPointwiseDivide(rhocV,rhocV,_rho);
  VecPointwiseDivide(rhocV,rhocV,_c);
  MatDuplicate(_I,MAT_DO_NOT_COPY_VALUES,&_rcInv);
  MatDiagonalSet(_rcInv,rhocV,INSERT_VALUES);

  // create _D2ath = (rho*c)^-1 H D2
  Mat D2;
  _sbp->getA(D2);
  MatMatMult(_rcInv,D2,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_D2ath);

  MatDuplicate(_D2ath,MAT_COPY_VALUES,&_B);

  // ensure diagonal of _D2ath has been allocated, even if 0
  PetscScalar v=0.0;
  PetscInt Ii,Istart,Iend=0;
  MatGetOwnershipRange(_D2ath,&Istart,&Iend);
  for (Ii = Istart; Ii < Iend; Ii++) {
    MatSetValues(_D2ath,1,&Ii,1,&Ii,&v,ADD_VALUES);
  }
  MatAssemblyBegin(_D2ath,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(_D2ath,MAT_FINAL_ASSEMBLY);

  VecDestroy(&rhocV);

  return ierr;
}


// compute heat flux (full body field and surface heat flux) for output
PetscErrorCode HeatEquation::computeHeatFlux()
{
  PetscErrorCode ierr = 0;

  // total heat flux in lithosphere
  ierr = _sbp->muxDz(_T,_kTz); CHKERRQ(ierr);
  VecScale(_kTz,1e9);

  // extract surface heat flux
  VecScatterBegin(_scatters["body2T"], _kTz, _kTz_z0, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_scatters["body2T"], _kTz, _kTz_z0, INSERT_VALUES, SCATTER_FORWARD);

  return ierr;
}


PetscErrorCode HeatEquation::writeStep1D(const PetscInt stepCount, const PetscScalar time,const string outputDir)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();

  VecMax(_dT, NULL, &_maxTemp); // compute max T for output

  if (_viewers1D.empty()) {
    ierr = initiate_appendVecToOutput(_viewers1D, "kTz_z0", _kTz_z0, outputDir + "he_kTz_z0", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers1D, "he_bcL", _bcL, outputDir + "he_bcL", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers1D, "he_bcR", _bcR, outputDir + "he_bcR", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers1D, "he_bcT", _bcT, outputDir + "he_bcT", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers1D, "he_bcB", _bcB, outputDir + "he_bcB", _D->_outFileMode); CHKERRQ(ierr);
  }
  else {
    ierr = VecView(_kTz_z0,_viewers1D["kTz_y0"].first); CHKERRQ(ierr);
    ierr = VecView(_bcL,_viewers1D["he_bcL"].first); CHKERRQ(ierr);
    ierr = VecView(_bcR,_viewers1D["he_bcR"].first); CHKERRQ(ierr);
    ierr = VecView(_bcT,_viewers1D["he_bcT"].first); CHKERRQ(ierr);
    ierr = VecView(_bcB,_viewers1D["he_bcB"].first); CHKERRQ(ierr);
  }

  if (_maxTempV == NULL) {
    ierr = initiateWriteASCII(outputDir, "he_maxT.txt", _D->_outFileMode, _maxTempV, "%.15e\n", _maxTemp); CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_maxTempV, "%.15e\n", _maxTemp); CHKERRQ(ierr);
  }

  _writeTime += MPI_Wtime() - startTime;

  return ierr;
}


PetscErrorCode HeatEquation::writeStep2D(const PetscInt stepCount, const PetscScalar time,const string outputDir)
{
  PetscErrorCode ierr = 0;

  double startTime = MPI_Wtime();

  if (_viewers2D.empty()) {
    ierr = initiate_appendVecToOutput(_viewers2D, "T", _T, outputDir + "he_T", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers2D, "dT", _dT, outputDir + "he_dT", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers2D, "kTz", _kTz, outputDir + "he_kTz", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers2D, "Qfric", _Qfric, outputDir + "he_Qfric", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers2D, "Qvisc", _Qvisc, outputDir + "he_Qvisc", _D->_outFileMode); CHKERRQ(ierr);
    ierr = initiate_appendVecToOutput(_viewers2D, "Q", _Q, outputDir + "he_Q", _D->_outFileMode); CHKERRQ(ierr);
  }
  else {
    ierr = VecView(_T,_viewers2D["T"].first); CHKERRQ(ierr);
    ierr = VecView(_dT,_viewers2D["dT"].first); CHKERRQ(ierr);
    ierr = VecView(_kTz,_viewers2D["kTz"].first); CHKERRQ(ierr);
    ierr = VecView(_Qfric,_viewers2D["Qfric"].first); CHKERRQ(ierr);
    ierr = VecView(_Qvisc,_viewers2D["Qvisc"].first); CHKERRQ(ierr);
    ierr = VecView(_Q,_viewers2D["Q"].first); CHKERRQ(ierr);
  }

  _writeTime += MPI_Wtime() - startTime;
  return ierr;
}


PetscErrorCode HeatEquation::view()
{
  PetscErrorCode ierr = 0;
  ierr = PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Heat Equation Runtime Summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in be (s): %g\n",_beTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent writing output (s): %g\n",_writeTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   number of times linear system was solved: %i\n",_linSolveCount);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent solving linear system (s): %g\n",_linSolveTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% be time spent solving linear system: %g\n",_linSolveTime/_beTime*100.);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"\n");CHKERRQ(ierr);

  return ierr;
}


// Save all scalar fields to text file named he_domain.txt in output directory.
// Note that only the rank 0 processor's values will be saved.
PetscErrorCode HeatEquation::writeDomain(const string outputDir)
{
  PetscErrorCode ierr = 0;

  // output scalar fields
  string str = outputDir + "he_context.txt";
  PetscViewer    viewer;

  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  ierr = PetscViewerASCIIPrintf(viewer,"heatEquationType = %s\n",_heatEquationType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"withViscShearHeating = %s\n",_wViscShearHeating.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"withFrictionalHeating = %s\n",_wFrictionalHeating.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"withRadioHeatGeneration = %s\n",_wRadioHeatGen.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer,"Nz_lab = %i\n",_Nz_lab);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Lz_lab = %g\n",_Lz_lab);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer,"TVals = %s\n",vector2str(_TVals).c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"TDepths = %s\n",vector2str(_TDepths).c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"w = %.5e\n",_w);CHKERRQ(ierr);


  PetscMPIInt size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  ierr = PetscViewerASCIIPrintf(viewer,"numProcessors = %i\n",size);CHKERRQ(ierr);
  PetscViewerDestroy(&viewer);

  return ierr;
}


// write out material properties
PetscErrorCode HeatEquation::writeContext(const string outputDir)
{
  PetscErrorCode ierr = 0;

  writeDomain(outputDir);

  ierr = writeVec(_k,outputDir + "he_k"); CHKERRQ(ierr);
  ierr = writeVec(_c,outputDir + "he_c"); CHKERRQ(ierr);
  ierr = writeVec(_Tamb,outputDir + "he_Tamb"); CHKERRQ(ierr);
  ierr = writeVec(_T,outputDir + "he_T0"); CHKERRQ(ierr);
  if (_wFrictionalHeating == "yes") {
    ierr = writeVec(_Gw,outputDir + "he_Gw"); CHKERRQ(ierr);
    VecScale(_w,1e3); // output w in m
    ierr = writeVec(_w,outputDir + "he_w"); CHKERRQ(ierr);
    VecScale(_w,1e-3); // convert w from m to km
  }

  if (_wRadioHeatGen == "yes") {
    ierr = writeVec(_Qrad,outputDir + "he_Qrad"); CHKERRQ(ierr);
  }

  return ierr;
}


//======================================================================
// MMS  tests
double HeatEquation::zzmms_rho(const double y,const double z) { return 1.0; }
double HeatEquation::zzmms_c(const double y,const double z) { return 1.0; }
double HeatEquation::zzmms_h(const double y,const double z) { return 0.0; }
double HeatEquation::zzmms_k(const double y,const double z) { return sin(y)*sin(z) + 30.; }
double HeatEquation::zzmms_k_y(const double y,const double z) {return cos(y)*sin(z);}
double HeatEquation::zzmms_k_z(const double y,const double z) {return sin(y)*cos(z);}

double HeatEquation::zzmms_f(const double y,const double z) {return cos(y)*sin(z);}
double HeatEquation::zzmms_f_y(const double y,const double z) { return -sin(y)*sin(z); }
double HeatEquation::zzmms_f_yy(const double y,const double z) { return -cos(y)*sin(z); }
double HeatEquation::zzmms_f_z(const double y,const double z) { return cos(y)*cos(z); }
double HeatEquation::zzmms_f_zz(const double y,const double z) { return -cos(y)*sin(z); }
double HeatEquation::zzmms_g(const double t) { return exp(-2.*t); }
double HeatEquation::zzmms_g_t(const double t) { return -2.*exp(-2.*t); }

double HeatEquation::zzmms_T(const double y,const double z,const double t) { return zzmms_f(y,z)*zzmms_g(t); }
double HeatEquation::zzmms_T_y(const double y,const double z,const double t) { return zzmms_f_y(y,z)*zzmms_g(t); }
double HeatEquation::zzmms_T_yy(const double y,const double z,const double t) { return zzmms_f_yy(y,z)*zzmms_g(t); }
double HeatEquation::zzmms_T_z(const double y,const double z,const double t) { return zzmms_f_z(y,z)*zzmms_g(t); }
double HeatEquation::zzmms_T_zz(const double y,const double z,const double t) { return zzmms_f_zz(y,z)*zzmms_g(t); }
double HeatEquation::zzmms_T_t(const double y,const double z,const double t) { return zzmms_f(y,z)*zzmms_g_t(t); }

double HeatEquation::zzmms_dT(const double y,const double z,const double t) { return zzmms_T(y,z,t) - zzmms_T(y,z,0.); }
double HeatEquation::zzmms_dT_y(const double y,const double z,const double t) { return zzmms_T_y(y,z,t) - zzmms_T_y(y,z,0.); }
double HeatEquation::zzmms_dT_yy(const double y,const double z,const double t) { return zzmms_T_yy(y,z,t) - zzmms_T_yy(y,z,0.); }
double HeatEquation::zzmms_dT_z(const double y,const double z,const double t) { return zzmms_T_z(y,z,t) - zzmms_T_z(y,z,0.); }
double HeatEquation::zzmms_dT_zz(const double y,const double z,const double t) { return zzmms_T_zz(y,z,t) - zzmms_T_zz(y,z,0.); }
double HeatEquation::zzmms_dT_t(const double y,const double z,const double t) { return zzmms_T_t(y,z,t) - zzmms_T_t(y,z,0.); }

double HeatEquation::zzmms_SSTsource(const double y,const double z,const double t)
{
  PetscScalar k = zzmms_k(y,z);
  PetscScalar k_y = zzmms_k_y(y,z);
  PetscScalar k_z = zzmms_k_z(y,z);
  PetscScalar T_y = zzmms_T_y(y,z,t);
  PetscScalar T_yy = zzmms_T_yy(y,z,t);
  PetscScalar T_z = zzmms_T_z(y,z,t);
  PetscScalar T_zz = zzmms_T_zz(y,z,t);
  return k*(T_yy + T_zz) + k_y*T_y + k_z*T_z;
}

double HeatEquation::zzmms_SSdTsource(const double y,const double z,const double t)
{
  PetscScalar k = zzmms_k(y,z);
  PetscScalar k_y = zzmms_k_y(y,z);
  PetscScalar k_z = zzmms_k_z(y,z);
  PetscScalar dT_y = zzmms_dT_y(y,z,t);
  PetscScalar dT_yy = zzmms_dT_yy(y,z,t);
  PetscScalar dT_z = zzmms_dT_z(y,z,t);
  PetscScalar dT_zz = zzmms_dT_zz(y,z,t);
  return k*(dT_yy + dT_zz) + k_y*dT_y + k_z*dT_z;
}
