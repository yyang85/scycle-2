#include "strikeSlip_linearElastic_qd.hpp"

#define FILENAME "strikeSlip_linearElastic_qd.cpp"

using namespace std;

StrikeSlip_LinearElastic_qd::StrikeSlip_LinearElastic_qd(Domain &D)
  : _D(&D),_delim(D._delim),_isMMS(D._isMMS),_inputDir(D._inputDir),
    _outputDir(D._outputDir),_faultTypeScale(D._faultTypeScale),
    _currTime(0),_minDeltaT(D._minDeltaT),_stepCount(0),
    _integrateTime(0),_writeTime(0),_linSolveTime(0),_factorTime(0),
    _startTime(MPI_Wtime()),_totalRunTime(0),_miscTime(0),
    _timeV1D(NULL),_dtimeV1D(NULL),_timeV2D(NULL),_dtimeV2D(NULL),
    _quadEx(NULL),_quadImex(NULL),_fault(NULL),_material(NULL),_he(NULL),_p(NULL)
{
  _currTime = _D->_initTime;
  parseBCs();

  // fault
  //_scatters is a VecScatter object in domain.cpp
  _body2fault = &(D._scatters["body2L"]);
  _fault = new Fault_qd(D,D._scatters["body2L"],_faultTypeScale);

  // heat equation
  if (_D->_thermalCoupling != "no") {
    _he = new HeatEquation(D);
  }
  // flash heating with temperature evolution
  if (_D->_thermalCoupling != "no" && _D->_stateLaw == "flashHeating") {
    _fault->setThermalFields(_he->_Tamb,_he->_k,_he->_c);
  }

  // porosity-pressure equation
  if (_D->_hydraulicCoupling != "no") {
    _p = new PorosityPressure(D);
      // copy sN solved from pressure equation (pressure profile + constant sNEff)
    if (_D->_hydraulicCoupling == "coupled") {
      VecCopy(_p->_sN, _fault->_sN);
    }
  }

  // set SNEff if not loading from checkpoint
  if (_D->_ckptNumber == 0 && _D->_hydraulicCoupling == "coupled") {
    _fault->setSNEff(_p->_p);
  }
  
  // initiate momentum balance equation
  if (_D->_computeICs == 1) {
    _material = new LinearElastic(D,_mat_bcRType,_mat_bcTType,"Neumann",_mat_bcBType);
  }
  else {
    _material = new LinearElastic(D,_mat_bcRType,_mat_bcTType,_mat_bcLType,_mat_bcBType);
  }

  // body forcing term for ice stream
  _forcingTerm = NULL;
  _forcingTermPlain = NULL;
  if (_D->_forcingType == "iceStream") {
    constructIceStreamForcingTerm();
  }

  // compute min allowed time step for adaptive time stepping method
  computeMinTimeStep();
}


// destructor
StrikeSlip_LinearElastic_qd::~StrikeSlip_LinearElastic_qd()
{
  {
    map<string,Vec>::iterator it;
    for (it = _varEx.begin(); it!=_varEx.end(); it++ ) {
      VecDestroy(&it->second);
    }
    for (it = _varIm.begin(); it!=_varIm.end(); it++ ) {
      VecDestroy(&it->second);
    }
  }

  {  // destroy viewers for steady state iteration
    map<string,pair<PetscViewer,string>>::iterator it;
    for (it = _viewers.begin(); it!=_viewers.end(); it++ ) {
      PetscViewerDestroy(& (_viewers[it->first].first) );
    }
  }

  PetscViewerDestroy(&_timeV1D);
  PetscViewerDestroy(&_dtimeV1D);
  PetscViewerDestroy(&_timeV2D);
  PetscViewerDestroy(&_dtimeV2D);
  
  delete _quadImex;    _quadImex = NULL;
  delete _quadEx;      _quadEx = NULL;
  delete _material;    _material = NULL;
  delete _fault;       _fault = NULL;
  delete _he;          _he = NULL;
  delete _p;           _p = NULL;

  VecDestroy(&_forcingTerm);
  VecDestroy(&_forcingTermPlain);
}


// compute recommended min time step based on grid spacing and shear wave speed
// Note: defaults to user specified value
// recommended minDeltaT <= min(dy/cs, dz/cs)
PetscErrorCode StrikeSlip_LinearElastic_qd::computeMinTimeStep()
{
  PetscErrorCode ierr = 0;

  // compute grid spacing in y and z
  Vec dy, dz;
  VecDuplicate(_D->_y,&dy);
  VecDuplicate(_D->_y,&dz);

  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    MatGetDiagonal(yq, dy);
    VecScale(dy,1.0/(_D->_Ny-1));
    MatGetDiagonal(zr, dz);
    VecScale(dz,1.0/(_D->_Nz-1));
  }
  else {
    VecSet(dy,_D->_Ly/(_D->_Ny-1.0));
    VecSet(dz,_D->_Lz/(_D->_Nz-1.0));
  }

  // compute time for shear wave to travel one dy or dz
  Vec ts_dy,ts_dz;
  VecDuplicate(_D->_y,&ts_dy);
  VecDuplicate(_D->_z,&ts_dz);
  VecPointwiseDivide(ts_dy,dy,_material->_cs);
  VecPointwiseDivide(ts_dz,dz,_material->_cs);

  PetscScalar min_ts_dy, min_ts_dz;
  VecMin(ts_dy,NULL,&min_ts_dy);
  VecMin(ts_dz,NULL,&min_ts_dz);

  // clean up memory usage
  VecDestroy(&dy);
  VecDestroy(&dz);
  VecDestroy(&ts_dy);
  VecDestroy(&ts_dz);

  // smallest reasonable time step
  PetscScalar min_deltaT = min(min_ts_dy,min_ts_dz);

  // use min_deltaT if not user specified
  if (_minDeltaT == -1) {
    _minDeltaT = min_deltaT;
  }
  // if user provided _minDeltaT is larger, use the calculated value
  else if (_minDeltaT > min_deltaT) {
    PetscPrintf(PETSC_COMM_WORLD,"Warning: minimum requested time step (minDeltaT) is larger than recommended.");
    PetscPrintf(PETSC_COMM_WORLD," Requested: %e s, Recommended (min(dy/cs,dz/cs)): %e s\n",_minDeltaT,min_deltaT);
    _minDeltaT = min_deltaT;    
  }

  return ierr;
}


// parse boundary conditions
PetscErrorCode StrikeSlip_LinearElastic_qd::parseBCs()
{
  PetscErrorCode ierr = 0;

  if (_D->_qd_bcRType == "symmFault" || _D->_qd_bcRType == "rigidFault" || _D->_qd_bcRType == "remoteLoading") {
    _mat_bcRType = "Dirichlet";
  }
  else if (_D->_qd_bcRType == "freeSurface" || _D->_qd_bcRType == "outGoingCharacteristics") {
    _mat_bcRType = "Neumann";
  }

  if (_D->_qd_bcTType == "symmFault" || _D->_qd_bcTType == "rigidFault" || _D->_qd_bcTType == "remoteLoading") {
    _mat_bcTType = "Dirichlet";
  }
  else if (_D->_qd_bcTType == "freeSurface" || _D->_qd_bcTType == "outGoingCharacteristics") {
    _mat_bcTType = "Neumann";
  }

  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault" || _D->_qd_bcLType == "remoteLoading") {
    _mat_bcLType = "Dirichlet";
  }
  else if (_D->_qd_bcLType == "freeSurface" || _D->_qd_bcLType == "outGoingCharacteristics") {
    _mat_bcLType = "Neumann";
  }

  if (_D->_qd_bcBType == "symmFault" || _D->_qd_bcBType == "rigidFault" || _D->_qd_bcBType == "remoteLoading") {
    _mat_bcBType = "Dirichlet";
  }
  else if (_D->_qd_bcBType == "freeSurface" || _D->_qd_bcBType == "outGoingCharacteristics") {
    _mat_bcBType = "Neumann";
  }

  // determine if material is symmetric about the fault, or if one side is rigid
  _faultTypeScale = 2.0;
  if (_D->_qd_bcLType == "rigidFault" ) {
    _faultTypeScale = 1.0;
  }

  return ierr;
}


// initiate variables to be integrated in time
PetscErrorCode StrikeSlip_LinearElastic_qd::initiateIntegrand()
{
  PetscErrorCode ierr = 0;

  Mat A;
  _material->_sbp->getA(A);
  _material->setupKSP(_material->_ksp,_material->_pc,A);

  if (_isMMS) {
    _material->setMMSInitialConditions(_D->_initTime);
  }

  Vec slip;
  VecDuplicate(_material->_bcL,&slip);
  VecCopy(_material->_bcL,slip);
  VecScale(slip,_faultTypeScale);
  _varEx["slip"] = slip;

  if (_D->_computeICs == 1) {
    solveInit();
  }

  // set initial plastic porosity using slip velocity
  if (_D->_hydraulicCoupling != "no" && _p->_simu == "inject" && _p->_dilatancy == "yes") {
    _p->setInitialVals(_fault->_slipVel);
  }

  _fault->initiateIntegrand(_D->_initTime,_varEx);

  if (_D->_thermalCoupling != "no") {
    _he->initiateIntegrand(_D->_initTime,_varEx,_varIm);
    _fault->updateTemperature(_he->_T);
  }

  if (_D->_hydraulicCoupling != "no") {
    _p->initiateIntegrand(_D->_initTime,_varEx,_varIm);
  }

  return ierr;
}


// monitoring function for ode solvers, called within odeSolver
PetscErrorCode StrikeSlip_LinearElastic_qd::timeMonitor(PetscScalar time, PetscScalar deltaT, PetscInt stepCount, int& stopIntegration)
{
  PetscErrorCode ierr = 0;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"%i: t = %.15e s, dt = %.5e \n",stepCount,time,deltaT);CHKERRQ(ierr);

  double startTime = MPI_Wtime();

  _stepCount = stepCount;
  _deltaT = deltaT;
  _currTime = time;

  if ( (_D->_stride1D > 0 && _currTime == _D->_maxTime) || (_D->_stride1D > 0 && stepCount % _D->_stride1D == 0)) {
    ierr = writeStep1D(_stepCount, _currTime, _deltaT, _outputDir); CHKERRQ(ierr);
    ierr = _material->writeStep1D(_stepCount, _outputDir); CHKERRQ(ierr);
    ierr = _fault->writeStep(_stepCount, _outputDir); CHKERRQ(ierr);
    if (_D->_hydraulicCoupling != "no") { _p->writeStep(_stepCount,_currTime,_outputDir); }
    if (_D->_thermalCoupling != "no") { _he->writeStep1D(_stepCount,_currTime,_outputDir); }
  }

  if ( (_D->_stride2D > 0 && _currTime == _D->_maxTime) || (_D->_stride2D > 0 && stepCount % _D->_stride2D == 0)) {
    ierr = writeStep2D(_stepCount, _currTime, _deltaT, _outputDir); CHKERRQ(ierr);
    ierr = _material->writeStep2D(_stepCount, _outputDir); CHKERRQ(ierr);
    if (_D->_thermalCoupling != "no") { _he->writeStep2D(_stepCount, _currTime,_outputDir); }
  }

  // write checkpoint and stop time integration
  if (_D->_ckpt > 0 && (stepCount % _D->_interval == 0 || stepCount >= _D->_maxStepCount || time >= _D->_maxTime)) {
    ierr = writeCheckpoint(); CHKERRQ(ierr);
    ierr = _material->writeCheckpoint(); CHKERRQ(ierr);
    ierr = _fault->writeCheckpoint(); CHKERRQ(ierr);
    if (_D->_hydraulicCoupling != "no") { _p->writeCheckpoint(); }
    // if (_D->_thermalCoupling != "no") { _he->writeCheckpoint(); }
    stopIntegration = 1;
  }

  // stop integration when sNEff becomes negative at some point
  if (_D->_hydraulicCoupling == "coupled") {
    PetscReal minsNEff;
    Vec sNEff;
    VecDuplicate(_p->_p, &sNEff);
    VecWAXPY(sNEff,-1,_p->_p,_fault->_sN);
    VecMin(sNEff, NULL, &minsNEff);
    if (minsNEff < 0) {
      stopIntegration = 1;
      PetscPrintf(PETSC_COMM_WORLD, "Min effective normal stress = %e is negative. Stop time integration.\n", minsNEff);
    }
    VecDestroy(&sNEff);
  }
    
  _writeTime += MPI_Wtime() - startTime;
  return ierr;
}


// write out time and _deltaT at each time step
PetscErrorCode StrikeSlip_LinearElastic_qd::writeStep1D(PetscInt stepCount, PetscScalar time, PetscScalar deltaT, const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV1D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time1D.txt", _D->_outFileMode, _timeV1D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV1D, "%.15e\n", time); CHKERRQ(ierr);
  }
  if (_dtimeV1D == NULL ) {
    initiateWriteASCII(outputDir, "med_dt1D.txt", _D->_outFileMode, _dtimeV1D, "%.15e\n", deltaT);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV1D, "%.15e\n", deltaT); CHKERRQ(ierr);
  }

  return ierr;
}


// write out time at each time step
PetscErrorCode StrikeSlip_LinearElastic_qd::writeStep2D(PetscInt stepCount, PetscScalar time, PetscScalar deltaT, const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV2D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time2D.txt", _D->_outFileMode, _timeV2D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV2D, "%.15e\n", time); CHKERRQ(ierr);
  }
  if (_dtimeV2D == NULL ) {
    initiateWriteASCII(outputDir, "med_dt2D.txt", _D->_outFileMode, _dtimeV2D, "%.15e\n", deltaT);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV2D, "%.15e\n", deltaT); CHKERRQ(ierr);
  }

  return ierr;
}


// write out time and _deltaT at each time step
PetscErrorCode StrikeSlip_LinearElastic_qd::writeCheckpoint()
{
  PetscErrorCode ierr = 0;

  _D->_ckptNumber++;
  writeASCII(_outputDir, "ckptNumber", _D->_ckptNumber,"%i\n");
  writeASCII(_outputDir, "chkpt_currT", _currTime,"%.15e\n");
  writeASCII(_outputDir, "chkpt_stepCount", _stepCount,"%i\n");

  if (_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") {
    writeASCII(_outputDir, "chkpt_prevErr", _quadImex->_errA[1],"%.15e\n");
    writeASCII(_outputDir, "chkpt_currErr", _quadImex->_errA[0],"%.15e\n");
    writeASCII(_outputDir, "chkpt_deltaT", _quadImex->_newDeltaT,"%.15e\n");
  }
  else {
    writeASCII(_outputDir, "chkpt_prevErr", _quadEx->_errA[1],"%.15e\n");
    writeASCII(_outputDir, "chkpt_currErr", _quadEx->_errA[0],"%.15e\n");
    writeASCII(_outputDir, "chkpt_deltaT", _quadEx->_newDeltaT,"%.15e\n");
  }

  return ierr;
}


// print fields to screen
PetscErrorCode StrikeSlip_LinearElastic_qd::view()
{
  PetscErrorCode ierr = 0;

  double totRunTime = MPI_Wtime() - _startTime;

  _material->view(_integrateTime);
  _fault->view(_integrateTime);
  if ((_D->_timeIntegrator == "RK32" || _D->_timeIntegrator == "RK43") && _quadEx!=NULL) {
    ierr = _quadEx->view();
  }
  if ((_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") && _quadImex!=NULL) {
    ierr = _quadImex->view();
  }
  if (_D->_thermalCoupling != "no") { _he->view(); }

  ierr = PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"StrikeSlip_LinearElastic_qd Runtime Summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in integration (s): %g\n",_integrateTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent writing output (s): %g\n",_writeTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   total run time (s): %g\n",totRunTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent writing output: %g\n",(_writeTime/_integrateTime)*100.);CHKERRQ(ierr);

  return ierr;
}


// write out context files
PetscErrorCode StrikeSlip_LinearElastic_qd::writeContext()
{
  PetscErrorCode ierr = 0;

  // write context from other classes
  _D->write();
  _material->writeContext(_outputDir);
  _fault->writeContext(_outputDir);

  if (_D->_thermalCoupling != "no") { _he->writeContext(_outputDir); }
  if (_D->_hydraulicCoupling != "no") { _p->writeContext(_outputDir); }
  if (_D->_forcingType == "iceStream") {
    ierr = writeVec(_forcingTermPlain,_outputDir + "momBal_forcingTerm"); CHKERRQ(ierr);
  }

  return ierr;
}


//======================================================================
// Adaptive time stepping functions
//======================================================================

// test the correct loading of the checkpoint values
PetscErrorCode StrikeSlip_LinearElastic_qd::testLoading() {
  PetscErrorCode ierr = 0;
  Vec bcL_c, bcR_c, bcRShift_c, slip_c, slipVel_c, tauP_c, tauQSP_c, psi_c, sNEff_c, prestress_c;
  VecDuplicate(_material->_bcL, &bcL_c);
  VecDuplicate(_material->_bcR, &bcR_c);
  VecDuplicate(_material->_bcRShift, &bcRShift_c);
  VecDuplicate(_fault->_slip, &slip_c);
  VecDuplicate(_fault->_slipVel, &slipVel_c);
  VecDuplicate(_fault->_tauP, &tauP_c);
  VecDuplicate(_fault->_tauQSP, &tauQSP_c);
  VecDuplicate(_fault->_psi, &psi_c);
  VecDuplicate(_fault->_sNEff, &sNEff_c);
  VecDuplicate(_fault->_prestress, &prestress_c);

  ierr = loadVecFromInputFile(bcL_c, _outputDir, "chkpt_momBal_bcL"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(bcR_c, _outputDir, "chkpt_momBal_bcR"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(bcRShift_c, _outputDir, "chkpt_momBal_bcRShift"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(slip_c, _outputDir, "chkpt_slip"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(slipVel_c, _outputDir, "chkpt_slipVel"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(tauP_c, _outputDir, "chkpt_tau"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(tauQSP_c, _outputDir, "chkpt_tauQS"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(psi_c, _outputDir, "chkpt_psi"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(sNEff_c, _outputDir, "chkpt_sNEff"); CHKERRQ(ierr);
  ierr = loadVecFromInputFile(prestress_c, _outputDir, "chkpt_prestress"); CHKERRQ(ierr);

  PetscBool flg;
  VecEqual(_material->_bcL, bcL_c, &flg);
  if (!flg) {
    printf("Warning: bcL not equal after initiateIntegrand\n");
    printVec(_material->_bcL);
    printVec(bcL_c);
  }
  else {
    printf("bcL equal after initiateIntegrand\n");
  }

  VecEqual(_material->_bcR, bcR_c, &flg);
  if (!flg) {
    printf("Warning: bcR not equal after initiateIntegrand\n");
    printVec(_material->_bcR);
    printVec(bcR_c);
  }
  else {
    printf("bcR equal after initiateIntegrand\n");
  }

  VecEqual(_material->_bcRShift, bcRShift_c, &flg);
  if (!flg) {
    printf("Warning: bcRShift not equal after initiateIntegrand\n");
    printVec(_material->_bcRShift);
    printVec(bcRShift_c);
  }
  else {
    printf("bcRShift equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_slip, slip_c, &flg);
  if (!flg) {
    printf("Warning: slip not equal after initiateIntegrand\n");
    printVec(_fault->_slip);
    printVec(slip_c);
  }
  else {
    printf("slip equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_slipVel, slipVel_c, &flg);
  if (!flg) {
    printf("Warning: slipVel not equal after initiateIntegrand\n");
    printVec(_fault->_slipVel);
    printVec(slipVel_c);
  }
  else {
    printf("slipVel equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_tauP, tauP_c, &flg);
  if (!flg) {
    printf("Warning: tauP not equal after initiateIntegrand\n");
    printVec(_fault->_tauP);
    printVec(tauP_c);
  }
  else {
    printf("tauP equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_tauQSP, tauQSP_c, &flg);
  if (!flg) {
    printf("Warning: tauQSP not equal after initiateIntegrand\n");
    printVec(_fault->_tauQSP);
    printVec(tauQSP_c);
  }
  else {
    printf("tauQSP equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_psi, psi_c, &flg);
  if (!flg) {
    printf("Warning: psi not equal after initiateIntegrand\n");
    printVec(_fault->_psi);
    printVec(psi_c);
  }
  else {
    printf("psi equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_sNEff, sNEff_c, &flg);
  if (!flg) {
    printf("Warning: sNEff not equal after initiateIntegrand\n");
    printVec(_fault->_sNEff);
    printVec(sNEff_c);
  }
  else {
    printf("sNEff equal after initiateIntegrand\n");
  }

  VecEqual(_fault->_prestress, prestress_c, &flg);
  if (!flg) {
    printf("Warning: prestress not equal after initiateIntegrand\n");
    printVec(_fault->_prestress);
    printVec(prestress_c);
  }
  else {
    printf("prestress equal after initiateIntegrand\n");
  }

  VecDestroy(&bcL_c);
  VecDestroy(&bcR_c);
  VecDestroy(&bcRShift_c);
  VecDestroy(&slip_c);
  VecDestroy(&slipVel_c);
  VecDestroy(&tauP_c);
  VecDestroy(&tauQSP_c);
  VecDestroy(&psi_c);
  VecDestroy(&sNEff_c);
  VecDestroy(&prestress_c);

  return ierr;
}


// perform all integration and time stepping
PetscErrorCode StrikeSlip_LinearElastic_qd::integrate()
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  // put initial conditions into var for integration
  initiateIntegrand();
  
  // test the vectors are correctly loaded
  if (_D->_ckptNumber > 0) {
    testLoading();
  }
  
  // initialize time integrator
  if (_D->_timeIntegrator == "FEuler") {
    _quadEx = new FEuler(_D->_maxStepCount,_D->_maxTime,_D->_initDeltaT,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32") {
    _quadEx = new RK32(_D->_maxStepCount,_D->_maxTime,_D->_initDeltaT,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43") {
    _quadEx = new RK43(_D->_maxStepCount,_D->_maxTime,_D->_initDeltaT,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32_WBE") {
    _quadImex = new RK32_WBE(_D->_maxStepCount,_D->_maxTime,_D->_initDeltaT,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43_WBE") {
    _quadImex = new RK43_WBE(_D->_maxStepCount,_D->_maxTime,_D->_initDeltaT,_D->_timeControlType);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: timeIntegrator type not understood\n");
    assert(0); // automatically fail
  }

  // with backward Euler, implicit time stepping
  if (_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") {
    ierr = _quadImex->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    ierr = _quadImex->setTimeStepBounds(_minDeltaT,_D->_maxDeltaT);CHKERRQ(ierr);
    ierr = _quadImex->setTimeRange(_D->_initTime,_D->_maxTime);
    ierr = _quadImex->setToleranceType(_D->_normType); CHKERRQ(ierr);
    ierr = _quadImex->setTimeStepType(_D->_dtType); CHKERRQ(ierr);
    ierr = _quadImex->setInitialConds(_varEx,_varIm);CHKERRQ(ierr);
    ierr = _quadImex->setErrInds(_D->_timeIntInds,_D->_scale);CHKERRQ(ierr);

    if (_D->_ckpt > 0 && _D->_ckptNumber > 0) {
      loadValueFromCheckpoint(_outputDir, "chkpt_prevErr", _quadImex->_errA[1]);
      loadValueFromCheckpoint(_outputDir, "chkpt_currErr", _quadImex->_errA[0]);
      _quadImex->_stepCount = _stepCount;
    }

    ierr = _quadImex->integrate(this);CHKERRQ(ierr);
  }

  // explicit time stepping
  else {
    ierr = _quadEx->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    ierr = _quadEx->setTimeStepBounds(_minDeltaT,_D->_maxDeltaT);CHKERRQ(ierr);
    ierr = _quadEx->setTimeRange(_D->_initTime,_D->_maxTime);
    ierr = _quadEx->setToleranceType(_D->_normType); CHKERRQ(ierr);
    ierr = _quadEx->setTimeStepType(_D->_dtType); CHKERRQ(ierr);
    ierr = _quadEx->setInitialConds(_varEx);CHKERRQ(ierr);
    ierr = _quadEx->setErrInds(_D->_timeIntInds,_D->_scale);

    if (_D->_ckpt > 0 && _D->_ckptNumber > 0) {
      loadValueFromCheckpoint(_outputDir, "chkpt_prevErr", _quadEx->_errA[1]);
      loadValueFromCheckpoint(_outputDir, "chkpt_currErr", _quadEx->_errA[0]);
      _quadEx->_stepCount = _stepCount;
    }

    ierr = _quadEx->integrate(this);CHKERRQ(ierr);
  }

  // calculate time used in integration
  _integrateTime = MPI_Wtime() - startTime;

  return ierr;
}


// purely explicit time stepping
// note that the heat equation and pressure diffusion are only solved implicitly
PetscErrorCode StrikeSlip_LinearElastic_qd::d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx)
{
  PetscErrorCode ierr = 0;

  // 1. update state of each class from integrated variables varEx

  // update for momBal; var holds slip, bcL is displacement at y=0+
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(varEx.find("slip")->second,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  _fault->updateFields(varEx);

  if (_D->_hydraulicCoupling != "no" && varEx.find("phip") != varEx.end()) {
    _p->updateFields(varEx);
  }
  
  // 2. compute rates
  ierr = solveMomentumBalance(time,varEx,dvarEx); CHKERRQ(ierr);

  // update fields on fault from other classes
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // PENALTY TERM CALCULATION FOR QUASI-STATIC SHEAR STRESS
  // extract grid value of displacement on fault
  if (_D->_penalty == "yes") {
    Vec uFault;
    VecDuplicate(_fault->_tauQSP, &uFault);
    ierr = VecScatterBegin(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

    // penalty term = alpha*(uFault - slip/2), where alpha=qy*alphaDy
    Vec penalty;
    VecDuplicate(uFault, &penalty);
    VecAXPY(uFault,-0.5,varEx.find("slip")->second);
    VecScale(uFault, _material->_alpha); // scale by alpha/dq

    if (_D->_yGridSpacingType == "variableGridSpacing") {
      Mat J,Jinv,qy,rz,yq,zr;
      _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
      MatMult(qy,uFault,penalty); // qy*penalty
    }
    else {
      PetscScalar dy_inv = (_D->_Ny-1)/(_D->_Ly);
      PetscScalar dq = 1/(_D->_Ny-1);
      VecScale(uFault, dq);
      VecScale(uFault,dy_inv);  // change from alpha/dq -> alpha/dy
      VecCopy(uFault, penalty);
    }

    // add penalty term to tauQSP
    VecAXPY(_fault->_tauQSP, 1, penalty);
    VecDestroy(&uFault);
    VecDestroy(&penalty);
  }
  
  // rates for fault
  ierr = _fault->d_dt(time,varEx,dvarEx); // sets rates for slip and state
  
  if (_D->_hydraulicCoupling != "no" && varEx.find("phip") != varEx.end()) {
    _p->d_dt(time, varEx, dvarEx);
  }

  return ierr;
}


// implicit/explicit time stepping
PetscErrorCode StrikeSlip_LinearElastic_qd::d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx, map<string,Vec>& varIm,const map<string,Vec>& varImo,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // 1. update state of each class from integrated variables varEx and varImo

  // update for momBal; var holds slip, bcL is displacement at y=0+
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(varEx.find("slip")->second,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  _fault->updateFields(varEx);

  if (_D->_hydraulicCoupling != "no") {
    _p->updateFields(varEx,varImo);
  }

  if (varImo.find("Temp") != varImo.end() && _D->_thermalCoupling == "coupled") {
    _fault->updateTemperature(varImo.find("Temp")->second);
  }

  // update effective normal stress in fault using pore pressure
  if (_D->_hydraulicCoupling == "coupled") {
    _fault->setSNEff(_p->_p);
  }

  // 2. compute explicit rates
  ierr = solveMomentumBalance(time,varEx,dvarEx); CHKERRQ(ierr);

  // update shear stress on fault from momentum balance computation
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // PENALTY TERM CALCULATION FOR QUASI-STATIC SHEAR STRESS
  // extract grid value of displacement on fault
  if (_D->_penalty == "yes") {
    Vec uFault;
    VecDuplicate(_fault->_tauQSP, &uFault);
    ierr = VecScatterBegin(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

    // penalty term = alpha*(uFault - slip/2), where alpha=qy*alphaDy
    Vec penalty;
    VecDuplicate(uFault, &penalty);
    VecAXPY(uFault,-0.5,varEx.find("slip")->second);
    VecScale(uFault, _material->_alpha); // scale by alpha/dq

    if (_D->_yGridSpacingType == "variableGridSpacing") {
      Mat J,Jinv,qy,rz,yq,zr;
      _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
      MatMult(qy,uFault,penalty); // qy*penalty
    }
    else {
      PetscScalar dy_inv = (_D->_Ny-1)/(_D->_Ly);
      PetscScalar dq = 1/(_D->_Ny-1);
      VecScale(uFault, dq);
      VecScale(uFault,dy_inv);  // change from alpha/dq -> alpha/dy
      VecCopy(uFault, penalty);
    }

    // add penalty term to tauQSP
    VecAXPY(_fault->_tauQSP, 1, penalty);
    VecDestroy(&uFault);
    VecDestroy(&penalty);
  }

  // rates for fault
  ierr = _fault->d_dt(time,varEx,dvarEx); // sets rates for slip and state

  // pressure equation
  if (_D->_hydraulicCoupling != "no") {
    _p->d_dt(time,varEx,dvarEx,varIm,varImo,dt);
  }

  // 3. Implicit time step
  // heat equation
  // solve heat equation implicitly
  if (_D->_thermalCoupling != "no") {
    Vec V = dvarEx.find("slip")->second;
    Vec tau = _fault->_tauP;
    Vec gVxy_t = NULL;
    Vec gVxz_t = NULL;
    Vec Told = varImo.find("Temp")->second;
    // arguments: time, slipVel, txy, sigmadev, dgxy, dgxz, T, old T, dt
    ierr = _he->be(time,V,tau,NULL,gVxy_t,gVxz_t,varIm["Temp"],Told,dt); CHKERRQ(ierr);
  }

  return ierr;
}


// momentum balance equation and constitutive laws portion of d_dt
PetscErrorCode StrikeSlip_LinearElastic_qd::solveMomentumBalance(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx)
{
  PetscErrorCode ierr = 0;

  // update rhs
  if (_isMMS) {
    _material->setMMSBoundaryConditions(time);
  }
  _material->setRHS();
  if (_isMMS) {
    _material->addRHS_MMSSource(time,_material->_rhs);
  }

  // add source term for driving the ice stream to rhs Vec
  if (_D->_forcingType == "iceStream") { VecAXPY(_material->_rhs,1.0,_forcingTerm); }

  // compute displacement and stresses
  _material->computeU();
  _material->computeStresses();

  return ierr;
}


// solve for initial condition on the fault based on user-defined psi and tauQSP,
// or using plate loading rate as slipVel to set steady-state conditions
PetscErrorCode StrikeSlip_LinearElastic_qd::solveInit()
{
  PetscErrorCode ierr = 0;

  // set initial conditions for fault, material based on strain rate
  if (_D->_initState == "unsteady") {
    _fault->setPsiTauV();
  }
  else {
    _fault->guessSS(_D->_vInit); // sets: slipVel, psi, tau
  }
  
  // compute u that satisfies tau at left boundary (fault)
  VecCopy(_fault->_tauP,_material->_bcL);
  _material->setRHS();
  _material->computeU();
  _material->computeStresses();

  // update fault to contain correct stresses
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // update boundary conditions, stresses
  solveInitBC();
  _material->changeBCTypes(_mat_bcRType,_mat_bcTType,_mat_bcLType,_mat_bcBType);

  // steady state temperature
  if (_D->_thermalCoupling != "no") {
    Vec T;
    VecDuplicate(_material->_sxy,&T);
    _he->computeSteadyStateTemp(_currTime,_fault->_slipVel,_fault->_tauP,NULL,NULL,NULL,T);
    VecDestroy(&T);
  }

  return ierr;
}


// update the boundary conditions based on computed u that satisfies tau
PetscErrorCode StrikeSlip_LinearElastic_qd::solveInitBC()
{
  PetscErrorCode ierr = 0;

  // adjust u so it has no negative values
  PetscScalar minVal = 0;
  VecMin(_material->_u,NULL,&minVal);
  if (minVal < 0) {
    minVal = abs(minVal);
    Vec temp;
    VecDuplicate(_material->_u,&temp);
    VecSet(temp,minVal);
    VecAXPY(_material->_u,1.,temp);
    VecDestroy(&temp);
  }

  // extract R boundary from u, to set _material->bcR
  VecScatterBegin(_D->_scatters["body2R"], _material->_u, _material->_bcRShift, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2R"], _material->_u, _material->_bcRShift, INSERT_VALUES, SCATTER_FORWARD);
  VecCopy(_material->_bcRShift,_material->_bcR);

  // extract L boundary from u to set slip and _material->_bcL
  Vec uL;
  VecDuplicate(_material->_bcL,&uL);
  VecScatterBegin(_D->_scatters["body2L"], _material->_u, uL, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2L"], _material->_u, uL, INSERT_VALUES, SCATTER_FORWARD);

  // reset _bcL
  VecCopy(uL,_varEx["slip"]);
  VecScale(_varEx["slip"], _faultTypeScale);
  VecCopy(uL,_material->_bcL);

  VecDestroy(&uL);

  return ierr;
}


// constructs the body forcing term for an ice stream
// includes allocation of memory for this forcing term
PetscErrorCode StrikeSlip_LinearElastic_qd::constructIceStreamForcingTerm()
{
  PetscErrorCode ierr = 0;

  // compute forcing term using scalar input
  VecDuplicate(_material->_u,&_forcingTerm);
  VecSet(_forcingTerm,_D->_forcingVal);
  VecDuplicate(_material->_u,&_forcingTermPlain);
  VecCopy(_forcingTerm,_forcingTermPlain);

  // alternatively, load forcing term from user input
  ierr = loadVecFromInputFile(_forcingTerm,_inputDir,"iceForcingTerm"); CHKERRQ(ierr);

  // multiply forcing term H*J if using a curvilinear grid (the H matrix and the Jacobian)
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Vec temp1;
    Mat J,Jinv,qy,rz,yq,zr,H;
    VecDuplicate(_forcingTerm,&temp1);
    ierr = _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_forcingTerm,temp1); CHKERRQ(ierr);
    _material->_sbp->getH(H);
    ierr = MatMult(H,temp1,_forcingTerm); CHKERRQ(ierr);
    VecDestroy(&temp1);
  }
  // multiply forcing term by H if grid is regular
  else {
    Vec temp1;
    Mat H;
    VecDuplicate(_forcingTerm,&temp1);
    _material->_sbp->getH(H);
    ierr = MatMult(H,_forcingTerm,temp1); CHKERRQ(ierr);
    VecCopy(temp1,_forcingTerm);
    VecDestroy(&temp1);
  }

  return ierr;
}


// measure MMS error for various outputs
PetscErrorCode StrikeSlip_LinearElastic_qd::measureMMSError()
{
  PetscErrorCode ierr = 0;

  _material->measureMMSError(_currTime);
  _he->measureMMSError(_currTime);
  // _p->measureMMSError(_currTime);

  return ierr;
}
