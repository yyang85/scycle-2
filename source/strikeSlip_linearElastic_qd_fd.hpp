#ifndef STRIKESLIP_LINEARELASTIC_QD_FD_H_INCLUDED
#define STRIKESLIP_LINEARELASTIC_QD_FD_H_INCLUDED

#include <petscksp.h>
#include <string>
#include <cmath>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <map>

#include "integratorContextEx.hpp"
#include "integratorContextImex.hpp"
#include "integratorContext_WaveEq.hpp"
#include "integratorContext_WaveEq_Imex.hpp"

#include "odeSolver.hpp"
#include "odeSolverImex.hpp"
#include "odeSolver_WaveEq.hpp"
#include "odeSolver_WaveImex.hpp"
#include "genFuncs.hpp"
#include "domain.hpp"
#include "sbpOps.hpp"
#include "sbpOps_m_constGrid.hpp"
#include "sbpOps_m_varGrid.hpp"
#include "fault.hpp"
#include "porosityPressure.hpp"
#include "heatEquation.hpp"
#include "linearElastic.hpp"

using namespace std;

/*
 * Mediator-level class for the simulation of earthquake cycles on a vertical strike-slip fault
 * with linear elastic material properties.
 * Uses the quasi-dynamic approximation.
 */


class strikeSlip_linearElastic_qd_fd: public IntegratorContextEx, public IntegratorContextImex, public IntegratorContext_WaveEq, public IntegratorContext_WaveEq_Imex
{
private:
  // disable default copy constructor and assignment operator
  strikeSlip_linearElastic_qd_fd(const strikeSlip_linearElastic_qd_fd &that);
  strikeSlip_linearElastic_qd_fd& operator=(const strikeSlip_linearElastic_qd_fd &rhs);

  Domain *_D;

  // IO information
  string       _delim; // format is: var delim value (without the white space)

  // problem properties
  string       _inputDir;
  string       _outputDir; // output data
  string       _stateLaw;
  int          _computeICs; // 0 = no, 1 = yes
  PetscScalar  _faultTypeScale; // = 2 if symmetric fault, 1 if one side of fault is rigid

  PetscInt     _cycleCount,_maxNumCycles;
  PetscScalar  _deltaT, _deltaT_fd, _CFL; // current time step size, time step for fully dynamic, CFL factor
  Vec         *_y,*_z;
  Vec          _ay;
  Vec          _alphay;
  bool         _inDynamic,_allowed;
  PetscScalar  _trigger_qd2fd, _trigger_fd2qd, _limit_qd, _limit_fd, _limit_stride_fd;

  // time stepping data
  map <string,Vec>  _varFD,_varFDPrev; // holds variables for time step: n+1, n (current), n-1
  map <string,Vec>  _varQSEx; // holds variables for explicit integration in time
  map <string,Vec>  _varIm; // holds variables for implicit integration in time
  Vec               _u0; // total displacement at start of fd
  PetscInt          _stride1D,_stride2D; // stride
  PetscInt          _stride1D_qd, _stride2D_qd, _stride1D_fd, _stride2D_fd, _stride1D_fd_end, _stride2D_fd_end;
  PetscScalar       _currTime;
  int               _stepCount;
  PetscScalar       _dT;

  // viewers
  PetscViewer       _timeV1D,_dtimeV1D,_timeV2D,_dtimeV2D, _regime1DV, _regime2DV;

  // runtime data
  double _integrateTime,_writeTime,_linSolveTime,_factorTime,_startTime,_miscTime, _propagateTime, _dynTime, _qdTime;

  // forcing term for ice stream problem
  Vec _forcingTerm, _forcingTermPlain; // body forcing term, copy of body forcing term for output

  // for mapping from body fields to the fault
  VecScatter* _body2fault;

  // boundary conditions
  string     _mat_qd_bcRType,_mat_qd_bcTType,_mat_qd_bcLType,_mat_qd_bcBType;
  string     _mat_fd_bcRType,_mat_fd_bcTType,_mat_fd_bcLType,_mat_fd_bcBType;

  map <string,pair<PetscViewer,string> >  _viewers;

  PetscErrorCode loadSettings(const char *file);
  PetscErrorCode checkInput();
  PetscErrorCode parseBCs(); // parse boundary conditions
  PetscErrorCode computeTimeStep();
  PetscErrorCode computePenaltyVectors(); // computes alphay and alphaz
  PetscErrorCode constructIceStreamForcingTerm(); // ice stream forcing term

public:
  OdeSolver                 *_quadEx_qd; // explicit time stepping
  OdeSolverImex             *_quadImex_qd; // implicit time stepping
  OdeSolver_WaveEq          *_quadWaveEx;
  OdeSolver_WaveEq_Imex     *_quadWaveImex;

  Fault_qd                   *_fault_qd;
  Fault_fd                   *_fault_fd;
  LinearElastic              *_material; // linear elastic off-fault material properties
  HeatEquation               *_he;
  PorosityPressure           *_p;

  strikeSlip_linearElastic_qd_fd(Domain&D);
  ~strikeSlip_linearElastic_qd_fd();

  // estimating steady state conditions
  PetscErrorCode solveInit();
  PetscErrorCode solveInitBC();

  // time stepping functions
  PetscErrorCode integrate(); // will call OdeSolver method by same name
  PetscErrorCode integrate_qd();
  PetscErrorCode integrate_fd();
  PetscErrorCode integrate_singleQDTimeStep(); // take 1 quasidynamic time step with deltaT = deltaT_fd
  PetscErrorCode initiateIntegrands(); // allocate space for vars, guess steady-state initial conditions
  PetscErrorCode solveMomentumBalance(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx);
  PetscErrorCode propagateWaves(const PetscScalar time, const PetscScalar deltaT,
        map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev);

  // help with switching between fully dynamic and quasidynamic
  bool checkSwitchRegime(const Fault* _fault);
  PetscErrorCode prepare_qd2fd(); // switch from quasidynamic to fully dynamic
  PetscErrorCode prepare_fd2qd(); // switch from fully dynamic to quasidynamic

  // explicit time-stepping methods
  PetscErrorCode d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx); // quasidynamic

  PetscErrorCode d_dt(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev); // fully dynamic

  // methods for implicit/explicit time stepping
  PetscErrorCode d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx, map<string,Vec>& varIm,const map<string,Vec>& varImo,const PetscScalar dt); // quasidynamic

  PetscErrorCode d_dt(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev, map<string,Vec>& varIm, const map<string,Vec>& varImPrev); // fully dynamic


  // IO functions
  PetscErrorCode view();
  PetscErrorCode writeContext();
  PetscErrorCode writeCheckpoint();
  PetscErrorCode timeMonitor(PetscScalar time, PetscScalar deltaT, PetscInt stepCount,int& stopIntegration);
  PetscErrorCode writeStep1D(PetscInt stepCount, PetscScalar time, const string outputDir);
  PetscErrorCode writeStep2D(PetscInt stepCount, PetscScalar time,const string outputDir);

};


#endif
