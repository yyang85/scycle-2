#include "odeSolver_WaveEq.hpp"

using namespace std;

OdeSolver_WaveEq::OdeSolver_WaveEq(PetscInt maxNumSteps,PetscScalar initT,PetscScalar finalT,PetscScalar deltaT)
: _initT(initT),_finalT(finalT),_currT(initT),_deltaT(deltaT),
  _maxNumSteps(maxNumSteps),_stepCount(0),
  _lenVar(0),_runTime(0)
{}


OdeSolver_WaveEq::~OdeSolver_WaveEq()
{
  // destruct temporary containers
  destroyVector(_varNext);
  destroyVector(_var);
  destroyVector(_varPrev);
}


PetscErrorCode OdeSolver_WaveEq::setStepSize(const PetscReal deltaT)
{
  _deltaT = deltaT;
  return 0;
}


// if starting with a nonzero initial step count
PetscErrorCode OdeSolver_WaveEq::setInitialStepCount(const PetscReal stepCount)
{
  _stepCount = stepCount;
  return 0;
}


PetscErrorCode OdeSolver_WaveEq::setTimeRange(const PetscReal initT,const PetscReal finalT)
{
  _initT = initT;
  _currT = initT;
  _finalT = finalT;
  return 0;
}


PetscErrorCode OdeSolver_WaveEq::view()
{
  PetscErrorCode ierr = 0;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"\nTimeSolver summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   integration algorithm: wave equation\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time interval: %g to %g\n",
                     _initT,_finalT);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   total number of steps taken: %i/%i\n",
                     _stepCount,_maxNumSteps);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   final time reached: %g\n",
                     _currT);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   total run time: %g\n",
                     _runTime);CHKERRQ(ierr);
  return 0;
}


PetscErrorCode OdeSolver_WaveEq::setInitialConds(map<string,Vec>& var)
{
  double startTime = MPI_Wtime();
  PetscErrorCode ierr = 0;

  for (map<string,Vec>::iterator it = var.begin(); it != var.end(); it++) {
    // allocate n: var
    VecDuplicate(var[it->first],&_var[it->first]);
    VecCopy(var[it->first],_var[it->first]);
    // allocate n-1: varPrev
    VecDuplicate(var[it->first],&_varPrev[it->first]);
    VecCopy(var[it->first],_varPrev[it->first]);
    // allocate n+1: varNext
    VecDuplicate(var[it->first],&_varNext[it->first]);
    VecSet(_varNext[it->first],0);
  }

  _runTime += MPI_Wtime() - startTime;
  return ierr;
}


PetscErrorCode OdeSolver_WaveEq::setInitialConds(map<string,Vec>& var,map<string,Vec>& varPrev)
{
  double startTime = MPI_Wtime();
  PetscErrorCode ierr = 0;

  for (map<string,Vec>::iterator it = var.begin(); it != var.end(); it++ ) {
    // allocate n: var
    VecDuplicate(var[it->first],&_var[it->first]);
    VecCopy(var[it->first],_var[it->first]);
    // allocate n-1: varPrev
    VecDuplicate(varPrev[it->first],&_varPrev[it->first]);
    VecCopy(varPrev[it->first],_varPrev[it->first]);
    // allocate n+1: varNext
    VecDuplicate(var[it->first],&_varNext[it->first]);
    VecSet(_varNext[it->first],0);
  }

  _runTime += MPI_Wtime() - startTime;
  return ierr;
}


PetscErrorCode OdeSolver_WaveEq::integrate(IntegratorContext_WaveEq *obj)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();
  int   stopIntegration = 0;

  if (_finalT==_initT) { return ierr; }
  else if (_deltaT==0) { _deltaT = (_finalT-_initT)/_maxNumSteps; }

  // write initial condition
  obj->timeMonitor(_currT,_deltaT,_stepCount,stopIntegration);

  while (_stepCount<_maxNumSteps && _currT<_finalT) {
    _currT = _currT + _deltaT;

    if (_currT>_finalT) { _currT = _finalT; }
    _stepCount++;
    obj->d_dt(_currT,_deltaT,_varNext,_var,_varPrev);

    // accept time step and update
    for (map<string,Vec>::iterator it = _var.begin(); it != _var.end(); it++ ) {
      VecCopy(_var[it->first],_varPrev[it->first]);
      VecCopy(_varNext[it->first],_var[it->first]);
      VecSet(_varNext[it->first],0.0);
    }

    obj->timeMonitor(_currT,_deltaT,_stepCount,stopIntegration);
    if (stopIntegration > 0) {
      PetscPrintf(PETSC_COMM_WORLD,"OdeSolver WaveEq: Detected stop time integration request.\n");
      break;
    }
  }

  _runTime += MPI_Wtime() - startTime;
  return ierr;
}
