#ifndef ODESOLVER_WAVEIMEX_HPP_INCLUDED
#define ODESOLVER_WAVEIMEX_HPP_INCLUDED

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <petscts.h>
#include <string>
#include <vector>

#include "genFuncs.hpp"
#include "integratorContext_WaveEq_Imex.hpp"

using namespace std;

class OdeSolver_WaveEq_Imex
{
  public:

    PetscScalar     _initT,_finalT,_currT,_deltaT;
    PetscInt        _maxNumSteps,_stepCount;
    map<string,Vec> _varNext,_var,_varPrev; // variable at time step: n+1, n, n-1
    map<string,Vec> _varIm, _varImPrev; // integration variable and rate
    int             _lenVar;
    double          _runTime;

  public:

    OdeSolver_WaveEq_Imex(PetscInt maxNumSteps,PetscScalar initT,PetscScalar finalT,PetscScalar deltaT);
    virtual ~OdeSolver_WaveEq_Imex() {};

    PetscErrorCode setStepSize(const PetscReal deltaT);
    PetscErrorCode setInitialStepCount(const PetscReal stepCount);
    PetscErrorCode setTimeRange(const PetscReal initT,const PetscReal finalT);
    PetscErrorCode setInitialConds(map<string,Vec>& varEx, map<string,Vec>& varIm);
    PetscErrorCode setInitialConds(map<string,Vec>& varEx,map<string,Vec>& varExPrev, map<string,Vec>& varIm);
    PetscErrorCode view();
    PetscErrorCode integrate(IntegratorContext_WaveEq_Imex *obj);
    map<string,Vec>& getVar(){return _var;};
};

#endif

