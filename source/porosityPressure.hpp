#ifndef POROSITY_HPP_INCLUDED
#define POROSITY_HPP_INCLUDED

#include <assert.h>
#include <cmath>
#include <vector>
#include "domain.hpp"
#include "fault.hpp"
#include "genFuncs.hpp"
#include "integratorContextEx.hpp"
#include "integratorContextImex.hpp"
#include "sbpOps.hpp"
#include "sbpOps_m_constGrid.hpp"
#include "sbpOps_m_varGrid.hpp"

/* This class solves for the fluid pressure evolution coupled with slip, 
 * permeability and porosity during earthquake cycle simulations. 

 * Governing equations:
 * dp/dt = d/dz (a*dp/dz) - 1/beta*(dphip/dt)
 * where a = k/eta/beta/phie, phip is the plastic porosity, phie elastic
 * beta is assumed to be constant in time
 * a is a variable coefficient as permeability k can change at every time step.
 * dphip/dt = -V/dc(phip - phip0 - eps*ln(V/v0))
 * k/k0 = ((phie + phip)/phi0)^alpha
 * dk/dt = alpha*k0/phi0 * (phie + phip)^(alpha-1) * (dphie/dt + dphip/dt)
 * dphie/dt = dp/dt * ((1 - phie)/Kd - 1/Ks)
 */

using namespace std;

class PorosityPressure
{
public:
  Domain *_D;         // shallow copy of domain
  Vec _p  = NULL;     // pressure (MPa)
  Vec _k  = NULL;     // permeability (m^2)
  Vec _flux   = NULL; // fluid flux = k/eta*(dp/dz) (m/s)
  Vec _phie   = NULL; // elastic rock porosity
  Vec _phip   = NULL; // plastic rock porosity
  Vec _phi    = NULL; // total porosity
  Vec _phie0  = NULL; // reference elastic rock porosity
  Vec _phip0  = NULL; // reference plastic rock porosity
  Vec _phi0   = NULL; // phi_e0 + phi_p0
  Vec _k0     = NULL; // reference permeability at phi0
  Vec _v0     = NULL; // initial slip velocity
  Vec _sN     = NULL; // normal stress (MPa)
  Vec _sNEff  = NULL; // effective normal stress (MPa)
  Vec _T      = NULL; // constant geotherm (K)
  Vec _A      = NULL; // inverse of limiting viscosity (1/MPa/s)
  Vec _B      = NULL; // Ea/R = activation energy/ideal gas constant
  string _phiEvolve;  // include evolution of porosity
  string _kEvolve;    // include evolution of permeability (yes/no)
  string _dilatancy;  // include dilatancy (plastic) (yes/no)
  string _viscous;    // include viscous porosity changes (yes/no)
  string _elastic;    // include elastic porosity changes (yes/no)
  string _viscBound;  // whether to bound viscosity at some value (yes/no)
  string _simu;       // simulation type: inject, undrained, testUndrained, and others
  PetscScalar _alpha; // coefficient for permeability-porosity relation
  PetscScalar _maxVisc;  // maximum shear viscosity
private:
  const char *_file;      // input file
  string      _delim;     // format is: var delim value (without the white space)
  string      _outputDir; // directory for output
  string      _inputDir;  // directory for input

  // domain settings
  Vec         _z = NULL;
  PetscInt    _Nz;    // number of grid points
  PetscScalar _Lz;    // length of domain
  PetscScalar _h;     // grid spacing
  PetscScalar _hinv;  // 1/h
  int         _order;
  PetscInt    _injectLoc; // index of location of injection site (default Nz-1)
  
  // material properties
  Vec _beta_f   = NULL; // fluid compressibility (1/MPa)
  Vec _beta_phi = NULL; // elastic pore compressibility (1/MPa)
  Vec _beta     = NULL; // combined fluid and pore compressibility
  Vec _eta_f    = NULL; // fluid viscosity (Pa.s)
  Vec _rhof     = NULL; // fluid density (g/cm^3)
  Vec _dc       = NULL; // state evolution distance (m)
  Vec _L        = NULL; // porosity evolution distance (m)
  Vec _p_t      = NULL; // dp/dt
  Vec _phiMax   = NULL; // max porosity at zero effective stress
  Vec _phip_t   = NULL; // plastic porosity time evolution
  Vec _phiv_t   = NULL; // viscous porosity time evolution
  Vec _phie_t   = NULL; // elastic porosity time evolution
  Vec _coeff    = NULL; // variable coefficient k/eta/beta for D2
  Vec _bcCoeff  = NULL; // variable coefficient for boundary (flux)
  Vec _pSource  = NULL; // point source term if injection from middle
  Vec _rC       = NULL; // creep rate, A*exp(-Ea/RT)*sNEff, (1/s)

  // initial phi = phi0*exp(-sNEff*s)/(1+r_c*t_e)
  // k = k0*(phi/phi0)^alpha
  PetscScalar _Q;     // injection rate (m/s)
  PetscScalar _phiref;// porosity at k0, zero effective stress
  PetscScalar _phimax;// max porosity at zero effective stress
  PetscScalar _s;     // stress sensitivity parameter for max phi (1/MPa)
  PetscScalar _dt;    // time step, to be used in backward Euler
  PetscScalar _eps;   // dilatancy coefficient (only used for simu = inject)

  // input fields
  vector<double> _phieVals, _phieDepths, _phipVals, _phipDepths;
  vector<double> _rhofVals,_rhofDepths,_betafVals, _betafDepths, _betaphiVals, _betaphiDepths, _kVals, _kDepths;
  // porosity enhancement time t_e = L/V
  vector<double> _etafVals, _etafDepths, _DcVals, _DcDepths, _LVals, _LDepths;
  vector<double> _pVals, _pDepths, _sNVals, _sNDepths, _sNEffVals, _sNEffDepths;
  vector<double> _qVals, _qTimes;
  // creep rate r_c = A*exp(-Ea/RT)*sNEff
  vector<double> _TVals, _TDepths, _AVals, _ADepths, _BVals, _BDepths; // (K, 1/MPa, B = Ea/R)
  
  // linear system
  string      _linSolver;
  KSP         _ksp;
  PC          _pc;
  PetscScalar _kspTol;
  PetscInt    _maxBEIteration;
  PetscScalar _BETol;

  // SBP settings
  SbpOps *_sbp;
  Vec     _phi_beta_inv = NULL;   // 1/phi/beta
  Mat     _Diag_phi_beta = NULL;  // diagonal matrix with 1/phi/beta on diagonal
  Mat     _D2_scale = NULL;        // scale D2 by _Diag_phi_beta
  Vec     _rhs_scale = NULL;       // scale rhs by _Diag_phi_beta
  
  // boundary conditions for setting SBP
  Vec _bcL = NULL;
  Vec _bcT = NULL;
  Vec _bcB = NULL;
  Vec _rhs = NULL;
  VecScatter _scatters1, _scattersN;

  // boundary condition
  string      _bcB_type;     // can be Q (flux) or p (pressure)
  string      _mat_bcB_type; // for setting SBP matrix
  PetscScalar _bcB_flux;     // impose flux on bottom, Q
  PetscScalar _bcB_pressure; // set boundary pressure, p

  // top boundary condition
  string      _bcT_type;     // can be Q (flux) or p (pressure)
  string      _mat_bcT_type; // for setting SBP matrix
  PetscScalar _bcT_flux;     // impose flux on top, Q
  PetscScalar _bcT_pressure; // set boundary pressure, p
  
  // viewers:
  // 1st string = key naming relevant field, e.g. "slip"
  // 2nd PetscViewer = PetscViewer object for file IO
  // 3rd string = full file path name for output
  map<string, pair<PetscViewer, string>> _viewers;

  // disable default copy constructor and assignment operator
  PorosityPressure(const PorosityPressure &that);
  PorosityPressure &operator=(const PorosityPressure &rhs);

  // private member functions
  PetscErrorCode loadSettings(const char *file);
  PetscErrorCode loadFieldsFromFiles();
  PetscErrorCode setFields(Domain &D);
  PetscErrorCode checkInput();
  PetscErrorCode setupSBP(Domain &D);
  PetscErrorCode parseBCs();
  PetscErrorCode imposeBCs();
  PetscErrorCode setupBackEuler(Domain &D);
  PetscErrorCode setupKSP(KSP &ksp, PC &pc, Mat &A);
  PetscErrorCode computeVarCoeff();
  PetscErrorCode transformCoord(const Mat &trans, Vec &vec);
  PetscErrorCode transformCoord(const Mat &trans, Mat &mat);
  PetscErrorCode injectSource(const PetscScalar time);
  PetscErrorCode relativeErr(Vec &p_prev, PetscReal &err);
  
  // constructor and destructor
public:
  PorosityPressure(Domain &D);
  ~PorosityPressure();

  // public member functions
  PetscErrorCode setInitialVals(const Vec &V);
  PetscErrorCode computePhiK();
  PetscErrorCode computeDPhiv();
  PetscErrorCode computeDPhip(const Vec &V);
  PetscErrorCode initiateIntegrand(const PetscScalar time, map<string, Vec> &varEx, map<string, Vec> &varIm);
  PetscErrorCode updateFields(const map<string, Vec> &varEx);
  PetscErrorCode updateFields(const map<string, Vec> &varEx, const map<string, Vec> &varIm);

  // explicit time integration
  PetscErrorCode dphi_dt(const Vec &Phi, const Vec &dPdt, const Vec &dPhipv, Vec &dPhi, const Vec &V);
  PetscErrorCode dphipv_dt(Vec &dPhi, const Vec &V);
  PetscErrorCode dphip_dt(const map<string, Vec> &varEx, map<string, Vec> &dvarEx);
  PetscErrorCode dphip_dt(const Vec &Phip, const Vec &V, Vec &dPhip);
  PetscErrorCode dphie_dt(const Vec &Phie, const Vec &dPdt, Vec &dPhie);
  PetscErrorCode dp_dt(const PetscScalar time, const Vec &P, const Vec &dPhip, Vec &dPdt);
    
  // backward Euler - implicit time stepping
  PetscErrorCode backEuler_phi(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);
  PetscErrorCode backEuler_phie(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);
  PetscErrorCode backEuler_p(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt, const PetscScalar time);
  PetscErrorCode undrained_p(const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt, const PetscScalar time);
  
  // to be called in mediator class, includes d/dt functions above
  PetscErrorCode d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx);
  PetscErrorCode d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);

  // IO
  PetscErrorCode writeContext(const string outputDir);
  PetscErrorCode writeStep(const PetscInt stepCount, const PetscScalar time, const string outputDir);
  PetscErrorCode writeCheckpoint();
  PetscErrorCode loadCheckpoint();

  // functions to be used in mediator classes
  PetscErrorCode computeFlux (Vec &P);
  PetscErrorCode getPressure(Vec &P);
  PetscErrorCode setPressure(const Vec &P);
  PetscErrorCode getPhie(Vec &Phie);
  PetscErrorCode setPhie(const Vec &Phie);
  PetscErrorCode getPhip(Vec &Phip);
  PetscErrorCode setPhip(const Vec &Phip);
  PetscErrorCode getPhi(Vec &Phi);
  PetscErrorCode setPhi(const Vec &Phi);
  PetscErrorCode getPermeability(Vec &K);
  PetscErrorCode setPermeability(const Vec &K);
  PetscErrorCode setSNEff(const Vec &P);
};

#endif
