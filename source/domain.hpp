#ifndef DOMAIN_HPP_INCLUDED
#define DOMAIN_HPP_INCLUDED

#include <petscts.h>
#include <string>
#include <sstream>
#include <fstream>
#include <assert.h>
#include <vector>
#include <iostream>
#include <petscdmda.h>
#include <petscdm.h>
#include "genFuncs.hpp"

/*
 * Class containing basic details of the domain and problem type which
 * must be either kept constant throughout all other classes in the program
 * (such as the size of the domain), or which are used to determine which
 * functions are called within main.cpp.
 *
 * All Vecs should be constructed with the same parallel structure as
 * this class's Vecs y0 (Vec of length Ny), z0 (Vec of length Nz), and y or z
 * (both Ny*Nz long), to ensure that Vec entries match up across classes.
 *
 */

using namespace std;

class Domain
{
public:

  const char *_file;
  string      _delim; // format is: var delim value (without the white space)

  // domain properties
  PetscInt    _order; // accuracy of spatial operators
  PetscInt    _Ny,_Nz; // # of points in y and z directions
  PetscScalar _Ly,_Lz; // (km) domain size in y and z directions

  // problem type
  string      _bulkDeformationType; // options: linearElastic, powerLaw
  string      _momentumBalanceType; // options: quasidynamic, dynamic, quasidynamic_and_dynamic
  string      _operatorType; // matrix-based or matrix-free
  string      _sbpCompatibilityType; // compatible or fullyCompatible
  string      _yGridSpacingType; // variableGridSpacing or constantGridSpacing
  PetscInt    _bCoordTrans;     // grid stretching extent
  string      _zGridSpacingType; // variableGridSpacing or constantGridSpacing
  string      _linSolver; // type of linear solver
  PetscScalar _kspTol;    // tolerance for iterative linear solver
  int         _isMMS; // run MMS test or not
  PetscScalar _vL; // loading velocity
  PetscScalar _vInit;  // initial velocity for steady state
  PetscInt    _computeICs; // 0 for no, and 1 for yes
  string      _initState; // steady or unsteady (start simulation from steady-state or not)
  string      _stateLaw;
  string      _forcingType; // what body forcing term to include (i.e. iceStream)
  PetscScalar _forcingVal;
  PetscScalar _faultTypeScale; // 2 if symmetric fault, 1 if one side is rigid
  string      _thermalCoupling;
  string      _hydraulicCoupling;
  string      _penalty; // test penalty term for tauQS

  // coordinate system
  Vec   _q,_r,_y,_z,_y0,_z0; // q(y), r(z)
  PetscScalar _dq,_dr;  // spacing in q and r

  // boundary conditions
  // Options: freeSurface, tau, outgoingCharacteristics, remoteLoading, symmFault, rigidFault
  // default for qd: bcR (remoteLoading), bcT (freeSurface), bcL (symmFault), bcB (freeSyrface)
  // default for fd: bcR (outgoingCharacteristics), bcT (freeSurface), bcL (symmFault), bcB (outgoingCharacteristics)
  string _qd_bcRType,_qd_bcTType,_qd_bcLType,_qd_bcBType;
  string _fd_bcRType,_fd_bcTType,_fd_bcLType,_fd_bcBType;

  // time stepping settings
  string _timeIntegrator;  // RK32,RK43,RK32_WBE,RK43_WBE
  string _timeControlType; // control type for time step selection (P, PID)
  string _dtType;          // constant or adaptive
  PetscInt _maxStepCount;
  PetscScalar _initTime;
  PetscScalar _initDeltaT;
  PetscScalar _minDeltaT;
  PetscScalar _maxDeltaT;
  PetscScalar _maxTime;
  PetscScalar _timeStepTol;     // tolerance for time step selection 
  string _normType;             // error control norm type
  vector<string> _timeIntInds;  // variables used to compute time step
  vector<double> _scale;        // scaling factor for _timeIntInds
  
  // scatters to take values from body field(s) to 1D fields
  // naming convention for key (string): body2<boundary>, example: "body2L>"
  map<string, VecScatter> _scatters;

  // input/output settings
  PetscInt _stride1D;  // interval to output 1D fields (on the fault)
  PetscInt _stride2D;  // interval to output 2D fields (stress)
  string   _inputDir;  // directory for optional input vectors
  string   _outputDir; // directory for output

  // checkpoint enabling
  PetscInt _ckpt, _ckptNumber, _interval;
  PetscFileMode _outFileMode; // FILE_MODE_WRITE or FILE_MODE_APPEND

  // public member functions  
  Domain(const char *file);
  ~Domain();

  PetscErrorCode view(PetscMPIInt rank);
  PetscErrorCode write();

private:

  // disable default copy constructor and assignment operator
  Domain(const Domain &that);
  Domain& operator=(const Domain &rhs);

  PetscErrorCode loadSettings(const char *file); // load settings from input file
  PetscErrorCode checkInput();
  PetscErrorCode setFields();
  // scatters indices of result vector to new vectors (e.g. displacement -> slip)
  PetscErrorCode setScatters();
  PetscErrorCode testScatters();
};

#endif
