#include "domain.hpp"

#define FILENAME "domain.cpp"

using namespace std;

// member function definitions including constructor
// first type of constructor with 1 parameter
Domain::Domain(const char *file)
  : _file(file),_delim(" = "),_order(4),_Ny(-1),_Nz(-1),_Ly(-1),_Lz(-1),
    _bulkDeformationType("linearElastic"),_momentumBalanceType("quasidynamic"),
    _operatorType("matrix-based"),_sbpCompatibilityType("fullyCompatible"),
    _yGridSpacingType("variableGridSpacing"),_bCoordTrans(-1),
    _zGridSpacingType("constantGridSpacing"),
    _linSolver("AMG"),_kspTol(1e-10),_isMMS(0),_vL(1e-9),
    _vInit(1e-9),_computeICs(1),_initState("steady"),
    _stateLaw("agingLaw"),_forcingType("no"),_faultTypeScale(2.0),
    _thermalCoupling("no"),_hydraulicCoupling("no"),_penalty("no"),
    _q(NULL),_r(NULL),_y(NULL),_z(NULL),_y0(NULL),_z0(NULL),_dq(1),_dr(1),
    _qd_bcRType("remoteLoading"),_qd_bcTType("freeSurface"),
    _qd_bcLType("symmFault"),_qd_bcBType("freeSurface"),
    _fd_bcRType("outGoingCharacteristics"),_fd_bcTType("freeSurface"),
    _fd_bcLType("symmFault"),_fd_bcBType("outGoingCharacteristics"),
    _timeIntegrator("RK43"),_timeControlType("PID"),
    _dtType("adaptive"),_maxStepCount(1e8),_initTime(0),_initDeltaT(1e-3),
    _minDeltaT(-1),_maxDeltaT(1e10),_maxTime(1e15),_timeStepTol(1e-8),
    _normType("L2_absolute"),_stride1D(10),_stride2D(0),
    _inputDir("unspecified_"),_outputDir("data/"),
    _ckpt(0), _ckptNumber(0), _interval(1e4),_outFileMode(FILE_MODE_WRITE)
{
  // load data from file
  loadSettings(_file);

  // load checkpoint values
  if (_ckpt > 0) {
    loadValueFromCheckpoint(_outputDir, "ckptNumber", _ckptNumber);
    if (_ckptNumber > 0) {
      _outFileMode = FILE_MODE_APPEND;
      loadValueFromCheckpoint(_outputDir, "chkpt_currT", _initTime);
      loadValueFromCheckpoint(_outputDir, "chkpt_deltaT", _initDeltaT);
      _computeICs = 0;
    }
  }

  // grid spacing for logical coordinates
  if (_Ny > 1) { _dq = 1.0/(_Ny - 1.0); }
  if (_Nz > 1) { _dr = 1.0/(_Nz - 1.0); }

  checkInput(); // perform some basic value checking to prevent NaNs
  setFields();
  setScatters();
}


// destructor
Domain::~Domain()
{
  VecDestroy(&_q);
  VecDestroy(&_r);
  VecDestroy(&_y);
  VecDestroy(&_z);
  VecDestroy(&_y0);
  VecDestroy(&_z0);

  // set map iterator, free memory from VecScatter
  map<string,VecScatter>::iterator it;
  for (it = _scatters.begin(); it != _scatters.end(); it++) {
    VecScatterDestroy(&it->second);
  }
}


// load settings from input file
PetscErrorCode Domain::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;
  PetscMPIInt rank,size;

  MPI_Comm_size(PETSC_COMM_WORLD,&size); // global number of processors
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank); // current processor number

  // read file inputs
  ifstream infile(file);
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line))
  {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos+_delim.length(),line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after the appearance of a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0,pos);

    // spatial domain properties
    if (var == "order") { _order = atoi(rhs.c_str()); }
    else if (var == "Ny" && _Ny < 0) { _Ny = atoi(rhs.c_str()); }
    else if (var == "Nz" && _Nz < 0) { _Nz = atoi(rhs.c_str()); }
    else if (var == "Ly") { _Ly = atof(rhs.c_str()); }
    else if (var == "Lz") { _Lz = atof(rhs.c_str()); }

    // problem type
    else if (var == "bulkDeformationType") { _bulkDeformationType = rhs; }
    else if (var == "momentumBalanceType") { _momentumBalanceType = rhs; }
    else if (var == "operatorType") { _operatorType = rhs; }
    else if (var == "sbpCompatibilityType") { _sbpCompatibilityType = rhs; }
    else if (var == "yGridSpacingType") { _yGridSpacingType = rhs; }
    else if (var == "zGridSpacingType") { _zGridSpacingType = rhs; }
    else if (var == "bCoordTrans") { _bCoordTrans = atof(rhs.c_str()); }
    else if (var == "linSolver") { _linSolver = rhs.c_str(); }
    else if (var == "kspTol") { _kspTol = atof(rhs.c_str()); }
    else if (var == "isMMS") { _isMMS = atoi(rhs.c_str()); }
    else if (var == "vL") { _vL = atof(rhs.c_str()); }
    else if (var == "vInit") {_vInit = atof(rhs.c_str()); }
    else if (var == "computeICs") { _computeICs = atoi(rhs.c_str()); }
    else if (var == "initState") { _initState = rhs; }
    else if (var == "stateLaw") { _stateLaw = rhs; }
    else if (var == "forcingType") { _forcingType = rhs; }
    else if (var == "bodyForce") { _forcingVal = atof(rhs.c_str()); }
    else if (var == "thermalCoupling") { _thermalCoupling = rhs; }
    else if (var == "hydraulicCoupling") { _hydraulicCoupling = rhs; }
    else if (var == "penalty") { _penalty = rhs; }

    // boundary condition types for quasidynamic momentum balance equation
    else if (var == "momBal_bcR_qd") { _qd_bcRType = rhs; }
    else if (var == "momBal_bcT_qd") { _qd_bcTType = rhs; }
    else if (var == "momBal_bcL_qd") { _qd_bcLType = rhs; }
    else if (var == "momBal_bcB_qd") { _qd_bcBType = rhs; }

    // boundary condition types for fully dynamic momentum balance equation
    else if (var == "momBal_bcR_fd") { _fd_bcRType = rhs; }
    else if (var == "momBal_bcT_fd") { _fd_bcTType = rhs; }
    else if (var == "momBal_bcL_fd") { _fd_bcLType = rhs; }
    else if (var == "momBal_bcB_fd") { _fd_bcBType = rhs; }

    // time domain properties
    else if (var == "timeIntegrator") { _timeIntegrator = rhs; }
    else if (var == "timeControlType") { _timeControlType = rhs; }
    else if (var == "dtType") { _dtType = rhs; }
    else if (var == "maxStepCount") { _maxStepCount = (int)atof(rhs.c_str()); }
    else if (var == "initTime") { _initTime = atof(rhs.c_str()); }
    else if (var == "initDeltaT") { _initDeltaT = atof(rhs.c_str()); }
    else if (var == "minDeltaT") { _minDeltaT = atof(rhs.c_str()); }
    else if (var == "maxDeltaT") { _maxDeltaT = atof(rhs.c_str()); }    
    else if (var == "maxTime") { _maxTime = atof(rhs.c_str()); }
    else if (var == "timeStepTol") { _timeStepTol = atof(rhs.c_str()); }
    else if (var == "normType") { _normType = rhs; }
    else if (var == "timeStepInds") { loadVectorFromInputFile(rhsFull,_timeIntInds); }
    else if (var == "scale") { loadVectorFromInputFile(rhsFull,_scale); }

    // I/O properties
    else if (var == "stride1D"){ _stride1D = (int)atof(rhs.c_str()); }
    else if (var == "stride2D"){ _stride2D = (int)atof(rhs.c_str()); }
    else if (var == "inputDir") { _inputDir = rhs; }
    else if (var == "outputDir") { _outputDir =  rhs; }

    // checkpoint enabling
    else if (var == "enableCheckpointing") { _ckpt = atoi(rhs.c_str()); }
    else if (var == "interval") { _interval = (int)atof(rhs.c_str()); }
  }

  return ierr;
}


// Specified processor prints scalar/string data members to stdout.
PetscErrorCode Domain::view(PetscMPIInt rank)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt localRank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&localRank);

  if (localRank == rank) {
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n\nrank=%i in Domain::view\n",rank); CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"order = %i\n",_order);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"Ny = %i\n",_Ny);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"Nz = %i\n",_Nz);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"Ly = %e\n",_Ly);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"Lz = %e\n",_Lz);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"isMMS = %i\n",_isMMS);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"computeICs = %i\n",_computeICs);CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"momBalType = %s\n",_momentumBalanceType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"bulkDeformationType = %s\n",_bulkDeformationType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"operatorType = %s\n",_operatorType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"sbpCompatibilityType = %s\n",_sbpCompatibilityType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"yGridSpacingType = %s\n",_yGridSpacingType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"zGridSpacingType = %s\n",_zGridSpacingType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"outputDir = %s\n",_outputDir.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"\n");CHKERRQ(ierr);

    // boundary conditions for momentum balance equation
    ierr = PetscPrintf(PETSC_COMM_SELF,"momBal_bcR = %s\n",_qd_bcRType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"momBal_bcT = %s\n",_qd_bcTType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"momBal_bcL = %s\n",_qd_bcLType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"momBal_bcB = %s\n",_qd_bcBType.c_str());CHKERRQ(ierr);

    // other processes
    ierr = PetscPrintf(PETSC_COMM_SELF,"thermalCoupling = %s\n",_thermalCoupling.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"hydraulicCoupling = %s\n",_hydraulicCoupling.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"forcingType = %s\n",_forcingType.c_str());CHKERRQ(ierr);
    ierr = PetscPrintf(PETSC_COMM_SELF,"faultTypeScale = %g\n",_faultTypeScale);CHKERRQ(ierr);
  }

  return ierr;
}


// Check that required fields have been set by the input file
PetscErrorCode Domain::checkInput()
{
  PetscErrorCode ierr = 0;

  // check problem type
  assert(_bulkDeformationType == "linearElastic" ||
         _bulkDeformationType == "powerLaw");

  assert(_sbpCompatibilityType == "fullyCompatible" ||
         _sbpCompatibilityType == "compatible");

  assert(_yGridSpacingType == "variableGridSpacing" ||
         _yGridSpacingType == "constantGridSpacing");

  assert(_zGridSpacingType == "variableGridSpacing" ||
         _zGridSpacingType == "constantGridSpacing");

  assert(_momentumBalanceType == "quasidynamic" ||
         _momentumBalanceType == "dynamic" ||
         _momentumBalanceType == "quasidynamic_and_dynamic" ||
         _momentumBalanceType == "steadyStateIts");

  assert(_computeICs == 0 || _computeICs == 1);
  if (_ckptNumber > 0) { assert(_computeICs == 0); }

  assert(_thermalCoupling == "coupled" ||
         _thermalCoupling == "uncoupled" ||
         _thermalCoupling == "no" );

  assert(_hydraulicCoupling == "coupled" ||
         _hydraulicCoupling == "uncoupled" ||
         _hydraulicCoupling == "no" );

  assert(_forcingType == "iceStream" || _forcingType == "no" );

  assert(_stateLaw == "agingLaw"
	 || _stateLaw == "slipLaw"
	 || _stateLaw == "flashHeating"
	 || _stateLaw == "constantState" );
  
  if (_stateLaw == "flashHeating") {
    assert(_thermalCoupling != "no");
  }

  // check spatial settings
  assert(_order == 2 || _order == 4);
  assert(_Ly > 0 && _Lz > 0);
  assert(_dq > 0 && !std::isnan(_dq));
  assert(_dr > 0 && !std::isnan(_dr));

  // check linear solver settings
  assert(_linSolver == "MUMPSCHOLESKY" ||
         _linSolver == "MUMPSLU" ||
	 _linSolver == "AMG" ||
         _linSolver == "CG" );

  if (_linSolver == "CG" || _linSolver == "AMG") {
    assert(_kspTol >= 1e-14);
  }

  // check boundary condition types for momentum balance equation
  assert(_qd_bcRType == "freeSurface" || _qd_bcRType == "remoteLoading");
  assert(_qd_bcTType == "freeSurface" || _qd_bcTType == "remoteLoading");
  assert(_qd_bcLType == "symmFault" || _qd_bcLType == "rigidFault");
  assert(_qd_bcBType == "freeSurface" || _qd_bcBType == "remoteLoading" );
  assert(_fd_bcRType == "freeSurface" || _fd_bcRType == "outGoingCharacteristics");
  assert(_fd_bcTType == "freeSurface" || _fd_bcTType == "outGoingCharacteristics");
  assert(_fd_bcLType == "symmFault" || _fd_bcLType == "rigidFault");
  assert(_fd_bcBType == "freeSurface" || _fd_bcBType == "outGoingCharacteristics");

  // check time integrator settings
  assert(_timeIntegrator == "FEuler" ||
	 _timeIntegrator == "RK32" ||
	 _timeIntegrator == "RK43" ||
	 _timeIntegrator == "RK32_WBE" ||
	 _timeIntegrator == "RK43_WBE" );

  assert(_timeControlType == "P" ||
	 _timeControlType == "PID" );

  if (_initDeltaT < _minDeltaT || _initDeltaT < 1e-14) {
    _initDeltaT = _minDeltaT;
  }
  assert(_initTime >= 0);
  assert(_maxTime >= 0 && _maxTime>=_initTime);
  assert(_timeStepTol >= 1e-14);
  assert(_maxDeltaT >= 1e-14  &&  _maxDeltaT >= _minDeltaT);
  assert(_initDeltaT > 0 && _initDeltaT >= _minDeltaT && _initDeltaT <= _maxDeltaT);

  // check checkpoint settings
  assert(_ckpt >= 0 && _ckptNumber >= 0);
  assert(_interval >= 0);

  return ierr;
}


// Save all scalar fields to text file named domain.txt in output directory.
// Also writes out coordinate systems q, r, y, z into respective files in output directory
// Note that only the rank 0 processor's values will be saved.
PetscErrorCode Domain::write()
{
  PetscErrorCode ierr = 0;

  // output scalar fields
  string str = _outputDir + "domain.txt";

  PetscViewer viewer;
  // write into file using PetscViewer
  ierr = PetscViewerCreate(PETSC_COMM_WORLD, &viewer); CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer, PETSCVIEWERASCII); CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(viewer, FILE_MODE_WRITE); CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer, str.c_str()); CHKERRQ(ierr);

  // domain properties
  ierr = PetscViewerASCIIPrintf(viewer,"order = %i\n",_order);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Ny = %i\n",_Ny);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Nz = %i\n",_Nz);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Ly = %g # (km)\n",_Ly);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Lz = %g # (km)\n",_Lz);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"vL = %g\n",_vL);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  // boundary conditions for momentum balance equation
  ierr = PetscViewerASCIIPrintf(viewer,"momBalType = %s\n",_momentumBalanceType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcR_qd = %s\n",_qd_bcRType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcT_qd = %s\n",_qd_bcTType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcL_qd = %s\n",_qd_bcLType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcB_qd = %s\n",_qd_bcBType.c_str());CHKERRQ(ierr);

  if (_momentumBalanceType == "quasidynamic_and_dynamic" || _momentumBalanceType =="dynamic") {
    ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcR_fd = %s\n",_fd_bcRType.c_str());CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcT_fd = %s\n",_fd_bcTType.c_str());CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcL_fd = %s\n",_fd_bcLType.c_str());CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer,"momBal_bcB_fd = %s\n",_fd_bcBType.c_str());CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  // problem properties
  ierr = PetscViewerASCIIPrintf(viewer,"stateLaw = %s\n",_stateLaw.c_str());CHKERRQ(ierr);  
  ierr = PetscViewerASCIIPrintf(viewer,"bulkDeformationType = %s\n",_bulkDeformationType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"operatorType = %s\n",_operatorType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"yGridSpacingType = %s\n",_yGridSpacingType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"zGridSpacingType = %s\n",_zGridSpacingType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"linSolver = %s\n",_linSolver.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"bCoordTrans = %.15e\n",_bCoordTrans);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"isMMS = %i\n",_isMMS);CHKERRQ(ierr);  
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"outputDir = %s\n",_outputDir.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  // time integration settings
  ierr = PetscViewerASCIIPrintf(viewer,"timeIntegrator = %s\n",_timeIntegrator.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"timeControlType = %s\n",_timeControlType.c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"maxStepCount = %i\n",_maxStepCount);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride1D = %i\n",_stride1D);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride2D = %i\n",_stride1D);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"initTime = %.15e # (s)\n",_initTime);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"maxTime = %.15e # (s)\n",_maxTime);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"minDeltaT = %.15e # (s)\n",_minDeltaT);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"maxDeltaT = %.15e # (s)\n",_maxDeltaT);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"initDeltaT = %.15e # (s)\n",_initDeltaT);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"timeStepTol = %g\n",_timeStepTol);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"timeIntInds = %s\n",vector2str(_timeIntInds).c_str());CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"normType = %s\n",_normType.c_str());CHKERRQ(ierr);
  if (_scale.size() > 0) {
    ierr = PetscViewerASCIIPrintf(viewer,"scale = %s\n",vector2str(_scale).c_str());CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);
  
  // checkpoint settings
  ierr = PetscViewerASCIIPrintf(viewer,"checkpoint enabled = %i\n",_ckpt);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"checkpoint number = %i\n",_ckptNumber);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"checkpoint interval = %i\n",_interval);CHKERRQ(ierr);

  // get number of processors
  PetscMPIInt size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  ierr = PetscViewerASCIIPrintf(viewer,"numProcessors = %i\n",size);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  // output context vectors
  //ierr = writeVec(_q,_outputDir + "q"); CHKERRQ(ierr);
  //ierr = writeVec(_r,_outputDir + "r"); CHKERRQ(ierr);
  ierr = writeVec(_y,_outputDir + "y"); CHKERRQ(ierr);
  ierr = writeVec(_z,_outputDir + "z"); CHKERRQ(ierr);

  return ierr;
}


// construct coordinate transform, setting vectors q, r, y, z
PetscErrorCode Domain::setFields()
{
  PetscErrorCode ierr = 0;

  // generate vector _y with size _Ny*_Nz
  ierr = VecCreate(PETSC_COMM_WORLD,&_y); CHKERRQ(ierr);
  ierr = VecSetSizes(_y,PETSC_DECIDE,_Ny*_Nz); CHKERRQ(ierr);
  ierr = VecSetFromOptions(_y); CHKERRQ(ierr);

  ierr = VecDuplicate(_y,&_z); CHKERRQ(ierr);
  ierr = VecDuplicate(_y,&_q); CHKERRQ(ierr);
  ierr = VecDuplicate(_y,&_r); CHKERRQ(ierr);

  // construct q and r
  {
    PetscInt Ii,Istart,Iend;
    ierr = VecGetOwnershipRange(_q,&Istart,&Iend);CHKERRQ(ierr);
    PetscScalar *q,*r;
    VecGetArray(_q,&q);
    VecGetArray(_r,&r);
    PetscInt Jj = 0;
    for (Ii=Istart;Ii<Iend;Ii++) {
      q[Jj] = _dq*(Ii/_Nz);
      r[Jj] = _dr*(Ii-_Nz*(Ii/_Nz));
      Jj++;
    }
    VecRestoreArray(_q,&q);
    VecRestoreArray(_r,&r);
  }

  // construct y (grid stretching enabled when _bCoordTrans > 0)
  bool fileExists = 0;
  if (_ckptNumber > 0) { loadVecFromInputFile(_y, _outputDir, "y",fileExists); }
  // fileExists = 0 means the file does not exist
  if (fileExists == 0) { loadVecFromInputFile(_y, _inputDir, "y",fileExists); }
  // construct y when no input y exists
  if (fileExists == 0) {
    if (_bCoordTrans <= 0) {
      VecCopy(_q,_y);
      VecScale(_y,_Ly);
    }
    else {
      PetscInt Ii,Istart,Iend;
      ierr = VecGetOwnershipRange(_q,&Istart,&Iend);CHKERRQ(ierr);
      const PetscScalar *q;
      PetscScalar *y;
      VecGetArrayRead(_q,&q);
      VecGetArray(_y,&y);
      PetscInt Jj = 0;
      for (Ii=Istart;Ii<Iend;Ii++) {
         y[Jj] = _Ly * sinh(_bCoordTrans*q[Jj])/sinh(_bCoordTrans);
        Jj++;
      }
      VecRestoreArrayRead(_q,&q);
      VecRestoreArray(_y,&y);
    }
  }

  // construct z
  fileExists = 0;
  if (_ckptNumber > 0) { loadVecFromInputFile(_z, _outputDir, "z",fileExists); }
  if (fileExists == 0) { loadVecFromInputFile(_z, _inputDir, "z",fileExists); }
  // construct z when no input z exists
  if (fileExists == 0) {
    VecCopy(_r,_z);
    VecScale(_z,_Lz);
  }

  return ierr;
}


// scatters values from one vector to another
// used to get slip on the fault from the displacement vector, i.e., slip = u(1:Nz); shear stress on the fault from the stress vector sxy; surface displacement; surface heat flux
PetscErrorCode Domain::setScatters()
{
  PetscErrorCode ierr = 0;

  // some example 1D vectors
  VecCreate(PETSC_COMM_WORLD,&_y0); VecSetSizes(_y0,PETSC_DECIDE,_Nz); VecSetFromOptions(_y0); VecSet(_y0,0.0);
  VecCreate(PETSC_COMM_WORLD,&_z0); VecSetSizes(_z0,PETSC_DECIDE,_Ny); VecSetFromOptions(_z0); VecSet(_z0,0.0);


  { // set up scatter context to take values for y=0 from body field and put them on a Vec of size Nz
    PetscInt *indices; PetscMalloc1(_Nz,&indices);
    for (PetscInt Ii=0; Ii<_Nz; Ii++) { indices[Ii] = Ii; }
    IS is;
    ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Nz, indices, PETSC_COPY_VALUES, &is);
    ierr = VecScatterCreate(_y, is, _y0, is, &_scatters["body2L"]); CHKERRQ(ierr);
    PetscFree(indices);
    ISDestroy(&is);
  }

  { // set up scatter context to take values for y=Ly from body field and put them on a Vec of size Nz
    // indices to scatter from
    PetscInt *fi; PetscMalloc1(_Nz,&fi);
    for (PetscInt Ii=0; Ii<_Nz; Ii++) { fi[Ii] = Ii + (_Ny*_Nz-_Nz); }
    IS isf; ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Nz, fi, PETSC_COPY_VALUES, &isf);

    // indices to scatter to
    PetscInt *ti; PetscMalloc1(_Nz,&ti);
    for (PetscInt Ii=0; Ii<_Nz; Ii++) { ti[Ii] = Ii; }
    IS ist; ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Nz, ti, PETSC_COPY_VALUES, &ist);

    ierr = VecScatterCreate(_y, isf, _y0, ist, &_scatters["body2R"]); CHKERRQ(ierr);
    PetscFree(fi); PetscFree(ti);
    ISDestroy(&isf); ISDestroy(&ist);
  }

  { // set up scatter context to take values for z=0 from body field and put them on a Vec of size Ny
    // indices to scatter from
    IS isf; ierr = ISCreateStride(PETSC_COMM_WORLD, _Ny, 0, _Nz, &isf);

    // indices to scatter to
    PetscInt *ti; PetscMalloc1(_Ny,&ti);
    for (PetscInt Ii=0; Ii<_Ny; Ii++) { ti[Ii] = Ii; }
    IS ist; ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Ny, ti, PETSC_COPY_VALUES, &ist);

    ierr = VecScatterCreate(_y, isf, _z0, ist, &_scatters["body2T"]); CHKERRQ(ierr);
    PetscFree(ti);
    ISDestroy(&isf); ISDestroy(&ist);
  }

  { // set up scatter context to take values for z=Lz from body field and put them on a Vec of size Ny
    // indices to scatter from
    IS isf; ierr = ISCreateStride(PETSC_COMM_WORLD, _Ny, _Nz-1, _Nz, &isf);

    // indices to scatter to
    PetscInt *ti; PetscMalloc1(_Ny,&ti);
    for (PetscInt Ii=0; Ii<_Ny; Ii++) { ti[Ii] = Ii; }
    IS ist; ierr = ISCreateGeneral(PETSC_COMM_WORLD, _Ny, ti, PETSC_COPY_VALUES, &ist);

    ierr = VecScatterCreate(_y, isf, _z0, ist, &_scatters["body2B"]); CHKERRQ(ierr);
    PetscFree(ti);
    ISDestroy(&isf); ISDestroy(&ist);
  }

  return ierr;
}


// create example vector for testing purposes
PetscErrorCode Domain::testScatters() {
  PetscErrorCode ierr = 0;

  Vec body;
  ierr = VecDuplicate(_y,&body); CHKERRQ(ierr);
  PetscInt      Istart,Iend,Jj = 0;
  PetscScalar   *bodyA;
  ierr = VecGetOwnershipRange(body,&Istart,&Iend); CHKERRQ(ierr);
  ierr = VecGetArray(body,&bodyA); CHKERRQ(ierr);

  for (PetscInt Ii = Istart; Ii<Iend; Ii++) {
    PetscInt Iy = Ii/_Nz;
    PetscInt Iz = (Ii-_Nz*(Ii/_Nz));
    bodyA[Jj] = 10.*Iy + Iz;
    ierr = PetscPrintf(PETSC_COMM_WORLD,"%i %i %g\n",Iy,Iz,bodyA[Jj]); CHKERRQ(ierr);
    Jj++;
  }
  ierr = VecRestoreArray(body,&bodyA); CHKERRQ(ierr);

  // test various mappings
  // y = 0: mapping to L
  Vec out;
  ierr = VecDuplicate(_y0,&out); CHKERRQ(ierr);
  ierr = VecScatterBegin(_scatters["body2L"], body, out, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(_scatters["body2L"], body, out, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecDestroy(&out); CHKERRQ(ierr);

  // y = Ly: mapping to R
  Vec out1;
  ierr = VecDuplicate(_y0,&out1); CHKERRQ(ierr);
  ierr = VecSet(out1,-1.); CHKERRQ(ierr);
  ierr = VecScatterBegin(_scatters["body2R"], body, out1, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(_scatters["body2R"], body, out1, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecDestroy(&out1); CHKERRQ(ierr);

  // z=0: mapping to T
  Vec out2;
  ierr = VecDuplicate(_z0,&out2); CHKERRQ(ierr);
  ierr = VecSet(out2,-1.); CHKERRQ(ierr);
  ierr = VecScatterBegin(_scatters["body2T"], body, out2, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(_scatters["body2T"], body, out2, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // z=Lz: mapping to B
  ierr = VecScatterBegin(_scatters["body2B"], body, out2, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(_scatters["body2B"], body, out2, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // z=Lz: mapping from B to body
  ierr = VecScatterBegin(_scatters["body2T"], out2, body, INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);
  ierr = VecScatterEnd(_scatters["body2T"], out2, body, INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);

  ierr = VecDestroy(&out2); CHKERRQ(ierr);
  ierr = VecDestroy(&body); CHKERRQ(ierr);

  return ierr;
}
