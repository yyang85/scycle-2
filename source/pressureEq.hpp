#ifndef PRESSURE_HPP_INCLUDED
#define PRESSURE_HPP_INCLUDED

#include <petscksp.h>
#include <cmath>
#include <assert.h>
#include <vector>
#include "genFuncs.hpp"
#include "domain.hpp"
#include "fault.hpp"
#include "sbpOps.hpp"
#include "sbpOps_m_constGrid.hpp"
#include "sbpOps_m_varGrid.hpp"
#include "integratorContextEx.hpp"
#include "integratorContextImex.hpp"

/* This class solves for the fluid pressure evolution coupled with slip during 
 * earthquake cycle simulations. It solves for the permeability changes due to 
 * fault slip and pore pressure. Results show a significant change of fluid pressure
 * during earthquake cycles, implying possible existence of overpressure and fault-
 * valve behavior in Earth's crust. Uses a 1D vertical strike-slip fault.
 * If coupled, permeability can be slip-dependent and/or pressure-dependent.
 * If uncoupled, permeability is constant.
 */

/* Governing equation:
 * a * (dp'/dt) = d/dz (b * dp'/dz)
 * where a = rho * n * beta, b = rho * k / eta,
 * p' = p - rho*g*z is the deviation from hydrostatic pressure.
 * rho, n, beta (thus a) is assumed constant in time, but can vary in space.
 * b is a variable coefficient as permeability k can change at every time step.
 * Dirichlet or Neumann boundary conditions are imposed on the top and bottom.
 * Boundary conditions are enforced via SAT penalty terms.
 */

using namespace std;

class PressureEq
{
public:
  Domain     *_D;          // shallow copy of domain
  Vec         _p = NULL;   // pressure
  Vec         _k_p = NULL; // final permeability
  string      _permSlipDependent, _permPressureDependent;
  
private:
  const char *_file;      // input file
  string      _delim;     // format is: var delim value (without the white space)
  string      _outputDir; // directory for output
  string      _inputDir;  // directory for input
  string      _hydraulicTimeIntType; // time integration type
  int         _guessSteadyStateICs;
  Vec         _z;     // vector of z-coordinates on fault

  // material properties
  Vec _n_p     = NULL;   // solid porosity
  Vec _beta_p  = NULL;   // fluid compressibility
  Vec _eta_p   = NULL;   // fluid viscosity
  Vec _rho_f   = NULL;   // fluid density
  Vec _k_slip  = NULL;   // slip-dependent permeability
  Vec _k_press = NULL;   // pressure-dependent permeability
  Vec _kL_p    = NULL;   // slip-dependent permeability's characteristic length
  Vec _kT_p    = NULL;   // slip-dependent permeability's characteristic time scale
  Vec _kmin_p  = NULL;   // minimum permeability
  Vec _kmax_p  = NULL;   // maximum permeability
  Vec _kmin2_p = NULL;   // pressure-dependent minimum permeability
  Vec _sigma_p = NULL;   // pressure-dependent characteristic effective normal stress
  Vec _sN      = NULL;   // total normal stress
  Vec _p_t     = NULL;   // dp/dt
  Vec _flux    = NULL;   // flux
  Vec _rhs     = NULL;   // right-hand side vector for linear system
  Vec _coeff   = NULL;   // coefficient for updating SBP matrices
  Vec _hydroBC = NULL;   // add hydrostatic flux to bc
  Vec _gSource = NULL;   // source term from gravity
  
  // boundary conditions for setting SBP
  Vec _bcL = NULL, _bcT = NULL, _bcB = NULL;
  
  Mat _Diag_n_beta = NULL;  // make 1/(rho * n * beta) a diagonal matrix
  Mat _D2_n_beta = NULL;    // D2 / (rho * n * beta)
 
  PetscScalar _g;        // gravitational acceleration

  // boundary condition
  string      _bcB_type;     // can be Q (flux) or p (pressure)
  string      _mat_bcB_type; // for setting SBP matrix
  PetscScalar _bcB_flux;     // impose flux on bottom, Q
  PetscScalar _bcB_pressure; // set boundary pressure, p

  // top boundary condition
  string      _bcT_type;     // can be Q (flux) or p (pressure)
  string      _mat_bcT_type; // for setting SBP matrix
  PetscScalar _bcT_flux;     // impose flux on top, Q
  PetscScalar _bcT_pressure; // set boundary pressure, p  
  
  // iteration settings
  int         _maxBEIteration; // maximum Backward Euler iteration
  double      _BETol; // tolerance for fixed-point iteration, used in BE

  // linear system
  string      _linSolver;
  PetscScalar _kspTol;
  KSP         _ksp;
  PC          _pc;
  SbpOps     *_sbp;
  int         _linSolveCount;
  
  // input fields
  vector<double> _n_pVals, _n_pDepths, _beta_pVals, _beta_pDepths, _k_pVals, _k_pDepths;
  vector<double> _eta_pVals, _eta_pDepths, _rho_fVals, _rho_fDepths;
  vector<double> _pVals, _pDepths, _dpVals, _dpDepths;
  vector<double> _kL_pVals, _kL_pDepths, _kT_pVals, _kT_pDepths, _kmin_pVals, _kmin_pDepths, _kmax_pVals, _kmax_pDepths;
  vector<double> _kmin2_pVals, _kmin2_pDepths, _sigma_pVals, _sigma_pDepths;
  vector<double> _sigmaNVals,_sigmaNDepths;

  // run time monitoring
  double _writeTime, _linSolveTime, _ptTime, _startTime, _miscTime;
  double _invTime;

  // analytical solution and source terms for MMS
  Vec _pA = NULL;
  Vec _source = NULL;
  Vec _Hxsource = NULL;
  
  // viewers:
  // 1st string = key naming relevant field, e.g. "slip"
  // 2nd PetscViewer = PetscViewer object for file IO
  // 3rd string = full file path name for output
  map<string, pair<PetscViewer, string>> _viewers;

  VecScatter _scatters;

  // disable default copy constructor and assignment operator
  PressureEq(const PressureEq &that);
  PressureEq &operator=(const PressureEq &rhs);

  // private member functions
  PetscErrorCode setupSBP(const double time);
  PetscErrorCode setupBackEuler(Domain &D);
  PetscErrorCode setupKSP(KSP &ksp, PC &pc, Mat &A);
  PetscErrorCode parseBCs();
  PetscErrorCode imposeBCs();
  PetscErrorCode computeInitialSSPressure();
  PetscErrorCode computeVarCoeff(Vec &coeff);
  PetscErrorCode gravitySource();
  PetscErrorCode computeFlux();
  PetscErrorCode updatePermPressure(const float rate);
  PetscErrorCode transformCoord(const Mat &trans, Vec &vec);
  PetscErrorCode transformCoord(const Mat &trans, Mat &mat);
  PetscErrorCode relativeErr(Vec &p_prev, PetscReal &err);    
  
  // constructor and destructor
public:
  PressureEq(Domain &D);
  ~PressureEq();

  // public member functions
  PetscErrorCode getPressure(Vec& P);
  PetscErrorCode setPressure(const Vec& P);
  PetscErrorCode getPermeability(Vec& K);
  PetscErrorCode setPremeability(const Vec& K);
  PetscErrorCode setFields(Domain &D);
  PetscErrorCode loadSettings(const char *file);
  PetscErrorCode checkInput();

  PetscErrorCode initiateIntegrand(const PetscScalar time, map<string, Vec> &varEx, map<string, Vec> &varIm);
  PetscErrorCode updateFields(const map<string, Vec> &varEx);
  PetscErrorCode updateFields(const map<string, Vec> &varEx, const map<string, Vec> &varIm);
  PetscErrorCode loadFieldsFromFiles();

  // ============ explicit time integration =======================
  PetscErrorCode d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx);
  // time derivative of pressure
  PetscErrorCode dp_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx);
  PetscErrorCode dp_dt(const PetscScalar time, const Vec& P, Vec& dPdt);
  PetscErrorCode d_dt_mms(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx);


  // ============= implicit time integration ======================
  PetscErrorCode d_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);
  // backward Euler
  PetscErrorCode backEuler(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);
  // time derivative of permeability
  PetscErrorCode dk_dt(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx);
  PetscErrorCode dk_dt(const PetscScalar time, const Vec slipVel, const Vec &K, Vec &dKdt);

  // MMS test for backward Euler
  PetscErrorCode backEuler_mms(const PetscScalar time, const map<string, Vec> &varEx, map<string, Vec> &dvarEx, map<string, Vec> &varIm, const map<string, Vec> &varImo, const PetscScalar dt);

  // IO
  PetscErrorCode view(const double totRunTime);
  PetscErrorCode writeContext(const string outputDir);
  PetscErrorCode writeStep(const PetscInt stepCount, const PetscScalar time);
  PetscErrorCode writeStep(const PetscInt stepCount, const PetscScalar time, const string outputDir);
  PetscErrorCode writeCheckpoint();
  PetscErrorCode loadCheckpoint();
  
  // MMS error
  PetscErrorCode measureMMSError(const double time);
  static double zzmms_pSource1D(const double z, const double t);
  static double zzmms_pA1D(const double y, const double t);
  static double zzmms_pt1D(const double z, const double t);
};

#endif
