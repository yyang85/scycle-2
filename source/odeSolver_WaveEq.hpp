#ifndef ODESOLVER_WAVEEQ_HPP_INCLUDED
#define ODESOLVER_WAVEEQ_HPP_INCLUDED

#include <algorithm>
#include <assert.h>
#include <cmath>
#include <petscts.h>
#include <string>
#include <vector>

#include "genFuncs.hpp"
#include "integratorContext_WaveEq.hpp"

using namespace std;

class OdeSolver_WaveEq
{
  public:

    PetscScalar     _initT,_finalT,_currT,_deltaT;
    PetscInt        _maxNumSteps,_stepCount;
    map<string,Vec> _varNext,_var,_varPrev; // variable at time step: n+1, n, n-1
    int             _lenVar;
    double          _runTime;

  public:

    OdeSolver_WaveEq(PetscInt maxNumSteps,PetscScalar initT,PetscScalar finalT,PetscScalar deltaT);
    ~OdeSolver_WaveEq();

    PetscErrorCode setStepSize(const PetscReal deltaT);
    PetscErrorCode setInitialStepCount(const PetscReal stepCount);
    PetscErrorCode setTimeRange(const PetscReal initT,const PetscReal finalT);
    PetscErrorCode setInitialConds(map<string,Vec>& var);
    PetscErrorCode setInitialConds(map<string,Vec>& var, map<string,Vec>& varPrev);
    PetscErrorCode view();
    PetscErrorCode integrate(IntegratorContext_WaveEq *obj);
    map<string,Vec>& getVar() { return _var; };
};

#endif

