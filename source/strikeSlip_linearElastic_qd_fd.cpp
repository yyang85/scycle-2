#include "strikeSlip_linearElastic_qd_fd.hpp"

#define FILENAME "strikeSlip_linearElastic_qd_fd.cpp"

using namespace std;


strikeSlip_linearElastic_qd_fd::strikeSlip_linearElastic_qd_fd(Domain&D)
  : _D(&D),_delim(D._delim),_inputDir(D._inputDir),_outputDir(D._outputDir),
    _computeICs(D._computeICs),_faultTypeScale(D._faultTypeScale),
    _cycleCount(0),_maxNumCycles(1e3),_deltaT(-1), _CFL(-1),_y(&D._y),_z(&D._z),
    _inDynamic(false),_allowed(false),_trigger_qd2fd(1e-3),_trigger_fd2qd(1e-3),
    _limit_qd(10*D._vL), _limit_fd(1e-1),_limit_stride_fd(-1),_u0(NULL),
    _stride1D_qd(10),_stride2D_qd(10),_stride1D_fd(10),_stride2D_fd(10),
    _stride1D_fd_end(10),_stride2D_fd_end(10),_currTime(0),_stepCount(0),
    _timeV1D(NULL),_dtimeV1D(NULL),_timeV2D(NULL),_dtimeV2D(NULL),
    _regime1DV(NULL),_regime2DV(NULL),
    _integrateTime(0),_writeTime(0),_linSolveTime(0),_factorTime(0),
    _startTime(MPI_Wtime()),_miscTime(0),_dynTime(0), _qdTime(0),
    _mat_fd_bcRType("Neumann"),_mat_fd_bcTType("Neumann"),
    _mat_fd_bcLType("Neumann"),_mat_fd_bcBType("Neumann"),
    _quadEx_qd(NULL),_quadImex_qd(NULL), _quadWaveEx(NULL),
    _fault_qd(NULL),_fault_fd(NULL), _material(NULL),_he(NULL),_p(NULL)
{
  loadSettings(D._file);
  _currTime = _D->_initTime;
  checkInput();
  parseBCs();

  _body2fault = &(D._scatters["body2L"]);
  // fault for quasidynamic problem
  _fault_qd = new Fault_qd(D,D._scatters["body2L"],_faultTypeScale);
  // fault for fully dynamic problem
  _fault_fd = new Fault_fd(D, D._scatters["body2L"],_faultTypeScale);

  // heat equation
  if (_D->_thermalCoupling != "no") {
    _he = new HeatEquation(D);
  }
  // flash heating with temperature evolution
  if (_D->_thermalCoupling != "no" && _D->_stateLaw == "flashHeating") {
    _fault_qd->setThermalFields(_he->_Tamb,_he->_k,_he->_c);
  }

  // pressure diffusion equation
  if (_D->_hydraulicCoupling != "no") {
    _p = new PorosityPressure(D);
    // copy sN solved from pressure equation (pressure profile + constant sNEff)
    if (_D->_hydraulicCoupling == "coupled") {
      VecCopy(_p->_sN, _fault_qd->_sN);
      VecCopy(_p->_sN, _fault_fd->_sN);
    }
  }

  // set effective normal stress if not loading from checkpoint
  if (_D->_ckptNumber == 0 && _D->_hydraulicCoupling == "coupled") {
    _fault_qd->setSNEff(_p->_p);
  }

  // initiate momentum balance equation
  if (_computeICs == 1) {
    _material = new LinearElastic(D,_mat_qd_bcRType,_mat_qd_bcTType,"Neumann",_mat_qd_bcBType);
  }
  else {
    _material = new LinearElastic(D,_mat_qd_bcRType,_mat_qd_bcTType,_mat_qd_bcLType,_mat_qd_bcBType);
  }
  computePenaltyVectors();

  // body forcing term for ice stream
  _forcingTerm = NULL;
  _forcingTermPlain = NULL;
  if (_D->_forcingType == "iceStream") {
    constructIceStreamForcingTerm();
  }

  computeTimeStep(); // compute fully dynamic time step size
}


strikeSlip_linearElastic_qd_fd::~strikeSlip_linearElastic_qd_fd()
{
  // adaptive time stepping containers
  map<string,Vec>::iterator it;
  for (it = _varQSEx.begin(); it!=_varQSEx.end(); it++ ) {
    VecDestroy(&it->second);
  }
  for (it = _varIm.begin(); it!=_varIm.end(); it++ ) {
    VecDestroy(&it->second);
  }

  // wave equation time stepping containers
  for (it = _varFD.begin(); it!=_varFD.end(); it++ ) {
    VecDestroy(&it->second);
  }
  for (it = _varFDPrev.begin(); it!=_varFDPrev.end(); it++ ) {
    VecDestroy(&it->second);
  }

  PetscViewerDestroy(&_timeV1D);
  PetscViewerDestroy(&_dtimeV1D);
  PetscViewerDestroy(&_timeV2D);
  PetscViewerDestroy(&_regime1DV);
  PetscViewerDestroy(&_regime2DV);
  VecDestroy(&_u0);
  VecDestroy(&_ay);
  VecDestroy(&_forcingTerm);
  VecDestroy(&_forcingTermPlain);

  delete _quadImex_qd;    _quadImex_qd = NULL;
  delete _quadEx_qd;      _quadEx_qd = NULL;
  delete _material;       _material = NULL;
  delete _fault_qd;       _fault_qd = NULL;
  delete _fault_fd;       _fault_fd = NULL;
  delete _he;             _he = NULL;
  delete _p;              _p = NULL;
}


// loads settings from the input text file
PetscErrorCode strikeSlip_linearElastic_qd_fd::loadSettings(const char *file)
{
  PetscErrorCode ierr = 0;

  PetscMPIInt rank,size;
  MPI_Comm_size(PETSC_COMM_WORLD,&size);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  ifstream infile( file );
  string line, var, rhs, rhsFull;
  size_t pos = 0;
  while (getline(infile, line))
  {
    istringstream iss(line);
    pos = line.find(_delim); // find position of the delimiter
    var = line.substr(0,pos);
    rhs = "";
    if (line.length() > (pos + _delim.length())) {
      rhs = line.substr(pos+_delim.length(),line.npos);
    }
    rhsFull = rhs; // everything after _delim

    // interpret everything after the appearance of a space on the line as a comment
    pos = rhs.find(" ");
    rhs = rhs.substr(0,pos);

    // time integration properties
    if (var == "stride1D_qd"){
      _stride1D_qd = (int)atof(rhs.c_str() );
      _stride1D = _stride1D_qd;
    }
    else if (var == "stride2D_qd"){
      _stride2D_qd = (int)atof(rhs.c_str() );
      _stride2D = _stride2D_qd;
    }
    else if (var == "stride1D_fd") { _stride1D_fd = (int)atof(rhs.c_str() ); }
    else if (var == "stride2D_fd") { _stride2D_fd = (int)atof(rhs.c_str() ); }
    else if (var == "stride1D_fd_end") { _stride1D_fd_end = (int)atof(rhs.c_str() ); }
    else if (var == "stride2D_fd_end") { _stride2D_fd_end = (int)atof(rhs.c_str() ); }
    else if (var == "trigger_qd2fd") { _trigger_qd2fd = atof(rhs.c_str() ); }
    else if (var == "trigger_fd2qd") { _trigger_fd2qd = atof(rhs.c_str() ); }
    else if (var == "limit_qd") { _limit_qd = atof(rhs.c_str() ); }
    else if (var == "limit_fd") { _limit_fd = atof(rhs.c_str() ); }
    else if (var == "limit_stride_fd") { _limit_stride_fd = atof(rhs.c_str() ); }
    else if (var == "deltaT_fd") { _deltaT = atof(rhs.c_str() ); }
    else if (var == "CFL") { _CFL = atof(rhs.c_str() ); }
    else if (var == "maxNumCycles") { _maxNumCycles = atoi(rhs.c_str() ); }
  }
  return ierr;
}


// check that required fields have been set by the input file
PetscErrorCode strikeSlip_linearElastic_qd_fd::checkInput()
{
  PetscErrorCode ierr = 0;

  assert(_computeICs == 0 || _computeICs == 1);

  if (_limit_fd < _trigger_qd2fd){
    _limit_fd = 10 * _trigger_qd2fd;
  }

  if (_limit_qd > _trigger_fd2qd){
    _limit_qd = _trigger_qd2fd / 10.0;
  }

  if (_limit_stride_fd == -1){
    _limit_stride_fd = _limit_fd / 10.0;
  }

  return ierr;
}


// parse boundary conditions (only necessary for qd)
// for fd, all matrix types are Neumann
PetscErrorCode strikeSlip_linearElastic_qd_fd::parseBCs()
{
  PetscErrorCode ierr = 0;

  if (_D->_qd_bcRType == "symmFault" || _D->_qd_bcRType == "rigidFault" || _D->_qd_bcRType == "remoteLoading") {
    _mat_qd_bcRType = "Dirichlet";
  }
  else if (_D->_qd_bcRType == "freeSurface" || _D->_qd_bcRType == "outGoingCharacteristics") {
    _mat_qd_bcRType = "Neumann";
  }

  if (_D->_qd_bcTType == "symmFault" || _D->_qd_bcTType == "rigidFault" || _D->_qd_bcTType == "remoteLoading") {
    _mat_qd_bcTType = "Dirichlet";
  }
  else if (_D->_qd_bcTType == "freeSurface" || _D->_qd_bcTType == "outGoingCharacteristics") {
    _mat_qd_bcTType = "Neumann";
  }

  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault" || _D->_qd_bcLType == "remoteLoading") {
    _mat_qd_bcLType = "Dirichlet";
  }
  else if (_D->_qd_bcLType == "freeSurface" || _D->_qd_bcLType == "outGoingCharacteristics") {
    _mat_qd_bcLType = "Neumann";
  }

  if (_D->_qd_bcBType == "symmFault" || _D->_qd_bcBType == "rigidFault" || _D->_qd_bcBType == "remoteLoading") {
    _mat_qd_bcBType = "Dirichlet";
  }
  else if (_D->_qd_bcBType == "freeSurface" || _D->_qd_bcBType == "outGoingCharacteristics") {
    _mat_qd_bcBType = "Neumann";
  }

  // determine if material is symmetric about the fault, or if one side is rigid
  _faultTypeScale = 2.0;
  if (_D->_qd_bcLType == "rigidFault" ) { _faultTypeScale = 1.0; }

  return ierr;
}


// compute allowed time step in fd stage based on CFL condition and user input
PetscErrorCode strikeSlip_linearElastic_qd_fd::computeTimeStep()
{
  PetscErrorCode ierr = 0;

  // coefficient for CFL condition
  PetscScalar gcfl = 0.7071; // if order = 2
  if (_D->_order == 4) { gcfl = 0.7071/sqrt(1.4498); }
  if (_D->_order == 6) { gcfl = 0.7071/sqrt(2.1579); }

  // compute grid spacing in y and z
  Vec dy, dz;
  VecDuplicate(*_y,&dy);
  VecDuplicate(*_y,&dz);
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
    MatGetDiagonal(yq, dy);
    VecScale(dy,1.0/(_D->_Ny-1));
    MatGetDiagonal(zr, dz);
    VecScale(dz,1.0/(_D->_Nz-1));
  }
  else {
    VecSet(dy,_D->_Ly/(_D->_Ny-1.0));
    VecSet(dz,_D->_Lz/(_D->_Nz-1.0));
  }

  // compute time for shear wave to travel 1 dy or dz and take the minimum
  Vec ts_dy,ts_dz;
  VecDuplicate(*_y,&ts_dy);
  VecDuplicate(*_z,&ts_dz);
  VecPointwiseDivide(ts_dy,dy,_material->_cs);
  VecPointwiseDivide(ts_dz,dz,_material->_cs);
  PetscScalar min_ts_dy, min_ts_dz;
  VecMin(ts_dy,NULL,&min_ts_dy);
  VecMin(ts_dz,NULL,&min_ts_dz);

  // largest possible time step permitted by CFL condition
  PetscScalar max_deltaT = gcfl * min(abs(min_ts_dy),abs(min_ts_dz));

  // compute time step requested by user
  PetscScalar cfl_deltaT = _CFL * max_deltaT;
  PetscScalar request_deltaT = _deltaT;

  // ensure deltaT is sensible even if the conditionals have an error
  _deltaT = max_deltaT;

  // if user did not specify deltaT or CFL
  if ( request_deltaT <= 0 && cfl_deltaT <= 0) {
    _deltaT = max_deltaT;
  }
  // if user specified deltaT but not CFL
  else if (request_deltaT > 0 && cfl_deltaT <= 0) {
    _deltaT = request_deltaT;
    assert(request_deltaT > 0);
    if (request_deltaT > max_deltaT) {
      PetscPrintf(PETSC_COMM_WORLD,"Warning: requested deltaT of %g is larger than maximum recommended deltaT of %g\n",request_deltaT,max_deltaT);
    }
  }
  // if user specified CFL but not deltaT
  else if (request_deltaT <= 0 && cfl_deltaT > 0) {
    _deltaT = cfl_deltaT;
    assert(_CFL <= 1);
  }
  // if user specified both CFL and deltaT
  else if (request_deltaT > 0 && cfl_deltaT > 0) {
    _deltaT = request_deltaT;
    if (request_deltaT > max_deltaT) {
      PetscPrintf(PETSC_COMM_WORLD,"Warning: requested deltaT of %g is larger than maximum recommended deltaT of %g\n",request_deltaT,max_deltaT);
    }
  }
  _deltaT_fd = _deltaT;

  VecDestroy(&dy);
  VecDestroy(&dz);
  VecDestroy(&ts_dy);
  VecDestroy(&ts_dz);

  return ierr;
}


// compute alphay and alphaz for use in time stepping routines
PetscErrorCode strikeSlip_linearElastic_qd_fd::computePenaltyVectors()
{
  PetscErrorCode ierr = 0;

  PetscScalar h11y, h11z;
  _material->_sbp->geth11(h11y, h11z);

  Vec alphay,alphaz;
  VecDuplicate(*_y, &alphay); VecSet(alphay,h11y);
  VecDuplicate(*_y, &alphaz); VecSet(alphaz,h11z);
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
    Vec temp1, temp2;
    VecDuplicate(alphay, &temp1);
    VecDuplicate(alphay, &temp2);
    MatMult(yq, alphay, temp1);
    MatMult(zr, alphaz, temp2);
    VecCopy(temp1, alphay);
    VecCopy(temp2, alphaz);
    VecDestroy(&temp1);
    VecDestroy(&temp2);
  }
  VecScatterBegin(_D->_scatters["body2L"], alphay, _fault_fd->_alphay, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2L"], alphay, _fault_fd->_alphay, INSERT_VALUES, SCATTER_FORWARD);
  VecDestroy(&alphay);
  VecDestroy(&alphaz);

  // compute vectors
  VecDuplicate(*_y, &_ay);
  VecSet(_ay, 0.0);

  PetscInt Ii,Istart,Iend;
  VecGetOwnershipRange(_ay,&Istart,&Iend);
  PetscScalar *ay;
  VecGetArray(_ay,&ay);
  PetscInt Jj = 0;
  for (Ii=Istart;Ii<Iend;Ii++) {
    ay[Jj] = 0;

    if ( (Ii/_D->_Nz == _D->_Ny-1) && (_D->_fd_bcRType == "outGoingCharacteristics") ) { ay[Jj] += 0.5 / h11y; }
    
    if ( (Ii%_D->_Nz == 0) && (_D->_fd_bcTType == "outGoingCharacteristics" )) { ay[Jj] += 0.5 / h11z; }
    
    if ( ((Ii+1)%_D->_Nz == 0) && (_D->_fd_bcBType == "outGoingCharacteristics") ) { ay[Jj] += 0.5 / h11z; }

    if ( (Ii/_D->_Nz == 0) && ( _D->_fd_bcLType == "outGoingCharacteristics" || _D->_fd_bcLType == "symmFault" || _D->_fd_bcLType == "rigidFault" ) ) {}

    Jj++;
  }
  VecRestoreArray(_ay,&ay);
  ierr = VecPointwiseMult(_ay, _ay, _material->_cs);

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_qd_fd::integrate()
{
  PetscErrorCode ierr = 0;
  double startTime_integrateTime = MPI_Wtime();

  // first cycle
  initiateIntegrands();

  // if start with quasidynamic phase
  {
    double startTime_qd = MPI_Wtime();
    _allowed = false;
    _inDynamic = false;
    prepare_fd2qd();
    integrate_qd();
    _qdTime += MPI_Wtime() - startTime_qd;

    if (_currTime >= _D->_maxTime || _stepCount >= _D->_maxStepCount) { return 0; }

    double startTime_fd = MPI_Wtime();
    _allowed = false;
    _inDynamic = true;
    prepare_qd2fd();
    integrate_fd();
    _dynTime += MPI_Wtime() - startTime_fd;
  }

  if (_currTime >= _D->_maxTime || _stepCount >= _D->_maxStepCount || _maxNumCycles <= 1) { return 0; }

  // for all cycles after 1st cycle
  _cycleCount++;
  while (_cycleCount < _maxNumCycles && _stepCount <= _D->_maxStepCount && _currTime <= _D->_maxTime) {
    _allowed = false;
    _inDynamic = false;
    double startTime_qd = MPI_Wtime();
    prepare_fd2qd();
    integrate_qd();
    _qdTime += MPI_Wtime() - startTime_qd;

    double startTime_fd = MPI_Wtime();
    _allowed = false;
    _inDynamic = true;
    prepare_qd2fd();
    integrate_fd();
    _dynTime += MPI_Wtime() - startTime_fd;

    _cycleCount++;
  }

  _integrateTime += MPI_Wtime() - startTime_integrateTime;
  return ierr;
}


// returns true if it's time to switch from qd to fd, or fd to qd, or if the maximum time or step count has been reached
bool strikeSlip_linearElastic_qd_fd::checkSwitchRegime(const Fault* _fault)
{
  bool mustSwitch = false;

  // if using max slip velocity as switching criterion
  // Vec absSlipVel;
  // VecDuplicate(_fault->_slipVel, &absSlipVel);
  // VecCopy(_fault->_slipVel, absSlipVel);
  // PetscScalar maxV;
  // VecAbs(absSlipVel);
  // VecMax(absSlipVel, NULL, &maxV);
  // VecDestroy(&absSlipVel);

  // if using R = eta*V / tauQS as switching criterion
  Vec R;
  VecDuplicate(_fault->_slipVel,&R);
  VecPointwiseMult(R,_fault_qd->_eta_rad,_fault->_slipVel);
  VecPointwiseDivide(R,R,_fault->_tauQSP);
  PetscScalar maxV;
  VecMax(R,NULL,&maxV);
  VecDestroy(&R);

  // check if switching from qd to fd, or from fd to qd, is allowed:

  // switching from fd to qd is allowed if maxV has ever been > limit_dyn
  if( _inDynamic && !_allowed && maxV > _limit_fd) { _allowed = true; }

  // switching from qd to fd is allowed if maxV has ever been < limit_qd
  if( !_inDynamic && !_allowed && maxV < _limit_qd) { _allowed = true; }

  // If switching is allowed, assess if the switching criteria has been reached:

  // switching from fd to qd happens if maxV < _trigger_fd2qd
  if (_inDynamic && _allowed && maxV < _trigger_fd2qd) { mustSwitch = true; }

  // switching from qd to fd happens if maxV > _trigger_qd2fd
  if (!_inDynamic && _allowed && maxV > _trigger_qd2fd) { mustSwitch = true; }

  // also change IO stride to avoid writing out too many time steps at the end of an earthquake
  if (_inDynamic && _allowed && maxV < _limit_stride_fd) {
    _stride1D = _stride1D_fd_end;
    _stride2D = _stride2D_fd_end;
  }

  return mustSwitch;
}


// initiate varQSEx, varIm, and varFD
// includes computation of steady-state initial conditions if necessary
// should only be called once before the 1st earthquake cycle
PetscErrorCode strikeSlip_linearElastic_qd_fd::initiateIntegrands()
{
  PetscErrorCode ierr = 0;

  // initiate integrand for QD

  // LinearElastic does not set up its KSP, so must set it up here
  Mat A;
  _material->_sbp->getA(A);
  _material->setupKSP(_material->_ksp,_material->_pc,A);

  Vec slip;
  VecDuplicate(_material->_bcL,&slip);
  VecCopy(_material->_bcL,slip);
  if (_D->_qd_bcLType == "symmFault") {
    VecScale(slip, _faultTypeScale);
  }
  _varQSEx["slip"] = slip;

  if (_computeICs == 1) {
    solveInit();
  }

  // set initial plastic porosity using slip velocity
  if (_D->_hydraulicCoupling != "no" && _p->_dilatancy == "yes" && _p->_simu == "inject") {
    _p->setInitialVals(_fault_qd->_slipVel);
  }

  VecCopy(_varQSEx["slip"],_fault_qd->_slip);
  _fault_qd->initiateIntegrand(_D->_initTime,_varQSEx);

  // initiate integrand for varIm
  if (_D->_thermalCoupling != "no") {
    _he->initiateIntegrand(_D->_initTime,_varQSEx,_varIm);
  }

  if (_D->_hydraulicCoupling != "no") {
    _p->initiateIntegrand(_D->_initTime,_varQSEx,_varIm);
  }

  // initiate integrand for FD:
  // ensure fault_fd == fault_qd
  VecCopy(_fault_qd->_psi,      _fault_fd->_psi);
  VecCopy(_fault_qd->_slipVel,  _fault_fd->_slipVel);
  VecCopy(_fault_qd->_slip,     _fault_fd->_slip);
  VecCopy(_fault_qd->_slip,     _fault_fd->_slip0);
  VecCopy(_fault_qd->_tauQSP,   _fault_fd->_tauQSP);
  VecCopy(_fault_qd->_tauP,     _fault_fd->_tauP);
  VecCopy(_fault_qd->_strength, _fault_fd->_tau0);
  VecCopy(_fault_qd->_prestress,_fault_fd->_prestress);
  VecCopy(_fault_qd->_sN,       _fault_fd->_sN);
  VecCopy(_fault_qd->_sNEff,    _fault_fd->_sNEff);

  // add psi and slip to varFD
  _fault_fd->initiateIntegrand(_D->_initTime,_varFD); // adds psi and slip

  // add u
  VecDuplicate(_material->_u, &_varFD["u"]);
  VecCopy(_material->_u,_varFD["u"]);
  VecDuplicate(_material->_u, &_u0);
  VecSet(_u0,0.0);

  // if solving the heat equation, add temperature to varFD
  if (_D->_thermalCoupling != "no") {
    VecDuplicate(_varIm["Temp"], &_varFD["Temp"]);
    VecCopy(_varIm["Temp"], _varFD["Temp"]);
  }

  // if solving the pressure equation, add pressure and porosity (if it's allowed to evolve) to varFD
  if (_D->_hydraulicCoupling != "no") {
    VecDuplicate(_varIm["pressure"], &_varFD["pressure"]);
    VecCopy(_varIm["pressure"], _varFD["pressure"]);

    if (_p->_phiEvolve == "yes") {
      if (_p->_simu == "inject") {
	VecDuplicate(_varIm["phie"], &_varFD["phie"]);
	VecCopy(_varIm["phie"], _varFD["phie"]);
	if (_p->_dilatancy == "yes") {
	  VecDuplicate(_varQSEx["phip"], &_varFD["phip"]);
	  VecCopy(_varQSEx["phip"], _varFD["phip"]);
	}
      }
      else {
	VecDuplicate(_varIm["phi"], &_varFD["phi"]);
	VecCopy(_varIm["phi"], _varFD["phi"]);	
      }
    }
  }

   // copy varFD into varFDPrev
  for (map<string,Vec>::iterator it = _varFD.begin(); it != _varFD.end(); it++ ) {
    VecDuplicate(_varFD[it->first],&_varFDPrev[it->first]);
    VecCopy(_varFD[it->first],_varFDPrev[it->first]);
  }

  return ierr;
}


// move from a fully dynamic phase to a quasidynamic phase
PetscErrorCode strikeSlip_linearElastic_qd_fd::prepare_fd2qd()
{
  PetscErrorCode ierr = 0;

  // switch strides to qd values
  _stride1D = _stride1D_qd;
  _stride2D = _stride2D_qd;

  // update explicitly integrated variables
  VecCopy(_fault_fd->_psi, _varQSEx["psi"]);
  VecCopy(_fault_fd->_slip, _varQSEx["slip"]);

  // update implicitly integrated T
  if (_D->_thermalCoupling != "no") {
    VecCopy(_varFD["Temp"],_varIm["Temp"]);
  }

  if (_D->_hydraulicCoupling != "no") {
    VecCopy(_varFD["pressure"], _varIm["pressure"]);
    if (_p->_phiEvolve == "yes") {
      if (_p->_simu == "inject") {
	VecCopy(_varFD["phie"], _varIm["phie"]);
	if (_p->_dilatancy == "yes") {
	  VecCopy(_varFD["phip"], _varQSEx["phip"]);
	}
      }
      else {
	VecCopy(_varFD["phi"], _varIm["phi"]);
      }
    }
    if (_D->_hydraulicCoupling == "coupled"){
      _fault_qd->setSNEff(_p->_p);
    }
  }

  // update fault internal variables
  VecCopy(_fault_fd->_psi,       _fault_qd->_psi);
  VecCopy(_fault_fd->_slipVel,   _fault_qd->_slipVel);
  VecCopy(_fault_fd->_slip,      _fault_qd->_slip);
  VecCopy(_fault_fd->_tauP,      _fault_qd->_tauP);
  VecCopy(_fault_fd->_tauQSP,    _fault_qd->_tauQSP);
  VecCopy(_fault_fd->_strength,  _fault_qd->_strength);
  VecCopy(_fault_fd->_sN,        _fault_qd->_sN);
  VecCopy(_fault_fd->_sNEff,     _fault_qd->_sNEff);

  // update viewers to keep IO consistent
  _fault_fd->_viewers.swap(_fault_qd->_viewers);

  // update momentum balance equation boundary conditions
  _material->changeBCTypes(_mat_qd_bcRType,_mat_qd_bcTType,_mat_qd_bcLType,_mat_qd_bcBType);

  return ierr;
}


// move from a quasidynamic phase to a fully dynamic phase
PetscErrorCode strikeSlip_linearElastic_qd_fd::prepare_qd2fd()
{
  PetscErrorCode ierr = 0;

  // switch strides to qd values
  _stride1D = _stride1D_fd;
  _stride2D = _stride2D_fd;

  // save current variables as n-1 time step
  VecCopy(_fault_qd->_slip,_varFDPrev["slip"]);
  VecCopy(_fault_qd->_psi,_varFDPrev["psi"]);
  VecCopy(_material->_u,_varFDPrev["u"]);

  if (_D->_thermalCoupling != "no") {
    VecCopy(_varIm["Temp"], _varFDPrev["Temp"]);
  }

  if (_D->_hydraulicCoupling != "no") {
    VecCopy(_varIm["pressure"], _varFDPrev["pressure"]);
    if (_p->_phiEvolve == "yes") {
      if (_p->_simu == "inject") {
	VecCopy(_varIm["phie"], _varFDPrev["phie"]);
	if (_p->_dilatancy == "yes") {
	  VecCopy(_varQSEx["phip"], _varFDPrev["phip"]);
	}
      }
      else {
	VecCopy(_varIm["phi"], _varFDPrev["phi"]);
      }
    }
  }

  // take 1 quasidynamic time step to compute variables at time n
  _inDynamic = 0;
  integrate_singleQDTimeStep();
  _inDynamic = 1;

  // update varFD to reflect latest values
  VecCopy(_fault_qd->_slip,_varFD["slip"]);
  VecCopy(_fault_qd->_psi,_varFD["psi"]);
  VecCopy(_material->_u,_varFD["u"]);

  if (_D->_thermalCoupling != "no") {
    VecCopy(_varIm["Temp"], _varFD["Temp"]);
  }

  if (_D->_hydraulicCoupling != "no") {
    VecCopy(_varIm["pressure"], _varFD["pressure"]);
    if (_p->_phiEvolve == "yes") {
      if (_p->_simu == "inject") {
	VecCopy(_varIm["phie"], _varFD["phie"]);
	if (_p->_dilatancy == "yes") {
	  VecCopy(_varQSEx["phip"], _varFD["phip"]);
	}
      }
      else {
	VecCopy(_varIm["phi"], _varFD["phi"]);
      }
    }
    if (_D->_hydraulicCoupling == "coupled") {
      _fault_fd->setSNEff(_p->_p);
    }
  }

  // now change u to du, compute additional slip only during FD
  VecAXPY(_varFD["u"],-1.0,_varFDPrev["u"]);
  VecCopy(_varFDPrev["u"],_u0);
  VecSet(_varFDPrev["u"],0.0);

  // update fault internal variables
  VecCopy(_fault_qd->_psi,       _fault_fd->_psi);
  VecCopy(_fault_qd->_slipVel,   _fault_fd->_slipVel);
  VecCopy(_fault_qd->_slip,      _fault_fd->_slip);
  VecCopy(_fault_qd->_slip,      _fault_fd->_slip0);
  VecCopy(_fault_qd->_strength,  _fault_fd->_strength);
  VecCopy(_fault_qd->_strength,  _fault_fd->_tau0);
  VecCopy(_fault_qd->_tauP,      _fault_fd->_tauP);
  VecCopy(_fault_qd->_tauQSP,    _fault_fd->_tauQSP);
  VecCopy(_fault_qd->_sN,        _fault_fd->_sN);
  VecCopy(_fault_qd->_sNEff,     _fault_fd->_sNEff);
  VecAXPY(_fault_fd->_tau0, 1.0, _fault_fd->_prestress); // add prestress to tau0
  _fault_fd->_viewers.swap(_fault_qd->_viewers);

  // update momentum balance equation boundary conditions
  _material->changeBCTypes(_mat_fd_bcRType,_mat_fd_bcTType,_mat_fd_bcLType,_mat_fd_bcBType);

  return ierr;
}


// take 1 quasidynamic time step to set up varFDPrev and varFD
PetscErrorCode strikeSlip_linearElastic_qd_fd::integrate_singleQDTimeStep()
{
  PetscErrorCode ierr = 0;

  OdeSolver      *quadEx = NULL; // explicit time stepping
  OdeSolverImex  *quadImex = NULL; // implicit time stepping

  // initialize time integrator
  if (_D->_timeIntegrator == "FEuler") {
    quadEx = new FEuler(1,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32") {
    quadEx = new RK32(1,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43") {
    quadEx = new RK43(1,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32_WBE") {
    quadImex = new RK32_WBE(1,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43_WBE") {
    quadImex = new RK43_WBE(1,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: timeIntegrator type not understood\n");
    assert(0); // automatically fail
  }

  // integrate
  if (_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") {
    quadImex->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    quadImex->setTimeStepBounds(_deltaT_fd,_deltaT_fd);
    quadImex->setTimeRange(_currTime,_currTime+_deltaT_fd);
    quadImex->setInitialStepCount(_stepCount);
    quadImex->setInitialConds(_varQSEx,_varIm);
    quadImex->setToleranceType(_D->_normType);
    quadImex->setErrInds(_D->_timeIntInds,_D->_scale);

    ierr = quadImex->integrate(this); CHKERRQ(ierr);
  }
  else {
    quadEx->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    quadEx->setTimeStepBounds(_deltaT_fd,_deltaT_fd);
    quadEx->setTimeRange(_currTime,_currTime+_deltaT_fd);
    quadEx->setInitialStepCount(_stepCount);
    quadEx->setToleranceType(_D->_normType);
    quadEx->setInitialConds(_varQSEx);
    quadEx->setErrInds(_D->_timeIntInds,_D->_scale);

    ierr = quadEx->integrate(this); CHKERRQ(ierr);
  }

  delete quadEx;
  delete quadImex;

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_qd_fd::writeStep1D(PetscInt stepCount, PetscScalar time, const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV1D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time1D.txt", _D->_outFileMode, _timeV1D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV1D, "%.15e\n", time); CHKERRQ(ierr);
  }

  if (_dtimeV1D == NULL ) {
    initiateWriteASCII(outputDir, "med_dt1D.txt", _D->_outFileMode, _dtimeV1D, "%.15e\n", _deltaT);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV1D, "%.15e\n", _deltaT); CHKERRQ(ierr);
  }

  if (_regime1DV == NULL ) {
    initiateWriteASCII(outputDir, "regime1D.txt", _D->_outFileMode, _regime1DV, "%i\n", (int)_inDynamic);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_regime1DV, "%i\n",_inDynamic);CHKERRQ(ierr);
  }

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_qd_fd::writeStep2D(const PetscInt stepCount, const PetscScalar time,const string outputDir)
{
  PetscErrorCode ierr = 0;

  if (_timeV2D == NULL ) {
    ierr = initiateWriteASCII(outputDir, "med_time2D.txt", _D->_outFileMode, _timeV2D, "%.15e\n", time);
    CHKERRQ(ierr);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_timeV2D, "%.15e\n", time); CHKERRQ(ierr);
  }
  if (_dtimeV2D == NULL ) {
    initiateWriteASCII(outputDir, "med_dt2D.txt", _D->_outFileMode, _dtimeV2D, "%.15e\n", _deltaT);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_dtimeV2D, "%.15e\n", _deltaT); CHKERRQ(ierr);
  }

  if (_regime2DV == NULL ) {
    initiateWriteASCII(outputDir, "regime2D.txt", _D->_outFileMode, _regime2DV, "%i\n", (int)_inDynamic);
  }
  else {
    ierr = PetscViewerASCIIPrintf(_regime2DV, "%i\n",_inDynamic);CHKERRQ(ierr);
  }

  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_qd_fd::view()
{
  PetscErrorCode ierr = 0;

  double totRunTime = MPI_Wtime() - _startTime;

  _material->view(_integrateTime);
  _fault_qd->view(_integrateTime);
  if (_D->_thermalCoupling != "no") { _he->view(); }

  ierr = PetscPrintf(PETSC_COMM_WORLD,"-------------------------------\n\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"strikeSlip_linearElastic_qd_fd Runtime Summary:\n");CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in integration (s): %g\n",_integrateTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent writing output (s): %g\n",_writeTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent propagating the wave (s): %g\n",_propagateTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in quasidynamic (s): %g\n",_qdTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   time spent in dynamic (s): %g\n",_dynTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   total run time (s): %g\n",totRunTime);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"   %% integration time spent writing output: %g\n",(_writeTime/_integrateTime)*100.);CHKERRQ(ierr);
  return ierr;
}


PetscErrorCode strikeSlip_linearElastic_qd_fd::writeContext()
{
  PetscErrorCode ierr = 0;

  // output scalar fields
  string str = _outputDir + "mediator_context.txt";
  PetscViewer    viewer;
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, str.c_str());

  // time integration settings
  ierr = PetscViewerASCIIPrintf(viewer,"stride1D_qd = %i\n",_stride1D_qd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride2D_qd = %i\n",_stride2D_qd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride1D_fd = %i\n",_stride1D_fd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride2D_fd = %i\n",_stride2D_fd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride1D_fd_end = %i\n",_stride1D_fd_end);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"stride2D_fd_end = %i\n",_stride2D_fd_end);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"\n");CHKERRQ(ierr);

  
  ierr = PetscViewerASCIIPrintf(viewer,"trigger_qd2fd = %.15e\n",_trigger_qd2fd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"trigger_fd2qd = %.15e\n",_trigger_fd2qd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"limit_qd = %.15e\n",_limit_qd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"limit_fd = %.15e\n",_limit_fd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"limit_stride_fd = %.15e\n",_limit_stride_fd);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"CFL = %.15e\n",_CFL);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"deltaT_fd = %.15e\n",_deltaT_fd);CHKERRQ(ierr);

  PetscViewerDestroy(&viewer);

  _D->write();
  _material->writeContext(_outputDir);
  _fault_qd->writeContext(_outputDir);
  if (_D->_thermalCoupling != "no") { _he->writeContext(_outputDir); }
  if (_D->_hydraulicCoupling != "no") { _p->writeContext(_outputDir); }

  if (_D->_forcingType == "iceStream") {
    ierr = writeVec(_forcingTermPlain,_outputDir + "momBal_forcingTerm"); CHKERRQ(ierr);
  }

  return ierr;
}


// momentum balance equation and constitutive laws portion of d_dt
PetscErrorCode strikeSlip_linearElastic_qd_fd::solveMomentumBalance(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx)
{
  PetscErrorCode ierr = 0;

  _material->setRHS();

  // add source term for driving the ice stream to rhs Vec
  if (_D->_forcingType == "iceStream") {
    VecAXPY(_material->_rhs,1.0,_forcingTerm);
  }

  _material->computeU();
  _material->computeStresses();

  return ierr;
}


// fully dynamic: off-fault portion of the momentum balance equation
PetscErrorCode strikeSlip_linearElastic_qd_fd::propagateWaves(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev)
{
  PetscErrorCode ierr = 0;

  double startPropagation = MPI_Wtime();

  // compute D2u = (Dyy+Dzz)*u
  Vec D2u, temp;
  VecDuplicate(*_y, &D2u);
  VecDuplicate(*_y, &temp);

  Mat A;
  _material->_sbp->getA(A);
  MatMult(A, var.find("u")->second, temp);
  _material->_sbp->Hinv(temp, D2u);
  VecDestroy(&temp);

  if (_D->_yGridSpacingType == "variableGridSpacing") {
    Mat J,Jinv,qy,rz,yq,zr;
    _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
    Vec temp;
    VecDuplicate(D2u, &temp);
    MatMult(Jinv, D2u, temp);
    VecCopy(temp, D2u);
    VecDestroy(&temp);
  }
  ierr = VecScatterBegin(*_body2fault, D2u, _fault_fd->_d2u, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, D2u, _fault_fd->_d2u, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // Propagate waves and compute displacement at the next time step
  // includes boundary conditions except for fault
  PetscInt       Ii,Istart,Iend;
  PetscScalar   *uNextA; // changed in this loop
  const PetscScalar   *u, *uPrev, *d2u, *ay, *rho; // unchchanged in this loop
  VecGetArray(varNext["u"], &uNextA);
  VecGetArrayRead(var.find("u")->second, &u);
  VecGetArrayRead(varPrev.find("u")->second, &uPrev);
  VecGetArrayRead(_ay, &ay);
  VecGetArrayRead(D2u, &d2u);
  VecGetArrayRead(_material->_rho, &rho);

  VecGetOwnershipRange(varNext["u"],&Istart,&Iend);
  PetscInt Jj = 0;
  for (Ii = Istart; Ii < Iend; Ii++){
    PetscScalar c1 = deltaT*deltaT / rho[Jj];
    PetscScalar c2 = deltaT*ay[Jj] - 1.0;
    PetscScalar c3 = deltaT*ay[Jj] + 1.0;
    uNextA[Jj] = (c1*d2u[Jj] + 2.*u[Jj] + c2*uPrev[Jj]) / c3;
    Jj++;
  }
  VecRestoreArray(varNext["u"], &uNextA);
  VecRestoreArrayRead(var.find("u")->second, &u);
  VecRestoreArrayRead(varPrev.find("u")->second, &uPrev);
  VecRestoreArrayRead(_ay, &ay);
  VecRestoreArrayRead(D2u, &d2u);
  VecRestoreArrayRead(_material->_rho, &rho);

  VecDestroy(&D2u);

  _propagateTime += MPI_Wtime() - startPropagation;

  return ierr;
}


// solve for initial condition on fault (assume start always from qd)
// based on user-defined psi and tau, or using vL as slipVel to set steady state
PetscErrorCode strikeSlip_linearElastic_qd_fd::solveInit()
{
  PetscErrorCode ierr = 0;

  // estimate steady-state conditions for fault, material based on strain rate
  if (_D->_initState == "unsteady") {
    _fault_qd->setPsiTauV();
  }
  else {
    _fault_qd->guessSS(_D->_vL); // sets: slipVel, psi, tau
  }
  
  // compute u that satisfies tau at left boundary
  VecCopy(_fault_qd->_tauP,_material->_bcL);
  _material->setRHS();
  _material->computeU();
  _material->computeStresses();

  // update fault to contain correct stresses
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // update boundary conditions, stresses
  solveInitBC();
  _material->changeBCTypes(_mat_qd_bcRType,_mat_qd_bcTType,_mat_qd_bcLType,_mat_qd_bcBType);

  // steady state temperature
  if (_D->_thermalCoupling != "no") {
    Vec T;
    VecDuplicate(_material->_sxy,&T);
    _he->computeSteadyStateTemp(_currTime,_fault_qd->_slipVel,_fault_qd->_tauP,NULL,NULL,NULL,T);
    VecDestroy(&T);
  }

  return ierr;
}


// update the boundary conditions based on new steady state u
PetscErrorCode strikeSlip_linearElastic_qd_fd::solveInitBC()
{
  PetscErrorCode ierr = 0;

  // adjust u so it has no negative values
  PetscScalar minVal = 0;
  VecMin(_material->_u,NULL,&minVal);
  if (minVal < 0) {
    minVal = abs(minVal);
    Vec temp;
    VecDuplicate(_material->_u,&temp);
    VecSet(temp,minVal);
    VecAXPY(_material->_u,1.,temp);
    VecDestroy(&temp);
  }

  // extract R boundary from u, to set _material->bcR
  VecScatterBegin(_D->_scatters["body2R"], _material->_u, _material->_bcRShift, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2R"], _material->_u, _material->_bcRShift, INSERT_VALUES, SCATTER_FORWARD);
  VecCopy(_material->_bcRShift,_material->_bcR);

  // extract L boundary from u to set slip, possibly _material->_bcL
  Vec uL;
  VecDuplicate(_material->_bcL,&uL);
  VecScatterBegin(_D->_scatters["body2L"], _material->_u, uL, INSERT_VALUES, SCATTER_FORWARD);
  VecScatterEnd(_D->_scatters["body2L"], _material->_u, uL, INSERT_VALUES, SCATTER_FORWARD);

  if (_varQSEx.find("slip") != _varQSEx.end() ) {
    VecCopy(uL,_varQSEx["slip"]);
  }
  else {
    Vec slip;
    VecDuplicate(_material->_bcL,&slip);
    VecCopy(uL,slip);
    _varQSEx["slip"] = slip;
  }

  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    VecCopy(uL,_material->_bcL);
  }

  if (_D->_qd_bcLType == "symmFault") {
    VecScale(_varQSEx["slip"], _faultTypeScale);
  }

  VecDestroy(&uL);

  return ierr;
}


// constructs the body forcing term for an ice stream
// includes allocation of memory for this forcing term
PetscErrorCode strikeSlip_linearElastic_qd_fd::constructIceStreamForcingTerm()
{
  PetscErrorCode ierr = 0;

  // matrix to map the value for the forcing term, which lives on the fault, to all other processors
  Mat MapV;
  MatCreate(PETSC_COMM_WORLD,&MapV);
  MatSetSizes(MapV,PETSC_DECIDE,PETSC_DECIDE,_D->_Ny*_D->_Nz,_D->_Nz);
  MatSetFromOptions(MapV);
  MatMPIAIJSetPreallocation(MapV,_D->_Ny*_D->_Nz,NULL,_D->_Ny*_D->_Nz,NULL);
  MatSeqAIJSetPreallocation(MapV,_D->_Ny*_D->_Nz,NULL);
  MatSetUp(MapV);

  PetscScalar v=1.0;
  PetscInt Ii=0,Istart=0,Iend=0,Jj=0;
  MatGetOwnershipRange(MapV,&Istart,&Iend);
  for (Ii = Istart; Ii < Iend; Ii++) {
    Jj = Ii % _D->_Nz;
    MatSetValues(MapV,1,&Ii,1,&Jj,&v,INSERT_VALUES);
  }
  MatAssemblyBegin(MapV,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(MapV,MAT_FINAL_ASSEMBLY);

  //~ // compute forcing term for momentum balance equation
  //~ // forcing = - tau_ss / Ly
  //~ Vec tauSS = NULL;
  //~ _fault_qd->computeTauRS(tauSS,_D->_vL);
  //~ VecScale(tauSS,-1./_D->_Ly);

  //~ VecDuplicate(_material->_u,&_forcingTerm); VecSet(_forcingTerm,0.0);
  //~ MatMult(MapV,tauSS,_forcingTerm);

  //~ MatDestroy(&MapV);
  //~ VecDestroy(&tauSS);

  // compute forcing term for momentum balance equation
  // forcing = (1/Ly) * (tau_ss + eta_rad*V_ss)
  //~ Vec tauSS = NULL,radDamp=NULL,V=NULL;
  //~ VecDuplicate(_fault_qd->_eta_rad,&V); VecSet(V,_D->_vL);
  //~ VecDuplicate(_fault_qd->_eta_rad,&radDamp); VecPointwiseMult(radDamp,_fault_qd->_eta_rad,V);
  //~ _fault_qd->computeTauRS(tauSS,_D->_vL);
  //~ VecAXPY(tauSS,1.0,radDamp);
  //~ VecScale(tauSS,-1./_D->_Ly);

  //~ VecDuplicate(_material->_u,&_forcingTerm); VecSet(_forcingTerm,0.0);
  //~ MatMult(MapV,tauSS,_forcingTerm);

  //~ MatDestroy(&MapV);
  //~ VecDestroy(&tauSS);
  //~ VecDestroy(&radDamp);

  // compute forcing term using scalar input
  VecDuplicate(_material->_u,&_forcingTerm); VecSet(_forcingTerm,_D->_forcingVal);
  VecDuplicate(_material->_u,&_forcingTermPlain); VecCopy(_forcingTerm,_forcingTermPlain);

  // alternatively, load forcing term from user input
  ierr = loadVecFromInputFile(_forcingTerm,_inputDir,"iceForcingTerm"); CHKERRQ(ierr);

  // multiply forcing term by H, or by J*H if using a curvilinear grid
  if (_D->_yGridSpacingType == "variableGridSpacing") {
    // multiply this term by H*J (the H matrix and the Jacobian)
    Vec temp1; VecDuplicate(_forcingTerm,&temp1);
    Mat J,Jinv,qy,rz,yq,zr;
    ierr = _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr); CHKERRQ(ierr);
    ierr = MatMult(J,_forcingTerm,temp1); CHKERRQ(ierr);
    Mat H; _material->_sbp->getH(H);
    ierr = MatMult(H,temp1,_forcingTerm); CHKERRQ(ierr);
    VecDestroy(&temp1);
  }
  else { // multiply forcing term by H
    Vec temp1; VecDuplicate(_forcingTerm,&temp1);
    Mat H; _material->_sbp->getH(H);
    ierr = MatMult(H,_forcingTerm,temp1); CHKERRQ(ierr);
    VecCopy(temp1,_forcingTerm);
    VecDestroy(&temp1);
  }

  return ierr;
}


// time integration for quasidynamic phase: can use both explicit and implicit
PetscErrorCode strikeSlip_linearElastic_qd_fd::integrate_qd()
{
  PetscErrorCode ierr = 0;

  OdeSolver      *quadEx = NULL; // explicit time stepping
  OdeSolverImex  *quadImex = NULL; // implicit time stepping

  // initialize time integrator
  if (_D->_timeIntegrator == "FEuler") {
    quadEx = new FEuler(_D->_maxStepCount,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32") {
    quadEx = new RK32(_D->_maxStepCount,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43") {
    quadEx = new RK43(_D->_maxStepCount,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK32_WBE") {
    quadImex = new RK32_WBE(_D->_maxStepCount,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else if (_D->_timeIntegrator == "RK43_WBE") {
    quadImex = new RK43_WBE(_D->_maxStepCount,_D->_maxTime,_deltaT_fd,_D->_timeControlType);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"ERROR: timeIntegrator type not understood\n");
    assert(0); // automatically fail
  }

  // integrate
  if (_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") {
    quadImex->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    quadImex->setTimeStepBounds(_D->_minDeltaT,_D->_maxDeltaT);
    quadImex->setTimeRange(_currTime,_D->_maxTime);
    quadImex->setInitialStepCount(_stepCount);
    quadImex->setInitialConds(_varQSEx,_varIm);
    quadImex->setToleranceType(_D->_normType);
    quadImex->setErrInds(_D->_timeIntInds,_D->_scale);

    // load this only at the beginning, and not after switching
    if (_D->_ckpt > 0 && _D->_ckptNumber > 0 && _cycleCount == 0) {
      loadValueFromCheckpoint(_outputDir, "chkpt_prevErr", quadImex->_errA[1]);
      loadValueFromCheckpoint(_outputDir, "chkpt_currErr", quadImex->_errA[0]);
    }

    ierr = quadImex->integrate(this); CHKERRQ(ierr);

    map<string,Vec> varOut = quadImex->_varEx;
    for (map<string,Vec>::iterator it = varOut.begin(); it != varOut.end(); it++ ) {
      VecCopy(varOut[it->first],_varQSEx[it->first]);
    }
  }
  else {
    quadEx->setTolerance(_D->_timeStepTol);CHKERRQ(ierr);
    quadEx->setTimeStepBounds(_D->_minDeltaT,_D->_maxDeltaT);
    quadEx->setTimeRange(_currTime,_D->_maxTime);
    quadEx->setInitialStepCount(_stepCount);
    quadEx->setToleranceType(_D->_normType);
    quadEx->setInitialConds(_varQSEx);
    quadEx->setErrInds(_D->_timeIntInds,_D->_scale);
    
    // load this only at the beginning, and not after switching
    if (_D->_ckpt > 0 && _D->_ckptNumber > 0 && _cycleCount == 0) {
      loadValueFromCheckpoint(_outputDir, "chkpt_prevErr", quadEx->_errA[1]);
      loadValueFromCheckpoint(_outputDir, "chkpt_currErr", quadEx->_errA[0]);
    }

    ierr = quadEx->integrate(this); CHKERRQ(ierr);

    map<string,Vec> varOut = quadEx->_var;
    for (map<string,Vec>::iterator it = varOut.begin(); it != varOut.end(); it++ ) {
      VecCopy(varOut[it->first],_varQSEx[it->first]);
    }
  }

  delete quadEx;
  delete quadImex;

  return ierr;
}


// time integration for fully dynamic phase, currently only uses explicit method
PetscErrorCode strikeSlip_linearElastic_qd_fd::integrate_fd()
{
  PetscErrorCode ierr = 0;

  OdeSolver_WaveEq          *quadWaveEx;

  // initialize time integrator
  quadWaveEx = new OdeSolver_WaveEq(_D->_maxStepCount,_currTime,_D->_maxTime,_deltaT_fd);
  quadWaveEx->setInitialConds(_varFD);
  quadWaveEx->setInitialStepCount(_stepCount);

  ierr = quadWaveEx->integrate(this);CHKERRQ(ierr);

  map<string,Vec> varOut = quadWaveEx->_var;
  for (map<string,Vec>::iterator it = varOut.begin(); it != varOut.end(); it++ ) {
    VecCopy(varOut[it->first],_varFD[it->first]);
  }

  delete quadWaveEx;

  return ierr;
}


// quasidynamic: purely explicit time stepping
PetscErrorCode strikeSlip_linearElastic_qd_fd::d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx)
{
  PetscErrorCode ierr = 0;

  // update for momBal; var holds slip, bcL is displacement at y=0+
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(varEx.find("slip")->second,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  _fault_qd->updateFields(varEx);

  // only integrate phip explicitly during quasidynamic
  if (_D->_hydraulicCoupling != "no" && varEx.find("phip") != varEx.end()) {
    _p->updateFields(varEx);  // this will only update phip
  }
  
  // compute rates
  ierr = solveMomentumBalance(time,varEx,dvarEx); CHKERRQ(ierr);

  // update fields on fault from other classes
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // PENALTY TERM CALCULATION FOR QUASI-STATIC SHEAR STRESS
  // extract grid value of displacement on fault
  if (_D->_penalty == "yes") {
    Vec uFault;
    VecDuplicate(_fault_qd->_tauQSP, &uFault);
    ierr = VecScatterBegin(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

    // penalty term = alpha*(uFault - slip/2), where alpha=qy*alphaDy
    Vec penalty;
    VecDuplicate(uFault, &penalty);
    VecAXPY(uFault,-0.5,varEx.find("slip")->second);
    VecScale(uFault, _material->_alpha); // scale by alpha/dq

    if (_D->_yGridSpacingType == "variableGridSpacing") {
      Mat J,Jinv,qy,rz,yq,zr;
      _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
      MatMult(qy,uFault,penalty); // qy*penalty
    }
    else {
      PetscScalar dy_inv = (_D->_Ny-1)/(_D->_Ly);
      PetscScalar dq = 1/(_D->_Ny-1);
      VecScale(uFault, dq);
      VecScale(uFault,dy_inv);  // change from alpha/dq -> alpha/dy
      VecCopy(uFault, penalty);
    }

    // add penalty term to tauQSP
    VecAXPY(_fault_qd->_tauQSP, 1, penalty);
    VecDestroy(&uFault);
    VecDestroy(&penalty);
  }
  
  // rates for fault
  ierr = _fault_qd->d_dt(time,varEx,dvarEx); // sets rates for slip and state

  // only integrate phip explicitly during quasidynamic
  if (_D->_hydraulicCoupling != "no" && varEx.find("phip") != varEx.end()) {
    _p->d_dt(time, varEx, dvarEx);
  }
  
  return ierr;
}


// quasidynamic: implicit/explicit time stepping
PetscErrorCode strikeSlip_linearElastic_qd_fd::d_dt(const PetscScalar time,const map<string,Vec>& varEx,map<string,Vec>& dvarEx,map<string,Vec>& varIm,const map<string,Vec>& varImo,const PetscScalar dt)
{
  PetscErrorCode ierr = 0;

  // 1. update BCs, state of each class from integrated variables varEx and varImo

  // update for momBal; var holds slip, bcL is displacement at y=0+
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(varEx.find("slip")->second,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  _fault_qd->updateFields(varEx);

  // integrate pressure and phi implicitly during quasidynamic
  if ( _D->_hydraulicCoupling != "no") {
    _p->updateFields(varEx,varImo);  // this updates all variables
  }

  // update temperature in momBal
  if (varImo.find("Temp") != varImo.end() && _D->_thermalCoupling == "coupled") {
    _fault_qd->updateTemperature(varImo.find("Temp")->second);
  }

  // update effective normal stress in fault using pore pressure
  if (_D->_hydraulicCoupling == "coupled") {
    _fault_qd->setSNEff(_p->_p);
  }

  // 2. compute rates, and update implicitly integrated variables
  ierr = solveMomentumBalance(time,varEx,dvarEx); CHKERRQ(ierr);

  // update shear stress on fault from momentum balance computation
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault_qd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

  // PENALTY TERM CALCULATION FOR QUASI-STATIC SHEAR STRESS
  // extract grid value of displacement on fault
  if (_D->_penalty == "yes") {
    Vec uFault;
    VecDuplicate(_fault_qd->_tauQSP, &uFault);
    ierr = VecScatterBegin(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecScatterEnd(*_body2fault, _material->_u, uFault, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);

    // penalty term = alpha*(uFault - slip/2), where alpha=qy*alphaDy
    Vec penalty;
    VecDuplicate(uFault, &penalty);
    VecAXPY(uFault,-0.5,varEx.find("slip")->second);
    VecScale(uFault, _material->_alpha); // scale by alpha/dq

    if (_D->_yGridSpacingType == "variableGridSpacing") {
      Mat J,Jinv,qy,rz,yq,zr;
      _material->_sbp->getCoordTrans(J,Jinv,qy,rz,yq,zr);
      MatMult(qy,uFault,penalty); // qy*penalty
    }
    else {
      PetscScalar dy_inv = (_D->_Ny-1)/(_D->_Ly);
      PetscScalar dq = 1/(_D->_Ny-1);
      VecScale(uFault, dq);
      VecScale(uFault,dy_inv);  // change from alpha/dq -> alpha/dy
      VecCopy(uFault, penalty);
    }

    // add penalty term to tauQSP
    VecAXPY(_fault_qd->_tauQSP, 1, penalty);
    VecDestroy(&uFault);
    VecDestroy(&penalty);
  }
  
  // rates for fault
  ierr = _fault_qd->d_dt(time,varEx,dvarEx); // sets rates for slip and state

  // pressure equation
  if (_D->_hydraulicCoupling != "no") {
    _p->d_dt(time,varEx,dvarEx,varIm,varImo,dt);
  }

  // heat equation
  if (_D->_thermalCoupling != "no") {
    Vec sxy,sxz,sdev;
    _material->getStresses(sxy,sxz,sdev);
    Vec V = dvarEx.find("slip")->second;
    Vec tau = _fault_qd->_tauP;
    Vec gVxy_t = NULL;
    Vec gVxz_t = NULL;
    Vec Told = varImo.find("Temp")->second;
    // arguments: time, slipVel, txy, sigmadev, dgxy, dgxz, T, old T, dt
    _he->be(time,V,tau,sdev,gVxy_t,gVxz_t,varIm["Temp"],Told,dt);
  }

  return ierr;
}


// fully dynamic: purely explicit time stepping
PetscErrorCode strikeSlip_linearElastic_qd_fd::d_dt(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev)
{
  PetscErrorCode ierr = 0;

  _fault_fd->updateFields(deltaT, var);

  // update temperature in momBal
  if (_D->_thermalCoupling == "coupled") {
    _fault_fd->updateTemperature(var.find("Temp")->second);
  }

  // update effective normal stress in fault using pore pressure
  if (_D->_hydraulicCoupling == "coupled") {
    _fault_fd->setSNEff(var.find("pressure")->second);
  }
  
  // momentum balance equation except for fault boundary
  propagateWaves(time, _deltaT, varNext, var, varPrev);

  // update fault
  _fault_fd->d_dt(time,_deltaT,varNext,var,varPrev);

  // update body u from fault u
  ierr = VecScatterBegin(*_body2fault, _fault_fd->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _fault_fd->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);

  // compute stresses
  VecCopy(varNext.find("u")->second, _material->_u);
  VecAXPY(_material->_u,1.0,_u0);
  _material->computeStresses();

  // update fault shear stress and quasi-static shear stress
  ierr = VecScatterBegin(*_body2fault, _material->_sxy, _fault_fd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _material->_sxy, _fault_fd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  VecAXPY(_fault_fd->_tauQSP, 1.0, _fault_fd->_prestress);

  // update shear stress tauP: tau = tauQS - eta_rad * slipVel
  VecPointwiseMult(_fault_fd->_tauP,_fault_qd->_eta_rad,_fault_fd->_slipVel);
  VecAYPX(_fault_fd->_tauP,-1.0,_fault_fd->_tauQSP); // tauP = -tauP + tauQSP = eta_rad*slipVel + tauQSP

  // update boundary conditions so they are consistent during output
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(_fault_fd->_slip,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  // explicitly integrate heat equation using forward Euler
  if (_D->_thermalCoupling != "no") {
    Vec V = _fault_fd->_slipVel;
    Vec tau = _fault_fd->_tauP;
    Vec sxy,sxz,sdev;
    _material->getStresses(sxy,sxz,sdev);
    Vec gVxy_t = NULL;
    Vec gVxz_t = NULL;
    Vec Tn = var.find("Temp")->second;
    Vec dTdt;
    VecDuplicate(Tn,&dTdt);
    _he->d_dt(time,V,tau,sdev,gVxy_t,gVxz_t,Tn,dTdt);
    VecWAXPY(varNext["Temp"], deltaT, dTdt, Tn); // Tn+1 = deltaT * dTdt + Tn
    _he->setTemp(varNext["Temp"]); // keep heat equation T up to date
    VecDestroy(&dTdt);
  }

  // explicitly integrate pressure equation using forward Euler
  if (_D->_hydraulicCoupling != "no") {
    Vec dPhip;
    VecDuplicate(_p->_p, &dPhip);
    Vec V = _fault_fd->_slipVel;
    // plastic porosity, as in Yang & Dunham, 2021
    if (_p->_simu == "inject" && _p->_dilatancy == "yes") {
      Vec Phip = var.find("phip")->second;
      _p->dphip_dt(Phip, V, dPhip);
      VecWAXPY(varNext["phip"], deltaT, dPhip, Phip);
      _p->setPhip(varNext["phip"]);
    }
    // viscous/plastic porosity
    else if (_p->_simu != "inject") {
      _p->dphipv_dt(dPhip, V);      
    }
    else {
      VecSet(dPhip, 0);
    }
    
    // compute dPdt and new P
    Vec Pn = var.find("pressure")->second;
    Vec dPdt;
    VecDuplicate(Pn, &dPdt);
    _p->dp_dt(time, Pn, dPhip, dPdt);
    VecWAXPY(varNext["pressure"], deltaT, dPdt, Pn);  // pn+1 = pn + deltaT*dPdt
    _p->setPressure(varNext["pressure"]);
    _p->setSNEff(varNext["pressure"]);
    _p->computeFlux(varNext["pressure"]);
    
    // compute dPhie/dt and new Phie, as in Yang & Dunham, 2021
    if (_p->_simu == "inject") {
      Vec dPhie;
      VecDuplicate(Pn, &dPhie);
      if (_p->_phiEvolve == "yes") {
	Vec Phie = var.find("phie")->second;
	_p->dphie_dt(Phie, dPdt, dPhie);
	VecWAXPY(varNext["phie"], deltaT, dPhie, Phie);
	_p->setPhie(varNext["phie"]);
      }
      VecDestroy(&dPhie);
      
      Vec Phi;
      VecDuplicate(Pn, &Phi);
      if (_p->_phiEvolve == "yes") {
	if (_p->_dilatancy == "yes") {
	  VecWAXPY(Phi, 1, varNext["phie"], varNext["phip"]);
	}
	else {
	  VecCopy(varNext["phie"],Phi);
	}
	_p->setPhi(Phi);
	VecDestroy(&Phi);
      }
    }
    else {
      Vec dPhi;
      VecDuplicate(Pn, &dPhi);
      if (_p->_phiEvolve == "yes") {
	Vec Phi = var.find("phi")->second;
	_p->dphi_dt(Phi, dPdt, dPhip, dPhi,V);
	VecWAXPY(varNext["phi"], deltaT, dPhi, Phi);
	_p->setPhi(varNext["phi"]);
      }
      VecDestroy(&dPhip);
      VecDestroy(&dPhi);
    }
    
    // compute new value of permeability = k0*(phi/phi0)^alpha
    Vec K;
    VecDuplicate(Pn, &K);
    VecPointwiseDivide(K, _p->_phi, _p->_phi0);
    VecPow(K, _p->_alpha);
    VecPointwiseMult(K, K, _p->_k0);
    _p->setPermeability(K);
    VecDestroy(&K);
    VecDestroy(&dPdt);
  }
  
  return ierr;
}


// fully dynamic: IMEX time stepping (currently not used)
PetscErrorCode strikeSlip_linearElastic_qd_fd::d_dt(const PetscScalar time, const PetscScalar deltaT, map<string,Vec>& varNext, const map<string,Vec>& var, const map<string,Vec>& varPrev, map<string,Vec>& varIm, const map<string,Vec>& varImPrev)
{
  PetscErrorCode ierr = 0;

  _fault_fd->updateFields(deltaT, var);

  // momentum balance equation except for fault boundary
  propagateWaves(time, _deltaT, varNext, var, varPrev);

  // update fault
  ierr = _fault_fd->d_dt(time,_deltaT,varNext,var,varPrev);CHKERRQ(ierr);

  // update body u from fault u
  ierr = VecScatterBegin(*_body2fault, _fault_fd->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, _fault_fd->_u, varNext["u"], INSERT_VALUES, SCATTER_REVERSE); CHKERRQ(ierr);

  // compute stresses
  VecCopy(varNext.find("u")->second, _material->_u);
  VecAXPY(_material->_u,1.0,_u0);
  _material->computeStresses();

  // update fault shear stress and quasi-static shear stress
  Vec sxy,sxz,sdev;
  _material->getStresses(sxy,sxz,sdev);
  ierr = VecScatterBegin(*_body2fault, sxy, _fault_fd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  ierr = VecScatterEnd(*_body2fault, sxy, _fault_fd->_tauQSP, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(ierr);
  VecAXPY(_fault_fd->_tauQSP, 1.0, _fault_fd->_prestress);

  // update shear stress: tau = tauQS - eta_rad * slipVel
  VecPointwiseMult(_fault_fd->_tauP,_fault_qd->_eta_rad,_fault_fd->_slipVel);
  VecAYPX(_fault_fd->_tauP,-1.0,_fault_fd->_tauQSP); // tauP = -tauP + tauQSP = eta_rad*slipVel + tauQSP

  // update boundary conditions so they are consistent during output
  if (_D->_qd_bcLType == "symmFault" || _D->_qd_bcLType == "rigidFault") {
    ierr = VecCopy(_fault_fd->_slip,_material->_bcL);CHKERRQ(ierr);
    ierr = VecScale(_material->_bcL,1.0/_faultTypeScale);CHKERRQ(ierr);
  }
  if (_D->_qd_bcRType == "remoteLoading") {
    ierr = VecSet(_material->_bcR,_D->_vL*time/_faultTypeScale);CHKERRQ(ierr);
    ierr = VecAXPY(_material->_bcR,1.0,_material->_bcRShift);CHKERRQ(ierr);
  }

  // put implicitly integrated variables here
  
  return ierr;
}


// write out time and _deltaT at each time step
PetscErrorCode strikeSlip_linearElastic_qd_fd::writeCheckpoint()
{
  PetscErrorCode ierr = 0;

  _D->_ckptNumber++;
  writeASCII(_outputDir, "ckptNumber", _D->_ckptNumber,"%i\n");
  writeASCII(_outputDir, "chkpt_currT", _currTime,"%.15e\n");
  writeASCII(_outputDir, "chkpt_stepCount", _stepCount,"%i\n");

  if (_D->_timeIntegrator == "RK32_WBE" || _D->_timeIntegrator == "RK43_WBE") {
    writeASCII(_outputDir, "chkpt_prevErr", _quadImex_qd->_errA[1],"%.15e\n");
    writeASCII(_outputDir, "chkpt_currErr", _quadImex_qd->_errA[0],"%.15e\n");
    writeASCII(_outputDir, "chkpt_deltaT", _quadImex_qd->_newDeltaT,"%.15e\n");
  }
  else {
    writeASCII(_outputDir, "chkpt_prevErr", _quadEx_qd->_errA[1],"%.15e\n");
    writeASCII(_outputDir, "chkpt_currErr", _quadEx_qd->_errA[0],"%.15e\n");
    writeASCII(_outputDir, "chkpt_deltaT", _quadEx_qd->_newDeltaT,"%.15e\n");
  }

  return ierr;
}


// write solution at each time step
PetscErrorCode strikeSlip_linearElastic_qd_fd::timeMonitor(PetscScalar time, PetscScalar deltaT, PetscInt stepCount, int& stopIntegration)
{
  PetscErrorCode ierr = 0;

  // write time and time step
  PetscPrintf(PETSC_COMM_WORLD,"%i: t = %.15e s, dt = %.5e \n",stepCount,time,deltaT);
    
  double startTime = MPI_Wtime();
  _currTime = time;
  _deltaT = deltaT;
  // don't write out the same step twice
  if (_stepCount == stepCount && _stepCount != 0) {
    return ierr;
  }
  _stepCount = stepCount;

  // write 1D fields
  if ( (_stride1D > 0 && _currTime == _D->_maxTime) || (_stride1D > 0 && stepCount % _stride1D == 0) ) {
    writeStep1D(_stepCount,time,_outputDir);
    _material->writeStep1D(_stepCount,_outputDir);

    if(_inDynamic){
      _fault_fd->writeStep(_stepCount,_outputDir);
    }
    else {
      _fault_qd->writeStep(_stepCount,_outputDir);
    }

    if (_D->_hydraulicCoupling != "no") {
      _p->writeStep(_stepCount,time,_outputDir);
    }

    if (_D->_thermalCoupling != "no") {
      _he->writeStep1D(_stepCount,time,_outputDir);
    }
  }

  // write 2D fields
  if ( (_stride2D > 0 && _currTime == _D->_maxTime) || (_stride2D > 0 && stepCount % _stride2D == 0) ) {
    writeStep2D(_stepCount,time,_outputDir);
    _material->writeStep2D(_stepCount,_outputDir);
    if (_D->_thermalCoupling != "no") {
      _he->writeStep2D(_stepCount,time,_outputDir);
    }
  }

  if (_inDynamic) {
    if (checkSwitchRegime(_fault_fd)) {
      stopIntegration = 1;
    }
  }
  else {
    if (checkSwitchRegime(_fault_qd)) {
      stopIntegration = 1;
    }
  }

  // write checkpoint and stop time integration
  if (_D->_ckpt > 0 && (stepCount % _D->_interval == 0 || stepCount >= _D->_maxStepCount || time >= _D->_maxTime)) {
    ierr = writeCheckpoint(); CHKERRQ(ierr);
    ierr = _material->writeCheckpoint(); CHKERRQ(ierr);
    ierr = _fault_qd->writeCheckpoint(); CHKERRQ(ierr);
    if (_D->_hydraulicCoupling != "no") { _p->writeCheckpoint(); }
    // if (_D->_thermalCoupling != "no") { _he->writeCheckpoint(); }
    stopIntegration = 1;
  }

  // stop integration when sNEff becomes negative at some point
  if (_D->_hydraulicCoupling == "coupled") {
    PetscReal minsNEff;
    Vec sNEff;
    VecDuplicate(_p->_p, &sNEff);
    VecSet(sNEff,0);
    VecWAXPY(sNEff,-1,_p->_p,_fault_qd->_sN);
    VecMin(sNEff, NULL, &minsNEff);
    if (minsNEff < -0.1) {
      stopIntegration = 1;
      PetscPrintf(PETSC_COMM_WORLD, "Min effective normal stress = %e is negative. Stop time integration.\n", minsNEff);
    }
    VecDestroy(&sNEff);
  }
  
  _writeTime += MPI_Wtime() - startTime;
  return ierr;
}
