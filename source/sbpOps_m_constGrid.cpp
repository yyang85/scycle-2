#include "sbpOps_m_constGrid.hpp"

#define FILENAME "sbpOps_m_constGrid.cpp"


//======================================================================
// constructor and destructor
//======================================================================
SbpOps_m_constGrid::SbpOps_m_constGrid(const int order,const PetscInt Ny,const PetscInt Nz,const PetscScalar Ly,const PetscScalar Lz,Vec& muVec)
: _order(order),_Ny(Ny),_Nz(Nz),_dy(Ly/(Ny-1.)),_dz(Lz/(Nz-1.)),
  _bcRType("unspecified"),_bcTType("unspecified"),
  _bcLType("unspecified"),_bcBType("unspecified"),
  _runTime(0),_compatibilityType("fullyCompatible"),_D2type("yz"),
  _multByH(0),_deleteMats(0)
{
  // ensure this is in an acceptable state
  setMatsToNull();
  assert(order == 2 || order == 4);
  assert(Ny > 0); assert(Nz > 0);
  assert(Ly > 0); assert(Lz > 0);
  if (Ny == 1) { _dy = Ly; }
  if (Nz == 1) { _dz = Lz; }
  assert(muVec != NULL);
  VecDuplicate(muVec, &_muVec);
  VecCopy(muVec, _muVec);

  // penalty weights
  _alphaT = -1.0; // von Neumann
  _beta= 1.0; // 1 part of Dirichlet
  if (_order == 2) {
    _alphaDy = -4.0/_dy;
    _alphaDz = -4.0/_dz;
    _h11y = 0.5 * _dy;
    _h11z = 0.5 * _dz;
  }
  else if (_order == 4) {
    _alphaDy = 2.0*-48.0/17.0 /_dy;
    _alphaDz = 2.0*-48.0/17.0 /_dz;
    _h11y = 17.0/48.0 * _dy;
    _h11z = 17.0/48.0 * _dz;
  }
}


SbpOps_m_constGrid::~SbpOps_m_constGrid()
{
  VecDestroy(&_muVec);
  MatDestroy(&_mu);

  MatDestroy(&_AR_N); MatDestroy(&_AT_N); MatDestroy(&_AL_N); MatDestroy(&_AB_N);
  MatDestroy(&_rhsL_N); MatDestroy(&_rhsR_N); MatDestroy(&_rhsT_N); MatDestroy(&_rhsB_N);
  MatDestroy(&_AR_D); MatDestroy(&_AT_D); MatDestroy(&_AL_D); MatDestroy(&_AB_D);
  MatDestroy(&_rhsL_D); MatDestroy(&_rhsR_D); MatDestroy(&_rhsT_D); MatDestroy(&_rhsB_D);

  MatDestroy(&_A);
  MatDestroy(&_D2);
  MatDestroy(&_Dy_Iz);
  MatDestroy(&_Iy_Dz);
  MatDestroy(&_Hinv); MatDestroy(&_H);
  MatDestroy(&_Hyinv_Iz); MatDestroy(&_Iy_Hzinv);
  MatDestroy(&_Hy_Iz); MatDestroy(&_Iy_Hz);
  MatDestroy(&_e0y_Iz); MatDestroy(&_eNy_Iz); MatDestroy(&_Iy_e0z); MatDestroy(&_Iy_eNz);
  MatDestroy(&_E0y_Iz); MatDestroy(&_ENy_Iz); MatDestroy(&_Iy_E0z); MatDestroy(&_Iy_ENz);
  MatDestroy(&_muxBySy_IzT); MatDestroy(&_Iy_muxBzSzT);
  MatDestroy(&_BSy_Iz); MatDestroy(&_Iy_BSz);
}


//======================================================================
// functions for setting options for class
//======================================================================

PetscErrorCode SbpOps_m_constGrid::setMatsToNull()
{
  PetscErrorCode ierr = 0;

  _mu = NULL;

  _AR = NULL; _AT = NULL; _AL = NULL; _AB = NULL;
  _rhsL = NULL; _rhsR = NULL; _rhsT = NULL; _rhsB = NULL;
  _AR_N = NULL; _AT_N = NULL; _AL_N = NULL; _AB_N = NULL;
  _rhsL_N = NULL; _rhsR_N = NULL; _rhsT_N = NULL; _rhsB_N = NULL;
  _AR_D = NULL; _AT_D = NULL; _AL_D = NULL; _AB_D = NULL;
  _rhsL_D = NULL; _rhsR_D = NULL; _rhsT_D = NULL; _rhsB_D = NULL;

  _A = NULL;
  _Dy_Iz = NULL; _Iy_Dz = NULL;
  _D2 = NULL;
  _Hinv = NULL; _H = NULL; _Hyinv_Iz = NULL; _Iy_Hzinv = NULL; _Hy_Iz = NULL; _Iy_Hz = NULL;
  _e0y_Iz = NULL; _eNy_Iz = NULL; _Iy_e0z = NULL; _Iy_eNz = NULL;
  _E0y_Iz = NULL; _ENy_Iz = NULL; _Iy_E0z = NULL; _Iy_ENz = NULL;
  _BSy_Iz = NULL; _Iy_BSz = NULL;
  _muxBySy_IzT = NULL; _Iy_muxBzSzT = NULL;

  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::setBCTypes(std::string bcR, std::string bcT, std::string bcL, std::string bcB)
{
  PetscErrorCode ierr = 0;

  // check that each string is a valid option
  assert(bcR == "Dirichlet" || bcR == "Neumann" );
  assert(bcT == "Dirichlet" || bcT == "Neumann" );
  assert(bcL == "Dirichlet" || bcL == "Neumann" );
  assert(bcB == "Dirichlet" || bcB == "Neumann" );

  _bcRType = bcR;
  _bcTType = bcT;
  _bcLType = bcL;
  _bcBType = bcB;

  return ierr;
}


// not used for this type of SBP operator
PetscErrorCode SbpOps_m_constGrid::setGrid(Vec* y, Vec* z) { return 0; }

PetscErrorCode SbpOps_m_constGrid::setMultiplyByH(const int multByH)
{
  assert( multByH == 1 || multByH == 0 );
  _multByH = multByH;
  return 0;
}

PetscErrorCode SbpOps_m_constGrid::setLaplaceType(const std::string type)
{
  assert(_D2type == "yz" || _D2type == "y" || _D2type == "z" );
  _D2type = type;
  return 0;
}

PetscErrorCode SbpOps_m_constGrid::setCompatibilityType(const string type)
{
  _compatibilityType = type;
  assert(_compatibilityType == "fullyCompatible" || _compatibilityType == "compatible" );
  return 0;
}

PetscErrorCode SbpOps_m_constGrid::changeBCTypes(std::string bcR, std::string bcT, std::string bcL, std::string bcB)
{
  PetscErrorCode ierr = 0;

  // check that each string is a valid option
  assert(bcR == "Dirichlet" || bcR == "Neumann" );
  assert(bcT == "Dirichlet" || bcT == "Neumann" );
  assert(bcL == "Dirichlet" || bcL == "Neumann" );
  assert(bcB == "Dirichlet" || bcB == "Neumann" );

  _bcRType = bcR;
  _bcTType = bcT;
  _bcLType = bcL;
  _bcBType = bcB;

  constructBCMats();
  updateA_BCs();

  if (_deleteMats) { deleteIntermediateFields(); }

  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::setDeleteIntermediateFields(const int deleteMats)
{
  PetscErrorCode ierr = 0;

  assert(deleteMats == 0 || deleteMats == 1);
  _deleteMats = deleteMats;
  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::deleteIntermediateFields()
{
  PetscErrorCode ierr = 0;

  if ( _bcRType == "Dirichlet" ) { MatDestroy(&_AR_N); MatDestroy(&_rhsR_N); }
  else if ( _bcRType == "Neumann" ) { MatDestroy(&_AR_D); MatDestroy(&_rhsR_D); }

  if ( _bcTType == "Dirichlet" ) { MatDestroy(&_AT_N); MatDestroy(&_rhsT_N); }
  else if ( _bcTType == "Neumann" ) { MatDestroy(&_AT_D); MatDestroy(&_rhsT_D); }

  if ( _bcLType == "Dirichlet" ) { MatDestroy(&_AL_N); MatDestroy(&_rhsL_N); }
  else if ( _bcLType == "Neumann" ) { MatDestroy(&_AL_D); MatDestroy(&_rhsL_D); }

  if ( _bcBType == "Dirichlet" ) { MatDestroy(&_AB_N); MatDestroy(&_rhsB_N); }
  else if ( _bcBType == "Neumann" ) { MatDestroy(&_AB_D); MatDestroy(&_rhsB_D); }

  MatDestroy(&_D2);
  MatDestroy(&_Hy_Iz); MatDestroy(&_Iy_Hz);

  return ierr;
}

//======================================================================
// functions for computing matrices
//======================================================================

// matrices not constructed until now
PetscErrorCode SbpOps_m_constGrid::computeMatrices()
{
  PetscErrorCode ierr = 0;

  sbp1D_m_constGrid sbp1D(_order,_Ny,_dy,_Nz,_dz,_compatibilityType);

  constructMu(_muVec);
  constructEs(sbp1D);
  constructes(sbp1D);
  constructHs(sbp1D);
  constructBs(sbp1D);

  construct1stDerivs(sbp1D);
  constructBCMats();
  constructA(sbp1D);

  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::constructMu(Vec& muVec)
{
  PetscErrorCode ierr = 0;

  // construct matrix mu
  MatCreate(PETSC_COMM_WORLD,&_mu);
  MatSetSizes(_mu,PETSC_DECIDE,PETSC_DECIDE,_Ny*_Nz,_Ny*_Nz);
  MatSetFromOptions(_mu);
  MatMPIAIJSetPreallocation(_mu,1,NULL,1,NULL);
  MatSeqAIJSetPreallocation(_mu,1,NULL);
  MatSetUp(_mu);
  MatDiagonalSet(_mu,muVec,INSERT_VALUES);

  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::constructEs(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode ierr = 0;

  Spmat E0y(_Ny,_Ny);
  if (_Ny > 1) { E0y(0,0,1.0); }
  kronConvert(E0y,sbp1D._Iz,_E0y_Iz,1,1);

  Spmat ENy(_Ny,_Ny);
  if (_Ny > 1) { ENy(_Ny-1,_Ny-1,1.0); }
  kronConvert(ENy,sbp1D._Iz,_ENy_Iz,1,1);

  Spmat E0z(_Nz,_Nz);
  if (_Nz > 1) { E0z(0,0,1.0); }
  kronConvert(sbp1D._Iy,E0z,_Iy_E0z,1,1);

  Spmat ENz(_Nz,_Nz);
  if (_Nz > 1) { ENz(_Nz-1,_Nz-1,1.0); }
  kronConvert(sbp1D._Iy,ENz,_Iy_ENz,1,1);

  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::constructes(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode ierr = 0;

  Spmat e0y(_Ny,1);
  if (_Ny > 1) { e0y(0,0,1.0); }
  kronConvert(e0y,sbp1D._Iz,_e0y_Iz,1,1);

  Spmat eNy(_Ny,1);
  if (_Ny > 1) { eNy(_Ny-1,0,1.0); }
  kronConvert(eNy,sbp1D._Iz,_eNy_Iz,1,1);

  Spmat e0z(_Nz,1);
  if (_Nz > 1) { e0z(0,0,1.0); }
  kronConvert(sbp1D._Iy,e0z,_Iy_e0z,1,1);

  Spmat eNz(_Nz,1);
  if (_Nz > 1) { eNz(_Nz-1,0,1.0); }
  kronConvert(sbp1D._Iy,eNz,_Iy_eNz,1,1);

  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::constructBs(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode ierr = 0;

  if (_order==2 && _BSy_Iz == NULL) { kronConvert(sbp1D._BSy,sbp1D._Iz,_BSy_Iz,3,0); }
  if (_order==4 && _BSy_Iz == NULL) { kronConvert(sbp1D._BSy,sbp1D._Iz,_BSy_Iz,5,0); }
  if (_muxBySy_IzT == NULL) { MatTransposeMatMult(_BSy_Iz,_mu,MAT_INITIAL_MATRIX,1.,&_muxBySy_IzT); }
  else{ MatTransposeMatMult(_BSy_Iz,_mu,MAT_REUSE_MATRIX,1.,&_muxBySy_IzT); }

  if (_order==2 && _Iy_BSz == NULL) { kronConvert(sbp1D._Iy,sbp1D._BSz,_Iy_BSz,3,0); }
  if (_order==4 && _Iy_BSz == NULL) { kronConvert(sbp1D._Iy,sbp1D._BSz,_Iy_BSz,5,0); }
  if (_Iy_muxBzSzT == NULL) { MatTransposeMatMult(_Iy_BSz,_mu,MAT_INITIAL_MATRIX,1.,&_Iy_muxBzSzT); }
  else{ MatTransposeMatMult(_Iy_BSz,_mu,MAT_REUSE_MATRIX,1.,&_Iy_muxBzSzT); }

  if (_deleteMats) {
    MatDestroy(&_BSy_Iz);
    MatDestroy(&_Iy_BSz);
  }

  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::constructHs(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode ierr = 0;

  // H, Hy, and Hz
  kronConvert(sbp1D._Hy,sbp1D._Iz,_Hy_Iz,1,0);
  kronConvert(sbp1D._Iy,sbp1D._Hz,_Iy_Hz,1,0);
  MatMatMult(_Hy_Iz,_Iy_Hz,MAT_INITIAL_MATRIX,1.,&_H);

  // Hinv, and Hinvy and Hinvz
  kronConvert(sbp1D._Hyinv,sbp1D._Iz,_Hyinv_Iz,1,0);
  kronConvert(sbp1D._Iy,sbp1D._Hzinv,_Iy_Hzinv,1,0);
  MatMatMult(_Hyinv_Iz,_Iy_Hzinv,MAT_INITIAL_MATRIX,1.,&_Hinv);

  return ierr;
}


// computes SAT term for von Neumann BC to be added to u
// out = alphaT * Bfact * Hinv* E * mu * D1
// out = alphaT * Bfact * H * Hinv* E * mu * D1
// scall = MAT_INITIAL_MATRIX, or MAT_REUSE_MATRIX
// Bfact = -1 or 1, instead of passing in matrix B
PetscErrorCode SbpOps_m_constGrid::constructBC_Neumann(Mat& out, Mat& Hinv, PetscScalar Bfact, Mat& E, Mat& mu, Mat& D1,MatReuse scall)
{
  PetscErrorCode ierr = 0;
  // temporary matrix using only diagonal matrices, should be efficient to generate and destroy
  Mat HinvxExmu;
  ierr = MatMatMatMult(Hinv,E,mu,MAT_INITIAL_MATRIX,1.,&HinvxExmu); CHKERRQ(ierr);

  if (!_multByH) { // if do not multiply by H
    ierr = MatMatMult(HinvxExmu,D1,scall,PETSC_DECIDE,&out); CHKERRQ(ierr);
  }
  else {
    // if do multiply by H
    ierr = MatMatMatMult(_H,HinvxExmu,D1,scall,PETSC_DECIDE,&out); CHKERRQ(ierr);
  }
  MatDestroy(&HinvxExmu);

  PetscScalar a = Bfact * _alphaT;
  MatScale(out,a);

  return ierr;
}


// computes SAT term for von Neumann BC for construction of rhs
// out = alphaT * Bfact * Hinv* e
// out = alphaT * Bfact * H * Hinv* e
// scall = MAT_INITIAL_MATRIX, or MAT_REUSE_MATRIX
// Bfact = -1 or 1, instead of passing in matrix B
PetscErrorCode SbpOps_m_constGrid::constructBC_Neumann(Mat& out, Mat& Hinv, PetscScalar Bfact, Mat& e, MatReuse scall)
{
  PetscErrorCode ierr = 0;

  if (!_multByH) { // if do not multiply by H
    ierr = MatMatMult(Hinv,e,scall,1.,&out); CHKERRQ(ierr);
  }
  else {
    // if do multiply by H
    ierr = MatMatMatMult(_H,Hinv,e,scall,1.,&out); CHKERRQ(ierr);
  }

  PetscScalar a = Bfact * _alphaT;
  MatScale(out,a);

  return ierr;
}

// computes SAT term for Dirichlet BC
// out =     Hinv * (alphaD * mu + BD1T) * E
// out = H * Hinv * (alphaD * mu + BD1T) * E
// scall = MAT_INITIAL_MATRIX, or MAT_REUSE_MATRIX
// Bfact = -1 or 1, instead of passing in matrix B
PetscErrorCode SbpOps_m_constGrid::constructBC_Dirichlet(Mat& out,PetscScalar alphaD,Mat& mu,Mat& Hinv,Mat& BD1T,Mat& E,MatReuse scall)
{
  PetscErrorCode ierr = 0;

  Mat HxHinv;
  if (!_multByH) { // if do not multiply by H
    HxHinv = Hinv;
  }
  else {
    ierr = MatMatMult(_H,Hinv,MAT_INITIAL_MATRIX,1.,&HxHinv); CHKERRQ(ierr);
  }

  Mat HinvxmuxE;
  ierr = MatMatMatMult(HxHinv,mu,E,MAT_INITIAL_MATRIX,1.,&HinvxmuxE); CHKERRQ(ierr);

  ierr = MatMatMatMult(HxHinv,BD1T,E,scall,PETSC_DECIDE,&out); CHKERRQ(ierr);
  ierr = MatAXPY(out,alphaD,HinvxmuxE,SUBSET_NONZERO_PATTERN);

  if (_multByH) { MatDestroy(&HxHinv); }

  MatDestroy(&HinvxmuxE);

  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::constructBCMats()
{
  PetscErrorCode ierr = 0;

  if (_bcRType == "Dirichlet") {
    if (_AR_D == NULL) { constructBC_Dirichlet(_AR_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_ENy_Iz,MAT_INITIAL_MATRIX); }
    if (_rhsR_D == NULL) { constructBC_Dirichlet(_rhsR_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_eNy_Iz,MAT_INITIAL_MATRIX); }
    _AR = _AR_D;
    _rhsR = _rhsR_D;
  }
  else if (_bcRType == "Neumann") {
    if (_AR_N == NULL) { constructBC_Neumann(_AR_N,_Hyinv_Iz, 1.,_ENy_Iz,_mu,_Dy_Iz,MAT_INITIAL_MATRIX); }
    if (_rhsR_N == NULL) { constructBC_Neumann(_rhsR_N,_Hyinv_Iz, 1.,_eNy_Iz,MAT_INITIAL_MATRIX); }
    _AR = _AR_N;
    _rhsR = _rhsR_N;
  }

  if (_bcTType == "Dirichlet") {
    if (_AT_D == NULL) { constructBC_Dirichlet(_AT_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_E0z,MAT_INITIAL_MATRIX); }
    if (_rhsT_D == NULL) { constructBC_Dirichlet(_rhsT_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_e0z,MAT_INITIAL_MATRIX); }
    _AT = _AT_D;
    _rhsT = _rhsT_D;
  }
  else if (_bcTType == "Neumann") {
    if (_AT_N == NULL) { constructBC_Neumann(_AT_N,_Iy_Hzinv, -1.,_Iy_E0z,_mu,_Iy_Dz,MAT_INITIAL_MATRIX); }
    if (_rhsT_N == NULL) { constructBC_Neumann(_rhsT_N,_Iy_Hzinv, -1.,_Iy_e0z,MAT_INITIAL_MATRIX); }
    _AT = _AT_N;
    _rhsT = _rhsT_N;
  }


  if (_bcLType == "Dirichlet") {
    if (_AL_D == NULL) { constructBC_Dirichlet(_AL_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_E0y_Iz,MAT_INITIAL_MATRIX); }
    if (_rhsL_D == NULL) { constructBC_Dirichlet(_rhsL_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_e0y_Iz,MAT_INITIAL_MATRIX); }
    _AL = _AL_D;
    _rhsL = _rhsL_D;
  }
  else if (_bcLType == "Neumann") {
    if (_AL_N == NULL) { constructBC_Neumann(_AL_N, _Hyinv_Iz, -1., _E0y_Iz, _mu, _Dy_Iz,MAT_INITIAL_MATRIX); }
    if (_rhsL_N == NULL) { constructBC_Neumann(_rhsL_N, _Hyinv_Iz, -1., _e0y_Iz,MAT_INITIAL_MATRIX); }
    _AL = _AL_N;
    _rhsL = _rhsL_N;
  }


  if (_bcBType == "Dirichlet") {
    if (_AB_D == NULL) { constructBC_Dirichlet(_AB_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_ENz,MAT_INITIAL_MATRIX); }
    if (_rhsB_D == NULL) { constructBC_Dirichlet(_rhsB_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_eNz,MAT_INITIAL_MATRIX); }
    _AB = _AB_D;
    _rhsB = _rhsB_D;
  }
  else if (_bcBType == "Neumann") {
    if (_AB_N == NULL) { constructBC_Neumann(_AB_N,_Iy_Hzinv, 1.,_Iy_ENz,_mu,_Iy_Dz,MAT_INITIAL_MATRIX); }
    if (_rhsB_N == NULL) { constructBC_Neumann(_rhsB_N,_Iy_Hzinv, 1.,_Iy_eNz,MAT_INITIAL_MATRIX); }
    _AB = _AB_N;
    _rhsB = _rhsB_N;
  }

  return ierr;
}

// construct // D2 = d/dy(mu d/dy) + d/dz(mu d/dz)
PetscErrorCode SbpOps_m_constGrid::constructD2(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode  ierr = 0;
  double startTime = MPI_Wtime();

  Mat Dyymu = NULL;
  Mat Dzzmu = NULL;

  if (_D2type == "yz") { // D2 = d/dy(mu d/dy) + d/dz(mu d/dz)
    ierr = constructDyymu(sbp1D,Dyymu); CHKERRQ(ierr);
    ierr = constructDzzmu(sbp1D,Dzzmu); CHKERRQ(ierr);
    ierr = MatDuplicate(Dyymu,MAT_COPY_VALUES,&_D2); CHKERRQ(ierr);
    ierr = MatAYPX(_D2,1.0,Dzzmu,DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
  }
  else if (_D2type == "y") { // D2 = d/dy(mu d/dy)
    ierr = constructDyymu(sbp1D,Dyymu); CHKERRQ(ierr);
    ierr = MatDuplicate(Dyymu,MAT_COPY_VALUES,&_D2); CHKERRQ(ierr);
  }
  else if (_D2type == "z") { // D2 = d/dz(mu d/dz)
    ierr = constructDzzmu(sbp1D,Dzzmu); CHKERRQ(ierr);
    ierr = MatDuplicate(Dzzmu,MAT_COPY_VALUES,&_D2); CHKERRQ(ierr);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Warning: sbp member 'type' not understood. Choices: 'yz', 'y', 'z'.\n");
    assert(0);
  }

  ierr = MatDestroy(&Dyymu);CHKERRQ(ierr);
  ierr = MatDestroy(&Dzzmu);CHKERRQ(ierr);

  _runTime = MPI_Wtime() - startTime;
  return 0;
}

// assumes A has not been computed before
PetscErrorCode SbpOps_m_constGrid::constructA(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode  ierr = 0;
  double startTime = MPI_Wtime();

  if (_D2 == NULL) { constructD2(sbp1D); }
  MatDuplicate(_D2,MAT_COPY_VALUES,&_A);

  if (_deleteMats) { MatDestroy(&_D2); }

  // add SAT boundary condition terms
  constructBCMats();

  if (_D2type == "yz") {
    // use new Mats _AL etc
    ierr = MatAXPY(_A,1.0,_AL,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AT,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "y") {
    ierr = MatAXPY(_A,1.0,_AL,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "z") {
    ierr = MatAXPY(_A,1.0,_AT,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Warning in SbpOps: D2type of %s not understood. Choices: 'yz', 'y', 'z'.\n",_D2type.c_str());
    assert(0);
  }
  ierr = PetscObjectSetName((PetscObject) _A, "_A");CHKERRQ(ierr);

  _runTime = MPI_Wtime() - startTime;
  return 0;
}

// update A based on new BCs
PetscErrorCode SbpOps_m_constGrid::updateA_BCs()
{
  PetscErrorCode  ierr = 0;
  double startTime = MPI_Wtime();

  if (_D2 == NULL) {
    sbp1D_m_constGrid sbp1D(_order,_Ny,_dy,_Nz,_dz,_compatibilityType);
    constructD2(sbp1D);
  }

  MatZeroEntries(_A);
  MatCopy(_D2,_A,SAME_NONZERO_PATTERN);

  if (_deleteMats) { MatDestroy(&_D2); }

  // add SAT boundary condition terms
  constructBCMats();

  if (_D2type == "yz") {
    // use new Mats _AL etc
    ierr = MatAXPY(_A,1.0,_AL,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AT,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "y") {
    ierr = MatAXPY(_A,1.0,_AL,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "z") {
    ierr = MatAXPY(_A,1.0,_AT,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Warning in SbpOps: D2type of %s not understood. Choices: 'yz', 'y', 'z'.\n",_D2type.c_str());
    assert(0);
  }

  _runTime = MPI_Wtime() - startTime;
  return 0;
}

// update A based on new BCs
PetscErrorCode SbpOps_m_constGrid::updateA_BCs(sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode  ierr = 0;
  double startTime = MPI_Wtime();

  // update D2 component of A
  if (_D2 == NULL) { constructD2(sbp1D); }
  MatZeroEntries(_A);
  MatCopy(_D2,_A,SAME_NONZERO_PATTERN);
  if (_deleteMats) { MatDestroy(&_D2); }

  // add SAT boundary condition terms
  constructBCMats();
  if (_D2type == "yz") {
    ierr = MatAXPY(_A,1.0,_AL,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AT,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "y") {
    ierr = MatAXPY(_A,1.0,_AL,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AR,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else if (_D2type == "z") {
    ierr = MatAXPY(_A,1.0,_AT,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
    ierr = MatAXPY(_A,1.0,_AB,SUBSET_NONZERO_PATTERN);CHKERRQ(ierr);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Warning in SbpOps: D2type of %s not understood. Choices: 'yz', 'y', 'z'.\n",_D2type.c_str());
    assert(0);
  }

  _runTime = MPI_Wtime() - startTime;
  return 0;
}

// update SAT matrices for boundary conditions if the variable coefficient has changed
PetscErrorCode SbpOps_m_constGrid::updateBCMats()
{
  PetscErrorCode ierr = 0;

  if (_bcRType == "Dirichlet") {
    constructBC_Dirichlet(_AR_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_ENy_Iz,MAT_REUSE_MATRIX);
    constructBC_Dirichlet(_rhsR_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_eNy_Iz,MAT_REUSE_MATRIX);
    _AR = _AR_D; _rhsR = _rhsR_D;
    MatDestroy(&_AR_N); MatDestroy(&_rhsR_N);
  }
  else if (_bcRType == "Neumann") {
    constructBC_Neumann(_AR_N,_Hyinv_Iz, 1.,_ENy_Iz,_mu,_Dy_Iz,MAT_REUSE_MATRIX);
    constructBC_Neumann(_rhsR_N,_Hyinv_Iz, 1.,_eNy_Iz,MAT_REUSE_MATRIX);
    _AR = _AR_N; _rhsR = _rhsR_N;
    MatDestroy(&_AR_D); MatDestroy(&_rhsR_D);
  }

  if (_bcTType == "Dirichlet") {
    constructBC_Dirichlet(_AT_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_E0z,MAT_REUSE_MATRIX);
    constructBC_Dirichlet(_rhsT_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_e0z,MAT_REUSE_MATRIX);
    _AT = _AT_D; _rhsT = _rhsT_D;
    MatDestroy(&_AR_D); MatDestroy(&_rhsR_D);
  }
  else if (_bcTType == "Neumann") {
    constructBC_Neumann(_AT_N,_Iy_Hzinv, -1.,_Iy_E0z,_mu,_Iy_Dz,MAT_REUSE_MATRIX);
    constructBC_Neumann(_rhsT_N,_Iy_Hzinv, -1.,_Iy_e0z,MAT_REUSE_MATRIX);
    _AT = _AT_N; _rhsT = _rhsT_N;
    MatDestroy(&_AT_D); MatDestroy(&_rhsT_D);
  }


  if (_bcLType == "Dirichlet") {
    constructBC_Dirichlet(_AL_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_E0y_Iz,MAT_REUSE_MATRIX);
    constructBC_Dirichlet(_rhsL_D,_alphaDy,_mu,_Hyinv_Iz,_muxBySy_IzT,_e0y_Iz,MAT_REUSE_MATRIX);
    _AL = _AL_D; _rhsL = _rhsL_D;
    MatDestroy(&_AL_N); MatDestroy(&_rhsL_N);
  }
  else if (_bcLType == "Neumann") {
    constructBC_Neumann(_AL_N, _Hyinv_Iz, -1., _E0y_Iz, _mu, _Dy_Iz,MAT_REUSE_MATRIX);
    constructBC_Neumann(_rhsL_N, _Hyinv_Iz, -1., _e0y_Iz,MAT_REUSE_MATRIX);
    _AL = _AL_N; _rhsL = _rhsL_N;
    MatDestroy(&_AL_D); MatDestroy(&_rhsL_D);
  }


  if (_bcBType == "Dirichlet") {
    constructBC_Dirichlet(_AB_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_ENz,MAT_REUSE_MATRIX);
    constructBC_Dirichlet(_rhsB_D,_alphaDz,_mu,_Iy_Hzinv,_Iy_muxBzSzT,_Iy_eNz,MAT_REUSE_MATRIX);
    _AB = _AB_D; _rhsB = _rhsB_D;
    MatDestroy(&_AB_N); MatDestroy(&_rhsB_N);
  }
  else if (_bcBType == "Neumann") {
    constructBC_Neumann(_AB_N,_Iy_Hzinv, 1.,_Iy_ENz,_mu,_Iy_Dz,MAT_REUSE_MATRIX);
    constructBC_Neumann(_rhsB_N,_Iy_Hzinv, 1.,_Iy_eNz,MAT_REUSE_MATRIX);
    _AB = _AB_N; _rhsB = _rhsB_N;
    MatDestroy(&_AB_D); MatDestroy(&_rhsB_D);
  }

  return ierr;
}

//======================================================================
// functions to allow user access to various matrices
//======================================================================

// map the boundary condition vectors to rhs
PetscErrorCode SbpOps_m_constGrid::setRhs(Vec&rhs,Vec &bcL,Vec &bcR,Vec &bcT,Vec &bcB)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  if (_D2type == "yz") {
    ierr = VecSet(rhs,0.0);
    ierr = MatMult(_rhsL,bcL,rhs);CHKERRQ(ierr); // rhs = _rhsL * _bcL
    ierr = MatMultAdd(_rhsR,bcR,rhs,rhs); // rhs = rhs + _rhsR * _bcR
    ierr = MatMultAdd(_rhsT,bcT,rhs,rhs);
    ierr = MatMultAdd(_rhsB,bcB,rhs,rhs);
  }
  else if (_D2type == "y") {
    ierr = VecSet(rhs,0.0);
    ierr = MatMult(_rhsL,bcL,rhs);CHKERRQ(ierr); // rhs = _rhsL * _bcL
    ierr = MatMultAdd(_rhsR,bcR,rhs,rhs); // rhs = rhs + _rhsR * _bcR
  }
  else if (_D2type == "z") {
    ierr = VecSet(rhs,0.0);
    ierr = MatMult(_rhsT,bcT,rhs);CHKERRQ(ierr);
    ierr = MatMultAdd(_rhsB,bcB,rhs,rhs);
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD,"Warning in SbpOps: D2type of %s not understood. Choices: 'yz', 'y', 'z'.\n",_D2type.c_str());
    assert(0);
  }

  _runTime += MPI_Wtime() - startTime;
  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::geth11(PetscScalar &h11y, PetscScalar &h11z) { h11y = _h11y; h11z = _h11z; return 0; }

PetscErrorCode SbpOps_m_constGrid::getAlpha(PetscScalar &alpha) { alpha = _alphaDy; return 0; }

PetscErrorCode SbpOps_m_constGrid::getA(Mat &mat) { mat = _A; return 0; }

PetscErrorCode SbpOps_m_constGrid::getH(Mat &mat) { mat = _H; return 0; }

PetscErrorCode SbpOps_m_constGrid::getHinv(Mat &mat) { mat = _Hinv; return 0; }

PetscErrorCode SbpOps_m_constGrid::getDs(Mat &Dy,Mat &Dz) { Dy = _Dy_Iz; Dz = _Iy_Dz; return 0; }

PetscErrorCode SbpOps_m_constGrid::getMus(Mat &mu,Mat &muqy,Mat &murz) { mu = _mu; muqy = _mu; murz = _mu; return 0; }

PetscErrorCode SbpOps_m_constGrid::getEs(Mat& E0y_Iz,Mat& ENy_Iz,Mat& Iy_E0z,Mat& Iy_ENz)
{
  E0y_Iz = _E0y_Iz;
  ENy_Iz = _ENy_Iz;
  Iy_E0z = _Iy_E0z;
  Iy_ENz = _Iy_ENz;
  return 0;
}

PetscErrorCode SbpOps_m_constGrid::getes(Mat& e0y_Iz,Mat& eNy_Iz,Mat& Iy_e0z,Mat& Iy_eNz)
{
  e0y_Iz = _e0y_Iz;
  eNy_Iz = _eNy_Iz;
  Iy_e0z = _Iy_e0z;
  Iy_eNz = _Iy_eNz;
  return 0;
}

PetscErrorCode SbpOps_m_constGrid::getHs(Mat& Hy_Iz,Mat& Iy_Hz)
{
  Hy_Iz = _Hy_Iz;
  Iy_Hz = _Iy_Hz;
  return 0;
}
PetscErrorCode SbpOps_m_constGrid::getHinvs(Mat& Hyinv_Iz,Mat& Iy_Hzinv)
{
  Hyinv_Iz = _Hyinv_Iz;
  Iy_Hzinv = _Iy_Hzinv;
  return 0;
}
PetscErrorCode SbpOps_m_constGrid::getCoordTrans(Mat&J, Mat& Jinv,Mat& qy,Mat& rz, Mat& yq, Mat& zr) { assert(0); return 0; }


//======================================================================


// compute Dyymu using my class Spmat
PetscErrorCode SbpOps_m_constGrid::constructDyymu(const sbp1D_m_constGrid& sbp1D, Mat &Dyymu)
{
  PetscErrorCode  ierr = 0;

  Mat Rymu,HinvxRymu;
  ierr = constructRymu(sbp1D,Rymu); CHKERRQ(ierr);
  ierr = MatMatMult(_Hyinv_Iz,Rymu,MAT_INITIAL_MATRIX,1.,&HinvxRymu); CHKERRQ(ierr);
  ierr = MatMatMatMult(_Dy_Iz,_mu,_Dy_Iz,MAT_INITIAL_MATRIX,1.,&Dyymu); CHKERRQ(ierr);
  ierr = MatAXPY(Dyymu,-1.,HinvxRymu,DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
  MatDestroy(&HinvxRymu);
  MatDestroy(&Rymu);

  if (_multByH) {
    Mat temp;
    MatMatMult(_H,Dyymu,MAT_INITIAL_MATRIX,1.,&temp); CHKERRQ(ierr);
    MatCopy(temp,Dyymu,SAME_NONZERO_PATTERN);
    MatDestroy(&temp);
  }

  return ierr;
}

// compute Dzzmu using my class Spmat
PetscErrorCode SbpOps_m_constGrid::constructDzzmu(const sbp1D_m_constGrid& sbp1D,Mat &Dzzmu)
{
  PetscErrorCode  ierr = 0;

  Mat Rzmu,HinvxRzmu;
  ierr = constructRzmu(sbp1D,Rzmu); CHKERRQ(ierr);
  ierr = MatMatMult(_Iy_Hzinv,Rzmu,MAT_INITIAL_MATRIX,1.,&HinvxRzmu); CHKERRQ(ierr);
  ierr = MatMatMatMult(_Iy_Dz,_mu,_Iy_Dz,MAT_INITIAL_MATRIX,1.,&Dzzmu); CHKERRQ(ierr);
  ierr = MatAXPY(Dzzmu,-1.,HinvxRzmu,DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);
  MatDestroy(&HinvxRzmu);
  MatDestroy(&Rzmu);

  if (_multByH) {
    Mat temp;
    MatMatMult(_H,Dzzmu,MAT_INITIAL_MATRIX,1.,&temp); CHKERRQ(ierr);
    MatCopy(temp,Dzzmu,SAME_NONZERO_PATTERN);
    MatDestroy(&temp);
  }

  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::constructRzmu(const sbp1D_m_constGrid& sbp1D,Mat &Rzmu)
{
  PetscErrorCode ierr = 0;

  switch ( _order ) {
    case 2:
    {
      Spmat D2z(_Nz,_Nz);
      Spmat C2z(_Nz,_Nz);
      sbp_Spmat2(_Nz,1.0/_dz,D2z,C2z);

      // kron(Iy,C2z)
      Mat Iy_C2z;
      {
        kronConvert(sbp1D._Iy,C2z,Iy_C2z,1,0);
      }

      // kron(Iy,D2z)
      Mat Iy_D2z;
      {
        kronConvert(sbp1D._Iy,D2z,Iy_D2z,5,0);
      }

      // Rzmu = (Iy_D2z^T x Iy_C2z x mu x Iy_D2z)/4/dz^3;
      Mat temp;
      Mat Iy_D2zT;
      MatTranspose(Iy_D2z,MAT_INITIAL_MATRIX,&Iy_D2zT);
      MatMatMult(Iy_D2zT,Iy_C2z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp);
      MatDestroy(&Iy_D2zT);
      ierr = MatMatMatMult(temp,_mu,Iy_D2z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Rzmu);CHKERRQ(ierr);
      ierr = MatScale(Rzmu,0.25*pow(_dz,3));CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) Rzmu, "Rzmu");CHKERRQ(ierr);

      MatDestroy(&temp);
      MatDestroy(&Iy_D2z);
      MatDestroy(&Iy_C2z);
      break;
    }

    case 4:
    {
      Spmat D3z(_Nz,_Nz);
      Spmat D4z(_Nz,_Nz);
      Spmat C3z(_Nz,_Nz);
      Spmat C4z(_Nz,_Nz);
      sbp_Spmat4(_Nz,1/_dz,D3z,D4z,C3z,C4z);

      Mat mu3;
      {
        PetscScalar mu=0;
        MatDuplicate(_mu,MAT_COPY_VALUES,&mu3);
        PetscInt Ii,Jj,Istart,Iend=0;
        VecGetOwnershipRange(_muVec,&Istart,&Iend);
        if (Istart==0) {
          Jj = Istart + 1;
          VecGetValues(_muVec,1,&Jj,&mu);
          MatSetValues(mu3,1,&Istart,1,&Istart,&mu,ADD_VALUES);
        }
        if (Iend==_Ny*_Nz) {
          Jj = Iend - 2;
          Ii = Iend - 1;
          VecGetValues(_muVec,1,&Jj,&mu);
          MatSetValues(mu3,1,&Ii,1,&Ii,&mu,ADD_VALUES);
        }
        for (Ii=Istart+1;Ii<Iend-1;Ii++) {
          VecGetValues(_muVec,1,&Ii,&mu);
          Jj = Ii - 1;
          MatSetValues(mu3,1,&Jj,1,&Jj,&mu,ADD_VALUES);
        }
        MatAssemblyBegin(mu3,MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(mu3,MAT_FINAL_ASSEMBLY);
        MatScale(mu3,0.5);
      }

      Mat Iy_D3z; kronConvert(sbp1D._Iy,D3z,Iy_D3z,6,0);
      Mat Iy_C3z; kronConvert(sbp1D._Iy,C3z,Iy_C3z,1,0);

      // Rzmu = (Iy_D3z^T x Iy_C3z x mu3 x Iy_D3z)/18/dy
      //      + (Iy_D4z^T x Iy_C4z x mu x Iy_D4z)/144/dy
      Mat temp1,temp2;
      Mat Iy_D3zT;
      MatTranspose(Iy_D3z,MAT_INITIAL_MATRIX,&Iy_D3zT);
      MatMatMult(Iy_D3zT,Iy_C3z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp1);
      MatDestroy(&Iy_D3zT);
      ierr = MatMatMatMult(temp1,mu3,Iy_D3z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp2);CHKERRQ(ierr);
      ierr = MatScale(temp2,1.0/_dz/18);CHKERRQ(ierr);
      MatDestroy(&temp1);
      MatDestroy(&Iy_D3z);
      MatDestroy(&Iy_C3z);
      MatDestroy(&mu3);

      Mat Iy_D4z; kronConvert(sbp1D._Iy,D4z,Iy_D4z,5,0);
      Mat Iy_C4z; kronConvert(sbp1D._Iy,C4z,Iy_C4z,1,0);

      // Rzmu = (Iy_D3z^T x Iy_C3z x mu3 x Iy_D3z)/18/dy
      //      + (Iy_D4z^T x Iy_C4z x mu x Iy_D4z)/144/dy
      Mat Iy_D4zT;
      MatTranspose(Iy_D4z,MAT_INITIAL_MATRIX,&Iy_D4zT);
      MatMatMult(Iy_D4zT,Iy_C4z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp1);
      MatDestroy(&Iy_D4zT);
      ierr = MatMatMatMult(temp1,_mu,Iy_D4z,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Rzmu);
      ierr = MatScale(Rzmu,1.0/_dz/144);CHKERRQ(ierr);

      ierr = MatAYPX(Rzmu,1.0,temp2,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) Rzmu, "Rzmu");CHKERRQ(ierr);

      MatDestroy(&temp1);
      MatDestroy(&temp2);
      MatDestroy(&Iy_D4z);
      MatDestroy(&Iy_C4z);

      break;
    }
    default:
      SETERRQ(PETSC_COMM_WORLD,1,"order not understood.");
      break;
  }

  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::constructRymu(const sbp1D_m_constGrid& sbp1D,Mat &Rymu)
{
  PetscErrorCode ierr = 0;

  switch ( _order ) {
  case 2:
    {
      Spmat D2y(_Ny,_Ny);
      Spmat C2y(_Ny,_Ny);
      sbp_Spmat2(_Ny,1/_dy,D2y,C2y);

      // kron(D2y,Iz)
      Mat D2y_Iz;
      {
        kronConvert(D2y,sbp1D._Iz,D2y_Iz,5,0);
      }

      // kron(C2y,Iz)
      Mat C2y_Iz;
      {
        kronConvert(C2y,sbp1D._Iz,C2y_Iz,5,0);
      }

      // Rymu = (D2y_Iz^T x C2y_Iz x mu x D2y_Iz)/4/dy^3;
      Mat temp;
      Mat D2y_IzT;
      MatTranspose(D2y_Iz,MAT_INITIAL_MATRIX,&D2y_IzT);
      MatMatMult(D2y_IzT,C2y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp);
      MatDestroy(&D2y_IzT);
      ierr = MatMatMatMult(temp,_mu,D2y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Rymu);CHKERRQ(ierr);
      ierr = MatScale(Rymu,0.25*pow(_dy,3));CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) Rymu, "Rymu");CHKERRQ(ierr);

      MatDestroy(&temp);
      MatDestroy(&D2y_Iz);
      MatDestroy(&C2y_Iz);

    break;
  }

  case 4:
    {
      Spmat D3y(_Ny,_Ny);
      Spmat D4y(_Ny,_Ny);
      Spmat C3y(_Ny,_Ny);
      Spmat C4y(_Ny,_Ny);
      sbp_Spmat4(_Ny,1/_dy,D3y,D4y,C3y,C4y);

      Mat mu3;
      {
        PetscScalar mu=0;
        MatDuplicate(_mu,MAT_COPY_VALUES,&mu3);
        PetscInt Ii,Jj,Istart,Iend=0;
        VecGetOwnershipRange(_muVec,&Istart,&Iend);
        if (Iend==_Ny*_Nz) {
          Jj = Iend - 2;
          Ii = Iend - 1;
          VecGetValues(_muVec,1,&Jj,&mu);
          MatSetValues(mu3,1,&Ii,1,&Ii,&mu,ADD_VALUES);
        }
        for (Ii=Istart+1;Ii<Iend;Ii++) {
          VecGetValues(_muVec,1,&Ii,&mu);
          Jj = Ii - 1;
          MatSetValues(mu3,1,&Jj,1,&Jj,&mu,ADD_VALUES);
        }
        MatAssemblyBegin(mu3,MAT_FINAL_ASSEMBLY);
        MatAssemblyEnd(mu3,MAT_FINAL_ASSEMBLY);
        MatScale(mu3,0.5);
      }

      Mat D3y_Iz; kronConvert(D3y,sbp1D._Iz,D3y_Iz,6,0);
      Mat C3y_Iz; kronConvert(C3y,sbp1D._Iz,C3y_Iz,1,0);


      // Rymu = (D3y_Iz^T x C3y_Iz x mu3 x D3y_Iz)/18/dy
      //      + (D4y_Iz^T x C4y_Iz x mu x D4y_Iz)/144/dy
      Mat temp1,temp2;
      Mat D3y_IzT;
      MatTranspose(D3y_Iz,MAT_INITIAL_MATRIX,&D3y_IzT);
      MatMatMult(D3y_IzT,C3y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp1);
      MatDestroy(&D3y_IzT);
      ierr = MatMatMatMult(temp1,mu3,D3y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp2);CHKERRQ(ierr);
      ierr = MatScale(temp2,1.0/_dy/18.0);CHKERRQ(ierr);
      MatDestroy(&temp1);
      MatDestroy(&D3y_Iz);
      MatDestroy(&C3y_Iz);
      MatDestroy(&mu3);


      Mat D4y_Iz; kronConvert(D4y,sbp1D._Iz,D4y_Iz,5,0);
      Mat C4y_Iz; kronConvert(C4y,sbp1D._Iz,C4y_Iz,1,0);

      Mat D4y_IzT;
      MatTranspose(D4y_Iz,MAT_INITIAL_MATRIX,&D4y_IzT);
      MatMatMult(D4y_IzT,C4y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp1);
      MatDestroy(&D4y_IzT);
      ierr = MatMatMatMult(temp1,_mu,D4y_Iz,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Rymu);CHKERRQ(ierr);
      ierr = MatScale(Rymu,1.0/_dy/144.0);CHKERRQ(ierr);

      ierr = MatAYPX(Rymu,1.0,temp2,DIFFERENT_NONZERO_PATTERN);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) Rymu, "Rymu");CHKERRQ(ierr);

      MatDestroy(&temp1);
      MatDestroy(&temp2);
      MatDestroy(&D4y_Iz);
      MatDestroy(&C4y_Iz);

      break;
    }
    default:
      SETERRQ(PETSC_COMM_WORLD,1,"order not understood.");
      break;
  }
  return ierr;
}


// compute matrices for 1st derivatives
PetscErrorCode SbpOps_m_constGrid::construct1stDerivs(const sbp1D_m_constGrid& sbp1D)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  kronConvert(sbp1D._D1y,sbp1D._Iz,_Dy_Iz,5,5);
  ierr = PetscObjectSetName((PetscObject) _Dy_Iz, "_Dy_Iz");CHKERRQ(ierr);

  kronConvert(sbp1D._Iy,sbp1D._D1z,_Iy_Dz,5,5);
  ierr = PetscObjectSetName((PetscObject) _Iy_Dz, "_Iy_Dz");CHKERRQ(ierr);

  _runTime = MPI_Wtime() - startTime;
  return ierr;
}

PetscErrorCode SbpOps_m_constGrid::updateVarCoeff(const Vec& coeff)
{
  PetscErrorCode  ierr = 0;
  double startTime = MPI_Wtime();

  MatDestroy(&_D2);

  // update coefficient Vec and Mat
  VecCopy(coeff,_muVec);
  MatDiagonalSet(_mu,coeff,INSERT_VALUES);

  // update Mats
  sbp1D_m_constGrid sbp1D(_order,_Ny,_dy,_Nz,_dz,_compatibilityType);
  constructBs(sbp1D);
  updateBCMats();
  updateA_BCs(sbp1D);

  _runTime = MPI_Wtime() - startTime;
  return ierr;
}


//======================== public member functions =====================


//======================= I/O functions ================================

PetscErrorCode SbpOps_m_constGrid::loadOps(const std::string inputDir)
{
  PetscErrorCode  ierr = 0;
  PetscViewer     viewer;
  double startTime = MPI_Wtime();

  int size;
  MatType matType;
  MPI_Comm_size (MPI_COMM_WORLD, &size);
  if (size > 1) {matType = MATMPIAIJ;}
  else {matType = MATSEQAIJ;}

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"A",FILE_MODE_READ,&viewer);CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_WORLD,&_A);CHKERRQ(ierr);
  ierr = MatSetType(_A,matType);CHKERRQ(ierr);
  ierr = MatLoad(_A,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,"Dy_Iz",FILE_MODE_READ,&viewer);CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_WORLD,&_Dy_Iz);CHKERRQ(ierr);
  ierr = MatSetType(_Dy_Iz,matType);CHKERRQ(ierr);
  ierr = MatLoad(_Dy_Iz,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  _runTime = MPI_Wtime() - startTime;
  return ierr;
}


PetscErrorCode SbpOps_m_constGrid::writeOps(const std::string outputDir)
{
  PetscErrorCode ierr = 0;
  double startTime = MPI_Wtime();

  writeMat(_A,outputDir + "A");

  writeMat(_Dy_Iz,outputDir + "Dy_Iz");
  writeMat(_Iy_Dz,outputDir + "Iy_Dz");

  writeMat(_rhsR,outputDir + "rhsR");
  writeMat(_rhsT,outputDir + "rhsT");
  writeMat(_rhsL,outputDir + "rhsL");
  writeMat(_rhsB,outputDir + "rhsB");
  writeMat(_AR,outputDir + "AR");
  writeMat(_AT,outputDir + "AT");
  writeMat(_AL,outputDir + "AL");
  writeMat(_AB,outputDir + "AB");

  writeMat(_H,outputDir + "H");
  writeMat(_Hinv,outputDir + "Hinv");
  writeMat(_Hyinv_Iz,outputDir + "Hyinv");
  writeMat(_Iy_Hzinv,outputDir + "Hzinv");

  writeMat(_E0y_Iz,outputDir + "E0y");
  writeMat(_ENy_Iz,outputDir + "ENy");
  writeMat(_Iy_E0z,outputDir + "E0z");
  writeMat(_Iy_ENz,outputDir + "ENz");
  writeMat(_e0y_Iz,outputDir + "ee0y");
  writeMat(_eNy_Iz,outputDir + "eeNy");
  writeMat(_Iy_e0z,outputDir + "ee0z");
  writeMat(_Iy_eNz,outputDir + "eeNz");

  _runTime = MPI_Wtime() - startTime;
  return ierr;
};




// out = Dy * in
PetscErrorCode SbpOps_m_constGrid::Dy(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  ierr = MatMult(_Dy_Iz,in,out); CHKERRQ(ierr);
  return ierr;
};

 // out = mu * Dy * in
PetscErrorCode SbpOps_m_constGrid::muxDy(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp;
  ierr = VecDuplicate(in,&temp); CHKERRQ(ierr);
  ierr = MatMult(_Dy_Iz,in,temp); CHKERRQ(ierr);
  ierr = MatMult(_mu,temp,out); CHKERRQ(ierr);

  VecDestroy(&temp);
  return ierr;
};

// out = Dy * mu * in
PetscErrorCode SbpOps_m_constGrid::Dyxmu(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp;
  ierr = VecDuplicate(in,&temp); CHKERRQ(ierr);
  ierr = MatMult(_mu,in,temp); CHKERRQ(ierr);
  ierr = MatMult(_Dy_Iz,temp,out); CHKERRQ(ierr);

  VecDestroy(&temp);
  return ierr;
};


// out = Dz * in
PetscErrorCode SbpOps_m_constGrid::Dz(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  ierr = MatMult(_Iy_Dz,in,out); CHKERRQ(ierr);
  return ierr;
};


// out = mu * Dz * in
PetscErrorCode SbpOps_m_constGrid::muxDz(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp;
  ierr = VecDuplicate(in,&temp); CHKERRQ(ierr);
  ierr = MatMult(_Iy_Dz,in,temp); CHKERRQ(ierr);
  ierr = MatMult(_mu,temp,out); CHKERRQ(ierr);

  VecDestroy(&temp);
  return ierr;
}

// out = Dz * mu * in
PetscErrorCode SbpOps_m_constGrid::Dzxmu(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp;
  ierr = VecDuplicate(in,&temp); CHKERRQ(ierr);
  ierr = MatMult(_mu,in,temp); CHKERRQ(ierr);
  ierr = MatMult(_Iy_Dz,temp,out); CHKERRQ(ierr);

  VecDestroy(&temp);
  return ierr;
}

// out = H * in
PetscErrorCode SbpOps_m_constGrid::H(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  ierr = MatMult(_H,in,out); CHKERRQ(ierr);
  return ierr;
}

// out = Hinv * in
PetscErrorCode SbpOps_m_constGrid::Hinv(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  ierr = MatMult(_Hinv,in,out); CHKERRQ(ierr);
  return ierr;
}

// out = Hy^-1 * e0y * in
PetscErrorCode SbpOps_m_constGrid::Hyinvxe0y(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_e0y_Iz,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Hyinv_Iz,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}

// out = Hy^-1 * eNy * in
PetscErrorCode SbpOps_m_constGrid::HyinvxeNy(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_eNy_Iz,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Hyinv_Iz,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}

// out = Hy^-1 * E0y * in
PetscErrorCode SbpOps_m_constGrid::HyinvxE0y(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_E0y_Iz,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Hyinv_Iz,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}

// out = Hy^-1 * eNy * in
PetscErrorCode SbpOps_m_constGrid::HyinvxENy(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_ENy_Iz,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Hyinv_Iz,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}

// out = Hz^-1 * e0z * in
PetscErrorCode SbpOps_m_constGrid::HzinvxE0z(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_Iy_E0z,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Iy_Hzinv,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}

// out = Hz^-1 * eNz * in
PetscErrorCode SbpOps_m_constGrid::HzinvxENz(const Vec &in, Vec &out)
{
  PetscErrorCode ierr = 0;
  Vec temp1;
  ierr = VecDuplicate(out,&temp1); CHKERRQ(ierr);
  ierr = MatMult(_Iy_ENz,in,temp1); CHKERRQ(ierr);
  ierr = MatMult(_Iy_Hzinv,temp1,out); CHKERRQ(ierr);

  VecDestroy(&temp1);
  return ierr;
}


//=================== functions for struct =============================

sbp1D_m_constGrid::sbp1D_m_constGrid(const PetscInt order,const PetscInt Ny,
      const PetscScalar dy,const PetscInt Nz,const PetscScalar dz,const string type)
: _order(order),_Ny(Ny),_Nz(Nz),_dy(dy),_dz(dz),
  _Hy(Ny,Ny),_Hyinv(Ny,Ny),_D1y(Ny,Ny),_D1yint(Ny,Ny),_BSy(Ny,Ny),_Iy(Ny,Ny),
  _Hz(Nz,Nz),_Hzinv(Nz,Nz),_D1z(Nz,Nz),_D1zint(Nz,Nz),_BSz(Nz,Nz),_Iz(Nz,Nz)
{
  _Iy.eye(); // matrix size is set during colon initialization
  _Iz.eye();

  sbp_Spmat(order,Ny,1./dy,_Hy,_Hyinv,_D1y,_D1yint,_BSy,type);
  sbp_Spmat(order,Nz,1./dz,_Hz,_Hzinv,_D1z,_D1zint,_BSz,type);
}

sbp1D_m_constGrid::~sbp1D_m_constGrid()
{}
