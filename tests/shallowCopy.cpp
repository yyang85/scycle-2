#include <map>
#include <string>
#include <iostream>
#include <petscvec.h>

using namespace std;

int main(int argc, char **argv) {
  Vec x, y;
  PetscInt n = 2.0;
  string outputDir = "/home/yyy910805/scycle/tests/";
  map<string, Vec> M;
  PetscErrorCode ierr = 0;
  
  PetscInitialize(&argc, &argv, NULL, NULL);
  ierr = VecCreate(PETSC_COMM_WORLD, &x); CHKERRQ(ierr);
  ierr = VecSetFromOptions(x); CHKERRQ(ierr);
  ierr = VecSetSizes(x, PETSC_DECIDE, n); CHKERRQ(ierr);
  ierr = VecSet(x, 1.0); CHKERRQ(ierr);

  ierr = VecDuplicate(x, &y); CHKERRQ(ierr);
  ierr = VecCopy(x, y); CHKERRQ(ierr);
  
  M["test"] = y;

  ierr = VecView(M["test"], PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  VecDestroy(&x);

  map<string,Vec>::iterator it;
  for (it = M.begin(); it != M.end(); it++) {
    VecDestroy(&it->second);
  }

  PetscFinalize();

  return ierr;
}

