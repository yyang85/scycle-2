
# Input file for a earthquake cycle simulation with a vertical 
# strike-slip fault and linear elastic off-fault material,
# with porosity-pressure-permeability evolution.
# Simulation starts from unsteady state with close to 0 slip velocity.

#======================================================================
# define the domain and problem type, options: 2, 4
order = 4

# set up the computational domain
Ny = 251 # points in y-direction
Nz = 5001 # points in z-direction
Ly = 50 # (km) horizontal domain size
Lz = 50 # (km) vertical domain size
vL = 0 # (m/s) loading velocity
bCoordTrans = 10 # controls grid stretching perpendicular to the fault: larger b means more grid stretching
yGridSpacingType = variableGridSpacing
zGridSpacingType = variableGridSpacing

# specify what problem to simulate
bulkDeformationType = linearElastic # off-fault constitutive law, options: powerLaw, linearElastic
## form of the momentum balance equation, options: quasidynamic, quasidynamic_and_dynamic
momentumBalanceType = quasidynamic_and_dynamic
computeICs = 1 # compute initial condition
linSolver = AMG   # AMG/CG/MUMPSLU/MUMPSCHOLESKY

#=======================================================================
# rate-and-state parameters
stateLaw = slipLaw # state variable evolution law
DcVals = [5e-4 5e-4] # (m) state evolution distance
DcDepths = [0 50] # (km)
aVals = [0.01 0.01] # direct effect parameter
aDepths = [0 50] # (km)
bVals = [0.005 0.005] # state evolution effect parameter
bDepths = [0 50] # (km)
sNVals = [50 50] # (MPa) normal stress
sNDepths = [0 50] # (km)
sNEffVals = [50 50] # (MPa) effective normal stress
sNEffDepths = [0 50] # (km)
sN_floor = 0.1 # minimum effective normal stress

#=======================================================================
# off-fault material parameters
muVals = [32.4 32.4] # (GPa) shear modulus
muDepths = [0 50] # (km)
rhoVals = [2.7 2.7] # (g/cm^3) rock density
rhoDepths = [0 50] # (km)
psiVals = [0.7 0.7]
psiDepths = [0 50]
tauQSVals = [28 28]     # MPa
tauQSDepths = [0 50]    
initState = unsteady    # options: steady, unsteady

#=======================================================================
# hydraulic coupling
hydraulicCoupling = coupled # options: no, uncoupled, coupled
simulation_type = inject  # options: perturb, steady, unsteady, inject

# if injection from middle
injectLoc = 0.5    # km
injectFlux = 2e-6  # m/s

# bottom boundary condition
bcB_type = p
bcB_pressure = 0  # MPa

# top boundary condition
bcT_type = p
bcT_pressure = 0  # MPa

# actual, physical values
phieVals = [0.05 0.05]
phieDepths = [0 50]
phipVals = [0.05 0.05]
phipDepths = [0 50]
betaVals = [1 1]        # GPa^-1
betaDepths = [0 50]
kVals = [1e-12 1e-12]   # m^2
kDepths = [0 50]
etaVals = [1e-12 1e-12] # GPa.s
etaDepths = [0 50]
pVals = [0 0]
pDepths = [0 50]
alpha = 20
eps = 2e-4
Kd = 1.3e4              # MPa
Ks = 3.6e4              # MPa

#=======================================================================
# settings for time integration
timeIntegrator = RK43_WBE # options: RK43, RK43_WBE
initTime = 0 # (s) initial time
maxTime = 2e7 # (s) final time
minDeltaT = 1e-3
maxDeltaT = 500
normType = L2_absolute # options: L2_absolute, L2_relative
timeStepTol = 1e-8  # absolute tolerance for time integration
timeIntInds = [psi slip] # variables to use to compute time step
maxStepCount = 1e7 # maximum number of time steps
maxNumCycles = 5 # max number of earthquake cycles to simulate

stride1D_qd = 25
stride2D_qd = 0 

stride1D_fd = 200
stride2D_fd = 0

stride1D_fd_end = 200 # change stride, don't write too many at end of EQ
stride2D_fd_end = 0

#=================================================================
# settings to control switching between quasidynamic and fully dynamic periods

limit_fd = 1e-1 # allow qd2fd if R has ever > limit_fd
limit_qd = 1e-7 # allow fd2qd if R has ever < limit_qd
limit_stride_fd = 5e-2 # R to switch strides from stride_fd to stride_qd
trigger_qd2fd = 1e-2   # R to transition from qd to fd
trigger_fd2qd = 1e-3   # R to transition from fd to qd
CFL = 0.8              # CFL condition to determine deltaT in coseismic

# directory for output
inputDir = /data/dunham/yyang/input_files/paper/
outputDir = /data/dunham/yyang/data/paper2/VSref_dc-_