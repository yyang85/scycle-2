#README#

Scyle-2 is a parallelized code that simulates earthquake cycles. It is coded in C++ with PETSc. The original version of the code, Scycle, developed by Kali Allison, can be viewed at: https://bitbucket.org/kallison/scycle/src. The fork is to facilitate independent code development to add additional features and implement further optimization.

Please see scycle-manual.pdf for more information on how to construct input files and run the code.

A developer's manual is now being written, and will be uploaded soon. This will provide more information to users who want a deeper knowledge of how the code works, and maybe build on the code further.

Both manuals are updated constantly as the code is modified. Please always use the most up-to-date version for reference.