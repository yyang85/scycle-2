#include <cmath>
#include <petscts.h>
#include <string>
#include <petscdmda.h>
#include <petscksp.h>

#include "genFuncs.hpp"
#include "sbpOps.hpp"
#include "sbpOps_fc.hpp"
#include "sbpOps_ms_constGrid.hpp"
#include "sbpOps_ms_varGrid.hpp"
#include "sbpOps_fc_coordTrans.hpp"
#include "spmat.hpp"

using namespace std;

double func1(const double y, const double z) { return y*y; }
double func1_D1(const double y, const double z) { return 2*y; }
double func1_D2(const double y, const double z) { return 2; }

double func2(const double y, const double z) { return sin(PETSC_PI*y); }
double func2_D1(const double y, const double z) { return PETSC_PI*cos(PETSC_PI*y); }
double func2_D2(const double y, const double z) { return -PETSC_PI*PETSC_PI*sin(PETSC_PI*y); }


double func3(const double y, const double z) { return y*y+z*z; }
double func3_D1(const double y, const double z) { return 2*y + 2*z; }
double func3_D2(const double y, const double z) { return 4; }


double func4(const double y, const double z) { return sin(PETSC_PI*y) + sin(PETSC_PI*z); }
double func4_D1(const double y, const double z) { return PETSC_PI*cos(PETSC_PI*y) + PETSC_PI*cos(PETSC_PI*z); }
double func4_D2(const double y, const double z) { return -PETSC_PI*PETSC_PI*sin(PETSC_PI*y) - PETSC_PI*PETSC_PI*sin(PETSC_PI*z); }


// set up 2D grids
PetscErrorCode setFields_DMDA(const int Ny, const int Nz, const double Ly, const double Lz, const DM& da,Vec& y, Vec& z,Vec& q, Vec& r)
{
  PetscErrorCode ierr = 0;
  PetscScalar dq = 1/((double)Ny-1);
  PetscScalar dr = 1/((double)Nz-1);

  DMCreateGlobalVector(da,&y);
  VecDuplicate(y,&z);
  VecDuplicate(y,&q);
  VecDuplicate(y,&r);

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;  // zS: start index; zE: end index
  PetscInt yE = yS + yn;

  PetscScalar  **qArr,**rArr,**yArr,**zArr;  // 2D arrays
  ierr = DMDAVecGetArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, z, &zArr);CHKERRQ(ierr);

  for (PetscInt zI = zS; zI < zE; zI++) {
    for (PetscInt yI = yS; yI < yE; yI++) {
      qArr[yI][zI] = dq * yI;   // q = 0:1 grid for y, repeated along z
      rArr[yI][zI] = dr * zI;   // r = 0:1 grid for z, repeated along y
      yArr[yI][zI] = qArr[yI][zI]*Ly;   // scale grid to Ly
      zArr[yI][zI] = rArr[yI][zI]*Lz;   // scale grid to Lz
    }
  }
  ierr = DMDAVecRestoreArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, z, &zArr);CHKERRQ(ierr);

  return ierr;
}


// function for out = Dy*in + Dz*in for 2D, only Dy*in or Dz*in for 1D
PetscErrorCode D1Mult(Mat D1, Vec in, Vec out)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(D1, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilD1(in, out);
  return(0);
}


// function for out = D2*in
PetscErrorCode D2Mult(Mat D2, Vec in, Vec out)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(D2, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilA(in, out);
  return(0);
}


// test linear solver using D1
int testKspD1(const DM &da, SbpOps_ms_constGrid &sbp, Vec &mu, const Vec &y, const Vec &z, const PetscInt Ny, const PetscInt Nz)
{
  PetscErrorCode ierr = 0;

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, yS, nz, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,NULL); CHKERRQ(ierr);
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbp,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))D1Mult); CHKERRQ(ierr);

  // create KSP context
  KSP ksp;
  PC pc;
  PetscScalar kspTol = 1e-8;
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A_Shell,A_Shell); CHKERRQ(ierr);
  KSPSetTolerances(ksp,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT);
  KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);

  // test 1st derivative
  Vec f;
  VecDuplicate(y,&f);
  VecSet(f, 0);
  if (Nz == 1) {
    mapToVec(f,func1,y,z,da);
  }
  else {
    mapToVec(f,func3,y,z,da);
  }
  VecPointwiseMult(f, f, mu);

  Vec D1f;
  VecDuplicate(y,&D1f);
  VecSet(D1f,0.0);

  Vec diff;
  VecDuplicate(D1f, &diff);
  VecSet(diff, 0);

  Vec sol_f;  // solution of f from D1*f = D1f
  VecDuplicate(f, &sol_f);
  VecSet(sol_f,0);
  PetscScalar norm;

  // KSP for stencil
  MatMult(A_Shell,f,D1f);
  KSPSolve(ksp, D1f, sol_f);
  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (stencil) difference norm = %.15e\n", norm);

  // KSP for matrix
  Mat Dy, Dz;
  sbp.getDs(Dy,Dz);
  Mat D1;
  MatDuplicate(Dy, MAT_COPY_VALUES, &D1);
  if (Nz > 1) {
    MatAXPY(D1, 1, Dz, DIFFERENT_NONZERO_PATTERN);
  }
  MatMult(D1,f,D1f);
  VecSet(sol_f,0);
  KSPSetOperators(ksp, D1, D1);
  KSPSetType(ksp,KSPCG);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCHYPRE);
  PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE);
  KSPSetFromOptions(ksp);
  KSPSetUp(ksp);
  KSPSolve(ksp, D1f, sol_f);
  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (matrix) difference norm = %.15e\n", norm);
  
  VecDestroy(&f);
  VecDestroy(&D1f);
  VecDestroy(&sol_f);
  VecDestroy(&diff);
  MatDestroy(&A_Shell);
  MatDestroy(&D1);
  KSPDestroy(&ksp);
  return ierr;
}



// test linear solver for D2
int testKspD2(const DM &da, SbpOps_ms_constGrid &sbp, Vec &mu, const Vec &y, const Vec &z, const PetscInt Ny, const PetscInt Nz)
{
  PetscErrorCode ierr = 0;

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, nz, yS, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,0); CHKERRQ(ierr); // start and end of corners, excluding ghosts
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbp,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))D2Mult); CHKERRQ(ierr);

  // create KSP context
  KSP ksp;
  PC pc;
  PetscScalar kspTol = 1e-8;
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp); CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A_Shell,A_Shell); CHKERRQ(ierr);
  KSPSetTolerances(ksp,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT);
  KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);
  ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
  ierr = KSPSetUp(ksp); CHKERRQ(ierr);

  // test that solving D2*f = D2f gives back f
  Vec f;
  VecDuplicate(y,&f);
  VecSet(f, 0);
  if (Nz == 1) {
    mapToVec(f,func1,y,z,da);
  }
  else {
    mapToVec(f,func3,y,z,da);
  }
  VecPointwiseMult(f, f, mu);

  Vec D2f;
  VecDuplicate(y,&D2f); // fyy + fzz = D2*f
  VecSet(D2f,0.0);

  Vec diff;
  VecDuplicate(D2f, &diff);
  VecSet(diff, 0);

  Vec sol_f;  // solution of f from D2*f = D2f
  VecDuplicate(f, &sol_f);
  VecSet(sol_f,0);
  PetscScalar norm;
 
  // KSP for stencil
  MatMult(A_Shell,f,D2f);
  KSPSolve(ksp, D2f, sol_f);
  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (stencil) difference norm = %.15e\n", norm);

  // KSP for matrix
  Mat D2;
  sbp.getA(D2);
  KSPSetOperators(ksp, D2, D2);
  KSPSetType(ksp,KSPCG);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCHYPRE);
  PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE);
  KSPSetFromOptions(ksp);
  KSPSetUp(ksp);
  VecSet(sol_f,0);
  KSPSolve(ksp, D2f, sol_f);
  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (matrix) difference norm = %.15e\n", norm);
  
  VecDestroy(&f);
  VecDestroy(&D2f);
  VecDestroy(&sol_f);
  VecDestroy(&diff);
  MatDestroy(&A_Shell);
  KSPDestroy(&ksp);
  return ierr;
}



int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,NULL,NULL);
  PetscErrorCode ierr = 0;

  {
    PetscInt order = 4;
    PetscInt s = order + 1;
    PetscInt Ny = 55;
    PetscScalar Ly = 1;
    PetscInt Nz = 1;
    PetscScalar Lz = 1;
    PetscScalar c = 1;

    // set 1D problem
    DM da1d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da1d);
    DMSetFromOptions(da1d);
    DMSetUp(da1d);

    Vec y,z,q,r;
    setFields_DMDA(Ny,Nz,Ly,Lz,da1d,y,z,q,r);
    Vec mu;
    VecDuplicate(y, &mu);
    VecSet(mu, c);  // set constant coefficient
    SbpOps_ms_constGrid sbp(order,Ny,Nz,Ly,Lz,mu,da1d);
    sbp.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbp.setLaplaceType("y"); CHKERRQ(ierr);
    ierr = sbp.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbp.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbp.computeMatrices(); CHKERRQ(ierr);
  
    // test if stencil implementation computes the correct 1st derivative
    PetscPrintf(PETSC_COMM_WORLD, "1D Problem:\n");
    PetscPrintf(PETSC_COMM_WORLD, "Test 1st derivative:\n");
    testKspD1(da1d, sbp, mu, y, z, Ny, Nz);

    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testKspD2(da1d, sbp, mu, y, z, Ny, Nz);
    PetscPrintf(PETSC_COMM_WORLD, "\n");
    
    // set 2D problem
    Nz = 55;
    DM da2d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da2d);
    DMSetFromOptions(da2d);
    DMSetUp(da2d);

    Vec yD,zD,qD,rD;
    setFields_DMDA(Ny,Nz,Ly,Lz,da2d,yD,zD,qD,rD);
    Vec muD;
    VecDuplicate(yD,&muD);
    VecSet(muD, c);  // set constant coefficient
    SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da2d);
    sbpD.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbpD.setLaplaceType("yz"); CHKERRQ(ierr);
    ierr = sbpD.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbpD.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbpD.computeMatrices(); CHKERRQ(ierr);

    // test if stencil implementation computes the correct 1st derivative
    PetscPrintf(PETSC_COMM_WORLD, "2D Problem:\n");
    PetscPrintf(PETSC_COMM_WORLD, "Test 1st derivative:\n");
    testKspD1(da2d, sbpD, muD, yD, zD, Ny, Nz);

    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testKspD2(da2d, sbpD, muD, yD, zD, Ny, Nz);

    VecDestroy(&y);
    VecDestroy(&z);
    VecDestroy(&q);
    VecDestroy(&r);
    VecDestroy(&mu);
    DMDestroy(&da1d);

    VecDestroy(&yD);
    VecDestroy(&zD);
    VecDestroy(&qD);
    VecDestroy(&rD);
    VecDestroy(&muD);
    DMDestroy(&da2d);
  }  

  PetscFinalize();
  return ierr;
}
