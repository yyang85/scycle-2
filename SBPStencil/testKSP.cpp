#include <cmath>
#include <petscts.h>
#include <string>
#include <petscdmda.h>
#include <petscksp.h>

#include "genFuncs.hpp"
#include "sbpOps.hpp"
#include "sbpOps_fc.hpp"
#include "sbpOps_ms_constGrid.hpp"
#include "sbpOps_ms_varGrid.hpp"
#include "sbpOps_fc_coordTrans.hpp"
#include "spmat.hpp"

using namespace std;

double func1(const double y, const double z) { return y; }
double func1_D1(const double y, const double z) { return 1; }
double func1_D2(const double y, const double z) { return 0; }


double func2(const double y, const double z) { return y+z; }
double func2_D1(const double y, const double z) { return 2; }
double func2_D2(const double y, const double z) { return 0; }


double func3(const double y, const double z) { return sin(PETSC_PI*y); }
double func3_D1(const double y, const double z) { return PETSC_PI*cos(PETSC_PI*y); }
double func3_D2(const double y, const double z) { return -PETSC_PI*PETSC_PI*sin(PETSC_PI*y); }


double func4(const double y, const double z) { return sin(PETSC_PI*y) + sin(PETSC_PI*z); }
double func4_D1(const double y, const double z) { return PETSC_PI*cos(PETSC_PI*y) + PETSC_PI*cos(PETSC_PI*z); }
double func4_D2(const double y, const double z) { return -PETSC_PI*PETSC_PI*sin(PETSC_PI*y) - PETSC_PI*PETSC_PI*sin(PETSC_PI*z); }


// set up 2D grids
PetscErrorCode setFields_DMDA(const int Ny, const int Nz, const double Ly, const double Lz, const DM& da,Vec& y, Vec& z,Vec& q, Vec& r)
{
  PetscErrorCode ierr = 0;
  PetscScalar dq = 1/((double)Ny-1);
  PetscScalar dr = 1/((double)Nz-1);

  DMCreateGlobalVector(da,&y);
  VecDuplicate(y,&z);
  VecDuplicate(y,&q);
  VecDuplicate(y,&r);

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;  // zS: start index; zE: end index
  PetscInt yE = yS + yn;

  PetscScalar  **qArr,**rArr,**yArr,**zArr;  // 2D arrays
  ierr = DMDAVecGetArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, z, &zArr);CHKERRQ(ierr);

  for (PetscInt zI = zS; zI < zE; zI++) {
    for (PetscInt yI = yS; yI < yE; yI++) {
      qArr[yI][zI] = dq * yI;   // q = 0:1 grid for y, repeated along z
      rArr[yI][zI] = dr * zI;   // r = 0:1 grid for z, repeated along y
      yArr[yI][zI] = qArr[yI][zI]*Ly;   // scale grid to Ly
      zArr[yI][zI] = rArr[yI][zI]*Lz;   // scale grid to Lz
    }
  }
  ierr = DMDAVecRestoreArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, z, &zArr);CHKERRQ(ierr);

  return ierr;
}


// function for out = D2*in
PetscErrorCode D2Mult(Mat D2, Vec in, Vec out)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(D2, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilA(in, out);
  return(0);
}


// test linear solver for D2
int testKspD2(const DM &da, SbpOps_ms_constGrid &sbp, Vec &mu, const Vec &y, const Vec &z, const PetscInt Ny, const PetscInt Nz)
{
  PetscErrorCode ierr = 0;

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, nz, yS, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,0); CHKERRQ(ierr); // start and end of corners, excluding ghosts
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbp,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))D2Mult); CHKERRQ(ierr);

  // get A (D2 + SAT) from sbp
  Mat D2;
  sbp.getA(D2);

  // create KSP context
  KSP ksp;
  PC pc;
  PetscScalar kspTol = 1e-8;
  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetOperators(ksp,A_Shell,D2);
  KSPSetType(ksp,KSPCG);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCHYPRE);
  PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE);
  KSPSetTolerances(ksp,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT);
  KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);
  KSPSetFromOptions(ksp);
  KSPSetUp(ksp);

  // test that solving D2*f = D2f gives back f, with SAT
  Vec f;
  VecDuplicate(y,&f);
  VecSet(f, 0);
  if (Nz == 1) {
    mapToVec(f,func1,y,z,da);
  }
  else {
    mapToVec(f,func2,y,z,da);
  }
  VecPointwiseMult(f, f, mu);

  Vec D2f;
  VecDuplicate(y,&D2f);
  VecSet(D2f,0.0);

  Vec diff;
  VecDuplicate(D2f, &diff);
  VecSet(diff, 0);

  Vec sol_f;  // solution of f from D2*f = D2f
  VecDuplicate(f, &sol_f);
  VecSet(sol_f,0);
  PetscScalar norm;
 
  // KSP for stencil
  MatMult(A_Shell,f,D2f);
  double startTime = MPI_Wtime();
  KSPSolve(ksp, D2f, sol_f);
  double runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"Time taken for one stencil solve = %.15e\n", runTime);
  PetscInt its;
  KSPGetIterationNumber(ksp,&its);
  PetscPrintf(PETSC_COMM_WORLD,"Number of KSP iterations = %d\n", (int)its);

  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (stencil) difference norm = %.15e\n", norm);

  // KSP for matrix
  KSP kspM;
  PC pcM;
  KSPCreate(PETSC_COMM_WORLD,&kspM);
  KSPSetOperators(kspM,D2,D2);
  KSPSetType(kspM,KSPCG);
  KSPGetPC(kspM,&pcM);
  PCSetType(pcM,PCHYPRE);
  PCFactorSetShiftType(pcM,MAT_SHIFT_POSITIVE_DEFINITE);
  KSPSetTolerances(kspM,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT);
  KSPSetInitialGuessNonzero(kspM,PETSC_TRUE);
  KSPSetFromOptions(kspM);
  KSPSetUp(kspM);

  VecSet(sol_f,0);
  VecSet(D2f,0);
  MatMult(D2,f,D2f);

  startTime = MPI_Wtime();
  KSPSolve(kspM, D2f, sol_f);
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"Time taken for one matrix solve = %.15e\n", runTime);
  KSPGetIterationNumber(kspM,&its);
  PetscPrintf(PETSC_COMM_WORLD,"Number of KSP iterations = %d\n", (int)its);

  VecWAXPY(diff, -1, sol_f, f);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"f and KSP solution (matrix) difference norm = %.15e\n", norm);
  
  VecDestroy(&f);
  VecDestroy(&D2f);
  VecDestroy(&sol_f);
  VecDestroy(&diff);
  MatDestroy(&A_Shell);
  KSPDestroy(&ksp);
  KSPDestroy(&kspM);
  return ierr;
}



int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,NULL,NULL);
  PetscErrorCode ierr = 0;

  {
    PetscInt order = 4;
    PetscInt s = order + 1;
    PetscInt Ny = 501;
    PetscScalar Ly = 1;
    PetscInt Nz = 1;
    PetscScalar Lz = 1;
    PetscScalar c = 1;

    // set 1D problem
    DM da1d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da1d);
    DMSetFromOptions(da1d);
    DMSetUp(da1d);

    Vec y,z,q,r;
    setFields_DMDA(Ny,Nz,Ly,Lz,da1d,y,z,q,r);
    Vec mu;
    VecDuplicate(y, &mu);
    VecSet(mu, c);  // set constant coefficient
    SbpOps_ms_constGrid sbp(order,Ny,Nz,Ly,Lz,mu,da1d);
    sbp.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbp.setLaplaceType("y"); CHKERRQ(ierr);
    ierr = sbp.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbp.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbp.computeMatrices(); CHKERRQ(ierr);
  
    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "1D Problem:\n");
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testKspD2(da1d, sbp, mu, y, z, Ny, Nz);
    PetscPrintf(PETSC_COMM_WORLD, "\n");
    
    // set 2D problem
    Nz = 501;
    DM da2d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da2d);
    DMSetFromOptions(da2d);
    DMSetUp(da2d);

    Vec yD,zD,qD,rD;
    setFields_DMDA(Ny,Nz,Ly,Lz,da2d,yD,zD,qD,rD);
    Vec muD;
    VecDuplicate(yD,&muD);
    VecSet(muD, c);  // set constant coefficient
    SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da2d);
    sbpD.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbpD.setLaplaceType("yz"); CHKERRQ(ierr);
    ierr = sbpD.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbpD.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbpD.computeMatrices(); CHKERRQ(ierr);

    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "2D Problem:\n");
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testKspD2(da2d, sbpD, muD, yD, zD, Ny, Nz);

    VecDestroy(&y);
    VecDestroy(&z);
    VecDestroy(&q);
    VecDestroy(&r);
    VecDestroy(&mu);
    DMDestroy(&da1d);

    VecDestroy(&yD);
    VecDestroy(&zD);
    VecDestroy(&qD);
    VecDestroy(&rD);
    VecDestroy(&muD);
    DMDestroy(&da2d);
  }  

  PetscFinalize();
  return ierr;
}
