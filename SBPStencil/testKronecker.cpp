#include <cmath>
#include <petscts.h>
#include <string>
#include <petscdmda.h>
#include <petscksp.h>
#include "spmat.hpp"

using namespace std;

int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,NULL,NULL);

  PetscErrorCode ierr = 0;

  PetscInt Ny = 3, Nz = 2;

  Spmat left(Ny, Ny);
  Spmat right(Nz, Nz);
  left.eye();  // left = identity matrix
  right(0,0,1); right(0,1,2); right(1,0,3); right(1,1,4);  // right = [1 2; 3 4]  

  Mat M;
  kronConvert(left, right, M, 2, 0);

  MatView(M, PETSC_VIEWER_STDOUT_WORLD);

  MatDestroy(&M);

  PetscFinalize();
  return ierr;
}
