#include <petscts.h>
#include <petscviewerhdf5.h>
#include <string>
#include <petscdmda.h>
#include <petscksp.h>

#include "genFuncs.hpp"
#include "sbpOps.hpp"
#include "sbpOps_fc.hpp"
#include "sbpOps_ms_constGrid.hpp"
#include "sbpOps_ms_varGrid.hpp"
#include "sbpOps_fc_coordTrans.hpp"
#include "spmat.hpp"

using namespace std;

double func(const double y,const double z) { return cos(y/45.0)*cos(z/45.0); }
double func_mu(const double y,const double z) { return cos(y/45.0)*cos(z/45.0) + 5.5; }


// set grids: y, z, q, r using DMDA
PetscErrorCode setFields_DMDA(const int Ny, const int Nz, const double Ly, const double Lz, const DM& da,Vec& y, Vec& z,Vec& q, Vec& r)
{
  PetscErrorCode ierr = 0;
  PetscScalar dq = 1.0/((double)Ny-1.);
  PetscScalar dr = 1.0/((double)Nz-1.);

  DMCreateGlobalVector(da,&y); PetscObjectSetName((PetscObject) y, "yD");
  VecDuplicate(y,&z); PetscObjectSetName((PetscObject) z, "zD");
  VecDuplicate(y,&q); PetscObjectSetName((PetscObject) q, "qD");
  VecDuplicate(y,&r); PetscObjectSetName((PetscObject) r, "rD");

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;  // zS: start index; zE: end index
v  PetscInt yE = yS + yn;

  PetscScalar  **qArr,**rArr,**yArr,**zArr;  // 2D arrays
  ierr = DMDAVecGetArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, z, &zArr);CHKERRQ(ierr);

  for (PetscInt zI = zS; zI < zE; zI++) {
    for (PetscInt yI = yS; yI < yE; yI++) {
      qArr[yI][zI] = dq * yI;   // q = 0:1 grid for y, repeated along z
      rArr[yI][zI] = dr * zI;   // r = 0:1 grid for z, repeated along y
      yArr[yI][zI] = qArr[yI][zI]*Ly;   // scale grid to Ly
      zArr[yI][zI] = rArr[yI][zI]*Lz;   // scale grid to Lz
    }
  }
  ierr = DMDAVecRestoreArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, z, &zArr);CHKERRQ(ierr);

  return ierr;
}


// set up grid using Vec
PetscErrorCode setFields_Vec(const int Ny, const int Nz, const double Ly, const double Lz, Vec& _y, Vec& _z)
{
  PetscErrorCode ierr = 0;

  Vec _q, _r;
  PetscScalar _dq = 1.0/(Ny-1.);
  PetscScalar _dr = 1.0/(Nz-1.);

  ierr = VecCreate(PETSC_COMM_WORLD,&_y); CHKERRQ(ierr);
  ierr = VecSetSizes(_y,PETSC_DECIDE,Ny*Nz); CHKERRQ(ierr);
  ierr = VecSetFromOptions(_y); CHKERRQ(ierr);
  PetscObjectSetName((PetscObject) _y, "yV");

  VecDuplicate(_y,&_z); PetscObjectSetName((PetscObject) _z, "zV");
  VecDuplicate(_y,&_q); PetscObjectSetName((PetscObject) _q, "qV");
  VecDuplicate(_y,&_r); PetscObjectSetName((PetscObject) _r, "rV");

  // construct coordinate transform
  PetscInt Ii,Istart,Iend;
  ierr = VecGetOwnershipRange(_q,&Istart,&Iend);CHKERRQ(ierr);
  PetscScalar *y,*z,*q,*r;
  VecGetArray(_y,&y);
  VecGetArray(_z,&z);
  VecGetArray(_q,&q);
  VecGetArray(_r,&r);
  PetscInt Jj = 0;
  for (Ii=Istart;Ii<Iend;Ii++) {
    q[Jj] = _dq*(Ii/Nz);
    r[Jj] = _dr*(Ii-Nz*(Ii/Nz));
    z[Jj] = r[Jj]*Lz;
    y[Jj] = q[Jj]*Ly;
    //~ double bCoordTrans = 5.0;
    //~ y[Jj] = Ly * sinh(bCoordTrans*q[Jj])/sinh(bCoordTrans);
    Jj++;
  }
  VecRestoreArray(_y,&y);
  VecRestoreArray(_z,&z);
  VecRestoreArray(_q,&q);
  VecRestoreArray(_r,&r);

  VecDestroy(&_q);
  VecDestroy(&_r);
  return ierr;
}


// compute out = d/dr(in) using central difference
PetscErrorCode compute_Dr(Vec& out, const Vec& in, const DM& da)
{
  PetscErrorCode ierr = 0;

  if (out == NULL) {
    VecDuplicate(in,&out);
    VecSet(out,0.0);
  }

  PetscInt Ny=0, Nz=0, dim=0;
  DMDAGetInfo(da,&dim,&Nz,&Ny,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
  PetscScalar dr = 1.0 / (Nz - 1.0);

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;
  PetscInt yE = yS + yn;

  Vec loutVec, linVec;
  PetscScalar **lout, **lin;
  ierr = DMCreateLocalVector(da, &loutVec);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(da, &linVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, loutVec, &lout);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da, in, INSERT_VALUES, linVec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da, in, INSERT_VALUES, linVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, linVec, &lin); CHKERRQ(ierr);

  PetscInt yI,zI;
  for (yI = yS; yI < yE; yI++) {
    for (zI = zS; zI < zE; zI++) {
      if (zI > 0 && zI < Nz - 1) { lout[yI][zI] = 0.5*(lin[yI][zI+1] - lin[yI][zI-1]); }
      else if (zI == 0) { lout[yI][zI] = -lin[yI][0] + lin[yI][1]; }
      else if (zI == Nz-1) { lout[yI][zI] = -lin[yI][zI-1] + lin[yI][zI]; }
      lout[yI][zI] = lout[yI][zI]/dr;
    }
  }

  ierr = DMDAVecRestoreArray(da, loutVec, &lout);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, linVec, &lin);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(da, loutVec, INSERT_VALUES, out);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(da, loutVec, INSERT_VALUES, out);CHKERRQ(ierr);

  VecDestroy(&loutVec);
  VecDestroy(&linVec);

  return ierr;
}


// compute out = d/dq(in) using central difference
PetscErrorCode compute_Dq(Vec& out, const Vec& in, const DM& da)
{
  PetscErrorCode ierr = 0;

  if (out == NULL) { VecDuplicate(in,&out); VecSet(out,0.0); }

  PetscInt Ny=0,Nz=0,dim=0;
  DMDAGetInfo(da,&dim,&Nz,&Ny, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
  PetscScalar dq = 1.0 / (Ny - 1.0);

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;
  PetscInt yE = yS + yn;

  Vec loutVec, linVec;
  PetscScalar** lout;
  PetscScalar** lin;
  ierr = DMCreateLocalVector(da, &loutVec);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(da, &linVec);CHKERRQ(ierr);

  ierr = DMDAVecGetArray(da, loutVec, &lout);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(da, in, INSERT_VALUES, linVec);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(da, in, INSERT_VALUES, linVec);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, linVec, &lin); CHKERRQ(ierr);

  PetscInt yI,zI;
  for (yI = yS; yI < yE; yI++) {
    for (zI = zS; zI < zE; zI++) {
      if (yI > 0 && yI < Ny - 1) { lout[yI][zI] = 0.5*(lin[yI+1][zI] - lin[yI-1][zI]); }
      else if (yI == 0) { lout[yI][zI] = -lin[0][zI] + lin[1][zI]; }
      else if (yI == Ny-1) { lout[yI][zI] = -lin[yI-1][zI] + lin[yI][zI]; }
      lout[yI][zI] = lout[yI][zI]/dq;
    }
  }

  ierr = DMDAVecRestoreArray(da, loutVec, &lout);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, linVec, &lin);CHKERRQ(ierr);
  ierr = DMLocalToGlobalBegin(da, loutVec, INSERT_VALUES, out);CHKERRQ(ierr);
  ierr = DMLocalToGlobalEnd(da, loutVec, INSERT_VALUES, out);CHKERRQ(ierr);

  VecDestroy(&loutVec);
  VecDestroy(&linVec);

  return ierr;
}


// qy = coordinate transform from q to y
PetscErrorCode compute_Dy(Vec& out, const Vec& in, const DM& da, const Vec& qy)
{
  PetscErrorCode ierr = 0;
  compute_Dq(out,in,da);
  VecPointwiseMult(out,qy,out);
  return ierr;
}


// rz = coordinate transform from r to z
PetscErrorCode compute_Dz(Vec& out, const Vec& in, const DM& da, const Vec& rz)
{
  PetscErrorCode ierr = 0;
  compute_Dr(out,in,da);
  VecPointwiseMult(out,rz,out);
  return ierr;
}


// function for Y = A*X
PetscErrorCode MyMatMult(Mat A, Vec X, Vec Y)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(A, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilA(X,Y);
  return(0);
}


// conduct timing tests
int m4()
{
  PetscErrorCode ierr = 0;

  std::string laplaceType = "yz";
  PetscInt order = 4;
  PetscInt Ny = 200, Nz = 201;
  PetscScalar Ly = 49, Lz = 50;

  // g = f but as a DMDA
  PetscInt s;  // stencil width
  if (order == 2) { s = 3; }
  if (order == 4) { s = 5; }
  DM da;
  DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da);

  // create DMDA-compatible matrices for derivatives
  Vec yD,zD,qD,rD;
  setFields_DMDA(Ny,Nz,Ly,Lz,da,yD,zD,qD,rD);
  Vec muD;
  VecDuplicate(yD,&muD);
  mapToVec(muD,func_mu,yD,zD,da);
  SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da);
  sbpD.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  ierr = sbpD.setLaplaceType(laplaceType); CHKERRQ(ierr);
  ierr = sbpD.setMultiplyByH(1); CHKERRQ(ierr);
  ierr = sbpD.setDeleteIntermediateFields(1); CHKERRQ(ierr);
  ierr = sbpD.computeMatrices(); CHKERRQ(ierr);

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, nz, yS, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,0); CHKERRQ(ierr); // start and end of corners, excluding ghosts
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbpD,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))MyMatMult_ms_constGrid); CHKERRQ(ierr);
  ierr = MatSetOption(A_Shell,MAT_SYMMETRIC,PETSC_TRUE); CHKERRQ(ierr); // if multiplyByH == 1

  // test DMDA derivative
  Vec g = NULL;
  VecDuplicate(yD,&g);
  mapToVec(g,func,yD,zD,da);
  Vec gyy = NULL;
  VecDuplicate(yD,&gyy);
  VecSet(gyy,0.0);

  PetscScalar kspTol = 1e-8;
  // create KSP context
  Mat AD;
  sbpD.getA(AD);
  KSP kspDShell = NULL;
  PC pcDShell = NULL;
  ierr = KSPCreate(PETSC_COMM_WORLD,&kspDShell); CHKERRQ(ierr);
  ierr = KSPSetType(kspDShell,KSPCG); CHKERRQ(ierr);
  ierr = KSPSetOperators(kspDShell,A_Shell,AD); CHKERRQ(ierr);
  ierr = KSPSetReusePreconditioner(kspDShell,PETSC_FALSE); CHKERRQ(ierr);
  ierr = KSPGetPC(kspDShell,&pcDShell); CHKERRQ(ierr);
  ierr = KSPSetTolerances(kspDShell,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
  ierr = PCSetType(pcDShell,PCHYPRE); CHKERRQ(ierr);
  ierr = PCFactorSetShiftType(pcDShell,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  ierr = KSPSetInitialGuessNonzero(kspDShell,PETSC_TRUE); CHKERRQ(ierr);
  ierr = KSPSetUp(kspDShell); CHKERRQ(ierr);

  Mat AD2;
  sbpD.getA(AD2);
  KSP kspDMat = NULL;
  PC pcDMat = NULL;
  ierr = KSPCreate(PETSC_COMM_WORLD,&kspDMat); CHKERRQ(ierr);
  ierr = KSPSetType(kspDMat,KSPCG); CHKERRQ(ierr);
  ierr = KSPSetOperators(kspDMat,AD2,AD2); CHKERRQ(ierr);
  ierr = KSPSetReusePreconditioner(kspDMat,PETSC_FALSE); CHKERRQ(ierr);
  ierr = KSPGetPC(kspDMat,&pcDMat); CHKERRQ(ierr);
  ierr = KSPSetTolerances(kspDMat,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
  ierr = PCSetType(pcDMat,PCHYPRE); CHKERRQ(ierr);
  ierr = PCFactorSetShiftType(pcDMat,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  ierr = KSPSetInitialGuessNonzero(kspDMat,PETSC_TRUE); CHKERRQ(ierr);
  ierr = KSPSetUp(kspDMat);

  // create Vec-compatible matrices for derivatives
  Vec yV,zV;
  setFields_Vec(Ny, Nz, Ly, Lz, yV, zV);
  Vec muV;
  VecDuplicate(yV,&muV);
  mapToVec(muV,func_mu,yV,zV);
  SbpOps_fc sbpV(order,Ny,Nz,Ly,Lz,muV);
  ierr = sbpV.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann"); CHKERRQ(ierr);
  ierr = sbpV.setLaplaceType(laplaceType);  CHKERRQ(ierr);
  ierr = sbpV.setMultiplyByH(1); CHKERRQ(ierr);
  ierr = sbpV.setDeleteIntermediateFields(1); CHKERRQ(ierr);
  ierr = sbpV.computeMatrices(); CHKERRQ(ierr);

  Vec f;
  VecDuplicate(yV,&f);
  mapToVec(f,func,yV,zV);
  Vec fyy;
  VecDuplicate(yV,&fyy);
  VecSet(fyy,0.0);

  // create KSP context
  Mat AV;
  sbpV.getA(AV);
  KSP kspV = NULL;
  PC pcV = NULL;
  ierr = KSPCreate(PETSC_COMM_WORLD,&kspV); CHKERRQ(ierr);
  ierr = KSPSetType(kspV,KSPCG); CHKERRQ(ierr);
  ierr = KSPSetOperators(kspV,AV,AV); CHKERRQ(ierr);
  ierr = KSPSetReusePreconditioner(kspV,PETSC_FALSE); CHKERRQ(ierr);
  ierr = KSPGetPC(kspV,&pcV); CHKERRQ(ierr);
  ierr = KSPSetTolerances(kspV,kspTol,kspTol,PETSC_DEFAULT,PETSC_DEFAULT); CHKERRQ(ierr);
  ierr = PCSetType(pcV,PCHYPRE); CHKERRQ(ierr);
  ierr = PCFactorSetShiftType(pcV,MAT_SHIFT_POSITIVE_DEFINITE); CHKERRQ(ierr);
  ierr = KSPSetInitialGuessNonzero(kspV,PETSC_TRUE); CHKERRQ(ierr);
  KSPSetUp(kspV);

  int NN = 5;
  PetscPrintf(PETSC_COMM_WORLD,"\n\nRun time for MatMult operations:\n");
  // time mat shell operation
  double startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    MatMult(A_Shell,g,gyy);
  }
  double runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"shell = %.15e\n",runTime/NN);

  // time DMDA Mat MatMult operation
  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    MatMult(AD,g,gyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"DMDA Mat = %.15e\n",runTime/NN);

  // time Mat MatMult operation
  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    MatMult(AV,f,fyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"non-DMDA Mat = %.15e\n",runTime/NN);


  PetscPrintf(PETSC_COMM_WORLD,"\n\nRun time for KSPSolve operations:\n");
  // time Mat MatMult operation
  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    KSPSolve(kspDShell,g,gyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"KSPSolve DMDA shell = %.15e\n",runTime/NN);

  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    KSPSolve(kspDMat,g,gyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"KSPSolve DMDA Mat = %.15e\n",runTime/NN);

  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    KSPSolve(kspV,f,fyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"KSPSolve non-DMDA Mat = %.15e\n",runTime/NN);

  
  // compare with update variable coefficient
  PetscPrintf(PETSC_COMM_WORLD,"\n\nRun time for updateVarCoeff operations:\n");

  Vec coeffD;
  VecDuplicate(muD,&coeffD);
  VecCopy(muD,coeffD);
  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    sbpD.updateVarCoeff(coeffD);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"updateVarCoeff DMDA Shell = %.15e\n",runTime/NN);

  Vec coeffV;
  VecDuplicate(muV,&coeffV);
  VecCopy(muV,coeffV);
  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    sbpV.updateVarCoeff(coeffV);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"updateVarCoeff Mat = %.15e\n",runTime/NN);


  // compare with update variable coefficient and KSPSolve
  PetscPrintf(PETSC_COMM_WORLD,"\n\nRun time for updateVarCoeff + KSPSolve operations:\n");

  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    sbpD.updateVarCoeff(coeffD);
    KSPSolve(kspDShell,g,gyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"updateVarCoeff + KSPSolve DMDA Shell = %.15e\n",runTime/NN);

  startTime = MPI_Wtime();
  for (int ii=0; ii<NN; ii++) {
    sbpV.updateVarCoeff(coeffV);
    KSPSolve(kspV,g,gyy);
  }
  runTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"updateVarCoeff + KSPSolve Mat = %.15e\n",runTime/NN);

  KSPDestroy(&kspDShell);
  KSPDestroy(&kspDMat);
  KSPDestroy(&kspV);

  return ierr;
}


// try to create a MatShell version of A
int m3()
{
  PetscErrorCode ierr = 0;

  std::string laplaceType = "yz";
  PetscInt order = 4;
  PetscInt Ny = 50, Nz = 51;
  PetscScalar Ly = 49, Lz = 50;

  // g = f but as a DMDA
  PetscInt s;
  if (order == 2) { s = 3; }
  if (order == 4) { s = 5; }
  DM da;
  DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da);

  // create DMDA-compatible matrices for derivatives
  Vec yD,zD,qD,rD;
  setFields_DMDA(Ny,Nz,Ly,Lz,da,yD,zD,qD,rD);
  Vec muD;
  VecDuplicate(yD,&muD);
  mapToVec(muD,func_mu,yD,zD,da);

  SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da);
  sbpD.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  sbpD.setLaplaceType(laplaceType);
  sbpD.setMultiplyByH(1);
  sbpD.setDeleteIntermediateFields(1);
  sbpD.computeMatrices();

  // test DMDA derivative
  Vec g = NULL;
  VecDuplicate(yD,&g);
  PetscObjectSetName((PetscObject) g, "g");
  mapToVec(g,func,yD,zD,da);
  Vec gy = NULL;
  VecDuplicate(yD,&gy);
  PetscObjectSetName((PetscObject) gy, "gy");
  sbpD.stencilA(g,gy);
  Vec gyym = NULL;
  VecDuplicate(yD,&gyym);
  PetscObjectSetName((PetscObject) gyym, "gyym");
  Mat AD;
  sbpD.getA(AD); // MatMult(AD,g,gyym);

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, nz, yS, zS;
  DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,0); // start and end of corners, excluding ghosts
  MatCreateShell(PETSC_COMM_WORLD,nz*ny,ny*ny,Nz*Ny,Nz*Ny,(void*)&sbpD,&A_Shell);
  MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))MyMatMult_ms_constGrid);
  MatSetOption(A_Shell,MAT_SYMMETRIC,PETSC_TRUE); // if multiplyByH == 1

  // time mat shell operation
  double startTime = MPI_Wtime();
  for (int ii=0; ii<100; ii++) {
    MatMult(A_Shell,g,gyym);
  }
  double shellTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"shellTime = %.15e\n",shellTime/100);

  // time DMDA Mat MatMult operation
  startTime = MPI_Wtime();
  for (int ii=0; ii<100; ii++) {
    MatMult(AD,g,gyym);
  }
  double matDTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"matDTime = %.15e\n",matDTime/100);


  // create Vec-compatible matrices for derivatives
  Vec yV,zV;
  setFields_Vec(Ny, Nz, Ly, Lz, yV, zV);
  Vec muV;
  VecDuplicate(yV,&muV);
  mapToVec(muV,func_mu,yV,zV); // VecSet(muV,1.0);
  SbpOps_fc sbpV(order,Ny,Nz,Ly,Lz,muV);
  sbpV.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  sbpV.setLaplaceType(laplaceType);
  sbpV.setMultiplyByH(1);
  sbpV.setDeleteIntermediateFields(1);
  sbpV.computeMatrices();
  Vec f;
  VecDuplicate(yV,&f);
  mapToVec(f,func,yV,zV);
  Vec fyy;
  VecDuplicate(yV,&fyy);
  VecSet(fyy,0.0);

  // time Mat MatMult operation
  Mat AV;
  sbpV.getA(AV);
  startTime = MPI_Wtime();
  for (int ii=0; ii<100; ii++) {
    MatMult(AV,f,fyy);
  }
  double matVTime = MPI_Wtime() - startTime;
  PetscPrintf(PETSC_COMM_WORLD,"matVTime = %.15e\n",matVTime/100);

  
  // time update coefficient step
  KSP ksp;
  PC pc;
  PetscScalar _kspTol = 1e-8;
  KSPCreate(PETSC_COMM_WORLD,&ksp);
  KSPSetType(ksp,KSPCG);
  //~ KSPSetOperators(ksp,AD,AD); // mat A, mat PC
  KSPSetOperators(ksp,A_Shell,AD); // this works! can use MatShell for CG
  KSPSetReusePreconditioner(ksp,PETSC_FALSE);
  KSPGetPC(ksp,&pc);
  KSPSetTolerances(ksp,_kspTol,_kspTol,PETSC_DEFAULT,PETSC_DEFAULT);
  PCSetType(pc,PCHYPRE);
  PCFactorSetShiftType(pc,MAT_SHIFT_POSITIVE_DEFINITE);
  KSPSetInitialGuessNonzero(ksp,PETSC_TRUE);

  VecSet(g,0.0);
  KSPSolve(ksp,g,gyym);

  return ierr;
}


// test how to create a matrix that performs a first derivative on a DMDA
int m2()
{
  PetscErrorCode ierr = 0;

  std::string laplaceType = "yz";
  PetscInt order = 4;
  PetscInt Ny = 51, Nz = 51;
  PetscScalar Ly = 50, Lz = 50;

  // g = f but as a DMDA
  PetscInt s;
  if (order == 2) { s = 3; }
  if (order == 4) { s = 5; }
  DM da;
  DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da);

  // create DMDA-compatible matrices for derivatives
  Vec yD,zD,qD,rD;
  setFields_DMDA(Ny,Nz,Ly,Lz,da,yD,zD,qD,rD);
  Vec muD;
  VecDuplicate(yD,&muD);
  mapToVec(muD,func_mu,yD,zD,da);

  SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da);
  sbpD.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  sbpD.setLaplaceType(laplaceType);
  sbpD.setMultiplyByH(1);
  sbpD.setDeleteIntermediateFields(1);
  sbpD.computeMatrices();

  // test DMDA derivative
  Vec g = NULL;
  VecDuplicate(yD,&g);
  PetscObjectSetName((PetscObject) g, "g");
  mapToVec(g,func,yD,zD,da);
  Vec gy = NULL;
  VecDuplicate(yD,&gy);
  PetscObjectSetName((PetscObject) gy, "gy");
  sbpD.stencilA(g,gy);
  Vec gyym = NULL;
  VecDuplicate(yD,&gyym);
  PetscObjectSetName((PetscObject) gyym, "gyym");
  Mat AD;
  sbpD.getA(AD);
  MatMult(AD,g,gyym);

  // create Vec-compatible matrices for derivatives
  Vec yV,zV;
  setFields_Vec(Ny, Nz, Ly, Lz, yV, zV);
  Vec muV;
  VecDuplicate(yV,&muV);
  mapToVec(muV,func_mu,yV,zV);
  SbpOps_fc sbpV(order,Ny,Nz,Ly,Lz,muV);
  sbpV.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  sbpV.setLaplaceType(laplaceType);
  sbpV.setMultiplyByH(1);
  sbpV.setDeleteIntermediateFields(1);
  sbpV.computeMatrices();

  // construct derivative for Vecs
  Vec f = NULL;
  VecDuplicate(yV,&f);
  PetscObjectSetName((PetscObject) f, "f");
  mapToVec(f,func,yV,zV);
  Vec fy = NULL;
  VecDuplicate(yV,&fy);
  PetscObjectSetName((PetscObject) fy, "fy");
  sbpV.Dy(f,fy);
  Mat A;
  sbpV.getA(A);
  MatMult(A,f,fy);

  return ierr;
}


// test how to create a matrix that performs a first derivative on a DMDA
int m1()
{
  PetscErrorCode ierr = 0;

  PetscInt Ny = 11, Nz = 11;
  PetscScalar Ly = 1, Lz = 1;

  // make coordinates y and z
  Vec yV,zV;
  setFields_Vec(Ny, Nz, Ly, Lz, yV, zV);

  // f = cos(y)*sin(z)
  Vec f;
  VecDuplicate(yV,&f);
  VecSet(f,0.0);
  mapToVec(f,func,yV,zV);

  // create matrices for derivatives
  Vec mu;
  VecDuplicate(yV,&mu);
  PetscObjectSetName((PetscObject) mu, "mu");
  VecSet(mu,1.0);
  SbpOps_fc sbp(2,Ny,Nz,Ly,Lz,mu);
  sbp.setBCTypes("Dirichlet","Neumann","Dirichlet","Neumann");
  sbp.setMultiplyByH(1);
  sbp.setDeleteIntermediateFields(1);
  sbp.computeMatrices(); // actually create the matrices

  // compute first derivative via matrix
  Vec fy;
  VecDuplicate(f,&fy);
  PetscObjectSetName((PetscObject) fy, "fy");
  Vec fz;
  VecDuplicate(f,&fz);
  PetscObjectSetName((PetscObject) fz, "fz");
  sbp.Dy(f,fy);
  sbp.Dz(f,fz);

  // g = f but as a DMDA
  DM da;
  PetscInt s = 2;
  DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da);
  Vec yD,zD,qD,rD,g;
  DMCreateGlobalVector(da,&g);
  setFields_DMDA(Ny,Nz,Ly,Lz,da,yD,zD,qD,rD);
  mapToVec(g,func,yD,zD,da);

  // compute Jacobian for stencils
  Vec qy = NULL;
  VecDuplicate(qD,&qy);
  PetscObjectSetName((PetscObject) qy, "qy");
  Vec rz = NULL;
  VecDuplicate(rD,&rz);
  PetscObjectSetName((PetscObject) rz, "rz");
  compute_Dq(qy, qD, da);
  VecReciprocal(qy);
  compute_Dr(rz, rD, da);
  VecReciprocal(rz);


  //~ // compute first derivative with stencil
  //~ Vec gy = NULL;  VecDuplicate(g,&gy); PetscObjectSetName((PetscObject) gy, "gy");
  //~ Vec gz = NULL;  VecDuplicate(g,&gz); PetscObjectSetName((PetscObject) gz, "gz");
  //~ compute_Dy(gy, g, da,qy);
  //~ compute_Dz(gz, g, da,rz);

  //~ Mat Dq;
  //~ compute_Dq_s(Dq,da);

  //~ Vec gy_Mat = NULL;  VecDuplicate(yD,&gy_Mat); PetscObjectSetName((PetscObject) gy_Mat, "gy_Mat");
  //~ MatMult(Dq,g,gy_Mat);



  // check implementation of computeStencil_y
  TempMats_fc tempMats(2,Ny,1.0/(Ny-1.0),Nz,1.0/(Nz-1.0),"fullyCompatible");
  Vec hy = NULL;  VecDuplicate(yD,&hy); PetscObjectSetName((PetscObject) hy, "hy");
  //~ computeStencil_y(hy,tempMats._D1y, g, da);
  computeStencil_y(hy,tempMats._BSy, g, da);
  //~ computeStencil_z(hy,tempMats._BSz, g, da);


  //~ Vec hz = NULL;  VecDuplicate(yD,&hz); PetscObjectSetName((PetscObject) hz, "hz");
  //~ computeStencil_z(hz,tempMats._BSy, g, da);
/*
  //~ // check implementation of kronCronvert for da
  Mat mat;
  //~ kronConvert(tempMats._D1y,tempMats._Iz,mat,5,5, da);
  kronConvert(tempMats._Iy,tempMats._D1z,mat,5,5, da);
  //~ kronConvert(tempMats._Hy,tempMats._Iz,mat,1,1, da);

  Vec gy_Mat2 = NULL;  VecDuplicate(yD,&gy_Mat2); PetscObjectSetName((PetscObject) gy_Mat2, "gy_Mat2");
  MatMult(mat,g,gy_Mat2);
  * */

  //~ PetscPrintf(PETSC_COMM_WORLD,"\n\nDq:\n");
  //~ MatView(Dq,PETSC_VIEWER_STDOUT_WORLD);
  //~ PetscPrintf(PETSC_COMM_WORLD,"\n\nmat:\n");
  //~ MatView(mat,PETSC_VIEWER_STDOUT_WORLD);

  //~ PetscInt n_DMDA;
  //~ VecGetSize(gy_Mat,&n_DMDA); PetscPrintf(PETSC_COMM_WORLD,"\nlength(gy_Mat) = %i\n",n_DMDA);

  //~ VecView(fy,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(gy,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(gy_Mat,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(hy,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(fz,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(gz,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(hz,PETSC_VIEWER_STDOUT_WORLD);

  //~ VecView(gy_Mat,PETSC_VIEWER_STDOUT_WORLD);
  //~ VecView(gy_Mat2,PETSC_VIEWER_STDOUT_WORLD);

  return ierr;
}


int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,NULL,NULL);

  PetscErrorCode ierr = 0;

  //~ m1(); // explore DMDA
  //~ m2(); // test implementation of SBP mixed matrix-based and matrix-free
  //~ m3(); // try to implement MatShell
  m4(); // run time analysis

  PetscFinalize();
  return ierr;
}
