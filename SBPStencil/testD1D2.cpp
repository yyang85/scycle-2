#include <cmath>
#include <petscts.h>
#include <string>
#include <petscdmda.h>
#include <petscksp.h>

#include "genFuncs.hpp"
#include "sbpOps.hpp"
#include "sbpOps_fc.hpp"
#include "sbpOps_ms_constGrid.hpp"
#include "sbpOps_ms_varGrid.hpp"
#include "sbpOps_fc_coordTrans.hpp"
#include "spmat.hpp"

using namespace std;


double func1(const double y, const double z) { return y+z; }
double func1_D1(const double y, const double z) { return 2; }
double func1_D2(const double y, const double z) { return 0; }


double func2(const double y, const double z) { return sin(PETSC_PI*y) + sin(PETSC_PI*z); }
double func2_D1(const double y, const double z) { return PETSC_PI*cos(PETSC_PI*y) + PETSC_PI*cos(PETSC_PI*z); }
double func2_D2(const double y, const double z) { return -PETSC_PI*PETSC_PI*sin(PETSC_PI*y) - PETSC_PI*PETSC_PI*sin(PETSC_PI*z); }


// set up 2D grids
PetscErrorCode setFields_DMDA(const int Ny, const int Nz, const double Ly, const double Lz, const DM& da,Vec& y, Vec& z,Vec& q, Vec& r)
{
  PetscErrorCode ierr = 0;
  PetscScalar dq = 1/((double)Ny-1);
  PetscScalar dr = 1/((double)Nz-1);

  DMCreateGlobalVector(da,&y);
  VecDuplicate(y,&z);
  VecDuplicate(y,&q);
  VecDuplicate(y,&r);

  PetscInt zS,yS,zn,yn;
  DMDAGetCorners(da, &zS, &yS, 0, &zn, &yn, 0);
  PetscInt zE = zS + zn;  // zS: start index; zE: end index
  PetscInt yE = yS + yn;

  PetscScalar  **qArr,**rArr,**yArr,**zArr;  // 2D arrays
  ierr = DMDAVecGetArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, z, &zArr);CHKERRQ(ierr);

  for (PetscInt zI = zS; zI < zE; zI++) {
    for (PetscInt yI = yS; yI < yE; yI++) {
      qArr[yI][zI] = dq * yI;   // q = 0:1 grid for y, repeated along z
      rArr[yI][zI] = dr * zI;   // r = 0:1 grid for z, repeated along y
      yArr[yI][zI] = qArr[yI][zI]*Ly;   // scale grid to Ly
      zArr[yI][zI] = rArr[yI][zI]*Lz;   // scale grid to Lz
    }
  }
  ierr = DMDAVecRestoreArray(da, q, &qArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, r, &rArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, y, &yArr);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da, z, &zArr);CHKERRQ(ierr);

  return ierr;
}


// function for out = Dy*in + Dz*in for 2D, only Dy*in or Dz*in for 1D
PetscErrorCode D1Mult(Mat D1, Vec in, Vec out)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(D1, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilD1(in, out);
  return(0);
}


// function for out = D2*in
PetscErrorCode D2Mult(Mat D2, Vec in, Vec out)
{
  void                        *ptr;
  SbpOps_ms_constGrid         *user;
  MatShellGetContext(D2, &ptr);
  user = (SbpOps_ms_constGrid*)ptr;
  user->stencilA(in, out);
  return(0);
}


// check if 1st derivative computations by stencil are correct
int testStencilD1(const DM &da, SbpOps_ms_constGrid &sbp, Vec &mu, const Vec &y, const Vec &z, const PetscInt Ny, const PetscInt Nz)
{
  PetscErrorCode ierr = 0;

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, yS, nz, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,NULL); CHKERRQ(ierr);
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbp,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))D1Mult); CHKERRQ(ierr);

  // test 1st derivative
  Vec f = NULL;
  VecDuplicate(y,&f);
  VecSet(f, 0);
  mapToVec(f,func1,y,z,da);
  VecPointwiseMult(f, f, mu); // function to be differentiated

  Vec D1f = NULL;  // to use stencil computation
  VecDuplicate(y,&D1f); // fy = D1*f
  VecSet(D1f,0.0);

  Vec D1f_D = NULL;  // to use matrix multiplication
  VecDuplicate(y,&D1f_D);
  VecSet(D1f_D,0.0);

  Vec D1f_real = NULL;
  VecDuplicate(y,&D1f_real);
  VecSet(D1f_real,0);
  mapToVec(D1f_real,func1_D1,y,z,da);
  VecPointwiseMult(D1f_real, D1f_real, mu);  // analytical solution

  Vec diff;
  VecDuplicate(D1f, &diff);
  VecSet(diff, 0);

  PetscScalar norm;
  
  // MatShell operation
  MatMult(A_Shell,f,D1f);
  VecWAXPY(diff, -1, D1f, D1f_real);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Stencil and analytical solution difference norm = %.15e\n", norm);

  // matrix multiplciation
  Mat Dy, Dz;
  sbp.getDs(Dy, Dz);
  MatMult(Dy, f, D1f_D);
  Vec temp;
  VecDuplicate(D1f_D, &temp);
  MatMult(Dz, f, temp);
  VecAXPY(D1f_D, 1, temp);
  VecDestroy(&temp);
  VecWAXPY(diff, -1, D1f, D1f_D);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Stencil and matrix computation difference norm = %.15e\n", norm);

  VecWAXPY(diff, -1, D1f_D, D1f_real);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Matrix and analytical solution difference norm = %.15e\n", norm);
  
  //writeVec(D1f, "D1f_11");
  //writeVec(D1f_real, "D1f_real_11");
  //writeVec(D1f_D, "D1f_D_11");

  VecDestroy(&f);
  VecDestroy(&D1f);
  VecDestroy(&D1f_D);
  VecDestroy(&D1f_real);
  VecDestroy(&diff);
  MatDestroy(&A_Shell);
  return ierr;
}



// check if 2nd derivative computations by stencil are correct
int testStencilD2(const DM &da, SbpOps_ms_constGrid &sbp, Vec &mu, const Vec &y, const Vec &z, const PetscInt Ny, const PetscInt Nz)
{
  PetscErrorCode ierr = 0;

  // create shell matrix
  Mat A_Shell;
  PetscInt ny, nz, yS, zS;
  ierr = DMDAGetCorners(da,&zS,&yS,NULL,&nz,&ny,0); CHKERRQ(ierr); // start and end of corners, excluding ghosts
  ierr = MatCreateShell(PETSC_COMM_WORLD,nz*ny,nz*ny,Nz*Ny,Nz*Ny,(void*)&sbp,&A_Shell); CHKERRQ(ierr);
  ierr = MatShellSetOperation(A_Shell,MATOP_MULT,(void(*)(void))D2Mult); CHKERRQ(ierr);

  // test 2nd derivative
  Vec f = NULL;
  VecDuplicate(y,&f);
  VecSet(f, 0);
  mapToVec(f,func1,y,z,da);
  VecPointwiseMult(f, f, mu); // function to be differentiated

  Vec D2f = NULL;  // to use stencil computation
  VecDuplicate(y,&D2f); // fyy + fzz = D2*f
  VecSet(D2f,0.0);

  Vec D2f_D = NULL;  // to use matrix multiplication
  VecDuplicate(y,&D2f_D);
  VecSet(D2f_D,0.0);

  Vec D2f_real = NULL;
  VecDuplicate(y,&D2f_real);
  VecSet(D2f_real,0);
  mapToVec(D2f_real,func1_D2,y,z,da);
  VecPointwiseMult(D2f_real, D2f_real, mu);  // analytical solution

  Vec diff;
  VecDuplicate(D2f, &diff);
  VecSet(diff, 0);

  PetscScalar norm;
  
  // MatShell operation
  MatMult(A_Shell,f,D2f);
  VecWAXPY(diff, -1, D2f, D2f_real);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Stencil and analytical solution difference norm = %.15e\n", norm);

  // matrix multiplciation
  Mat AD;
  sbp.getA(AD);
  MatMult(AD, f, D2f_D);
  VecWAXPY(diff, -1, D2f, D2f_D);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Stencil and matrix computation difference norm = %.15e\n", norm);

  VecWAXPY(diff, -1, D2f_D, D2f_real);
  VecNorm(diff, NORM_2, &norm);
  norm = norm/sqrt(Ny*Nz);
  PetscPrintf(PETSC_COMM_WORLD,"Matrix and analytical solution difference norm = %.15e\n", norm);

  //writeVec(D2f, "D2f_11");
  //writeVec(D2f_real, "D2f_real_11");
  //writeVec(D2f_D, "D2f_D_11");
  
  VecDestroy(&f);
  VecDestroy(&D2f);
  VecDestroy(&D2f_D);
  VecDestroy(&D2f_real);
  VecDestroy(&diff);
  MatDestroy(&A_Shell);
  return ierr;
}



int main(int argc,char **args)
{
  PetscInitialize(&argc,&args,NULL,NULL);
  PetscErrorCode ierr = 0;

  {
    PetscInt order = 4;
    PetscInt s = order + 1;
    PetscInt Ny = 11;
    PetscScalar Ly = 1;
    PetscInt Nz = 1;
    PetscScalar Lz = 1;
    PetscScalar c = 1;

    // set 1D problem
    DM da1d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da1d);
    DMSetFromOptions(da1d);
    DMSetUp(da1d);

    Vec y,z,q,r;
    setFields_DMDA(Ny,Nz,Ly,Lz,da1d,y,z,q,r);
    Vec mu;
    VecDuplicate(y, &mu);
    VecSet(mu, c);  // set constant coefficient
    SbpOps_ms_constGrid sbp(order,Ny,Nz,Ly,Lz,mu,da1d);
    sbp.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbp.setLaplaceType("y"); CHKERRQ(ierr);
    ierr = sbp.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbp.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbp.computeMatrices(); CHKERRQ(ierr);
  
    // test if stencil implementation computes the correct 1st derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 1st derivative:\n");
    testStencilD1(da1d, sbp, mu, y, z, Ny, Nz);

    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testStencilD2(da1d, sbp, mu, y, z, Ny, Nz);

    // set 2D problem
    Nz = 11;
    DM da2d;
    DMDACreate2d(PETSC_COMM_WORLD,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE,DMDA_STENCIL_BOX,Nz,Ny,PETSC_DECIDE,PETSC_DECIDE,1,s,NULL,NULL,&da2d);
    DMSetFromOptions(da2d);
    DMSetUp(da2d);

    Vec yD,zD,qD,rD;
    setFields_DMDA(Ny,Nz,Ly,Lz,da2d,yD,zD,qD,rD);
    Vec muD;
    VecDuplicate(yD,&muD);
    VecSet(muD, c);  // set constant coefficient
    SbpOps_ms_constGrid sbpD(order,Ny,Nz,Ly,Lz,muD,da2d);
    sbpD.setBCTypes("Dirichlet","Dirichlet","Dirichlet","Dirichlet");
    ierr = sbpD.setLaplaceType("yz"); CHKERRQ(ierr);
    ierr = sbpD.setMultiplyByH(0); CHKERRQ(ierr);
    ierr = sbpD.setDeleteIntermediateFields(1); CHKERRQ(ierr);
    ierr = sbpD.computeMatrices(); CHKERRQ(ierr);

    // test if stencil implementation computes the correct 1st derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 1st derivative:\n");
    testStencilD1(da2d, sbpD, muD, yD, zD, Ny, Nz);

    // test if stencil implementation computes the same 2nd derivative
    PetscPrintf(PETSC_COMM_WORLD, "Test 2nd derivative:\n");
    testStencilD2(da2d, sbpD, muD, yD, zD, Ny, Nz);

    VecDestroy(&y);
    VecDestroy(&z);
    VecDestroy(&q);
    VecDestroy(&r);
    VecDestroy(&mu);
    DMDestroy(&da1d);

    VecDestroy(&yD);
    VecDestroy(&zD);
    VecDestroy(&qD);
    VecDestroy(&rD);
    VecDestroy(&muD);
    DMDestroy(&da2d);
  }  

  PetscFinalize();
  return ierr;
}
