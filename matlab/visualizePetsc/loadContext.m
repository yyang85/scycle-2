function output = loadContext(sourceDir)
% loads all context fields

output.sourceDir = sourceDir;

% scalar meta-data
output.dom = loadStruct(strcat(sourceDir,'domain.txt'),' = ');

if exist(strcat(sourceDir,'mediator_context.txt'),'file') == 2
  output.med = loadStruct(strcat(sourceDir,'mediator_context.txt'),' = ');
end

if exist(strcat(sourceDir,'he_context.txt'),'file') == 2
  output.he = loadStruct(strcat(sourceDir,'he_context.txt'),' = ');
end

if exist(strcat(sourceDir,'fault_context.txt'),'file') == 2
  output.fault = loadStruct(strcat(sourceDir,'fault_context.txt'),' = ');
end

if exist(strcat(sourceDir,'pl_context.txt'),'file') == 2
  output.momBal = loadStruct(strcat(sourceDir,'pl_context.txt'),' = ');
end

% coordinate system
if exist(strcat(sourceDir,'y'),'file') == 2
  output.y = loadVec(sourceDir,'y');
  output.y = reshape(output.y,output.dom.Nz,output.dom.Ny);
end

if exist(strcat(sourceDir,'z'),'file') == 2
  output.z = loadVec(sourceDir,'z');
  output.z = reshape(output.z,output.dom.Nz,output.dom.Ny);
end

% rate and state context
if exist(strcat(sourceDir,'fault_a'),'file') == 2
  output.fault.a = loadVec(sourceDir,'fault_a');
end
if exist(strcat(sourceDir,'fault_b'),'file') == 2
  output.fault.b = loadVec(sourceDir,'fault_b');
end
if exist(strcat(sourceDir,'fault_sNEff'),'file') == 2
  output.fault.sNEff = loadVec(sourceDir,'fault_sNEff');
end
if exist(strcat(sourceDir,'fault_cohesion'),'file') == 2
  output.fault.Co = loadVec(sourceDir,'fault_cohesion');
end
if exist(strcat(sourceDir,'fault_Dc'),'file') == 2
  output.fault.Dc = loadVec(sourceDir,'fault_Dc');
end
if exist(strcat(sourceDir,'fault_eta_rad'),'file') == 2
  output.fault.eta_rad = loadVec(sourceDir,'fault_eta_rad');
end
if exist(strcat(sourceDir,'fault_locked'),'file') == 2
  output.fault.locked = loadVec(sourceDir,'fault_locked');
end

% dynamic wave equation
if exist(strcat(sourceDir,'ay'),'file') == 2
  output.ay = loadVec(sourceDir,'ay');
end
if exist(strcat(sourceDir,'cs'),'file') == 2
  output.cs = loadVec(sourceDir,'cs');
end

% shear modulus
if exist(strcat(sourceDir,'momBal_mu'),'file') == 2
  output.momBal.mu = PetscBinaryRead(strcat(sourceDir,'momBal_mu'),'cell');
end

% load pressure equation material properties
if exist(strcat(sourceDir,'hydro_rho_f'),'file') == 2
  output.hydro.rho = loadVec(sourceDir,'hydro_rho_f');
end
if exist(strcat(sourceDir,'hydro_n'),'file') == 2
  output.hydro.n = loadVec(sourceDir,'hydro_n');
end
if exist(strcat(sourceDir,'hydro_beta'),'file') == 2
  output.hydro.beta = loadVec(sourceDir,'hydro_beta');
end
if exist(strcat(sourceDir,'hydro_eta'),'file') == 2
  output.hydro.eta = loadVec(sourceDir,'hydro_eta');
end
if exist(strcat(sourceDir,'hydro_sN'),'file') == 2
  output.hydro.sN = loadVec(sourceDir,'hydro_sN');
end

% heat equation parameters
if exist(strcat(sourceDir,'he_T0'),'file')==2
  tt = loadVec(sourceDir,'he_T0',1,1,1);
  output.he.T0 = squeeze(reshape(tt,output.dom.Nz,output.dom.Ny));
end
if exist(strcat(sourceDir,'he_h'),'file') == 2
  output.he.h = reshape(loadVec(sourceDir,'he_h'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'he_c'),'file') == 2
  output.he.c = reshape(loadVec(sourceDir,'he_c'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'he_rho'),'file') == 2
  output.he.rho = reshape(loadVec(sourceDir,'he_rho'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'he_k'),'file') == 2
  output.he.k = reshape(loadVec(sourceDir,'he_k'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'he_w'),'file') == 2
  output.he.w = reshape(loadVec(sourceDir,'he_w'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'he_Gw'),'file') == 2
  output.he.Gw = reshape(loadVec(sourceDir,'he_Gw'),output.dom.Nz,output.dom.Ny);
end

% power law parameters
if exist(strcat(sourceDir,'effVisc'),'file')==2
  time2D = load(strcat(sourceDir,'time2D.txt'));
  visc = loadVec(sourceDir,'effVisc',1,length(time2D),length(time2D));
  output.Visc = squeeze(reshape(visc,output.dom.Nz,output.dom.Ny));
  
  visc0 = loadVec(sourceDir,'effVisc',1,1,1);
  output.Visc0 = squeeze(reshape(visc0,output.dom.Nz,output.dom.Ny));
end
if exist(strcat(sourceDir,'momBal_mu'),'file') == 2
  output.momBal.mu = reshape(loadVec(sourceDir,'momBal_mu'),output.dom.Nz,output.dom.Ny);
end
if exist(strcat(sourceDir,'momBal_A'),'file') == 2
  powerLawA = loadVec(sourceDir,'momBal_A');
  output.momBal.A = squeeze(reshape(powerLawA,output.dom.Nz,output.dom.Ny));
end
if exist(strcat(sourceDir,'momBal_QR'),'file') == 2
  powerLawB = loadVec(sourceDir,'momBal_QR');
  output.momBal.QR = squeeze(reshape(powerLawB,output.dom.Nz,output.dom.Ny));
end
if exist(strcat(sourceDir,'momBal_n'),'file') == 2
  n = loadVec(sourceDir,'momBal_n');
  output.momBal.n = squeeze(reshape(n,output.dom.Nz,output.dom.Ny));
end

end