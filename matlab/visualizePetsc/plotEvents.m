function [slipCo,slipInt] = plotEvents(slip,time,z,refSlip,eventInds,temp)
% plots the seismic and interseismic events found by the 
% findEvents function

% these times need to be modified depending on the simulation
intCo = 1; % (s) coseismic time interval
intInt = 8.64e4; % (s) interseismic time interval

% construct time list for interseismic period
timeInt = [];
timeInt = [timeInt time(1):intInt:time(eventInds(1,1));];
for ind = 1:length(eventInds(1,:))
  sI = eventInds(2,ind);
  eI = eventInds(2,ind)+1;
  timeInt = [timeInt time(sI):intInt:time(eI)];
end
timeInt = [timeInt time(eI):intInt:time(end)];

% construct time list for coseismic period
timeCo = [];
if eventStruct.finishInds(1) < eventInds(1,1)
  timeCo = [timeCo time(1):intCo:time(eventStruct.finishInds(1))];
end

for ind = 1:( length(eventInds(1,:)))
  sI = eventInds(1,ind);
  eI = eventInds(2,ind);
  timeCo = [timeCo time(sI):intCo:time(eI)];
end
if eventStruct.startInds(end) > eventInds(end,1) ...
    && time(end)-eventStruct.startInds(end)<300
  timeCo = [timeCo time(eventStruct.startInds(end)):intCo:time(end)];
end

% interpolate slip for interseismic period
[T,Z] = meshgrid(time,z);
[TqInt,ZqInt] = meshgrid(timeInt,z);
wInt = interp2(T,Z,slip,TqInt,ZqInt);
slipInt = bsxfun(@minus,wInt,refSlip);

% interpolate slip for coseismic period
[TqCo,ZqCo] = meshgrid(timeCo,z);
wCo = interp2(T,Z,slip,TqCo,ZqCo);
slipCo = bsxfun(@minus,wCo,refSlip);
