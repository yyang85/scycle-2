function output = loadData1D(output,sourceDir,stride,startInd,endInd)
% loads 1D fields

% process optional input arguments
if nargin < 3
  stride = 1;
end
if nargin < 4
  startInd = 1;
end
if nargin < 5
  endInd = Inf;
end

if startInd < 1, startInd = 1; end

% check that input's are valid
if endInd < startInd
  display('Error: final index must be > than initial index.')
  fprintf('initial indextInd = %i, final index = %i.\n',startInd,endInd)
  return
elseif stride < 1 || rem(stride,1)~=0
  display('Error: stride must be a positive whole number.')
  return
end

display(strcat('loading 1D data:',sourceDir))
time = load(strcat(sourceDir,'med_time1D.txt'));
fprintf('# of time steps: %i\n',length(time));
output.time = time( startInd:stride:min(endInd,length(time)) );

% pressure
if exist(strcat(sourceDir,'hydro_p'),'file') == 2
  output.hydro.p = loadVec(sourceDir,'hydro_p',stride,startInd,endInd);
  display('   finished loading hydro.p')
end

% effective normal stress
if exist(strcat(sourceDir,'sNEff'),'file') == 2
  output.hydro.sNEff = loadVec(sourceDir,'sNEff',stride,startInd,endInd);
  display('   finished loading hydro.sNEff')
end

% permeability
if exist(strcat(sourceDir,'hydro_k'),'file') == 2
  output.hydro.k = loadVec(sourceDir,'hydro_k',stride,startInd,endInd);
  display('   finished loading hydro.k')
end

% slip/pressure-dependent permeability
% if exist(strcat(sourceDir,'hydro_k_slip'),'file') == 2
%   output.hydro.k_slip = loadVec(sourceDir,'hydro_k_slip',stride,startInd,endInd);
%   display('   finished loading hydro.k_slip')
% end
% if exist(strcat(sourceDir,'hydro_k_press'),'file') == 2
%   output.hydro.k_press = loadVec(sourceDir,'hydro_k_press',stride,startInd,endInd);
%   display('   finished loading hydro.k_press')
% end

% slip velocity
if exist(strcat(sourceDir,'slipVel'),'file') == 2
  output.slipVel = loadVec(sourceDir,'slipVel',stride,startInd,endInd);
  display('   finished loading slip velocity')
end

% slip
if exist(strcat(sourceDir,'slip'),'file') == 2
  output.slip = loadVec(sourceDir,'slip',stride,startInd,endInd);
  display('   finished loading slip on fault')
end

% fault shear strength
if exist(strcat(sourceDir,'strength'),'file') == 2
  output.strength = loadVec(sourceDir,'strength',stride,startInd,endInd);
  display('   finished loading shear stress')
end

% state variable psi
if exist(strcat(sourceDir,'psi'),'file') == 2
  output.psi = loadVec(sourceDir,'psi',stride,startInd,endInd);
end

% surface displacement
if exist(strcat(sourceDir,'surfDisp'),'file') == 2
  output.surfDisp = loadVec(sourceDir,'surfDisp',stride,startInd,endInd);
%   temp = (output.surfU(:,2:end) - output.surfU(:,1:end-1));
%   if length(output.time)>2
%     dt = output.time(2:end) - output.time(1:end-1);
%     output.surfVel = bsxfun(@rdivide,temp,dt');
%   end
end

% load boundary conditions for debugging
% if exist(strcat(sourceDir,'bcR'),'file') == 2
%   output.momBal.bcR = loadVec(sourceDir,'bcR',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'bcL'),'file') == 2
%   output.momBal.bcL = loadVec(sourceDir,'bcL',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'bcT'),'file') == 2
%   output.momBal.bcT = loadVec(sourceDir,'bcT',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'bcB'),'file') == 2
%   output.momBal.bcB = loadVec(sourceDir,'bcB',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'bcRShift'),'file') == 2
%   output.bcRShift = loadVec(sourceDir,'bcRShift',stride,startInd,endInd);
% end


% if using flash heating and coupled to heat equation
if exist(strcat(sourceDir,'fault_T'),'file') == 2
  output.fault.T = loadVec(sourceDir,'fault_T',stride,startInd,endInd);
  display('   finished loading fault temperature')
end

if exist(strcat(sourceDir,'he_kTz_z0'),'file') == 2
  output.surfaceHeatFlux = loadVec(sourceDir,'he_kTz_z0',stride,startInd,endInd);
  display('   finished loading surface heat flux')
end


% load boundary condition to debug heat equation
% if exist(strcat(sourceDir,'he_bcL'),'file') == 2
%   output.he.bcL = loadVec(sourceDir,'he_bcL',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'he_bcR'),'file') == 2
%   output.he.bcR = loadVec(sourceDir,'he_bcR',stride,startInd,endInd);
% end


% for MMS  tests
% if exist(strcat(sourceDir,'mms_bcL'),'file') == 2
%   output.mms_bcL = loadVec(sourceDir,'mms_bcL',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'mms_bcR'),'file') == 2
%   output.mms_bcR = loadVec(sourceDir,'mms_bcR',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'mms_bcT'),'file') == 2
%   output.mms_bcT = loadVec(sourceDir,'mms_bcT',stride,startInd,endInd);
% end
% if exist(strcat(sourceDir,'mms_bcB'),'file') == 2
%   output.mms_bcB = loadVec(sourceDir,'mms_bcB',stride,startInd,endInd);
% end

end